﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	//config.filebrowserUploadUrl = '/utility/upload';

	//config.filebrowserImageBrowseUrl = '/scripts/kcfinder/browse.php?type=images';

	config.toolbar = 'MyToolbar';
	config.skin = 'v2';
	config.removePlugins = 'elementspath';
	config.resize_enabled = false;
	config.stylesSet = [];
    config.toolbar_MyToolbar =
[

		['Source'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link','Unlink'],
['Table'],
		'/',
		['Format','Font','FontSize'],
		['TextColor','BGColor'],
		['Maximize', '-','About']
    ];
    config.toolbar_MinimToolbar =
[
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
		];
};
