<?php
// Extend the TCPDF class to create custom Header and Footer
class SAMPUL extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES.'logoukdw.png';
        $this->setImageScale(1.5);
        $this->Image($image_file, 30, 20, '', '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 11);
        // Title
        $this->Cell(0, 0, '  ', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, '  Universitas Kristen Duta Wacana', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, '  Fakultas Teknologi Informasi Program Studi Sistem Informasi', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('helvetica', '', 10);
        $this->Cell(0, 0, '  Jl. Dr. Wahidin Sudirahusada 5-25 Yogyakarta 55224', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, '  Telp.: (0274)563929 Faks.: (0274)513235', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
    }
	
    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-40);
        // Set font
        $this->SetFont('times', '', 12);
        $this->Cell(0, 0, 'PROGRAM STUDI SISTEM INFORMASI FAKULTAS TEKNOLOGI INFORMASI', 0, 2, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, 'UNIVERSITAS KRISTEN DUTA WACANA', 0, 2, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, 'TAHUN ' . date('Y'), 0, 2, 'C', 0, '', 0, false, 'T', 'M');
    }
}
?>