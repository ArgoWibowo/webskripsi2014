<?php
// Extend the TCPDF class to create custom Header and Footer
class BADADAR extends TCPDF {

    //Page header
    public function Header() {
        // Logo
        $image_file = K_PATH_IMAGES.'logoukdw.png';
        $this->setImageScale(2);
        $this->Image($image_file, 20, 5, '', '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 10);
        // Title
        $this->Cell(0, 0, '  Universitas Kristen Duta Wacana', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, '  Fakultas Teknologi Informasi Program Studi Sistem Informasi', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
        $this->SetFont('helvetica', '', 10);
        $this->Cell(0, 0, '  Jl. Dr. Wahidin Sudirahusada 5-25 Yogyakarta 55224', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, '  Telp.: (0274)563929 Faks.: (0274)513235', 0, 2, 'L', 0, '', 0, false, 'T', 'M');
        $logosi = K_PATH_IMAGES. 'logosi_small.png';
        $this->Image($logosi, 145, 5, '', '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
?>