<?php
class Presensi extends AppModel {
	public $name = 'Presensi';
	public $useTable = 'presensi';
	public $belongsTo = array(
								'Dosen' => array(
									'className' => 'Dosen',
									'foreignKey' => 'id_dosen'
								),
								'Jadwal' => array(
									'className' => 'Jadwal',
									'foreignKey' => 'idtglkol'
								),
								'De' => array(
									'className' => 'De',
									'foreignKey' => 'id_tglde'
								)
							 );
}
?>