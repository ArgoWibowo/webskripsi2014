<?php
class Jwdpddr extends AppModel {
	public $name = 'Jwdpddr';
	public $useTable = 'jwdpddrs';
	
	var $virtualFields = array(
			'batasakhir' => 'DATE_FORMAT(Jwdpddr.batas,"%e %M %Y %H:%i:%s")'
		);
		
	var $validate = array(
		'batas' => array(
			'rule' => 'notEmpty'
		)
	);
}
?>