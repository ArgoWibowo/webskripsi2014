<?php
	class Log extends AppModel {
		var $name = 'Log';
		var $useTable = 'logs';
		
		function addLog($username, $event, $description) {
			$data['username'] = $username;
			$data['event'] = $event;
			$data['description'] = $description;
			
			$this->save($data);
		}
	}
?>