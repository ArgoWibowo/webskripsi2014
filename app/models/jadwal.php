<?php
class Jadwal extends AppModel {
	public $name = 'Jadwal';
	//public $useTable = 'jadwal';
	
	public $hasAndBelongsToMany = array(
						'Dosen' =>array(
								'className' => 'Dosen',
								'joinTable' => 'jadwals_dosens',
								'foreignKey' => 'jadwal_id',
								'associationForeignKey' => 'dosen_id',
								'with' => 'JadwalsDosens'
							)
					);
						
	var $virtualFields = array(
			'waktu' => 'DATE_FORMAT(Jadwal.tanggal,"%e %M %Y")',
			'batasakhir' => 'DATE_FORMAT(Jadwal.batas,"%e %M %Y")'
		);	
		
	var $validate = array(
			'tanggal' => array(
				'rule' => 'date'
			),
			'batas' => array(
				'rule' => 'notEmpty'
			),
			'tglkumpul' => array(
				'rule' => 'notEmpty'
			)
		);
}
?>