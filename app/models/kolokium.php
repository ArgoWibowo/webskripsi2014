<?php
class Kolokium extends AppModel {
	public $name = 'Kolokium';
	public $useTable = 'kolokiums';
	public $hasAndBelongsToMany = array(
						'Topik' =>array(
								'className' => 'Topik',
								'joinTable' => 'kolokiums_topiks',
								'foreignKey' => 'kolokium_id',
								'associationForeignKey' => 'topik_id',
								'with' => 'KolokiumsTopiks'
							)
					);
	
	public $belongsTo = array('User' => array(
									'className' => 'User',
									'foreignKey' => 'userid'
								),
							  'Jadwal' => array(
									'className' => 'Jadwal',
									'foreignKey' => 'tgl_kolokium'
							    ),
							  'Dosen' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'usulan_dosen1'
							    ),
							  'Dosen2' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'usulan_dosen2'
							    ),
							  'Proposal' => array(
							  		'className' => 'Proposal',
							  		'foreignKey' => 'proposal_id'
							    )
							 );
	
    public $validate = array(
			'nim' => array(
				'numeric' => array(
							'rule' => 'numeric',
							'check' => true,
							'message' => 'Your Student ID should be 8 numbers'
						),
				'between' => array(
							'rule' => array('between', 8, 8),
							'check' => true,
							'message' => 'Should be 8 characters'
						)
			),
			'judul' => array(
				'rule' => 'notEmpty',
				'check' => true,
				'message' => 'Silahkan masukkan usulan judul tugas akhir Anda.'
			),
			'proposal' => array(
				'uploadCheckInvalidMime' => array(
					'rule' => 'uploadCheckInvalidMime',
					'check' => true
				),
				'uploadCheckInvalidExt' => array(
					'rule' => 'uploadCheckInvalidExt',
					'check' => true
				),
				'uploadCheckMaxSize' => array(
					'rule' => 'uploadCheckMaxSize',
					'check' => true
				)
			)
		);

		
	public $actsAs = array(
		'MeioUpload.MeioUpload' => array('proposal' => array(
												'dir' => '../files{DS}artikel{DS}files', 
												'createDirectory' => true,
												'allowedMime' => array(
																	'application/vnd.oasis.opendocument.text', 
																	'application/msword', 
																	'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
																),
												'allowedExt' => array('.odt', '.doc', '.docx'),
												'default' => false,
												'maxSize' => 3145728
											)
										)
	);

}
?>