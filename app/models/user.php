<?php
	class User extends AppModel {
		var $name = 'User';
		var $useTable = 'users';
		var $validate = array(
									'username' => array(
										'alphaNumeric' => array(
													'rule' => 'alphaNumeric',
													'required' => true,
													'message' => 'Alphabets and numbers only'
												),
										'between' => array(
													'rule' => array('between', 5, 15),
													'message' => 'Between 5 to 15 characters'
												),
										'isUnique' => array(
													'rule' => array('isUnique'),
													'message' => 'Username already in use.'
												)
									),
									'nim' => array(
										'numeric' => array('rule' => 'numeric',
													'required' => true,
    												'message' => 'Should be 8-15 number'
    											),
 										'between' => array(
													'rule' => array('between', 8, 15),
													'message' => 'Should be 8-15 characters'
												) /*,
										'isUnique' => array(
													'rule' => array('isUnique'),
													'message' => 'NIM/NIDN already in use.'
												) */

									),
									'password' => array(
										'rule' => array('minLength', '8'),
										'message' => 'Mimimum 8 characters long.'
									),
									'fullname' => array(
										'notempty' => array(
											'rule' => array('notempty'),
											'message' => 'Fullname must be filled.'				
										)
									),
									'email' => array(
										'email' => array(
											'rule' => 'email',
											'required' => true,
											'message' => 'You have to provide your valid email address.'
										)
									),
									'telpno' => array(
										'notempty' => array(
											'rule' => array('notempty'),
											'message' => 'Please type your cellphone/work phone/home phone so we can contact you.'				
										)
									),
									'foto' => array(
										'Empty' => array(
											'rule' => array('uploadCheckEmpty'),
											'check' => false
										)
									)
								);

		var $actsAs = array(
				'MeioUpload.MeioUpload' => array('foto' => array(
												'dir' => '../files{DS}foto{DS}files', 
												'createDirectory' => true,
												'allowedMime' => array(
																	'image/png', 
																	'image/jpg',
																	'image/jpeg',
																	'image/gif'
																),
												'allowedExt' => array('.gif', '.jpg', '.png'),
												'default' => 'anonym.png',
												'length' => array(
													'minWidth' => 0, 
													'maxWidth' => 140,
													'minHeight' => 0,
													'maxHeight' => 140
												)
											)
								)
			);
    
/*		maybe next time :( */
		var $belongsTo = array(
				'Group' => array(
						'className' => 'Group',
						'foreignKey' => 'group_id'
					)
			);
		
		function beforeSave() {
			if ( isset($this->data['User']['password']) && $this->data['User']['password']) {
				$this->data['User']['password'] = md5($this->data['User']['password']);
			}
			$this->data['User']['modified'] = date( 'Y-m-d H:i:s' );
			return true;
		}
  		
		function validateLogin($data) {
			$user = $this->find('first', array(
								'conditions' => array (
									'User.username' => $data['username'], 
									'User.password' => md5($data['password'])
								),
								'fields' => array('id', 'username', 'fullname', 'group_id', 'active', 'nim')
							)); 
			if(empty($user) == false)
				return $user['User'];
			else
				return false;	
		}
		
		function changePassword($id, $newpassword) {
			if(isset($id) && isset($newpassword)) {
				$data = $this->findById($id);
				$data['User']['password'] = md5($newpassword);
				$this->save($data);
				return true;
			}
			else
				return false;
		}
	}
?>
