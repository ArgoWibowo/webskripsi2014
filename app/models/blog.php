<?php
class Blog extends AppModel 
{
	var $name = 'Blog';
	
	var $validate = array(
			'title' => array(
				'notempty' => array(
					'rule' => array('notempty'),
					'message' => 'Please specify the title of news.'
				)
			),
			'content' => array(
				'notempty' => array(
					'rule' => array('notempty'),
					'message' => 'Please type the news content.'
				)
			)				
		);
	//*/
}
?>