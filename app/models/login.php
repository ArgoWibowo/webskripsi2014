<?php
	class Login extends AppModel {
		var $name = 'Login';
		var $useTable = 'users';
		var $validate = array(
									'username' => array(
										'alphaNumeric' => array(
													'rule' => 'alphaNumeric',
													'required' => true,
													'message' => 'Alphabets and numbers only'
												)
									),
									'password' => array(
										'rule' => array('minLength', '8'),
										'required' => true,
										'message' => 'Mimimum 8 characters long.'
									),
									'email' => array(
										'email' => array(
											'rule' => 'email',
											'required' => true,
											'message' => 'You have to provide your valid email address.'
										)
									)
								);
		
		function validateLogin($data) {
			$user = $this->find('first', array(
								'conditions' => array (
									'Login.username' => $data['username'], 
									'Login.password' => md5($data['password'])
								),
								'fields' => array('id', 'username', 'fullname', 'group_id', 'active', 'nim', 'hashtag', 'telpno')
							)); 
			if(empty($user) == false)
				return $user['Login'];
			else
				return false;	
		}
	}
?>
