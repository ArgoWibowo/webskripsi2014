<?php
class Tafile extends AppModel {
	public $name = 'Tafile';
	
	public $belongsTo = array(
				'Jenisfile' => array(
									'className' => 'Jenisfile',
									'foreignKey' => 'jenisfiles_id',
								)
			);
			
	public $actsAs = array(
		'MeioUpload.MeioUpload' => array('filename' => array(
												'dir' => '../files{DS}artikel{DS}files{DS}skripsi', 
												'createDirectory' => true,
												'allowedMime' => array(
																	'application/vnd.oasis.opendocument.text', 
																	'application/msword', 
																	'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
																),
												'allowedExt' => array('.odt', '.doc', '.docx'),
												'default' => false,
												'maxSize' => 3145728
											)
										)
	);
	
	public function onError(){
		$db = ConnectionManager::getDataSource('default');
		$err = $db->lastError();
		$this->log($err);
		$this->log($this->data);
	}
}
?>