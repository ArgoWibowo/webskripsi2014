<?php
class De extends AppModel {
	public $name = 'De';
	public $useTable = 'des';
	
	public $hasAndBelongsToMany = array(
					'Dosen' =>array(
							'className' => 'Dosen',
							'joinTable' => 'des_dosens',
							'foreignKey' => 'de_id',
							'associationForeignKey' => 'dosen_id',
							'with' => 'DesDosens'
						),
					'Proposal' =>array(
							'className' => 'Proposal',
							'joinTable' => 'des_proposals',
							'foreignKey' => 'de_id',
							'associationForeignKey' => 'proposal_id',
							'with' => 'DesProposals'
						)
				);
	
	var $virtualFields = array(
			'pelaksanaan' => 'DATE_FORMAT(De.tanggal,"%e %M %Y %H:%i:%s")'
		);

	var $belongsTo = array(
		'Jwddeskevaluation' => array(
				'className' => 'Jwddeskevaluation',
				'foreignKey' => 'idbatas'
			)
	);
	
	var $validate = array(
		'tanggal' => array(
			'rule' => 'notEmpty'
		),
		'kelompok' => array(
			'rule' => 'notEmpty'
		),
		'idbatas' => array(
			'rule' => 'notEmpty'
		)
	);
}
?>