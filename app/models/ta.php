<?php
class Ta extends AppModel {
	public $name = 'Ta';
 	//public $useTable = 'ta';
	var $hasMany = array(
			'Tafile' => array(
				'className' => 'Tafile',
				'foreignKey' => 'ta_id',
				'order' => 'Tafile.modified DESC'
			)
		);
	public $belongsTo = array('Mahasiswa' => array(
									'className' => 'Mahasiswa',
									'associationForeignKey' => 'nim',
									'foreignKey' => 'nim',
								),
							  'Dosen' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'dosen1'
							    ),
							  'Dosen2' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'dosen2'
							    )
							 );

	public function getstat() {
		$data = $this->query('
select t.dosen, d.nidn, d.gelar_depan, d.nama, d.gelar, sum(jml) as "total" from (SELECT dosen1 as dosen, count(dosen1) as jml FROM `tas` WHERE aktif=1 group by dosen1
union all
SELECT dosen2, count(dosen2) FROM tas WHERE aktif=1 group by dosen2) t, dosens d
where t.dosen = d.id
group by dosen');
		return $data;
	}
}
?>