<?php
class Pendadaran extends AppModel {
	public $name = "Pendadaran";
	public $belongsTo = array('Ta' => array(
									'className' => 'Ta',
									'foreignKey' => 'id_ta',
								),
								'Dosen' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'penguji1'
							    ),
								'Dosen2' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'penguji2'
							    ),
								'Dosen3' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'penguji3'
							    ),
								'Dosen4' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'penguji4'
							    ),
							    'Jwdpddr' => array(
							    	'className' => 'Jwdpddr',
							    	'foreignKey' => 'id_tgldaftar'
							    )
							 );
							 
    function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->virtualFields['dekan'] = '"Restyandito, S.Kom., MSIS., Ph.D"';
		$this->virtualFields['kaprodi'] = '"Drs. JONG JEK SIANG, M.Sc."';
    }
}
?>