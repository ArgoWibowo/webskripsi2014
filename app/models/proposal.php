<?php
class Proposal extends AppModel {
	public $name = 'Proposal';
	public $useTable = 'proposals';
	
	public $belongsTo = array('User' => array(
									'className' => 'User',
									'foreignKey' => 'userid'
								),
							  'Topik' => array(
									'className' => 'Topik',
									'foreignKey' => 'konsentrasi'
								),
							  'Dosen' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'dosen'
							    ),
							  'Dosen2' => array(
							  		'className' => 'Dosen',
							  		'foreignKey' => 'dosen_kp'
							    ),
							  'Jwddeskevaluation' => array(
							  		'className' => 'Jwddeskevaluation',
							  		'foreignKey' => 'tglde'
							  	)
							 );
	
	public $hasAndBelongsToMany = array(
			'De' => array(
					'className' => 'De',
					'joinTable' => 'des_proposals',
					'foreignKey' => 'proposal_id',
					'associationForeignKey' => 'de_id',
					'with' => 'DesProposals'
				)
		);

    public $validate = array(
			'judul' => array(
				'rule' => 'notEmpty',
				'check' => true,
				'message' => 'Silahkan masukkan usulan judul tugas akhir Anda.'
			),
			'konsentrasi' => array(
				'rule' => 'notEmpty',
				'check' => true,
				'message' => 'Silahkan pilih konsentrasi dari usulan topik skripsi Anda.'
			),
			'ipk' => array(
				'rule' => array('decimal', 2),
				'check' => true,
				'message' => 'Silahkan masukkan IPK anda dengan 2 digit dibelakang koma (9.99).'
			),
			'ipro' => array(
				'rule' => array('decimal', 2),
				'check' => true,
				'message' => 'Silahkan masukkan IP semester anda dengan 2 digit dibelakang koma (9.99).'
			),
			'gambaran' => array(
				'rule' => 'notEmpty',
				'check' => true,
				'message' => 'Silahkan masukkan gambaran skripsi yang akan dibuat.'
			),
			'input_output' => array(
				'rule' => 'notEmpty',
				'check' => true,
				'message' => 'Silahkan masukkan Spefisikasi Masukan dan Keluaran.'
			),
			'pengguna' => array(
				'rule' => 'notEmpty',
				'check' => true,
				'message' => 'Silahkan masukkan Pengguna aplikasi Anda.'
			),
			'metode' => array(
				'rule' => 'notEmpty',
				'check' => true,
				'message' => 'Silahkan masukkan metode-metode yang digunakan dalam pengerjaan skripsi Anda.'
			),
			'data' => array(
				'rule' => 'notEmpty',
				'check' => true,
				'message' => 'Silahkan masukkan metode-metode yang digunakan dalam pengerjaan skripsi Anda.'
			),
		);

	function beforeSave() {
		$this->data['Proposal']['modified'] = date( 'Y-m-d H:i:s' );
		return true;
	}
}
?>