<?php
class Jwddeskevaluation extends AppModel {
	public $name = 'Jwddeskevaluation';
	public $useTable = 'jwddeskevaluations';
	
	/*public $hasAndBelongsToMany = array(
						'Dosen' =>array(
								'className' => 'Dosen',
								'joinTable' => 'jwddeskevaluations_dosens',
								'foreignKey' => 'jwddeskevaluation_id',
								'associationForeignKey' => 'dosen_id',
								'with' => 'JwddeskevaluationsDosens'
							)
					);*/
	
	var $virtualFields = array(
			'batasakhir' => 'DATE_FORMAT(Jwddeskevaluation.batas,"%e %M %Y %H:%i:%s")'
		);
	
	var $validate = array(
		'batas' => array(
			'rule' => 'notEmpty'
		),
		'tanggal' => array(
			'rule' => 'notEmpty'
		)
	);
}
?>