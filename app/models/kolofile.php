<?php
class Kolofile extends AppModel {
	public $name = 'Kolofile';
	public $useTable = 'kolofiles';
	public $belongsTo = array('Kolokium' => array(
									'className' => 'Kolokium',
									'foreignKey' => 'idkolokium'
								)
							 );
	
    public $validate = array(
			'proposal' => array(
				'uploadCheckInvalidMime' => array(
					'rule' => 'uploadCheckInvalidMime',
					'check' => true
				),
				'uploadCheckInvalidExt' => array(
					'rule' => 'uploadCheckInvalidExt',
					'check' => true
				),
				'uploadCheckMaxSize' => array(
					'rule' => 'uploadCheckMaxSize',
					'check' => true
				)
			)
		);

		
	public $actsAs = array(
		'MeioUpload.MeioUpload' => array('proposal' => array(
												'dir' => '../files{DS}artikel{DS}files', 
												'createDirectory' => true,
												'allowedMime' => array(
																	'application/vnd.oasis.opendocument.text', 
																	'application/msword', 
																	'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
																),
												'allowedExt' => array('.odt', '.doc', '.docx'),
												'default' => false,
												'maxSize' => 3145728
											)
										)
	);

}
?>