<?php
class Dosen extends AppModel {
	var $name = 'Dosen';

	var $validate = array(
			'nidn' => array(
				'alphaNumeric' => array(
					'rule' => 'alphaNumeric',
					'required' => true,
					'message' => 'Silahkan memasukan NIDN Dosen.'
				),
				'isUnique' => array(
					'rule' => 'isUnique',
					'message' => 'NIDN sudah tercatat!'
				)
			),
			'nama' => array(
				'notempty' => array(
					'rule' => array('notempty'),
					'message' => 'Silahkan memasukan nama lengkap dosen.'
				)
			),
			'email' => array(
				'email' => array(
					'rule' => 'email',
					'required' => true,
					'message' => 'You have to provide your valid email address.'
				)
			),
			'telpno' => array(
				'notempty' => array(
					'rule' => array('notempty'),
					'message' => 'Please type your cellphone/work phone/home phone so we can contact you.'				
				)
			),
			'boleh' => array(
				'allowedChoice' => array(
						'rule' => array('inList', array('B', 'M')),
						'message' => 'Masukkan (B)oleh atau belu(M) boleh membimbing!'
					)
				)
		);
		
    function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->virtualFields['nama_dosen'] = sprintf('CONCAT(%s.gelar_depan, " ", %s.nama, ", ", %s.gelar )', $this->alias, $this->alias, $this->alias);
    }

}
?>