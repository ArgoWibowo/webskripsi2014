<?php
class AppController extends Controller {
	/**
	* undocumented function
	*
	* @param string $model 
	* @return void
	* @access public
	*/
	function pageForPagination($model) {
		$page = 1;
		$sameModel = isset($this->params['named']['model']) && $this->params['named']['model'] == $model;
		$pageInUrl = isset($this->params['named']['page']);
		if ($sameModel && $pageInUrl) {
			$page = $this->params['named']['page'];
		}
		
		$this->passedArgs['page'] = $page;
		return $page;
	}

	function doPaging($model="", $limit=0, $orderby=null, $where=null, $recursive=1) {
		$page = $this->pageForPagination($model);
		$this->paginate[$model] = array(
			'limit' => $limit, 
			'order' => $orderby,
			'page' => $page,
			'recursive' => $recursive
		);
		if ($where) {
			return $this->paginate($model, $where);
		} else {
			return $this->paginate($model);
		}
	}
}
?>