<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/proposalpdf');

$tcpdf = new PROPOSALPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Form Perpanjangan - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(100);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(20, 30, 20, 30);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',10);

if (!empty($data)) {
    $saatini = date('Y-m-d H:i:s');
    $nmfile = 'perpanjangan-' . $data['Ta']['nim'] . '.pdf';
    $judulta = nl2br($data['Ta']['judul']);
    
	$html = 
'<table cellpadding="2" style="font-size: 1.0em;" width="100%" border="0">
<tr><td colspan="2"><span style="text-align: left;"><h3>Hal : Persetujuan Lanjut Skripsi</h3></span></td></tr>
<tr><td colspan="2">Kepada yang terhormat,<br/>
<strong>Koordinator Skripsi</strong><br/>
Program Studi Sistem Informasi Fakultas Teknologi Informasi<br/>
Universitas Kristen Duta Wacana Yogyakarta</td></tr>
<tr><td colspan="2">
<br/><br/>Yang bertanda tangan di bawah ini:
<p><table>
<tr style="line-height: 200%;"><td style="width:150px;">Nama</td><td style="width:15px;">:</td><td style="width:740px;">' . $data['Mahasiswa']['nama'] . '</td></tr>
<tr style="line-height: 200%;"><td>N I M</td><td>:</td><td>' . $data['Mahasiswa']['nim'] . '</td></tr>
<tr style="line-height: 200%;"><td>Judul Skripsi</td><td>:</td><td>' . $judulta . '</td></tr>
</table></p>
</td></tr>

<tr><td colspan="2">
<p style="line-height: 200%;">Mengajukan perpanjangan masa penyusunan skripsi selama 1 (satu) semester pada Semester (Gasal/Genap/Pendek)* Tahun Ajaran ______ / ______ dengan alasan : </p>
<ol>
<li><div  style="border-bottom:1px dotted #c0c0c0;">&nbsp;</div></li>
<li><div  style="border-bottom:1px dotted #c0c0c0;">&nbsp;</div></li>
<li><div  style="border-bottom:1px dotted #c0c0c0;">&nbsp;</div></li>
</ol>
<p></p>
<p>Demikian surat permohonan ini dibuat agar dapat digunakan sebagaimana mestinya.</p>
<p>&nbsp;</p>
</td></tr>

<tr><td></td><td><div style="text-align:center;">
Yogyakarta, ' .  $this->Tools->prnDate($saatini) . '<br/><br/><br/><br/>
<div style="border-bottom:1px solid #000;">'. $data['Mahasiswa']['nama'] . '</div>'. $data['Ta']['nim'].'
</div></td></tr>

<tr><td style="padding: 4px;"><div style="text-align:center;"><br/><br/>
Mengetahui,<br/><br/><br/><br/>
<div style="border-bottom:1px solid #000;">'. $data['Dosen']['gelar_depan'] . ' '. $data['Dosen']['nama'] . ', ' . $data['Dosen']['gelar'] . '</div>
Dosen Pembimbing I
</div></td><td><div style="text-align:center;">
<br/><br/><br/><br/><br/><br/>
<div style="border-bottom:1px solid #000;">'. $data['Dosen2']['gelar_depan'] . ' '. $data['Dosen2']['nama'] . ', ' . $data['Dosen2']['gelar'] . '</div>
Dosen Pembimbing II
</div></td></tr>
</table>
<div><small>* coret yang tidak perlu</small></div>';

} else {
	$html = '<p>&nbsp;</p><h2>Status skripsi Anda tidak ada yang aktif!</h2>';
	$nmfile = 'perpanjangan-UNK.pdf';
}
$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->Line(20,30, 190, 30);
$tcpdf->lastPage();

$tcpdf->Output($nmfile, 'I');
?>
