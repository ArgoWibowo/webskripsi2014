<div style="font-size:1.2em;">
<?php echo $this->element('header_report'); ?>
<div style="clear:both;"></div>
<p></p>
<section>
<?php if (!empty($data)): ?>
<div style="text-align:center;"><span style="font-weight: bold; font-size: 1.4em;">Daftar Peserta Skripsi</span><br/>
</div><br/>
<table id="box-report" style="width:100%;">
<thead>
	<tr>
		<th scope="col" rowspan="2">No.</th>
		<th scope="col" rowspan="2"><small>NIDN</small></th>
		<th scope="col" rowspan="2"><small>Nama Dosen</small></th>
		<th scope="col" rowspan="2"><small>Jumlah Bimbingan</small></th>
	</tr>
</thead>
<tbody>
<?php
$i=0;
foreach($data as $d) { 
	$i++; ?>
	<tr>
		<td><?php echo $i; ?>.</td>
		<td><?php echo $d['d']['nidn']; ?></td>
		<td><?php echo trim($d['d']['gelar_depan'] . ' ' . $d['d']['nama'] . ', '. $d['d']['gelar']); ?></td>
		<td><?php echo $d[0]['total']; ?></td>
	</tr>
<?php } ?>
</tbody>
</table>
<?php endif; ?>
</section>
</div>