<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/proposalpdf');

$tcpdf = new PROPOSALPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Form Usul Dosen Pembimbing - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(100);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(20, 30, 20, 30);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',10);

if (!empty($data)) {
    $saatini = date('Y-m-d H:i:s');
    $nmfile = 'usuldosen-' . $data['Ta']['nim'] . '.pdf';
    $judulta = nl2br($data['Ta']['judul']);
    
	$html = 
'<table cellpadding="2" style="font-size: 1.0em;" width="100%" border="0">
<tr><td colspan="2">
<div style="text-align: center;"><h2>FORMULIR PERMOHONAN<br/>PERGANTIAN DOSEN PEMBIMBING</h2>
<small>Dicetak tanggal: '. date('d-m-Y H:i:s') .'</small></div>
</td></tr>

<tr><td colspan="2">
<br/><br/>Yang bertanda tangan di bawah ini:
<p><table>
<tr style="line-height: 200%;"><td style="width:150px;">Nama</td><td style="width:15px;">:</td><td style="width:740px;">' . $data['Mahasiswa']['nama'] . '</td></tr>
<tr style="line-height: 200%;"><td>N I M</td><td>:</td><td>' . $data['Mahasiswa']['nim'] . '</td></tr>
<tr style="line-height: 200%;"><td>Judul Skripsi</td><td>:</td><td>' . $judulta . '</td></tr>
</table></p>
</td></tr>

<tr><td colspan="2">
<p style="line-height: 200%;">Mengajukan permohonan pergantian Dosen Pembimbing I / II*, <br/>
<table style="padding: 4px;">
<tr><td style="width: 50px;"><div>dari</div></td><td style="border-bottom: 1px dotted #c0c0c0; width: 440px;">&nbsp;</td></tr>
<tr><td style="width: 50px;"><div>ke</div></td><td style="border-bottom: 1px dotted #c0c0c0; width: 440px;">&nbsp;</td></tr>
</table>
</p>
</td></tr>

<tr><td colspan="2">
<p style="line-height: 200%;">Adapun alasan yang dapat saya sampaikan, antara lain: </p>
<div  style="border-bottom:1px dotted #c0c0c0;">&nbsp;</div>
<p>Demikian form permohonan ini dibuat agar dapat digunakan sebagaimana mestinya.</p>
<p>&nbsp;</p>
</td></tr>

<tr><td></td><td><div style="text-align:center;">
Yogyakarta, ' .  $this->Tools->prnDate($saatini) . '<br/><br/><br/><br/>
<div style="border-bottom:1px solid #000;">'. $data['Mahasiswa']['nama'] . '</div>'. $data['Ta']['nim'].'
</div></td></tr>
</table>';

} else {
	$html = '<p>&nbsp;</p><h2>Status skripsi Anda tidak ada yang aktif!</h2>';
	$nmfile = 'usuldosen-UNK.pdf';
}
$tcpdf->AddPage();
// output the HTML content
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->Line(20,30, 190, 30);
$tcpdf->lastPage();

$tcpdf->Output($nmfile, 'I');
?>
