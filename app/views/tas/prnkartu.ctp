<?php
App::import('Vendor','tcpdf/tcpdf');

$tcpdf = new TCPDF("P", PDF_UNIT, 'F4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Kartu Konsultasi Skripsi - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(10);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(10, 10, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',10);

if (!empty($data)) {
	$judulta = nl2br($data['Ta']['judul']);
        $dosen1 = trim($data['Dosen']['nama_dosen']);
        $dosen2 = trim($data['Dosen2']['nama_dosen']);

	$sem = '';
	if ($data['Ta']['sem']==1) {
		$sem = 'Gsl';
	} else if ($data['Ta']['sem']==2) {
		$sem = 'Gnp';
	}

	$logo = $this->Html->image('/app/webroot/img/logoukdw.png', array('align'=>'left', 'width'=>'35px;'));

  $html = '<table cellpadding="2">
	<tr><td style="width: 40px;">' . $logo . '</td><td style="width: 230px;">
	UNIVERSITAS KRISTEN DUTA WACANA<br/>
	FAKULTAS TEKNOLOGI INFORMASI<hr/>
  <strong style="font-size: 1.2em;">PROGRAM STUDI SISTEM INFORMASI</strong>
	</td><td style="width: 260px;">
	<p style="border: 1px solid #c0c0c0; font-size: 1.0em; width:150px; text-align: center; line-height: 200%; background-color: #c0c0c0;">
        <span style="font-size: 1.5em; text-align: center;"><strong>KARTU KONSULTASI SKRIPSI</strong></span><br/>
        Mulai Sem. ' . $sem . '/' . $data['Ta']['tahun'] . '
        </p>
	</td></tr>
	</table>';

  $html .= '<table width="100%" cellpadding="5" style="border: 2px solid black;">
	<tr><td>

	<table cellpadding="1">
  <tr>
  <td style="width: 65px">NIM</td><td style="width: 5px;">:</td><td style="width: 75px"><strong>'. $data['Ta']['nim'] . '</strong></td>
	<td style="width: 90px;">Nama Mahasiswa</td><td style="width: 5px;">:</td><td style="width: 350px;"><strong>'. $data['Mahasiswa']['nama'] .	'</strong></td>
	</tr>
	<tr>
	<td style="width: 65px;font-size: 0.8em;">Judul Skripsi</td><td style="width: 5px;">:</td><td colspan="4" style="font-size: 1.0em; width: 450px;"><strong>'. strtoupper($judulta) . '</strong></td>
	</tr>
	<tr>
	<td style="width: 65px; font-size: 0.8em;">Pembimbing I</td><td style="width: 5px;">:</td><td style="width: 200px; font-size: 0.8em;"><strong>'. $dosen1 . '</strong></td>
	<td style="width: 70px; font-size: 0.8em;">Pembimbing II</td><td style="width: 5px;">:</td><td style="width: 200px; font-size: 0.8em;"><strong>'. $dosen2 . '</strong></td>
	</tr>
	</table>

	</td></tr>
	</table>
<br/>';

$html .= '<table width="100%">
<tr>
<td style="width: 20px;"></td>
<td style="width: 75px;"></td><td style="width: 75px;"></td>
<td style="width: 80px;"></td>

<td style="width: 20px;"></td>

<td style="width: 20px;"></td>
<td style="width: 75px;"></td><td style="width: 75px;"></td>
<td style="width: 80px;"></td>
</tr>
<tr>
<td style="width: 95px; line-height: 200%; border: 1px solid black; text-align: center;" colspan="2">Pembimbing I</td>
<td style="width: 75px;"></td>
<td style="width: 80px;"></td>

<td style="width: 20px;"></td>

<td style="width: 95px; line-height: 200%; border: 1px solid black; text-align: center;" colspan="2">Pembimbing II</td>
<td style="width: 75px;"></td>
<td style="width: 80px;"></td>
</tr>';

for($m=1; $m<=5; $m++) {

$html .= '<tr>
<td style="width: 20px; background-color: black; border: 1px solid black; color: white; line-height: 200%; text-align: center; font-size: 1.2em;">' . $m .'</td>
<td style="width: 230px; border: 1px solid black;line-height: 150%; font-size: 0.9em;" colspan="3">  Tanggal Konsultasi :</td>

<td style="width: 20px;"></td>

<td style="width: 20px; background-color: black; border: 1px solid black; color: white; line-height: 200%; text-align: center; font-size: 1.2em;">' . $m . '</td>
<td style="width: 230px; line-height: 150%; border: 1px solid black;font-size: 0.9em;" colspan="3">  Tanggal Konsultasi :</td>
</tr>

<tr>
<td style="width: 170px; border: 1px solid black;" colspan="3" rowspan="2"><span style="font-size: 0.7em;">  Catatan Perkembangan/Revisi Skripsi:</span></td>
<td style="width: 80px; border: 1px solid black;"><span style="font-size: 0.8em;">  Paraf Dosen:</span>
<div style="line-height: 7px;"></div>
</td>
<td style="width: 20px;"></td>
<td style="width: 170px; border: 1px solid black;" colspan="3" rowspan="2"><span style="font-size: 0.7em;">  Catatan Perkembangan/Revisi Skripsi:</span></td>
<td style="width: 80px; border: 1px solid black;"><span style="font-size: 0.8em;">  Paraf Dosen:</span>
<div style="line-height: 7px;"></div>
</td>
</tr>

<tr>
<td style="width: 80px; border: 1px solid black;"><span style="font-size: 0.8em;">  Paraf Mahasiswa:</span>
<div style="line-height: 7px;"></div>
</td>
<td style="width: 20px;"></td>
<td style="width: 80px; border: 1px solid black;"><span style="font-size: 0.8em;">  Paraf Mahasiswa:</span>
<div style="line-height: 7px;"></div>
</td>
</tr>';

}
$html .= '</table>';

$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->lastPage();

$html = '<table width="100%">';

for($m=6; $m<=11; $m++) {

$html .= '<tr>
<td style="width: 20px; background-color: black; border: 1px solid black; color: white; line-height: 200%; text-align: center; font-size: 1.2em;">' . $m .'</td>
<td style="width: 230px; border: 1px solid black;line-height: 150%; font-size: 0.9em;" colspan="3">  Tanggal Konsultasi :</td>

<td style="width: 20px;"></td>

<td style="width: 20px; background-color: black; border: 1px solid black; color: white; line-height: 200%; text-align: center; font-size: 1.2em;">' . $m . '</td>
<td style="width: 230px; line-height: 150%; border: 1px solid black;font-size: 0.9em;" colspan="3">  Tanggal Konsultasi :</td>
</tr>

<tr>
<td style="width: 170px; border: 1px solid black;" colspan="3" rowspan="2"><span style="font-size: 0.7em;">  Catatan Perkembangan/Revisi Skripsi:</span></td>
<td style="width: 80px; border: 1px solid black;"><span style="font-size: 0.8em;">  Paraf Dosen:</span>
<div style="line-height: 7px;"></div>
</td>
<td style="width: 20px;"></td>
<td style="width: 170px; border: 1px solid black;" colspan="3" rowspan="2"><span style="font-size: 0.7em;">  Catatan Perkembangan/Revisi Skripsi:</span></td>
<td style="width: 80px; border: 1px solid black;"><span style="font-size: 0.8em;">  Paraf Dosen:</span>
<div style="line-height: 7px;"></div>
</td>
</tr>

<tr>
<td style="width: 80px; border: 1px solid black;"><span style="font-size: 0.8em;">  Paraf Mahasiswa:</span>
<div style="line-height: 7px;"></div>
</td>
<td style="width: 20px;"></td>
<td style="width: 80px; border: 1px solid black;"><span style="font-size: 0.8em;">  Paraf Mahasiswa:</span>
<div style="line-height: 7px;"></div>
</td>
</tr>';

}

$html .= '</table><br/>
Mengetahui,<br/>
Koordinator Skripsi SI<br/><br/><br/><br/>
(Drs. Wimmie Handiwidjojo, MIT.)
';

} else {
	$html = '<h2>NIM tersebut ada kemungkinan sedang tidak registrasi skripsi atau tidak aktif!</h2>';
}
$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->lastPage();

$tcpdf->Output('kartuskripsi-' . $data['Ta']['nim'] . '.pdf', 'I');
?>
