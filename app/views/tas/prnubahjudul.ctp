<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/proposalpdf');

$tcpdf = new PROPOSALPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Form Perubahan Judul - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(100);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(20, 30, 30, 30);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',10);

if (!empty($data)) {
    $saatini = date('Y-m-d H:i:s');
    $nmfile = 'ubahjudul-' . $data['Ta']['nim'] . '.pdf';
    $judulta = nl2br($data['Ta']['judul']);
    
	$html = 
'<table cellpadding="2" style="font-size: 1.0em;" width="100%" border="0">
<tr><td colspan="2">
<div style="text-align: center;"><h1>FORMULIR PERUBAHAN JUDUL SKRIPSI</h1>
<small>Dicetak tanggal: '. date('d-m-Y H:i:s') .'</small></div>
</td></tr>

<tr><td colspan="2">
<br/><br/><br/>Yang bertanda tangan di bawah ini:
<p><table>
<tr style="line-height: 200%;"><td style="width:185px;">Nama</td><td style="width:15px;">:</td><td style="width:760px;">' . $data['Mahasiswa']['nama'] . '</td></tr>
<tr style="line-height: 200%;"><td>N I M</td><td>:</td><td>' . $data['Mahasiswa']['nim'] . '</td></tr>
<tr style="line-height: 200%;"><td>Judul Skripsi Lama</td><td>:</td><td><strong>' . $judulta . '</strong></td></tr>
</table></p>
</td></tr>

<tr><td colspan="2">
<p style="line-height: 200%;">Mengajukan permohonan perubahan judul skripsi sebagai berikut: </p>
<p><table>
<tr style="line-height: 200%;"><td style="width:185px;">Judul Skripsi Baru</td><td style="width:15px;">:</td><td style="width:760px; border-bottom: 1px dotted #c0c0c0;">&nbsp;</td></tr>
<tr style="line-height: 200%;"><td></td><td></td><td style="border-bottom: 1px dotted #c0c0c0;">&nbsp;</td></tr>
<tr style="line-height: 200%;"><td></td><td></td><td style="border-bottom: 1px dotted #c0c0c0;">&nbsp;</td></tr>
<tr style="line-height: 200%;"><td></td><td></td><td style="border-bottom: 1px dotted #c0c0c0;">&nbsp;</td></tr>
</table></p>
<p></p>
<p>Demikian form permohonan ini dibuat agar dapat digunakan sebagaimana mestinya.</p>
<p>&nbsp;</p>
</td></tr>

<tr><td></td><td><div style="text-align:center;">
Yogyakarta, ' .  $this->Tools->prnDate($saatini) . '<br/><br/><br/><br/>
<div style="border-bottom:1px solid #000;">'. $data['Mahasiswa']['nama'] . '</div>'. $data['Ta']['nim'].'
</div></td></tr>

<tr><td style="padding: 4px;"><div style="text-align:center;"><br/><br/>
Mengetahui,<br/><br/><br/><br/>
<div style="border-bottom:1px solid #000;">'. $data['Dosen']['nama_dosen'] . '</div>
Dosen Pembimbing I
</div></td><td><div style="text-align:center;">
<br/><br/><br/><br/><br/><br/>
<div style="border-bottom:1px solid #000;">'. $data['Dosen2']['nama_dosen'] . '</div>
Dosen Pembimbing II
</div></td></tr>
</table>';

} else {
	$html = '<p>&nbsp;</p><h2>Status skripsi Anda tidak ada yang aktif!</h2>';
	$nmfile = 'ubahjudul-UNK.pdf';
}
$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->Line(20,30, 190, 30);
$tcpdf->lastPage();

$tcpdf->Output($nmfile, 'I');
?>
