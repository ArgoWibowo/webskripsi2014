<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-4'), array('class'=>'button', 'escape' => false)); ?>
<h3>Tambah Skripsi</h3>
<?php
	echo $this->Form->create('Ta', array('action' => 'add'));
	echo $this->Form->input('Ta.nim', array('id' => 'nim', 'label' => 'NIM: ', 'value' => '', 'error' => false, 'style'=> 'width: 100px;'));
	echo ('<div class="ceknim">');
	echo $this->Html->link('<span>Cek NIM</span>', '#', array('id' => 'ceknim_lnk', 'class'=>'button', 'style'=>'display:inline; vertical-align:middle;', 'escape' => false));
	echo ('<div id="nmmhs" style="display: inline; vertical-align:middle; font-size:1.2em; color: green;"></div>');
	echo ('</div>');
	echo $this->Form->input('Ta.judul', array('id' => 'judul', 'value' => '', 'label' => 'Judul Skripsi: ', 'error' => false, 'style'=> 'width: 100%;'));
	echo $this->Form->input('Ta.tahun', array(
		'div' => array('style' => 'clear:both; float:left; margin-top:11px;'),
		'id' => 'tahun', 
		'value'=>date('Y'), 
		'label' => 'Tahun Mulai Ta: ', 
		'error' => false, 
		'style'=> 'width: 100px;'
	));
	echo $this->Form->input('Ta.sem',array(
		'div' => array('style' => 'display: inline;'),
		'label' => __('Semester:',true),
		'type' => 'select',
		'options' => array(1 => 'Gasal', 2=>'Genap'),
		'empty' => false,
		'selected' => 1,
	));
	echo $this->Form->input('Ta.take', array(
		'id' => 'take', 
		'value'=>1, 
		'label' => __('Ta Ke: ', true), 
		'error' => false, 
		'style'=> 'width: 50px;'
	));
	echo $this->Form->input('Ta.thnkolokium', array(
		'div' => array('style' => 'clear:both; float:left; margin-top:11px;'),
		'id' => 'thnkolokium', 
		'value'=>'', 
		'label' => 'Tahun Lolos Kolokium: ', 
		'error' => false, 
		'style'=> 'width: 50px;'
	));
	echo $this->Form->input('Ta.blnkolokium',array(
		'div' => array('style' => 'display: inline;'),
		'label' => __('Bulan Lulus Kolokium:',true),
		'type' => 'select',
		'options' => array(1,2,3,4,5,6,7,8,9,10,11,12),
		'empty' => false,
		'selected' => 1,
	));
	echo $this->Form->input('Ta.aktif', array('id' => 'aktif', 'value'=>1, 'type' => 'hidden'));
	
	echo $this->Form->input('Ta.dosen1',array(
			'label' => __('Dosen Pembimbing I:',true),
			'type' => 'select',
			'options' => $dosens,
			'empty' => false
		));
	echo $this->Form->input('Ta.dosen2',array(
			'label' => __('Dosen Pembimbing II:',true),
			'type' => 'select',
			'options' => $dosens,
			'empty' => true
		));
	/*echo '<div id="tipeta">';
	echo $this->Form->input('Ta.tipe', array(
			'options' => array('R' => 'Reguler', 'L' => 'Literatur', 'T' => 'Terarah'),
			'type' => 'radio',
			'legend' => 'Tipe Skripsi',
			'default' => 'R'
		));
	echo '</div>';*/
    echo $this->Form->end( array('label' => 'Simpan TA Baru') ); 
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#tipeta').buttonset();
	$("#TaDosen1").multiselect({
	   multiple: false,
	   selectedList: 1
	});
	$("#TaDosen2").multiselect({
	   multiple: false,
	   selectedList: 1
	});
	
	$('#ceknim_lnk').click(function(event){
		$('#nmmhs').val('');
		$.getJSON('<?php echo($this->Html->url(array("controller"=>"tas", "action"=>"ceknim"))); ?>/' + $('#nim').val(), 
			  function(data){
					$('#nmmhs').text(data.Status.msg);
			  		if (data.Status.return != 0) {
						$('#nim').val('');
						$('#nmmhs').css('color', 'red');
					} else {
						$('#nmmhs').css('color', 'green');
					}
			  });
	});
});
</script>