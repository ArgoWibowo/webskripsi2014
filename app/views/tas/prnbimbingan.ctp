<div style="font-size:1.2em;">
<?php echo $this->element('header_report'); ?>
<div style="clear:both;"></div>
<p></p>
<section>
<?php if (!empty($data)): ?>
<div style="text-align:center;"><span style="font-weight: bold; font-size: 1.4em;">Daftar Peserta Skripsi</span><br/>
<span style="font-size: 0.85em;">Periode: 
<?php 
$blnkol = date('m', strtotime($data[0]['Ta']['periode_awal']));
$thnkol = date('Y', strtotime($data[0]['Ta']['periode_awal']));
if ($blnkol >= 1 && $blnkol <=6) {
	$sem = 'Genap';
	$tahun = $thnkol-1;
} else {
	$sem = 'Gasal';
	$tahun = $thnkol;
}
echo 'Semester ' . $sem . ' Tahun Ajaran ' . $tahun . '/' . ($tahun+1);
?>
</span></div><br/>
<?php if ($iddosen > 0) { ?>
<p style="font-size: 1.1em;">Dosen Pembimbing: <strong><?php echo $dosen['Dosen']['nama_dosen']; ?></strong></p>
<?php } ?>
<table id="box-report" style="width:100%;">
<thead>
	<tr>
		<th scope="col" rowspan="2">No.</th>
		<th scope="col" rowspan="2"><small>NIM</small></th>
		<th scope="col" rowspan="2"><small>Nama Mahasiswa</small></th>
		<th scope="col" rowspan="2"><small>Judul Tugas Akhir</small></th>
		<th scope="col" colspan="2"><small>Pembimbing</small></th>
		<th scope="col" rowspan="2"><small>Mulai</small></th>
	</tr>
	<tr>
		<th>I</th>
		<th>II</th>
	</tr>
</thead>
<tbody>
<?php 
$i = 0;
foreach($data as $d): ?>
	<tr>
		<td style="width: 20px; text-align:center;"><?php echo ++$i; ?></td>
		<td style="width: 80px; text-align:center;"><small><?php echo $d['Ta']['nim'] ?></small></td>
		<td style="width: 150px; text-align:left;"><small><?php echo $d['Mahasiswa']['nama'] ?></small></td>
		<td style="width: 470px; text-align:left;"><small><?php echo $d['Ta']['judul'] ?></small></td>
		<td style="width: 150px; font-size: 0.8em; text-align:left;"><small>
			<?php 
			if ($d['Dosen']['id'] == $iddosen) {
				echo '<strong>' . $d['Dosen']['nama_dosen'] . '</strong>';
			} else {
				echo $d['Dosen']['nama_dosen'];
			}
			?>
		</small></td>
		<td style="width: 150px; font-size: 0.8em; text-align:left;"><small>
			<?php 
			if ($d['Dosen2']['id'] == $iddosen) {
				echo '<strong>' . $d['Dosen2']['nama_dosen'] . '</strong>';
			} else {
				echo $d['Dosen2']['nama_dosen'];
			}
			?>
		</small></td>
		<td style="width: 40px; font-size: 0.8em; text-align:center; "><?php echo $d['Ta']['tahun'] ?> / <?php
					if ($d['Ta']['sem']==1) { 
						echo 'Gsl'; 
					} else if ($d['Ta']['sem']==2) {
						echo 'Gnp';
					} ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else: ?>
<h2>Belum ada peserta bimbingan untuk id tersebut</h2>
<?php endif; ?>
</section>
</div>