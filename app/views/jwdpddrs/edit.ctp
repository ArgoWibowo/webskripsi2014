<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', 
	array('controller'=>'admin', 'action'=>'home', '#tabs-8'), array('class'=>'button', 'escape' => false)); ?>
<h3>Ubah Jadwal Tenggat Pendadaran</h3>
<?php
	echo $this->Form->create('Jwdpddr', array('action' => 'edit'));
	echo $this->Form->input('Jwdpddr.id', array('type' => 'hidden', 
								'value' => $data['Jwdpddr']['id']));
	echo $this->Form->input('Jwdpddr.batas', array('value'=> $data['Jwdpddr']['batas'], 
								'label' => 'Batas Akhir Pendaftaran: ', 'dateFormat' => 'DMY'));
	echo $this->Form->end( array('label' => 'Save') ); 
?>