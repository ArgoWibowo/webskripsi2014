<?php
// file: /app/views/des/catat.ctp

echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-8'), array('class'=>'button', 'escape' => false));
?>
<p>&nbsp;</p>
<h3>Tambah Jadwal Pelaksanaan Desk Evaluation</h3>
<table style="font-size:1.2em;">
	<tr><td style="width:250px;">Tanggal Pelaksanaan Desk Evaluation</td>
		<td style="width:15px;">:</td>
		<td style="font-weight: bold;"><?php echo $this->Tools->prnDate($data['De']['tanggal'],4); ?> (Kelompok <?php echo $data['De']['kelompok']?>)</td>
	</tr>
	<tr><td>Daftar Dosen Evaluator</td>
		<td style="width:15px;">:</td>
		<td>
			<ul>
			<?php 
			$dsn = array();
			foreach($data['Dosen'] as $d) {
				array_push($dsn, $d['id']);
				echo '<li>' . $d['nidn'] . ' ' . $d['nama_dosen'] . '</li>';
			}
			?>
			</ul>
		</td>
	</tr>
	<tr>
		<td colspan="3">Daftar Mahasiswa yang dievaluasi dalam tim ini:</td>
	</tr>
	<tr>
		<td colspan="3">
			<?php 
			$mhs = array();
			$selected = array();
			foreach($data['Proposal'] as $pr) {
				array_push($selected, $pr['id']);
				$mhs[$pr['id']] = $pr['User']['nim'] . ' - ' . $pr['User']['fullname'] . '. Judul: '. $pr['judul'] . ' (' . (!empty($pr['Dosen'])?$pr['Dosen']['nama']:'-') . ')';
			}
			foreach($proposals as $p) {
				if (in_array($p['Dosen']['id'], $dsn) || !$p['Dosen']['id']) {
					if ($p['Dosen']['nama']) {
						$nmdosen = $p['Dosen']['nama'];
					} else {
						$nmdosen = 'none';
					}
					if (!isset($mhs[$p['Proposal']['id']])) {
						$mhs[$p['Proposal']['id']] = $p['User']['nim'] . ' - ' . $p['User']['fullname'] . '. Judul: '. $p['Proposal']['judul'] . ' (' . (!empty($p['Dosen'])?$p['Dosen']['nama']:'-') . ')';
					}
				}
			}
			
			echo $this->Form->create('De', array('action' => 'simpancatat'));
			echo $this->Form->input('De.id', array('type'=>'hidden', 'value' => $data['De']['id']));
			echo $this->Form->input('Proposal.Proposal', array('type'=>'select', 'multiple'=>'checkbox', 
					'options'=> $mhs, 
					'selected' => $selected, 'style'=>'width: 400px;', 
					'label' => ''));
			echo $this->Form->end( array('label' => 'Save') ); 
			?>

		</td>
	</tr>
</table>
