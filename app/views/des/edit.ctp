<?php 
// file: app/views/des/edit.ctp

echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-8'), array('class'=>'button', 'escape' => false)); ?>
<h3>Tambah Jadwal Pelaksanaan Desk Evaluation</h3>
<?php
$selected = array();
foreach($data['Dosen'] as $t){
	array_push($selected, $t['id']);
}

echo $this->Form->create('De', array('action' => 'add'));
echo $this->Form->input('De.id', array('type' => 'hidden', 'value' => $data['De']['id']));
echo $this->Form->input('De.tanggal', array('id' => 'tanggal', 
							'value' => $data['De']['tanggal'], 'dateFormat' => 'DMY', 
							'label' => 'Tanggal Pelaksanaan Desk Evaluation: '));
echo $this->Form->input('De.kelompok', array(
							'options' => array('A' => 'Kelompok A', 'B' => 'Kelompok B', 'C' => 'Kelompok C', 'D' => 'Kelompok D', 'E' => 'Kelompok E'),
							'type' => 'select',
							'div' => array('id' => 'kelompok_des'),
							'default' => $data['De']['kelompok']
						));
echo $this->Form->input('De.idbatas', array(
							'options' => $jwddeskevaluationlist,
							'type' => 'select',
							'label' => 'Pengaturan untuk tanggal batas pengumpulan desk evaluation:',
							'div' => array('id' => 'idbatasde'),
							'default' => $data['De']['idbatas']
						));
echo $this->Form->input('Dosen.Dosen', array('type'=>'select', 'multiple'=>'checkbox', 'options'=> $dosens, 
						'selected' => $selected, 'style'=>'width: 400px;', 
						'label' => 'Pilih Dosen anggota Kelompok Desk Evaluation:'));

echo $this->Form->end( array('label' => 'Save') ); 
?>