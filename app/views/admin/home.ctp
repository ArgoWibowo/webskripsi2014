<?php echo $this->Session->flash(); ?>
<?php echo $this->Html->link('<span>< Logout</span>', array('controller' => 'main', 'action' => 'logout'), array('class' => 'button', 'escape' => false) ); ?>
<h3><?php echo $judul; ?></h3>
<div id="please-wait"><h3 style="font-size: 1.3em; color: green;">Mohon ditunggu....</h3></div>
<?php
/**********************************************************************************************************************
 *  untuk ADMINISTRATOR
 *
 **********************************************************************************************************************/
if ($group == 1):
?>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Pengumuman</a></li>
		<li><a href="#tabs-2">Desk Evaluation</a></li>
		<li><a href="#tabs-3">Proposal</a></li>
		<li><a href="#tabs-4">Skripsi</a></li>
		<li><a href="#tabs-5">Pendadaran</a></li>
		<li><a href="#tabs-6">Akun User</a></li>
		<li><a href="#tabs-7">Dosen</a></li>
		<li><a href="#tabs-8">Jadwal</a></li>
		<li><a href="#tabs-9">Presensi</a></li>
	</ul>
	<div id="tabs-1">
		<div>	
			<?php 
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Add new news.")), '/blogs/add', 
			                                       array('escape' => false, 'title' => 'Add new News.'));
			
			if (isset( $blogs ) && is_array( $blogs )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(array('Title', 'Last modified', 'Published', 'Action')); ?>
				<?php if (sizeof( $blogs ) == 0) : ?>
				<tr>
					<td colspan="4">Belum ada pengumuman untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($blogs as $post) : ?>
				<tr class="sorot">			
					<td style="width: 600px;"><?php echo $post['Blog']['title']; ?></td>
					<td><?php echo date('D, d M Y h:i:s a', strtotime($post['Blog']['modified'])); ?></td>
					<td class="actions"><?php echo $this->Html->link(($post['Blog']['published'] == 1? 'Published' : 'Unpublished'),
						'/blogs/'. ($post['Blog']['published'] == 1? 'disable' : 'enable').'/'.$post['Blog']['id']); ?></td>
					
					<td class="actions"><?php echo $this->Html->link('Edit', '/blogs/edit/'.$post['Blog']['id']); ?> | 			
					 <?php echo $this->Html->link('Delete', '/blogs/delete/'.$post['Blog']['id'], array('class' => 'confirmLink') ); ?></td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>

			<?php
			echo $this->element('paging', array('model' => 'Blog', 'tabname' => 'tabs-1'));
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-1 -->
	
	<div id="tabs-2">
		<div>
			<?php 
			//echo $this->Html->link($this->Html->image("add.png", array("alt" => "Tambah Proposal baru.")), '/proposals/tambah', 
			//                                       array('escape' => false, 'title' => 'Tambah proposal baru.'));
			
			if (isset( $proposals ) && is_array( $proposals )): ?>
			<div class="searchbox" style="width: 800px;"> 
			<?php if (!empty($all_jwdde)) { ?>
				<p style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Cetak Peserta
				<select name="seltglde"  id="seltglde" style="vertical-align: middle;">
					<option value="" selected="selected"></option>
				<?php 
					foreach($all_jwdde as $j):
						echo '<option value="' . $j['De']['id'] . '">'. 
							 $this->Tools->prnDate($j['De']['tanggal'],2) . 
							 ' (Kelompok ' . $j['De']['kelompok'] . ')' . 
							 '</option>';
					endforeach;
				?>
				</select>
				<span class="actions">
				<?php
				echo $this->Html->link('<span>Tampilkan</span>', '#tabs-2', 
							   array('escape' => false, 'id'=>'prnprolist',
									 'title' => 'Tampilkan Daftar Peserta Ringkasan Proposal berdasar tanggal.'));
				?>
				</span>
				<!-- select name="mnuprint" id="mnuprint" style="vertical-align: middle;">
					<option value="" selected="selected"></option>
					<option value="j">Daftar Peserta (web)</option>
					<option value="s">Surat Pengantar TA</option>
					<option value="b">Berita Acara</option>
				</select -->
				</p>
			<?php } ?>

			<p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="formsearchbox2" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="proposal" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="hidden" value="Proposal" name="data[Cari][model]">
				
				<?php if (isset($pencarian)): ?>
				<input type="text" name="data[Cari][q]" style="width: 200px; vertical-align: middle;" value="<?php echo $pencarian; ?>"/>
				<?php else: ?>
				<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
				<?php endif; ?>
				<?php $qq = (isset($pilihproposal))? $pilihproposal : ""; ?>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="all" <?php echo ($qq == 'all')? 'selected="selected"': ""; ?>>Semua</option>
					<option value="nim" <?php echo ($qq == 'nim')? 'selected="selected"': ""; ?>>NIM</option>
					<option value="nama" <?php echo ($qq == 'nama')? 'selected="selected"': ""; ?>>Nama Mahasiswa</option>
					<option value="judul" <?php echo ($qq == 'judul')? 'selected="selected"': ""; ?>>Usulan Judul</option>
					<option value="dosen" <?php echo ($qq == 'dosen')? 'selected="selected"': ""; ?>>Nama Dosen</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			
			<table>
				<?php 
				echo $this->Html->tableHeaders(
							array('NIM', 'Nama Mahasiswa', 'Judul', 'Tgl DE', 'Status', '','')); ?>
				<?php if (sizeof( $proposals ) == 0) : ?>
				<tr>
					<td colspan="4">Belum ada ringkasan proposal terkumpul untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php 
				foreach ($proposals as $post) : ?>
				<tr class="sorot">
					<td style="width:60px;"><?php echo $post['User']['nim']; ?></td>
					<td style="width:120px;"><?php echo $post['User']['fullname']; ?></td>
					<td style="width: 500px;"><?php echo $post['Proposal']['judul']; ?></td>
					<?php if ($post['De']) { ?>
					<td><?php echo date('d-m-Y', strtotime($post['De'][0]['tanggal'])); ?></td>
					<?php } else { ?>
					<td>-</td>
					<?php } ?>
					<td style="width:80px;">
						<?php if ($post['Proposal']['status_proposal'] == 0) { ?>
						<span style="background-color:#5CAAFF; padding: 2px;">Baru</span>
						<?php } elseif ($post['Proposal']['status_proposal'] == 1) { ?>
						<span style="background-color:#FFE43E; padding: 2px;">Terkirim</span>
						<?php } elseif ($post['Proposal']['status_proposal'] == 2) { ?>
						<span style="background-color:#FF5228; padding: 2px;">Blm Diterima</span>
						<?php } else { ?>
						<span style="background-color:#79FF54; padding: 2px;">Diterima</span>
						<?php } ?>
					</td>
					<td  style="width:35px;">
						<?php 
						if ($post['Proposal']['valid'] == 0) {
							echo $this->Html->link( '<span class="icon-valid" style="background-position: 0px 0px;"></span>', '#tabs-2', 
												array('escape'=>false, 'class'=>'btnvalidate', 'isvalid' => '1', 'idproposal' => $post['Proposal']['id'], 
													  'id' => 'btnvalidate' . $post['Proposal']['id'],
													  'title'=>'BELUM tervalidasi. Klik untuk validasi Pembayaran Desk Evaluation') ); 
						} else {
							echo $this->Html->link( '<span class="icon-valid" style="background-position: -24px 0px;"></span>', '#tabs-2', 
												array('escape'=>false, 'class'=>'btnvalidate', 'isvalid' => '0', 'idproposal' => $post['Proposal']['id'], 
													  'id' => 'btnvalidate' . $post['Proposal']['id'],
													  'title'=>'SUDAH tervalidasi. Klik untuk menonvalidasikan Pembayaran Desk Evaluation') ); 
						}
						?>
					</td>
					<td class="actions">
						<?php 
							if ($post['Proposal']['status_proposal'] == 1 && $post['De'] && $post['Proposal']['valid']==1) {
								echo $this->Html->link('Evaluasi', '/proposals/evaluasi/'.$post['Proposal']['id']); 
							}
						?>
						<?php echo $this->Html->link('Lihat', '/proposals/view/'.$post['Proposal']['id'], 
								array('title' => 'Lihat Evaluasi')); ?>
					 	<?php echo $this->Html->link('Delete', '/proposals/delete/'.$post['Proposal']['id'], array('class' => 'confirmLink') ); ?>
						<?php echo $this->Html->link('Cetak', '/proposals/prnproposal/'.$post['Proposal']['id'], array('target' => '_blank')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			
			<?php
			echo $this->element('paging', array('model' => 'Proposal', 'tabname' => 'tabs-2'));
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-2 -->
	
	<div id="tabs-3">
		<div>	
			<?php 

			//echo $this->Html->link($this->Html->image("add.png", array("alt" => "Tambah Peserta Kolokium.")), '/papers/add', 
			//									   array('escape' => false, 'title' => 'Tambahkan Pendaftar Kolokium.'));
			
			if (isset( $kolokiums ) && is_array( $kolokiums )): ?>
			<div class="searchbox" style="width: 800px;"> 
			<?php if (!empty($all_jadwal)) { ?>
				<p style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Cetak Peserta
				<select name="tglkol"  id="tglkol" style="vertical-align: middle;">
					<option value="" selected="selected"></option>
				<?php 
					foreach($all_jadwal as $j):
						echo '<option value="' . $j['Jadwal']['id'] . '">'. date('d-m-Y', strtotime($j['Jadwal']['tanggal'])) .'</option>';
					endforeach;
				?>
				</select>
				<span class="actions">
				<?php
				echo $this->Html->link('<span>Tampilkan</span>', '#tabs-3', 
										   array('escape' => false, 'id'=>'prnkololist',
												 'title' => 'Tampilkan Daftar Peserta Kolokium berdasar tanggal.'));
				?>
				</span>
				<!-- select name="mnuprint" id="mnuprint" style="vertical-align: middle;">
					<option value="" selected="selected"></option>
					<option value="j">Daftar Peserta (web)</option>
					<option value="s">Surat Pengantar TA</option>
					<option value="b">Berita Acara</option>
				</select -->
				</p>
			<?php } ?>
			<p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="formsearchbox1" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="kolokium" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="hidden" value="Kolokium" name="data[Cari][model]">
				
				<?php if (isset($pencarian)): ?>
				<input type="text" name="data[Cari][q]" style="width: 200px; vertical-align: middle;" value="<?php echo $pencarian; ?>"/>
				<?php else: ?>
				<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
				<?php endif; ?>
				<?php $qq = (isset($pilihan))? $pilihan : ""; ?>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="nim" <?php echo ($qq == 'nim')? 'selected="selected"': ""; ?>>NIM</option>
					<option value="tglkol" <?php echo ($qq == 'tglkol')? 'selected="selected"': ""; ?>>Tanggal Kolokium</option>
					<option value="judul" <?php echo ($qq == 'judul')? 'selected="selected"': ""; ?>>Usulan Judul</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			<table>
				<?php echo $this->Html->tableHeaders(
								array('', 'NIM', 'Judul', 'Tanggal Kolokium', 'Tgl Kumpul', 'Status')); ?>
				<?php if (sizeof( $kolokiums ) == 0) : ?>
				<tr>
					<td colspan="6">Belum ada data Kolokium untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($kolokiums as $post) : ?>
				<tr class="sorot">
					<td style="width: 120px;">
						<div>
						<?php 
						if ($post['Kolokium']['status'] == 'N' || $post['Kolokium']['status'] == 'V'):
							echo $this->Html->link('<span class="icon-actions" style="background-position: -96px 0px;"></span>', '/papers/delete/'.$post['Kolokium']['id'], 
                               array('class' => 'confirmLink', 'escape'=>false, 'title'=>'Hapus Data Kolokium') );
						endif;
						if ($post['Kolokium']['status'] != 'N' && $post['Kolokium']['status'] != 'V'):
							echo $this->Html->link('<span class="icon-actions" style="background-position: 0px 0px;"></span>', '/papers/edit/'.$post['Kolokium']['id'], 
									array('escape'=>false, 'title'=>'Edit Data Kolokium') );
							echo $this->Html->link($this->Html->image("baicon.png", 
								   array("alt" => "Berita Acara")), '#tabs-3', 
								   array('escape' => false, 'class'=>'ba-icon', 'idba'=> $post['Kolokium']['id'], 
											  'title' => 'Memasukkan Berita Acara Kolokium.',
											  'status' => $post['Kolokium']['status'])); 
						endif;
						echo $this->Html->link('<span class="icon-actions" style="background-position: -72px 0px;"></span>', 
                           '/papers/prnba/'.$post['Kolokium']['id'], array('escape'=>false, 
																		   'target' => '_blank',
																		   'title'=>'Cetak Berita Acara Kolokium') );
						?>
						</div>
					</td>
					<td><?php echo $post['Kolokium']['nim']; ?></td>
					<td style="width: 600px;"><?php echo $post['Kolokium']['judul']; ?> (
						<?php echo $this->Html->link('Download', array('controller' => 'papers', 'action' => 'download', $post['Kolokium']['id']) ); ?>)
					</td>
					<td style="width: 100px;"><?php echo date('d-M-Y', strtotime($post['Jadwal']['tanggal'])); ?></td>
					<td style="width: 100px;">
						<?php
						/*if ($post['Kolokium']['status'] == 'L' || $post['Kolokium']['status'] == 'B'):
							if ($post['Kolokium']['tipe'] == 'L'):
								echo 'Literatur';
							elseif ($post['Kolokium']['tipe'] == 'R'):
								echo 'Regular';
							elseif ($post['Kolokium']['tipe'] == 'T'):
								echo 'Terarah';
							endif;
						else:
							echo '<span class="tipe-kolokium">';
							echo $this->Form->input('Kolokium.tipe.' . $post['Kolokium']['id'], array(
								'options' => array('R' => 'Regular', 'T' => 'Terarah', 'L' => 'Literatur'),
								'type' => 'select',
								'legend' => null,
								'label' => false,
								'class' => 'tipe-kolokium',
								'id' => 'tipekolokium'.$post['Kolokium']['id'],
								'default' => $post['Kolokium']['tipe']
							) );
							echo '</span>';
						endif;*/
						?>
						<?php echo date('d-M-Y', strtotime($post['Jadwal']['tglkumpul'])); ?>
					</td>
					<td>
						<?php
						if ($post['Kolokium']['status'] == 'L' || $post['Kolokium']['status'] == 'B' || 
							$post['Kolokium']['status'] == 'S' || 
							$post['Kolokium']['status'] == 'V'):
							if ($post['Kolokium']['status'] == 'N'):
								echo 'Baru'; 
							elseif ($post['Kolokium']['status'] == 'L'):
								echo 'Lulus';
							elseif ($post['Kolokium']['status'] == 'B'):
								echo 'Belum Disetujui';
							elseif ($post['Kolokium']['status'] == 'V'):
								echo 'Valid';
							elseif ($post['Kolokium']['status'] == 'S'):
								echo 'Lulus Bersyarat';
							endif;
						else:
						?>
						<form class="formstatuskolokium">
							<span class="statuskolokium">
								<?php echo $this->Form->input('Kolokium.status.' . $post['Kolokium']['id'], array(
										'options' => array('N' => 'Baru', 'V' => 'Valid'),
										'type' => 'select',
										'legend' => '',
										'label' => false,
										'class' => 'kolstat',
										'id' => $post['Kolokium']['id'],
										'default' => $post['Kolokium']['status']
									));
								?>
							</span>
						</form>
						<?php endif; ?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>

			<?php
			if (isset($m) && isset($o) && isset($q) && $m == 'kolokium') {
				echo $this->element('paging', array('model' => 'Kolokium', 'tabname' => 'tabs-3', 
									'opsi' => array('m' => $m, 'o' => $o, 'q' => $q)
								));
			} else {
				echo $this->element('paging', array('model' => 'Kolokium', 'tabname' => 'tabs-3'));
			}
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-3 -->
	
	<div id="tabs-4">
		<div>	
			<?php
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Tambah Skripsi.")), '/tas/add', 
			                                       array('escape' => false, 'title' => 'Tambahkan Skripsi.'));
			
			if (isset( $tas ) && is_array( $tas )): ?>
			<div class="searchbox" style="width: 1100px;"> 
			<p style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">
				<select name="menuskripsi"  id="menuskripsi" style="vertical-align: middle;">
					<option value="1">Daftar Skripsi</option>
					<option value="2">Jumlah Bimbingan</option>
				</select>
				<span class="actions"><a href="#tabs-4" id="btnlihatskripsi">Lihat</a></span>
			</p>
			<p style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Cetak Bimbingan
				<select name="dosenbimbing"  id="dosenbimbing" style="vertical-align: middle;">
					<option value="" selected="selected"></option>
					<option value="-1" selected="selected">Semua Dosen</option>
				<?php 
					foreach($alldosens as $j):
						echo '<option value="' . $j['Dosen']['id'] . '">'. $j['Dosen']['nama_dosen'] .'</option>';
					endforeach;
				?>
				</select>
			</p>
			<p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="formsearchbox4" style="display:inline;" method="post" 
						action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="ta" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="text" name="data[Cari][q]" value="" style="width: 150px; vertical-align: middle;"/>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="nim">NIM</option>
					<option value="nmmhs">Nama Mahasiswa</option>
					<option value="nmdsn">Nama Dosen</option>
					<option value="judul">Judul Skripsi</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			<table>
				<?php echo $this->Html->tableHeaders(
						array('', 'NIM', 'Nama', 'Judul', 'Dosen I', 'Dosen II', 'Thn.<br/>Mulai', 
                           	  'Sem.<br/>Mulai', 'Ta<br/>Ke', 'Lulus?', 'Status')); ?>
				<?php if (sizeof( $tas ) == 0) : ?>
				<tr>
					<td colspan="9">Belum ada data Skripsi untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($tas as $post) : ?>
				<tr class="sorot">
					<td style="width: 200px;"><div>
						<?php
							if (($post['Ta']['aktif']==1 || $post['Ta']['aktif']==0) && $post['Ta']['lulus']==0):
								echo $this->Html->link('<span class="icon-actions" style="background-position: 0px 0px;"></span>', '/tas/edit/'.$post['Ta']['id'], 
									array('escape'=>false, 'title'=>'Edit Data Skripsi') );
								echo $this->Html->link('<span class="icon-actions" style="background-position: -96px 0px;"></span>', '/tas/delete/'.$post['Ta']['id'], 
									array('class' => 'confirmLink', 'escape'=>false, 'title'=>'Hapus Data Skripsi') );
								echo $this->Html->link('<span class="icon-actions" style="background-position: -72px 0px;"></span>', '/papers/prnletter/'.$post['Ta']['id'], 
									array('escape'=>false, 'title'=>'Cetak Surat Pengantar Scripti', 'target' => '_blank') );
								if ($post['Ta']['aktif']==1){
									echo $this->Html->link('<span class="icon-actions" style="background-position: -120px 0px;"></span>', '#tabs-3', 
										array('escape'=>false, 'class' => 'regis-dadar', 
								  		'id' => 'dp' . $post['Ta']['id'], 'title'=>'Daftarkan Pendadaran') );
								}
							endif;
							echo $this->Html->link('<span class="icon-actions" style="background-position: 0px 0px;"></span>', '/turnitins/add/'.$post['Ta']['id'], 
								array('escape'=>false, 'title'=>'Upload PDF Scripsi') );
							echo $this->Html->link('<span class="icon-actions" style="background-position: -72px 0px;"></span>', '/files/scanskripsi/'.$post['Ta']['id'].'.pdf',
								array('escape'=>false, 'title'=>'Unduh Hasil PDF Scripsi', 'target' => '_blank') );
							echo $this->Html->link('<span class="icon-actions" style="background-position: 0px 0px;"></span>', '/turnitins/addhasil/'.$post['Ta']['id'], 
								array('escape'=>false, 'title'=>'Upload Hasil Turn It In Skripsi') );
							echo $this->Html->link('<span class="icon-actions" style="background-position: -72px 0px;"></span>', '/files/turnitin/'.$post['Ta']['id'].'.pdf', 
								array('escape'=>false, 'title'=>'Unduh Hasil Turn It In Skripsi', 'target' => '_blank') );
						?>
						</div>
					</td>
					<td style="width: 50px;">
						<?php echo $this->Html->link($post['Ta']['nim'], '/tas/prnkartu/'.$post['Ta']['id'], array('title' => 'Cetak Kartu Konsultasi Skripsi', 'target'=>'_blank')); ?>
					</td>
					<td style="width: 100px;"><?php echo $post['Mahasiswa']['nama']; ?></td>
					<?php if ($post['Ta']['aktif']==1 || $post['Ta']['aktif']==0){ ?>
					<td style="width: 600px;"><?php echo nl2br($post['Ta']['judul']); ?>&nbsp;&nbsp;&nbsp;(<a href="#tabs-3" class="editjudul" idta="<?php echo $post['Ta']['id']; ?>">edit</a>)</td>
					<?php } else { ?>
					<td style="width: 600px;"><?php echo nl2br($post['Ta']['judul']); ?></td>
					<?php } ?>
					<td style="width: 70px;"><?php echo $post['Dosen']['nama']; ?></td>
					<td style="width: 70px;"><?php echo $post['Dosen2']['nama']; ?></td>
					<td style="width: 50px;"><?php echo $post['Ta']['tahun']; ?></td>
					<td style="width: 50px;"><?php
					if ($post['Ta']['sem']==1) { 
						echo 'Gsl'; 
					} else if ($post['Ta']['sem']==2) {
						echo 'Gnp';
					} ?></td>
					<td style="width: 50px;"><?php echo $post['Ta']['take']; ?></td>
					<td style="width: 50px;"><?php echo ($post['Ta']['lulus']==1)?'Sdh.':'Blm.'; ?></td>
					<td style="width: 50px;"><?php 
						if ($post['Ta']['aktif']==1):
							echo 'Skrg';
						elseif ($post['Ta']['aktif']==0):
							echo 'NA';
						else:
							echo 'Batal';
						endif; ?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>

			<?php
			if (isset($m) && isset($o) && isset($q) && $m == 'ta') {
				echo $this->element('paging', array('model' => 'Ta', 'tabname' => 'tabs-4', 'opsi' => array('m' => $m, 'o' => $o, 'q' => $q)));
			} else {
				echo $this->element('paging', array('model' => 'Ta', 'tabname' => 'tabs-4'));
			}
			?>
			<?php endif; ?>
		</div>
	</div>  <!-- end of tabs-4 -->
	
	<div id="tabs-5">
		<div>
			<div class="searchbox" style="width: 800px;"> 
			<?php if (!empty($alljdwldadar)) { ?>
				<p style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Cetak Pendadaran
				<select name="batasdadar"  id="prndadarform-batasdadar" style="vertical-align: middle;">
					<option value="" selected="selected"></option>
				<?php 
					foreach($alljdwldadar as $j):
						echo '<option value="' . $j['Jwdpddr']['id'] . '">'. date('d-m-Y', strtotime($j['Jwdpddr']['batasakhir'])) .'</option>';
					endforeach;
				?>
				</select>
				</p>
			<?php } ?>
			<p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="saringdadarform" style="display:inline;" method="post" 
						action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="dadar" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="nim">NIM</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			<p>&nbsp;</p><p>&nbsp;</p>
			<?php
			if (isset( $dadars ) && is_array( $dadars )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(array('', 'NIM', 'Judul', 'Ujian Ke', 'Dosen I', 'Dosen II', 'Jadwal', 'Penguji I', 'Penguji II', 'Status')); ?>
				<?php if (sizeof( $dadars ) == 0) : ?>
				<tr>
					<td colspan="8">Belum ada data Pendadaran untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($dadars as $post) : ?>
				<tr class="sorot">
					<td style="width: 85px;">
						<div>
						<?php
							if ($post['Pendadaran']['status'] == 0 || $post['Pendadaran']['status'] == 1) {
								echo $this->Html->link('<span class="icon-actions" style="background-position: -96px 0px;"></span>', 
									'/pendadarans/delete/'.$post['Pendadaran']['id'], 
									array('class' => 'confirmLink', 'escape'=>false, 
										'title'=>'Hapus Data Pendadaran') );
								if ($post['Pendadaran']['status'] == 0) {
									echo $this->Html->link('<span class="icon-actions" style="background-position: -144px 0px;"></span>', 
										'#tabs-5', 
										array('class' => 'valdadar', 'escape'=>false, 
													  'id' => 'vd'.$post['Pendadaran']['id'],
													  'nim' => $post['Ta']['nim'],
													  'idta' => $post['Ta']['id'],
													  'title'=>'Validasi Pendaftaran Pendadaran') );
								} else if ($post['Pendadaran']['status'] == 1) {
									echo $this->Html->link('<span class="icon-actions" style="background-position: -192px 0px;"></span>', 
															'/pendadarans/prnletter/' . $post['Pendadaran']['id'], 
															array('escape'=>false, 
																  'title'=>'Cetak Surat Terima Pendaftaran Pendadaran',
																  'target' => '_blank') );
								}
							}
						?>
						</div>
					</td>
					<?php if($post['Pendadaran']['status'] == 1) {?>
					<td style="width: 40px;"><?php echo $this->Html->link($post['Ta']['nim'], array('controller'=>'pendadarans', 'action'=>'prnbadadar', $post['Pendadaran']['id']), array('title'=>'Cetak Berita Acara Pendadaran', 'target' => '_blank')); ?><br/>
						<?php echo $this->Html->link(
							'[History]', 
							'/history/history/' . $post['Ta']['nim'], 
							array(	'title'=>'Lihat History Skripsi Mahasiswa',
									'target' => '_blank') );
					  	?>
					</td>
					<?php } else {  ?>
					<td style="width: 40px;">
						<?php echo $post['Ta']['nim']; ?><br/>
						<?php echo $this->Html->link(
							'[History]', 
							'/history/history/' . $post['Ta']['nim'], 
							array(	'title'=>'Lihat History Skripsi Mahasiswa',
									'target' => '_blank') );
					  	?>
					</td>
					<?php } ?>
					<td style="width: 600px;">
						<?php echo nl2br($post['Ta']['judul']); ?> <br/>
						<?php 
							if ($post['Pendadaran']['status'] == 1) {
								echo '[' . $this->Html->link('Cetak Label', array('controller'=>'pendadarans', 'action'=>'prnlabel', $post['Pendadaran']['id']), array('title'=>'Cetak Label Berkas Pendadaran', 'target'=>'_blank') ) . ']'; 
							}
						?>
					</td>
					<td style="width: 40px; text-align: center;">
						<?php echo $post['Pendadaran']['ujianke']; ?>
					</td>
					<td style="width: 90px; font-size: 0.8em;">
						<?php
							if ($post['Pendadaran']['ketua'] == 1) {
								echo '<strong>' . $post['Dosen']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 90px; font-size: 0.8em;">
						<?php
							if ($post['Pendadaran']['ketua'] == 2) {
								echo '<strong>' . $post['Dosen2']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen2']['nama_dosen'];
							}
						?>
					</td>
					<?php if ($post['Pendadaran']['status'] == 1) { ?>
					<td style="width: 50px;"><?php echo $this->Html->link(date('d-m-Y H:i:s', strtotime($post['Pendadaran']['jadwal'])), 
						'#tabs-4', array('class' => 'aturjadwaldadar', 'dadarid' => $post['Pendadaran']['id'], 'title' => 'Pengaturan Jadwal Ujian Skripsi')); ?></td>
					<?php } else { ?>
					<td style="width: 50px;"><?php echo date('d-m-Y H:i:s', strtotime($post['Pendadaran']['jadwal'])); ?></td>
					<?php } ?>
					<td style="width: 90px; font-size: 0.8em;">
						<?php
							if ($post['Pendadaran']['ketua'] == 3) {
								echo '<strong>' . $post['Dosen3']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen3']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 90px; font-size: 0.8em;">
						<?php
							if ($post['Pendadaran']['ketua'] == 4) {
								echo '<strong>' . $post['Dosen4']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen4']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 50px;" class="actions"><?php 
						if ($post['Pendadaran']['status']==1):
							echo $this->Html->link('Valid', '#tabs-5', array('class'=>'hasildadar', 'iddadar'=> $post['Pendadaran']['id'], 'title'=>'Set Nilai Pendadaran') );
						elseif ($post['Pendadaran']['status']==0):
							echo 'Baru';
						else:
							echo $this->Html->link('Sudah', '#tabs-5', array('class'=>'hasildadar', 'iddadar'=> $post['Pendadaran']['id'], 'title'=>'Set Nilai Pendadaran') );
						endif; ?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			
			<?php
			if (isset($m) && isset($o) && isset($q) && $m == 'dadar') {
				echo $this->element('paging', array('model' => 'Pendadaran', 'tabname' => 'tabs-5', 'opsi' => array('m' => $m, 'o' => $o, 'q' => $q) ) );
			} else {
				echo $this->element('paging', array('model' => 'Pendadaran', 'tabname' => 'tabs-5'));
			}
			?>
			<?php endif; ?>
		</div>
	</div>  <!-- end of tabs-5 -->
	
	<div id="tabs-6">
		<div>
			<?php
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Add new user.")), '/users/tambah', 
			                                        array('escape' => false, 'title' => 'Add a New User.'));
			if (isset( $users ) && is_array( $users )):
			?>
				<div class="searchbox"> <p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
				<form id="formsearchbox6" style="display:inline;" method="post" 
				            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
					<input type="hidden" value="POST" name="_method">
					<input type="hidden" value="user" name="data[Cari][m]">
					<input type="hidden" value="1" name="data[Cari][page]">
					<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
					<select name="data[Cari][o]" style="vertical-align: middle;">
						<option value="nim">NIM/NIK</option>
						<option value="nama">Nama Akun</option>
					</select>
					<input type="submit" value="Cari" style="vertical-align: middle;"/>
				</form>
				</div>

				<table>
				<?php echo $this->Html->tableHeaders(array('Username', 'NIM', 'Nama Lengkap', 'email', 'No Telp.', '')); ?>
				<?php if (sizeof( $users ) == 0) : ?>
				<tr>
					<td colspan="6">Belum ada data user saat ini!</td>
				</tr>
				<?php else:?>
					<?php foreach ($users as $d) : ?>
						<tr class="sorot">
							<td><?php echo $d['User']['username']; ?></td>
							<td><?php echo $d['User']['nim']; ?></td>
							<td><?php echo $d['User']['fullname']; ?></td>
							<td><?php echo $d['User']['email']; ?></td>
							<td><?php echo $d['User']['telpno']; ?></td>
							<td class="actions">
								<?php 
								echo $this->Html->link('Edit', '/users/edit/'.$d['User']['id']);
								if ($this->Session->read('User.id') != $d['User']['id']):
									echo $this->Html->link('Delete', '/users/del/'.$d['User']['id'], array('class' => 'confirmLink'));
								endif;
								echo $this->Html->link('passwd', '/users/chpwd/'.$d['User']['id']);
								if ($this->Session->read('User.id') != $d['User']['id']):
									echo $this->Html->link(($d['User']['active'] == 1? 'Active' : 'Disable'),
									'/users/'. ($d['User']['active'] == 1? 'disable' : 'enable').'/'.$d['User']['id']); 
								endif;
								?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
				</table>
				
				<?php
				if (isset($m) && isset($o) && isset($q) && $m == 'user') {
					echo $this->element('paging', array('model' => 'User', 'tabname' => 'tabs-6', 'opsi'=>array('m' => $m, 'o' => $o, 'q' => $q)));
				} else {
					echo $this->element('paging', array('model' => 'User', 'tabname' => 'tabs-6'));
				}
				?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-6 -->
	
	<div id="tabs-7">
		<div>
			<?php 
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Add new Dosen.")), '/dosens/add', 
			                                       array('escape' => false, 'title' => 'Add new Dosen.'));
			if (isset( $dosens ) && is_array( $dosens )): ?>
			<div class="searchbox"> <p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="formsearchbox3" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="dosen" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="nidn">NIDN</option>
					<option value="nama">Nama Dosen</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			<table>
				<?php echo $this->Html->tableHeaders(array('NIDN', 'Nama Dosen', 'email', 'Telpon', 'Boleh?', 'Status', '')); ?>
				<?php if (sizeof( $dosens ) == 0) : ?>
				<tr>
					<td colspan="7">Belum ada data dosen untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($dosens as $dosen) : ?>
				<tr class="sorot">
					<td><?php echo $dosen['Dosen']['nidn']; ?></td>
					<td><?php echo $dosen['Dosen']['nama_dosen']; ?></td>
					<td><?php echo $dosen['Dosen']['email']; ?></td>
					<td><?php echo $dosen['Dosen']['telpno']; ?></td>
					<?php if ($dosen['Dosen']['boleh'] == 'B'): ?>
					<td><?php echo 'Boleh' ?></td>
					<?php else: ?>
					<td><?php echo 'Belum Boleh' ?></td>
					<?php endif; ?>
					<?php if ($dosen['Dosen']['status'] == 'K'): ?>
					<td><?php echo 'Kol./Skripsi'; ?></td>
					<?php else: ?>
					<td><?php echo 'Skripsi'; ?></td>
					<?php endif; ?>
					<td class="actions"><?php echo $this->Html->link('Edit', '/dosens/edit/'.$dosen['Dosen']['id']); ?> 
					 <?php echo $this->Html->link('Delete', '/dosens/delete/'.$dosen['Dosen']['id'], array('class' => 'confirmLink') ); ?></td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>

			<?php
			if (isset($m) && isset($o) && isset($q) && $m == 'dosen') {
				echo $this->element('paging', array('model' => 'Dosen', 'tabname' => 'tabs-7', 'opsi' => array('m' => $m, 'o' => $o, 'q' => $q)));
			} else {
				echo $this->element('paging', array('model' => 'Dosen', 'tabname' => 'tabs-7'));
			}
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-7 -->
	
	<div id="tabs-8">
		<div>	
			<?php 
			// jadwal desk evaluation
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Tambah Jadwal Desk Evaluation.")), 
							'/jwddeskevaluations/add', 
			                array('escape' => false, 'title' => 'Tambah Jadwal Desk Evaluation.'));
			echo '<span style="font-size: 1.7em; margin-left: 20px; font-weight: bold;">Jadwal Desk Evaluation</span>';
			if (isset( $jwddeskevaluations ) && is_array( $jwddeskevaluations )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(array('Jadwal Akhir Pendaftaran', '')); ?>
				<?php if (sizeof( $jwddeskevaluations ) == 0) : ?>
				<tr>
					<td colspan="3">Belum ada jadwal tenggat desk evaluation untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($jwddeskevaluations as $post) : ?>
				<tr class="sorot">
					<td><?php echo date('D, d M Y h:i:s a', strtotime($post['Jwddeskevaluation']['batas'])); ?></td>
					<td class="actions">
					 <?php echo $this->Html->link('Edit', 
					 		'/jwddeskevaluations/edit/'.$post['Jwddeskevaluation']['id']); ?> 
					 <?php echo $this->Html->link('Delete', 
					 		'/jwddeskevaluations/delete/'.$post['Jwddeskevaluation']['id'], 
					 		array('class' => 'confirmLink') ); ?>
					 <?php //echo $this->Html->link('Pencatat', '/jwddeskevaluations/catat/'.$post['Jwddeskevaluation']['id'] ); ?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			<?php
			echo $this->element('paging', array('model' => 'Jwddeskevaluation', 'tabname' => 'tabs-8'));
			?>
			<?php endif; ?>
			<p>&nbsp;</p>
			
			<?php
			// jadwal pelaksanaan desk evaluation
			echo $this->Html->link($this->Html->image("add.png", 
					array("alt" => "Tambah Jadwal Pelaksanaan Desk Evaluation.")), 
							'/des/add', 
			                array('escape' => false, 'title' => 'Tambah Jadwal Pelaksanaan Desk Evaluation.'));
			echo '<span style="font-size: 1.7em; margin-left: 20px; font-weight: bold;">Jadwal Pelaksanaan Desk Evaluation</span>';
			if (isset( $des ) && is_array( $des )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(
								array('Tanggal Pelaksanaan', 'Kelompok', 'Batas Kumpul DE', '')); ?>
				<?php if (sizeof( $des ) == 0) : ?>
				<tr>
					<td colspan="4">Belum ada jadwal pelaksanaan desk evaluation untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($des as $post) : ?>
				<tr class="sorot">
					<td><?php echo $this->Tools->prnDate($post['De']['tanggal'], 4); ?></td>
					<td><?php echo $post['De']['kelompok']; ?></td>
					<td><?php echo $this->Tools->prnDate($post['Jwddeskevaluation']['batas']); ?></td>
					<td class="actions">
					 <?php echo $this->Html->link('Edit', 
					 		'/des/edit/'.$post['De']['id']); ?> 
					 <?php echo $this->Html->link('Delete', 
					 		'/des/delete/'.$post['De']['id'], 
					 		array('class' => 'confirmLink') ); ?>
					 <?php echo $this->Html->link('Peserta', '/des/catat/'.$post['De']['id'] ); ?>
					</td>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			<?php
			echo $this->element('paging', array('model' => 'De', 'tabname' => 'tabs-8'));
			?>
			<?php endif; ?>
			<p>&nbsp;</p>

			<?php 
			// Jadwal Kolokium
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Tambah Jadwal Kolokium.")), '/jadwals/add', 
			                                        array('escape' => false, 'title' => 'Tambah Jadwal Kolokium.'));
			echo '<span style="font-size: 1.7em; margin-left: 20px; font-weight: bold;">Jadwal Pendaftaran Kolokium</span>';
			if (isset( $jadwals ) && is_array( $jadwals )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(array('Jadwal Kolokium', 'Jadwal Akhir Pendaftaran', 'Tgl Kumpul', '')); ?>
				<?php if (sizeof( $jadwals ) == 0) : ?>
				<tr>
					<td colspan="3">Belum ada jadwal kolokium untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($jadwals as $post) : ?>
				<tr class="sorot">
					<td><?php echo date('D, d M Y h:i:s a', strtotime($post['Jadwal']['tanggal'])); ?></td>
					<td><?php echo date('D, d M Y h:i:s a', strtotime($post['Jadwal']['batas'])); ?></td>
					<td><?php echo date('D, d M Y h:i:s a', strtotime($post['Jadwal']['tglkumpul'])); ?></td>
					<td class="actions">
						<?php echo $this->Html->link('Edit', '/jadwals/edit/'.$post['Jadwal']['id']); ?> 
						<?php echo $this->Html->link('Delete', '/jadwals/delete/'.$post['Jadwal']['id'], array('class' => 'confirmLink') ); ?>
						<?php echo $this->Html->link('Tim Penguji', '/jadwals/catat/'.$post['Jadwal']['id'] ); ?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			<?php
			echo $this->element('paging', array('model' => 'Jadwal', 'tabname' => 'tabs-8'));
			?>
			<?php endif; ?>
			<p>&nbsp;</p>
			
			<?php 
			// jadwal pendadaran
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Tambah Jadwal Pendadaran.")), '/jwdpddrs/add', 
			                                        array('escape' => false, 'title' => 'Tambah Jadwal Pendadaran.'));
			echo '<span style="font-size: 1.7em; margin-left: 20px; font-weight: bold;">Jadwal Pendaftaran Pendadaran</span>';
			if (isset( $jwdpddrs ) && is_array( $jwdpddrs )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(array('Jadwal Akhir Pendaftaran', '')); ?>
				<?php if (sizeof( $jwdpddrs ) == 0) : ?>
				<tr>
					<td colspan="3">Belum ada jadwal tenggat pendadaran untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($jwdpddrs as $post) : ?>
				<tr class="sorot">
					<td><?php echo date('D, d M Y h:i:s a', strtotime($post['Jwdpddr']['batas'])); ?></td>
					<td class="actions"><?php echo $this->Html->link('Edit', '/jwdpddrs/edit/'.$post['Jwdpddr']['id']); ?> 
					 <?php echo $this->Html->link('Delete', '/jwdpddrs/delete/'.$post['Jwdpddr']['id'], array('class' => 'confirmLink') ); ?></td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			<?php
			echo $this->element('paging', array('model' => 'Jwdpddr', 'tabname' => 'tabs-8'));
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-8 -->
	
	<div id="tabs-9">
		<div id="box-tgl-presensi">
			<form id="formsearchbox4" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="presensi" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="hidden" name="data[Cari][q]" value="-"/>
				<span style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Presensi: </span>
				<select name="data[Cari][t]" id="tipepresensi" style="vertical-align: middle;">
					<?php if ($tipepresensi == 0) { ?>
					<option value="0" selected="selected">Desk Evaluation</option>
					<option value="1">Kolokium</option>
					<?php } elseif ($tipepresensi == 1) { ?>
					<option value="0">Desk Evaluation</option>
					<option value="1" selected="selected">Kolokium</option>
					<?php } ?>
				</select>
				<span id="blok_kolokium">
					<span id="label_tglpresensi" style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Tanggal Kolokium</span>
					<select name="data[Cari][o]" id="tgl-presensi" style="vertical-align: middle;">
						<option value="" selected="selected"></option>
						<?php 
						if ($tipepresensi == 1) {
							foreach($all_jadwal as $j) {
								$strtglformat = $this->Tools->prnDate($j['Jadwal']['tanggal']);
								if (!empty($tglpresensi) && $tglpresensi['Jadwal']['id'] == $j['Jadwal']['id']) {
									echo '<option value="' . $j['Jadwal']['id'] . '" selected="selected">'. $strtglformat .'</option>';
								} else {
									echo '<option value="' . $j['Jadwal']['id'] . '">'. $strtglformat .'</option>';
								}
							}
						} elseif ($tipepresensi == 0) {
							foreach($all_jwdde as $j) {
								$strtglformat = $this->Tools->prnDate($j['De']['tanggal'],4) . ' (Kelompok: '. $j['De']['kelompok'] .')';
								if (!empty($tglpresensi) && $tglpresensi['De']['id'] == $j['De']['id']) {
									echo '<option value="' . $j['De']['id'] . '" selected="selected">'. $strtglformat .'</option>';
								} else {
									echo '<option value="' . $j['De']['id'] . '">'. $strtglformat .'</option>';
								}
							}
						}
						?>
					</select>
				</span>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
		</div>
		<p style="text-align:center;">
			<?php if (!empty($tglpresensi)) { 
				if ($tipepresensi == 1) {
					$isbtncari = $tglpresensi['Jadwal']['id'];
					$strlabel = "Presensi Kolokium Periode: " . $this->Tools->prnDate($tglpresensi['Jadwal']['tanggal']);
				} elseif ($tipepresensi == 0) {
					$isbtncari = $tglpresensi['De']['id'];
					$strlabel = "Presensi Desk Evaluation Periode: " . 
								$this->Tools->prnDate($tglpresensi['De']['tanggal'], 4);
				} else {
					$isbtncari = "";
					$strlabel = "Tidak Valid";
				}
			?>
			<span id="isbtncari" value="<?php echo $isbtncari ?>"></span>
			<span style="display:inline; font-size: 1.5em; font-weight: bold;">
				<?php echo $strlabel; ?>
			</span>
			<?php } else { ?>
			<span id="isbtncari" value=""></span>
			<?php } ?>
		</p>
		<div id="data-presensi" style="margin-top: 30px;">
			<a href="#tabs-9" class="button" id="new-presensi"><span>+ Tambah Dosen yang hadir</span></a>
			<a href="#tabs-9" class="button" id="print-presensi"><span>Cetak</span></a>
			<!-- a href="#tabs-9" class="button" id="printall-presensi"><span>Cetak</span></a -->
			<p>&nbsp;</p><p>&nbsp;</p>
			<table class="display" id="tabelpresensi">
				<thead>
					<tr>
						<th>iddata</th>
						<th>Dosen</th>
						<th>Jam Datang</th>
						<th>Jam Selesai</th>
						<th>Lama (jam)</th>
						<th>Ubah</th>
						<th>Hapus</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$i=0; 
					foreach($presensis as $d):  ?>
						<tr class="<?php echo ($i %2 == 0)?'even':'odd'; ?>">
							<td><?php echo $d['Presensi']['id']; ?></td>
							<td><?php echo $d['Presensi']['id_dosen'] . '-' . $d['Dosen']['nama']; ?></td>
							<td><?php echo date('d-m-Y H:i:s', strtotime($d['Presensi']['waktu_datang'])); ?></td>
							<td><?php echo date('d-m-Y H:i:s', strtotime($d['Presensi']['waktu_pulang'])); ?></td>
							<td><?php echo $d['Presensi']['lama']; ?></td>
							<td><a href="#tabs-9" class="edit" id="pu_<?php echo $d['Presensi']['id']; ?>">Edit</a></td>
							<td><a href="#tabs-9" class="delete" id="ph_<?php echo $d['Presensi']['id']; ?>">Delete</a></td>
						</tr>
					<?php  endforeach;  ?>
				</tbody>
			</table>
			<p>&nbsp;</p>
		</div>
	</div> <!-- end of tabs-9 -->
</div>

<div id="berita-acara" title="Berita Acara Kolokium">
<div>NIM/NAMA MAHASISWA: <span style="color: green; font-size: 1.25em;" id="baform-nim">Mohon ditunggu...</span> / <span style="color: green; font-size: 1.25em;" id="baform-nama"></span></div>
<form id="baform">
	<input type="hidden" name="data[Ba][id]" id="baform-id" value="" />
	<div><label for="baform-judulproposal">Judul Proposal</label>
		<textarea row="1" cols="80" style="width:100%" id="baform-judulproposal" name="data[Ba][judul_proposal]"></textarea>
	</div>
	<div><label for="baform-judul">Judul Skripsi yang Ditetapkan</label>
		<textarea row="1" cols="80" style="width:100%" id="baform-judul" name="data[Ba][judul]"></textarea>
	</div>
	<div>
		<label id="label-catatan" for="baformcatatan" style="cursor: pointer;"><span class="icon-collapse" style="background-position: -16px 0px;"></span>&nbsp;&nbsp;Catatan Hasil Kolokium</label>
		<span id="blokcatatan">
	    	<textarea row="3" cols="80" style="width:100%" id="baformcatatan" name="data[Ba][catatan]"></textarea>
		</span>
	</div>
	<div>
		<label id="label-internal" for="baforminternal" style="cursor: pointer;"><span  class="icon-collapse" style="background-position: 0px 0px;"></span>&nbsp;&nbsp;Catatan Internal Kolokium</label>
		<span id="blokinternal" style="display:none;">
	    	<textarea row="3" cols="80" style="width:100%" id="baforminternal" name="data[Ba][catatan_internal]"></textarea>
		</span>
	</div>
	<table>
	<tr><td>
	<?php echo $this->Form->input('dosen1',array(
			//'div' => array('style' => 'float:left; margin-top:11px;'),
			'label' => __('Dosen Pembimbing I:',true),
			'type' => 'select',
			'options' => $listdosens,
			'id' => 'baform-dosen1',
			'name' => 'data[Ba][dosen1]',
			'empty' => true
		));	?></td><td>
	<?php
		echo $this->Form->input('status', array(
			//'div' => array('style' => 'clear:both; float:left; margin-top:11px;'),
			'options' => array('L' => 'Lulus', 'S' => 'Lulus dengan Syarat', 'B' => 'Belum bisa diterima'),
			'type' => 'select',
			'id' => 'baform-status',
			'name' => 'data[Ba][status]',
			'default' => 'B'
		)); ?></td><td>
	<?php echo $this->Form->input('batas_kumpul', array(
			'id' => 'baform-batas_kumpul',
			'name' => 'data[Ba][batas_kumpul]'
		) );
	?></td>
	</tr></table>
</form>
</div>

<div id="form-edit-judul" title="Edit Judul Skripsi">
<div>NIM/NAMA MAHASISWA: <span style="color: green; font-size: 1.25em;" id="editjudulform-nim">Mohon ditunggu...</span> / <span style="color: green; font-size: 1.25em;" id="editjudulform-nama"></span></div>
<form id="editjudulform">
	<input type="hidden" name="id" id="editjudulform-id" value="" />
	<div><label for="editjudulform-judul">Judul Skripsi</label>
		<textarea rows="4" cols="80" style="width:100%" id="editjudulform-judul" name="judul"></textarea>
	</div>
</form>
</div>

<div id="form-daftar-pendadaran" title="Pendaftaran Pendadaran">
<div>NIM/NAMA MAHASISWA: <span style="color: green; font-size: 1.25em;" id="regdadarform-nim">Mohon ditunggu...</span> / <span style="color: green; font-size: 1.25em;" id="regdadarform-nama"></span></div>
<form id="regdadarform">
	<input type="hidden" name="id" id="regdadarform-id" value="" />
	<div><label for="regdadarform-judul">Judul Skripsi</label>
		<textarea row="2" cols="80" style="width:100%" id="regdadarform-judul" name="judul"></textarea>
	</div>
	Pendaftaran Pendadaran dengan pendaftaran terakhir:
	<?php echo $this->Form->input('batas',array(
			'div' => array('style'=>'display: inline; vertical-align: middle;'),
			'type' => 'select',
			'options' => $jdwlpendadaran,
			'id' => 'regdadarform-batas',
			'name' => 'batas',
			'label' => false,
			'empty' => false
		));	?>
</form>
</div>

<div id="form-atur-pendadaran" title="Pengaturan Jadwal Pendadaran">
<div>NIM/NAMA MAHASISWA: <span style="color: green; font-size: 1.25em;" id="aturdadarform-nim">Mohon ditunggu...</span> / <span style="color: green; font-size: 1.25em;" id="aturdadarform-nama"></span></div>
<form id="aturdadarform">
	<input type="hidden" name="id" id="aturdadarform-id" value="" />
	<div><label for="aturdadarform-judul">Judul Skripsi</label>
		<textarea row="2" cols="80" style="width:100%" id="aturdadarform-judul" name="judul"></textarea>
	</div>
	Ujian Ke: <input type="text" name="ujianke" id="aturdadarform-ujianke" value="" readonly="readonly" style="width: 50px;"/><br/>
	<?php echo $this->Form->input('penguji1',array(
			'div' => array('style' => 'float:left; margin-top:11px;'),
			'label' => __('Dosen Penguji 1:',true),
			'type' => 'select',
			'options' => $listdosens,
			'id' => 'aturdadarform-penguji1',
			'name' => 'penguji1'
		));	?>
	<?php echo $this->Form->input('penguji2',array(
			'div' => array('style' => 'display: inline'),
			'label' => __('Dosen Penguji 2:',true),
			'type' => 'select',
			'options' => $listdosens,
			'id' => 'aturdadarform-penguji2',
			'name' => 'penguji2'
		));	?>
	<div style="clear:both;"></div>
	<?php echo $this->Form->input('penguji3',array(
			'div' => array('style' => 'float:left;margin-top:11px;'),
			'label' => __('Dosen Penguji 3:',true),
			'type' => 'select',
			'options' => $listdosens,
			'id' => 'aturdadarform-penguji3',
			'name' => 'penguji3',
			'empty' => true
		));	?>
	<?php echo $this->Form->input('penguji4',array(
			'div' => array('style' => 'display: inline;'),
			'label' => __('Dosen Penguji 4:',true),
			'type' => 'select',
			'options' => $listdosens,
			'id' => 'aturdadarform-penguji4',
			'name' => 'penguji4',
			'empty' => true
		));	?>
	<div class="clear:both;">	
		<?php echo $this->Form->input('ketua',array(
			'div' => array('style' => 'display: inline;'),
			'label' => __('Ketua Penguji:',true),
			'type' => 'select',
			'options' => array("1" => "Penguji 1", "2" => "Penguji 2", "3" => "Penguji 3", "4" => "Penguji 4"),
			'id' => 'aturdadarform-ketua',
			'name' => 'ketua',
			'empty' => true
		));	?>
	</div>
	<div class="clear:both;">Jadwal Ujian Skripsi: <input style="width:150px;" type="text" name="jadwal" id="aturdadarform-jadwal" value=""/></div>
</form>
</div>

<div id="form-hasil-pendadaran" title="Hasil Pendadaran">
<div>NIM/NAMA MAHASISWA: <span style="color: green; font-size: 1.25em;" id="hasildadarform-nim">Mohon ditunggu...</span> / <span style="color: green; font-size: 1.25em;" id="hasildadarform-nama"></span></div>
<form id="hasildadarform">
	<input type="hidden" name="id" id="hasildadarform-id" value="" />
	<div><label for="hasildadarform-judul">Judul Skripsi</label>
		<textarea row="2" cols="80" style="width:100%" id="hasildadarform-judul" name="judul"></textarea>
	</div>
	<strong>Ujian Ke:&nbsp;&nbsp;</strong><span id="hasildadarform-ujianke"></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Jadwal Pendadaran:&nbsp;&nbsp;</strong><span id="hasildadarform-jadwal"></span><br/><br/>
	Nilai Pembimbing:<br/>
	<ul style="display: block; width: 500px;">
	<li><div><span id="hasildadarform-pembimbing1"></span>&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="nilai1" id="hasildadarform-nilai1" value="0" style="width: 50px; float: right;"/></div></li>
	<li><div><span id="hasildadarform-pembimbing2"></span>&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="nilai2" id="hasildadarform-nilai2" value="0" style="width: 50px; float: right;"/><div></li>
	</ul>
	Nilai Ujian:<br/>
	<ul style="display: block; width: 500px;">
	<li><div><span id="hasildadarform-penguji1"></span>&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="nilai3" id="hasildadarform-nilai3" value="0" style="width: 50px; float: right;"/></div></li>
	<li><div><span id="hasildadarform-penguji2"></span>&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="nilai4" id="hasildadarform-nilai4" value="0" style="width: 50px; float: right;"/></div></li>
	<li><div><span id="hasildadarform-penguji3"></span>&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="nilai5" id="hasildadarform-nilai5" value="0" style="width: 50px; float: right;"/></div></li>
	<li><div><span id="hasildadarform-penguji4"></span>&nbsp;&nbsp;:&nbsp;&nbsp;<input type="text" name="nilai6" id="hasildadarform-nilai6" value="0" style="width: 50px; float: right;"/></div></li>
	</ul>
	<div><strong>Hasil Ujian: </strong><span>
	<select name="lulus" id="hasildadarform-lulus">
	<option value="1">Belum Lulus</option>
	<option value="2">Lulus</option>
	</select>
	</span>&nbsp;&nbsp;&nbsp;&nbsp;<strong>NILAI AKHIR: </strong>
	<span style="font-size: 1.0em; color: blue;" id="hasildadarform-nilaiakhir"></span>&nbsp;&nbsp;(<span style="font-weight: bold; font-size: 1.2em;" id="hasildadarform-nilaihuruf"></span>)</div>
</form>
</div>

<div id="cetak-presensi-dialog" title="Periode Cetak Presensi">
<form id="cetak-periode-form">
	Jadwal Kolokium<br/><br/>Awal: 
	<select name="periode-awal" id="cetak-periode-form-awal" style="vertical-align: middle;">
	<?php 
	foreach($all_jadwal as $j):
		echo '<option value="' . $j['Jadwal']['id'] . '">'. $j['Jadwal']['tanggal'] .'</option>';
	endforeach;
	?>
	</select>
	&nbsp;&nbsp;&nbsp;Akhir: 
	<select name="periode-awal" id="cetak-periode-form-akhir" style="vertical-align: middle;">
	<?php 
	foreach($all_jadwal as $j):
		echo '<option value="' . $j['Jadwal']['id'] . '">'. $j['Jadwal']['tanggal'] .'</option>';
	endforeach;
	?>
	</select>
</form>
</div>

<?php
/**********************************************************************************************************************
 *  untuk MAHASISWA (for student)
 *
 **********************************************************************************************************************/
elseif ($group == 2):  
?>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Pengumuman</a></li>
		<li><a href="#tabs-2">Desk Evaluation</a></li>
		<li><a href="#tabs-3">Proposal</a></li>
		<li><a href="#tabs-4">Skripsi</a></li>
		<li><a href="#tabs-5">Pendadaran</a></li>
		<li><a href="#tabs-6">Profil</a></li>
	</ul>
	<div id="tabs-1">
		<div class="pengumuman">
			<?php 
			if (isset( $blogs ) && is_array( $blogs )): ?>
				<?php if (sizeof( $blogs ) == 0) : ?>
					<p>Belum ada pengumuman untuk saat ini.</p>
				<?php else: ?>
					<ul class="news-list">
					<?php foreach ($blogs as $post) : ?>
						<li><article>
						<header><h4 class="entry-title"><?php echo $post['Blog']['title']; ?></h4></header>
						<footer><?php echo date('D, d M Y h:i:s a', strtotime($post['Blog']['modified'])); ?></footer>
						<div class="news-content"><?php echo $post['Blog']['content']; ?></div>
						</article></li>
					<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			<?php endif; ?>
			
			<div class="paginating">
				<?php $this->Paginator->options(array('model' => 'Blog', 'url' => array_merge($this->passedArgs, array('#' => 'tabs-1')))); ?>
				<p><?php echo $this->Paginator->counter(array('model'=>'Blog', 'format' =>'Page {:page} of {:pages}'));?></p>
				<div class="paging">
					<?php echo $this->Paginator->prev('« Previous', array('model' => 'Blog'), null, array('class' => 'disabled')); ?>
					<?php echo $this->Paginator->numbers( array('model' => 'Blog', 'separator' => null) ); ?>
					<?php echo $this->Paginator->next('Next »', array('model' => 'Blog'), null, array('class' => 'disabled'));?>
				</div>
			</div>
		</div>
	</div> <!-- end of tabs-1 -->
	
	<div id="tabs-2">
		<div>
			<?php 
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Tambah Proposal baru.")), 
					'/proposals/tambah', 
               		array('escape' => false, 'title' => 'Tambah proposal baru.', 'id' => 'addproposal_btn'));
			
			if (isset( $proposals ) && is_array( $proposals )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(array('NIM', 'Nama Mahasiswa', 'Judul', 'Status', '')); ?>
				<?php if (sizeof( $proposals ) == 0) : ?>
				<tr>
					<td colspan="4">Belum ada proposal untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($proposals as $post) : ?>
				<tr class="sorot">
					<td><?php echo $post['User']['nim']; ?></td>
					<td><?php echo $post['User']['fullname']; ?></td>
					<td style="width: 600px;"><?php echo $post['Proposal']['judul']; ?></td>
					<td>
						<?php if ($post['Proposal']['status_proposal'] == 0) { ?>
						<span style="background-color:#5CAAFF; padding: 2px;">Baru</span>
						<?php } elseif ($post['Proposal']['status_proposal'] == 1) { ?>
						<span style="background-color:#FFE43E; padding: 2px;">Terkirim</span>
						<?php } elseif ($post['Proposal']['status_proposal'] == 2) { ?>
						<span style="background-color:#FF5228; padding: 2px;">Blm Diterima</span>
						<?php } else { ?>
						<span style="background-color:#79FF54; padding: 2px;">Diterima</span>
						<?php } ?>
					</td>
					<td class="actions">
					<?php if ($post['Proposal']['status_proposal'] == 0) { 
						echo $this->Html->link('Edit', '/proposals/edit/'.$post['Proposal']['id']);
					    echo $this->Html->link('Delete', '/proposals/delete/'.$post['Proposal']['id'], array('class' => 'confirmLink') ); 
					    echo $this->Html->link('Kumpul', '#'.$post['Proposal']['id'], array('class' => 'confirmKumpul', 'kirimid' => $post['Proposal']['id']) ); 
					}
					if ($post['Proposal']['status_proposal'] == 2 || $post['Proposal']['status_proposal'] == 3) {
						echo $this->Html->link('Cetak', '/proposals/prnproposal/'.$post['Proposal']['id'], array('target' => '_blank'));
					}?>
					</td>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			
			<?php
			echo $this->element('paging', array('model' => 'Proposal', 'tabname' => 'tabs-2'));
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-2 -->
	
	<div id="tabs-3">
		<div>
			<?php 
			echo $this->Html->link($this->Html->image("add.png", array("alt" => "Add new news.")), '#tabs-3', 
			                                        array('escape' => false, 'id' => 'addkolo3', 
			                                        	  'title' => 'Tambahkan Jadwal Kolokium.'));
			
			if (isset( $kolokiums ) && is_array( $kolokiums )): ?>
			<div class="searchbox"> <p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="formsearchbox" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="kolokium" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="nim">NIM</option>
					<option value="judul">Usulan Judul</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			<table>
				<?php echo $this->Html->tableHeaders(
							array('NIM', 'Judul', 'Tanggal Kolokium', 'Last modified', '')); ?>
				<?php if (sizeof( $kolokiums ) == 0) : ?>
				<tr>
					<td colspan="5">Belum ada data Kolokium untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($kolokiums as $post) : ?>
				<tr class="sorot">
					<td style="width: 100px;"><?php echo $post['Kolokium']['nim']; ?></td>
					<td style="width: 600px;"><?php echo $post['Kolokium']['judul']; ?> 
					<?php if ($nimaktif == $post['Kolokium']['nim']): ?>
						(<?php echo $this->Html->link('Download', array('controller' => 'papers', 'action' => 'download', $post['Kolokium']['id']) ); ?>)
					<?php endif; ?>
					</td>
					<td style="width: 100px;"><?php echo date('d-M-Y', strtotime($post['Jadwal']['tanggal'])); ?></td>
					<td style="width: 100px;"><?php echo date('d-M-Y', strtotime($post['Kolokium']['modified'])); ?></td>
					<td class="actions">
					<?php 
					if ($nimaktif == $post['Kolokium']['nim'] && $post['Kolokium']['status'] == 'N'): 
						echo $this->Html->link('Edit', '/papers/edit/'.$post['Kolokium']['id']);
						echo $this->Html->link('Delete', '/papers/delete/'.$post['Kolokium']['id'], array('class' => 'confirmLink') );
					else:
						if ($post['Kolokium']['status'] == 'N'):
							echo 'Baru'; 
						elseif ($post['Kolokium']['status'] == 'L'):
							echo 'Lulus';
						elseif ($post['Kolokium']['status'] == 'B'):
							echo 'Belum Disetujui';
						elseif ($post['Kolokium']['status'] == 'V'):
							echo 'Valid';
						elseif ($post['Kolokium']['status'] == 'S'):
							echo 'Lulus Bersyarat';
						endif;
					endif; ?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>

			<div class="paginating">
				<?php $this->Paginator->options(array('model' => 'Kolokium', 'url' => array_merge($this->passedArgs, array('#' => 'tabs-3')))); ?>
				<p>	<?php echo $this->Paginator->counter(array('model'=>'Kolokium', 'format' =>'Page {:page} of {:pages}'));?>	</p>
				<div class="paging">
					<?php echo $this->Paginator->prev('« Previous', array('model' => 'Kolokium'), null, array('class' => 'disabled')); ?>
					<?php echo $this->Paginator->numbers( array('model' => 'Kolokium', 'separator' => null) ); ?>
					<?php echo $this->Paginator->next('Next »', array('model' => 'Kolokium'), null, array('class' => 'disabled'));?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-3 -->
	
	<div id="tabs-4">
		<div>
			<?php
			if (isset( $tas ) && is_array( $tas )): 
				if (sizeof( $tas ) > 0) {
					echo $this->Html->link('<span>Cetak Form Perpanjangan</span>', '#tabs-4', 
										   array('escape' => false, 'id'=>'prnextend', 
												 'class'=>'button',
												 'title' => 'Cetak form perpanjangan skripsi.'));
					echo $this->Html->link('<span>Cetak Form Usulan Dosen</span>', '#tabs-4', 
										   array('escape' => false, 'id'=>'prnusuldosen', 
												 'class'=>'button',
												 'title' => 'Cetak form usulan pengganti dosen pembimbing.'));
					echo $this->Html->link('<span>Cetak Form Ubah Judul</span>', '#tabs-4', 
										   array('escape' => false, 'id'=>'prnubahjudul', 
												 'class'=>'button',
												 'title' => 'Cetak form usulan pengganti judul skripsi.'));
					echo $this->Html->link('<span>Cetak Sampul</span>', '#tabs-4', 
										   array('escape' => false, 'id'=>'prnsampul', 
												 'class'=>'button',
												 'title' => 'Cetak halaman-halaman sampul skripsi.'));
					/*echo $this->Html->link('<span>Download template</span>', '#tabs-4', 
										   array('escape' => false, 'id'=>'downloadusulan', 
												 'class'=>'button',
												 'title' => 'Download template bab-bab laporan skripsi.'));*/
				}
			?>
			<table id="tamhs-table">
				<?php echo $this->Html->tableHeaders(
					array('NIM', 'Nama', 'Judul', 'Dosen I', 'Dosen II', 'Thn.<br/>Mulai', 
                          'Sem.<br/>Mulai', 'Ta<br/>Ke', 'Lulus?', 'Status','PDF')); ?>
				<?php if (sizeof( $tas ) == 0) : ?>
				<tr>
					<td colspan="11">Belum ada data Skripsi untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($tas as $post) : ?>
				<tr class="sorot">
					<td style="width: 50px;"><?php echo $post['Ta']['nim']; ?>
						<?php if ($post['Ta']['aktif']==1) { ?>
						<div style="text-align:center;">
							<?php echo $this->Html->link($this->Html->image('down.png'), '#tabs-4', 
							           array('escape'=>false,'title'=>'Klik untuk manajemen dokumen skripsi', 
							                 'idtamhs' => $post['Ta']['id'], 'class' => 'expand-tamhs-table')); ?>
						</div>
						<?php 
						} 
						?>
					</td>
					<td style="width: 100px;"><?php echo $post['Mahasiswa']['nama']; ?></td>
					<td style="width: 600px;">
						<?php echo nl2br($post['Ta']['judul']); ?>
					</td>
					<td style="width: 100px;"><?php echo $post['Dosen']['nama']; ?></td>
					<td style="width: 100px;"><?php echo $post['Dosen2']['nama']; ?></td>
					<td style="width: 50px;"><?php echo $post['Ta']['tahun']; ?></td>
					<td style="width: 50px;"><?php
					if ($post['Ta']['sem']==1) { 
						echo 'Gsl'; 
					} else if ($post['Ta']['sem']==2) {
						echo 'Gnp';
					} ?></td>
					<td style="width: 50px;"><?php echo $post['Ta']['take']; ?></td>
					<td style="width: 50px;"><?php echo ($post['Ta']['lulus']==1)?'Sdh.':'Blm.'; ?></td>
					<td style="width: 50px;"><?php 
						if ($post['Ta']['aktif']==1):
							echo 'Skrg';
						elseif ($post['Ta']['aktif']==0):
							echo 'NA';
						else:
							echo 'Batal';
						endif; ?>
					</td>
					<td style="width: 50px;">
					<?php echo $this->Html->link('Unggah', '/turnitins/add/'.$post['Ta']['id'], 
									array('escape'=>false, 'title'=>'Upload PDF Scripsi') );?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			
			<div class="paginating">
				<?php $this->Paginator->options(array('model' => 'Ta', 'url' => array_merge($this->passedArgs, array('#' => 'tabs-4')))); ?>
				<p>	<?php echo $this->Paginator->counter(array('model'=>'Ta', 'format' =>'Page {:page} of {:pages}'));?>	</p>
				<div class="paging">
					<?php echo $this->Paginator->prev('« Previous', array('model' => 'Ta'), null, array('class' => 'disabled')); ?>
					<?php echo $this->Paginator->numbers( array('model' => 'Ta', 'separator' => null) ); ?>
					<?php echo $this->Paginator->next('Next »', array('model' => 'Ta'), null, array('class' => 'disabled'));?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-4 -->
	
	<div id="tabs-5">
		<div>
			<?php 
			if ($last_ta['Ta']['aktif']==1){ ?>
			<div class="searchbox">
			<form style="display:inline; vertical-align:middle; margin-right: 0; float:left;" id="daftarpddrform" name="FormDaftarPendadaran" >
			Pendaftaran Pendadaran dengan pendaftaran terakhir:
			<?php
				if (!empty($jdwlpendadaran)) {
				echo $this->Form->input('batas',array(
					'div' => array('style'=>'display: inline; vertical-align: middle;'),
					'type' => 'select',
					'options' => $jdwlpendadaran,
					'id' => 'daftarpddrform-batas',
					'name' => 'batas',
					'label' => false,
					'empty' => false
				));	
				echo $this->Form->button('Daftarkan saya', array('type'=>'button', 'id' => 'dadarregisbtn'));
				//echo $this->Html->link('<span>Daftarkan saya</span>', '#tabs-5', 
				//					   array('escape' => false, 'id'=>'dadarregisbtn', 
				//					         'class'=>'button',
				//							 'title' => 'Daftarkan diri saya untuk mengikuti pendadaran.')); 
				} else {
				echo $this->Form->input('batas',array(
					'div' => array('style'=>'display: inline; vertical-align: middle;'),
					'type' => 'select',
					'options' => array("" => "Belum tersedia"),
					'id' => 'daftarpddrform-batas',
					'name' => 'batas',
					'label' => false,
					'empty' => false
				));	
				}
			?>
			</form></div>
			<?php 
				echo $this->Html->link('<span>Cetak Form Revisi Skripsi</span>', '#tabs-5', 
									   array('escape' => false, 'id'=>'prnrevisi', 
									         'class'=>'button',
											 'title' => 'Cetak form bukti telah merevisi naskah/program skripsi.'));
			?>
			<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
			
			<?php } ?>
			
			<?php
			if (isset( $dadars ) && is_array( $dadars )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(array('', 'Judul', 'Dosen I', 'Dosen II', 'Jadwal', 'Penguji I', 'Penguji II', 'Status')); ?>
				<?php if (sizeof( $dadars ) == 0) : ?>
				<tr>
					<td colspan="8">Belum ada data Pendadaran Anda untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($dadars as $post) : ?>
				<tr class="sorot">
					<td style="width: 40px;">
						<div>
						<?php
							if ($post['Pendadaran']['status'] == 0) {
							echo $this->Html->link('<span class="icon-actions" style="background-position: -24px 0px;"></span>', 
								'/pendadarans/delete/'.$post['Pendadaran']['id'], array('class' => 'confirmLink', 'escape'=>false, 
									 'title'=>'Hapus Data Pendadaran') );
							}
						?>
						</div>
					</td>
					<td style="width: 600px;">
						<?php echo nl2br($post['Ta']['judul']); ?>
					</td>
					<td style="width: 100px;">
						<?php
							if ($post['Pendadaran']['ketua'] == 1) {
								echo '<strong>' . $post['Dosen']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 100px;">
						<?php
							if ($post['Pendadaran']['ketua'] == 2) {
								echo '<strong>' . $post['Dosen2']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen2']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 50px;"><?php echo date('d-m-Y H:i:s', strtotime($post['Pendadaran']['jadwal'])); ?></td>
					<td style="width: 100px;">
						<?php
							if ($post['Pendadaran']['ketua'] == 3) {
								echo '<strong>' . $post['Dosen3']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen3']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 100px;">
						<?php
							if ($post['Pendadaran']['ketua'] == 4) {
								echo '<strong>' . $post['Dosen4']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen4']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 50px;"><?php 
						if ($post['Pendadaran']['status']==1):
							echo 'Valid';
						elseif ($post['Pendadaran']['status']==0):
							echo 'Baru';
						else:
							echo 'Sudah';
						endif; ?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-5 -->
	
	<div id="tabs-6">
		<?php 
		if (isset($users) && sizeof($users) > 0): ?>
		<table><tbody>
			<tr><td style="width: 160px;">
		<?php
			if (!empty($users['User']['foto'])):
				echo '<img src="' . $this->Html->url(array('controller'=>'users', 'action'=>'foto', $users['User']['id'])) . '"/>';
			else:
				echo $this->Html->image('anonym.png');
			endif; 
		?></td><td>
			<?php echo $this->Html->link('<span>Ubah Data Profil Anda</span>', 
			                                                  array('controller'=>'users', 'action'=>'edit', 
															            $this->Session->read('User.id')), 
															  array('class'=>'button', 'escape' => false, 'title' => 'Ubah Data Profil Anda')); ?>
			<?php echo $this->Html->link('<span>Ubah password Anda</span>', 
			                                                  array('controller'=>'users', 'action'=>'chpwd', $this->Session->read('User.id')), 
															  array('class'=>'button', 'escape' => false, 'title' => 'Ubah password Anda')); ?>
			<p>&nbsp;</p>
			<ul class="profil">
				<li><fieldset><label>username</label><span><?php echo $users['User']['username']; ?></span></fieldset></li>
				<li><fieldset><label>NIM</label><span><?php echo $users['User']['nim']; ?></span></fieldset></li>
				<li><fieldset><label>Nama Lengkap</label><span><?php echo $users['User']['fullname']; ?></span></fieldset></li>
				<li><fieldset><label>Group</label><span><?php echo $users['Group']['groupname']; ?></span></fieldset></li>
				<li><fieldset><label>email</label><span><?php echo $users['User']['email']; ?></span></fieldset></li>
				<li><fieldset><label>No Telp.</label><span><?php echo $users['User']['telpno']; ?></span></fieldset></li>
			</ul>
		</td></tr></tbody></table>
		<?php endif; ?>
	</div> <!-- end of tabs-6 -->
</div>

<div id="konfirmasikirim" title="Konfirmasi kirim">
  Pengiriman Ringkasan Proposal Anda ditujukan untuk pelaksanaan Desk Evaluation tanggal: 
  <form id="kirimform">
  <input type="hidden" name="data[Kirim][id]" id="kirimform-id" value="" />
  <?php echo $this->Form->input('deskevaluation',array(
			'div' => array('style'=>'display: inline; vertical-align: middle;'),
			'type' => 'select',
			'options' => $jwddeskevaluationlist,
			'id' => 'kirimform-deskevaluation',
			'name' => 'data[Kirim][idtgl]',
			'label' => false,
			'empty' => false
		));	?>
  </form>
  <br/><br/>
  Apakah Anda yakin akan mengirimkan ringkasan proposal terpilih?
</div>

<div id="kolodialog" title="Pilih Proposal">
  Silahkan Anda pilih proposal yang telah berstatus DISETUJUI: <br/><br/>
  <form id="pilihproposalform">
  <?php echo $this->Form->input('proposalid',array(
			'div' => array('style'=>'display: inline; vertical-align: middle;'),
			'type' => 'select',
			'id' => 'kolodialog-proposalid',
			'name' => 'data[Proposal][id]',
			'label' => false,
			'empty' => false
		));	?>
  </form>
  <br/><br/>
</div>

<?php
/*********************************************************************************************************************
 *  untuk DOSEN 
 *
 *********************************************************************************************************************/
elseif ($group == 3 || $group == 4): 
?>
<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Pengumuman</a></li>
		<li><a href="#tabs-2">Desk Evaluation</a></li>
		<li><a href="#tabs-3">Proposal</a></li>
		<li><a href="#tabs-4">Bimbingan</a></li>
		<li><a href="#tabs-5">Pendadaran</a></li>
		<li><a href="#tabs-6">Profil</a></li>
		<li><a href="#tabs-7">Kehadiran</a></li>
	</ul>
	<div id="tabs-1">
		<div class="pengumuman">
			<?php 
			if (isset( $blogs ) && is_array( $blogs )): ?>
				<?php if (sizeof( $blogs ) == 0) : ?>
					<p>Belum ada pengumuman untuk saat ini.</p>
				<?php else: ?>
					<ul class="news-list">
					<?php foreach ($blogs as $post) : ?>
						<li><article>
						<header><h4 class="entry-title"><?php echo $post['Blog']['title']; ?></h4></header>
						<footer><?php echo date('D, d M Y h:i:s a', strtotime($post['Blog']['modified'])); ?></footer>
						<div class="news-content"><?php echo $post['Blog']['content']; ?></div>
						</article></li>
					<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php
			echo $this->element('paging', array('model' => 'Blog', 'tabname' => 'tabs-1'));
			?>
		</div>
	</div> <!-- end of tabs-1 -->
	
	<div id="tabs-2">
		<div>	
			<?php 
			if (isset( $proposals ) && is_array( $proposals )): ?>
			<div class="searchbox" style="width: 900px;"> 

			<?php if (!empty($all_jwdde)) { ?>
				<p style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Cetak Peserta
				<select name="seltglde"  id="seltglde" style="vertical-align: middle;">
					<option value="" selected="selected"></option>
				<?php 
					foreach($all_jwdde as $j):
						echo '<option value="' . $j['De']['id'] . '">'. 
							 $this->Tools->prnDate($j['De']['tanggal'],2) . 
							 ' (Kelompok ' . $j['De']['kelompok'] . ')' . 
							 '</option>';
					endforeach;
				?>
				</select>
				<span class="actions">
				<?php
				echo $this->Html->link('<span>Tampilkan</span>', '#tabs-2', 
							   array('escape' => false, 'id'=>'prnprolist',
									 'title' => 'Tampilkan Daftar Peserta Ringkasan Proposal berdasar tanggal.'));
				?>
				</span>
				</p>
			<?php } ?>

			<p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="formsearchbox2" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="proposal" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="hidden" value="Proposal" name="data[Cari][model]">
				
				<?php if (isset($pencarian)): ?>
				<input type="text" name="data[Cari][q]" style="width: 200px; vertical-align: middle;" value="<?php echo $pencarian; ?>"/>
				<?php else: ?>
				<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
				<?php endif; ?>
				<?php $qq = (isset($pilihan))? $pilihan : ""; ?>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="nim" <?php echo ($qq == 'nim')? 'selected="selected"': ""; ?>>NIM</option>
					<option value="nama" <?php echo ($qq == 'nama')? 'selected="selected"': ""; ?>>Nama Mahasiswa</option>
					<option value="judul" <?php echo ($qq == 'judul')? 'selected="selected"': ""; ?>>Usulan Judul</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			
			<table>
				<?php echo $this->Html->tableHeaders(array('NIM', 'Nama Mahasiswa', 'Judul', 'Status', '')); ?>
				<?php if (sizeof( $proposals ) == 0) { ?>
				<tr>
					<td colspan="4">Belum ada proposal untuk saat ini.</td>
				</tr>
				<?php } else { 
				foreach ($proposals as $post) : ?>
				<tr class="sorot">
					<td><?php echo $post['User']['nim']; ?></td>
					<td><?php echo $post['User']['fullname']; ?></td>
					<td style="width: 600px;"><?php echo $post['Proposal']['judul']; ?></td>
					<td>
						<?php if ($post['Proposal']['status_proposal'] == 0) { ?>
						<span style="background-color:#5CAAFF; padding: 2px;">Baru</span>
						<?php } elseif ($post['Proposal']['status_proposal'] == 1) { ?>
						<span style="background-color:#FFE43E; padding: 2px;">Terkirim</span>
						<?php } elseif ($post['Proposal']['status_proposal'] == 2) { ?>
						<span style="background-color:#FF5228; padding: 2px;">Blm Diterima</span>
						<?php } else { ?>
						<span style="background-color:#79FF54; padding: 2px;">Diterima</span>
						<?php } ?>
					</td>
					<td class="actions">
					<?php 
					$isok = false;
					if ($post['De']) {
						foreach($post['De'][0]['Dosen'] as $t){
							$tgl = date('Y-m-d', strtotime($post['De'][0]['tanggal']));
							$waktuawal = date('Y-m-d H:i:s', strtotime($tgl . ' 08:00:00'));
							$waktuakhir = date('Y-m-d H:i:s', strtotime($tgl . ' 23:00:00'));
							$tglsistem = date('Y-m-d H:i:s');
							if ($t['id'] == $currdosen['Dosen']['id'] && 
							    ($tglsistem >= $waktuawal && $tglsistem <= $waktuakhir) ) {
								$isok = true; break;
							}
						}
					}
					
					if ($isok && $post['Proposal']['status_proposal'] == 1 && $post['Proposal']['valid'] == 1) { 
						echo $this->Html->link('Evaluasi', '/proposals/evaluasi/'.$post['Proposal']['id']); 
					}
					echo $this->Html->link('Lihat', '/proposals/view/'.$post['Proposal']['id'], 
								array('title' => 'Lihat Evaluasi'));
					echo $this->Html->link('Cetak', '/proposals/prnproposal/'.$post['Proposal']['id'], array('target' => '_blank', 'title' => 'Cetak Formulir Desk Evaluation')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
				<?php } ?>
			</table>
			
			<?php
			echo $this->element('paging', array('model' => 'Proposal', 'tabname' => 'tabs-2'));
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-2 -->
	
	<div id="tabs-3">
		<div>
			<?php
			if (isset( $kolokiums ) && is_array( $kolokiums )): ?>
			<div class="link-kol-box">
				<form id="formselkol" style="float:left; vertical-align:middle; margin:0; width:180px;">
					<div class="selectkolokium">
					<?php echo $this->Form->input('select-kol', array(
							'options' => array('a' => 'Semua', 'u' => 'Terpilih'),
							'type' => 'radio',
							'legend' => '',
							'class' => 'selectkol',
							'default' => $qkol
						));
					?>
					</div>
				</form>
				<span id="select_title" style="font-size: 1.4em; float: right; width: 320px;"><h4>
				<?php if ($qkol == 'u'): ?>
					Usulan Judul Baru sesuai Dosen (yang sedang login)
				<?php else: ?>
					Semua Usulan Judul Baru
				<?php endif; ?>
				</h4></span>
			</div>
			<div class="searchbox"> <p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="formsearchbox" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="kolokium" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="hidden" value="<?php echo $qkol; ?>" name="data[Cari][qkol]">
				<?php if (isset($pencarian)): ?>
				<input type="text" name="data[Cari][q]" style="width: 200px; vertical-align: middle;" value="<?php echo $pencarian; ?>"/>
				<?php else: ?>
				<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
				<?php endif; ?>
				<?php $qq = (isset($pilihan))? $pilihan : ""; ?>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="nim" <?php echo ($qq == 'nim')? 'selected="selected"': ""; ?>>NIM</option>
					<option value="tglkol" <?php echo ($qq == 'tglkol')? 'selected="selected"': ""; ?>>Tanggal Kolokium</option>
					<option value="judul" <?php echo ($qq == 'judul')? 'selected="selected"': ""; ?>>Usulan Judul</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			<table id="tabkolokium">
				<?php 
					if ($group == 4):
						echo $this->Html->tableHeaders(array('NIM', 'Nama', 'Judul', 'Tanggal Kolokium', 'Dosen Pengarah', 'Status', 'BA')); 
					else:
						echo $this->Html->tableHeaders(array('NIM', 'Nama', 'Judul', 'Tanggal Kolokium', 'Dosen Pengarah', 'Status', 'BA')); 
					endif;
				?>
				<?php if (sizeof( $kolokiums ) == 0) : ?>
				<tr>
					<?php if ($group == 4): ?>
					<td colspan="7">Belum ada data Kolokium untuk saat ini.</td>
					<?php else: ?>
					<td colspan="6">Belum ada data Kolokium untuk saat ini.</td>
					<?php endif; ?>
				</tr>
				<?php else: ?>
				<?php foreach ($kolokiums as $post) : ?>
				<tr class="sorot">
					<td style="width: 50px;"><?php echo $post['Kolokium']['nim']; ?></td>
					<td style="width: 150px;"><?php echo $post['User']['fullname']; ?></td>
					<td style="width: 600px;"><?php echo $post['Kolokium']['judul']; ?> 
						(<?php echo $this->Html->link('Download', array('controller' => 'papers', 'action' => 'download', $post['Kolokium']['id']) ); ?>)
					</td>
					<td style="width: 75px;"><?php echo date('d-M-Y', strtotime($post['Jadwal']['tanggal'])); ?></td>
					<td style="width: 100px; font-size: 0.8em;"><?php echo $post['Dosen']['nama']; ?></td>
					<td style="width: 50px;">
						<?php
						if ($post['Kolokium']['status'] == 'N'):
							echo 'Baru'; 
						elseif ($post['Kolokium']['status'] == 'L'):
							echo 'Lulus';
						elseif ($post['Kolokium']['status'] == 'B'):
							echo 'Belum Disetujui';
						elseif ($post['Kolokium']['status'] == 'V'):
							echo 'Valid';
						elseif ($post['Kolokium']['status'] == 'S'):
							echo 'Lulus Bersyarat';
						endif;
						?>
					</td>
					<td class="action" style="width: 60px;">
						<?php 
						echo $this->Html->link($this->Html->image("baicon.png", 
							   array("alt" => "Berita Acara")), '#tabs-3', 
							   array('escape' => false, 'class'=>'ba-icon', 'idba'=> $post['Kolokium']['id'], 
										  'title' => 'Memasukkan Berita Acara Kolokium.',
										  'status' => $post['Kolokium']['status'])); 
						echo $this->Html->link($this->Html->image("print.png", 
								array("alt" => "Cetak Berita Acara")), '#tabs-3', 
							   array('escape' => false, 'class'=>'prn-ba', 'id'=>'pr' . $post['Kolokium']['id'], 
										  'title' => 'Cetak Berita Acara Kolokium.')); 
						?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			
			<?php
			if (isset($m) && isset($o) && isset($q) && $m == 'kolokium') {
				echo $this->element('paging', array('model' => 'Kolokium', 'tabname' => 'tabs-3', 'opsi'=>array('m' => $m, 'o' => $o, 'q' => $q, 'qkol' => $qkol)));
			} else {
				echo $this->element('paging', array('model' => 'Kolokium', 'tabname' => 'tabs-3'));
			}
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-3 -->
	
	<div id="tabs-4">
		<div>	
			<?php
			//debug($tas);
			if (isset( $tas ) && is_array( $tas )): ?>
			<div class="link-kol-box">
				<form id="formselta" style="float:left; vertical-align:middle; margin:0; width:380px;">
					<div class="radiota">
					<?php echo $this->Form->input('select-ta', array(
							'options' => array('a' => 'Semua Dosen', 'u' => 'Terpilih Sekarang', 's' => 'Terpilih Semua'),
							'type' => 'radio',
							'legend' => '',
							'class' => 'selectta',
							'default' => $qta
						));
					?>
					</div>
				</form>
				<span id="select_title_ta" style="font-size: 1.4em; float: right; width: 400px;"><h4>
				<?php if ($qta == 'u'): ?>
					Daftar Mahasiswa Bimbingan Dosen Saat ini
				<?php elseif ($qta == 's'): ?>
					Daftar Semua Mahasiswa Bimbingan Dosen
				<?php else: ?>
					Semua Mahasiswa Tugas Akhir/Skripsi
				<?php endif; ?>
				</h4></span>
			</div>
			<div class="searchbox" style="width: 750px;"> 
			<p style="display: inline; margin: 0;vertical-align: middle;">Cari: </p>
			<form id="formsearchbox1" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="ta" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="hidden" value="<?php echo $qta; ?>" name="data[Cari][qta]">
				<input type="text" name="data[Cari][q]" value="" style="width: 200px; vertical-align: middle;"/>
				<select name="data[Cari][o]" style="vertical-align: middle;">
					<option value="nim">NIM</option>
					<option value="judul">Judul Skripsi</option>
				</select>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
			</div>
			<table>
				<?php echo $this->Html->tableHeaders(
					array('NIM', 'Nama', 'Judul', 'Dosen I', 'Dosen II', 'Thn.<br/>Mulai', 
						  'Sem.<br/>Mulai', 'Ta<br/>Ke', 'Turnitin')); ?>
				<?php if (sizeof( $tas ) == 0) : ?>
				<tr>
					<td colspan="9">Belum ada data Skripsi untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($tas as $post) : ?>
				<tr class="sorot">
					<td style="width: 85px; text-align: center;"><?php 
						echo $post['Ta']['nim']; 
						if ($post['Ta']['lulus'] == 1):
							echo $this->Html->image('check.png', array('alt' => 'Sudah Lulus', 'style' => 'padding-left: 5px; vertical-align: middle;') );
						endif;
					?>
						<div style="text-align:center;">
							<?php echo $this->Html->link($this->Html->image('down.png'), '#tabs-3', 
							           array('escape'=>false,'title'=>'Klik untuk manajemen dokumen skripsi', 
							                 'idtamhs' => $post['Ta']['id'], 'class' => 'expand-tamhs-table')); ?>
						</div>
					</td>
					<td style="width: 100px;"><?php echo $post['Mahasiswa']['nama']; ?></td>
					<td style="width: 600px;"><?php echo nl2br($post['Ta']['judul']); ?></td>
					<td style="width: 100px;"><?php echo $post['Dosen']['nama']; ?></td>
					<td style="width: 100px;"><?php echo $post['Dosen2']['nama']; ?></td>
					<td style="width: 50px;"><?php echo $post['Ta']['tahun']; ?></td>
					<td style="width: 50px;"><?php
					if ($post['Ta']['sem']==1) { 
						echo 'Gsl'; 
					} else if ($post['Ta']['sem']==2) {
						echo 'Gnp';
					} ?></td>
					<td style="width: 50px;"><?php echo $post['Ta']['take']; ?></td>
					<td style="width: 50px;"><?php echo $this->Html->link('<span class="icon-actions" style="background-position: -72px 0px;"></span>', '/files/turnitin/'.$post['Ta']['id'].'.pdf', 
									array('escape'=>false, 'title'=>'Unduh Hasil Turn It In Skripsi', 'target' => '_blank') ); ?></td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			
			<?php
			if (isset($m) && isset($o) && isset($q) && $m == 'ta') {
				echo $this->element('paging', array('model' => 'Ta', 'tabname' => 'tabs-4', 'opsi'=> array('m' => $m, 'o' => $o, 'q' => $q, 'qta' => $qta) ));
			} else {
				echo $this->element('paging', array('model' => 'Ta', 'tabname' => 'tabs-4', 'opsi'=> array('qta' => $qta)));
			}
			?>
			<?php endif; ?>
		</div>
	</div> <!-- end of tabs-4 -->
	
	<div id="tabs-5">
		<?php
			if (isset( $dadars ) && is_array( $dadars )): ?>
			<table>
				<?php echo $this->Html->tableHeaders(array('NIM', 'Judul', 'Dosen I', 'Dosen II', 'Jadwal', 'Penguji I', 'Penguji II', 'Status')); ?>
				<?php if (sizeof( $dadars ) == 0) : ?>
				<tr>
					<td colspan="8">Belum ada data Pendadaran Anda untuk saat ini.</td>
				</tr>
				<?php else: ?>
				<?php foreach ($dadars as $post) : ?>
				<tr class="sorot">
					<td style="width: 40px;"><?php echo $post['Ta']['nim']; ?></td>
					<td style="width: 600px;">
						<?php echo nl2br($post['Ta']['judul']); ?>
					</td>
					<td style="width: 100px;">
						<?php
							if ($post['Pendadaran']['ketua'] == 1) {
								echo '<strong>' . $post['Dosen']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 100px;">
						<?php
							if ($post['Pendadaran']['ketua'] == 2) {
								echo '<strong>' . $post['Dosen2']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen2']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 50px;"><?php echo date('d-m-Y H:i:s', strtotime($post['Pendadaran']['jadwal'])); ?></td>
					<td style="width: 100px;">
						<?php
							if ($post['Pendadaran']['ketua'] == 3) {
								echo '<strong>' . $post['Dosen3']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen3']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 100px;">
						<?php
							if ($post['Pendadaran']['ketua'] == 4) {
								echo '<strong>' . $post['Dosen4']['nama_dosen'] . ' *</strong>';
							} else {
								echo $post['Dosen4']['nama_dosen'];
							}
						?>
					</td>
					<td style="width: 50px;"><?php 
						if ($post['Pendadaran']['status']==1):
							echo 'Valid';
						elseif ($post['Pendadaran']['status']==0):
							echo 'Baru';
						else:
							echo 'Sudah';
						endif; ?>
					</td>
				</tr>
				
				<?php endforeach; ?>
				<?php endif; ?>
			</table>
			
			<?php
			echo $this->element('paging', array('model' => 'Pendadaran', 'tabname' => 'tabs-5'));
			?>
			<?php endif; ?>
	</div>
	
	<div id="tabs-6">
		<?php 
		if (isset($users) && sizeof($users) > 0): ?>
		<table><tbody>
			<tr><td style="width: 160px;">
		<?php
			if (!empty($users['User']['foto'])):
				echo '<img src="' . $this->Html->url(array('controller'=>'users', 'action'=>'foto', $users['User']['id'])) . '"/>';
			else:
				echo $this->Html->image('anonym.png');
			endif; 
		?></td><td>
			<?php echo $this->Html->link('<span>Ubah Data Profil Anda</span>', 
			                                                   array('controller'=>'users', 'action'=>'edit', $this->Session->read('User.id')), 
															   array('class'=>'button', 'escape' => false, 'title' => 'Ubah Data Profil Anda')); ?>
			<?php echo $this->Html->link('<span>Ubah password Anda</span>', 
															   array('controller'=>'users', 'action'=>'chpwd', $this->Session->read('User.id')), 
															   array('class'=>'button', 'escape' => false, 'title' => 'Ubah password Anda')); ?>
			<p>&nbsp;</p>
			<ul class="profil">
				<li><fieldset><label>username</label><span><?php echo $users['User']['username']; ?></span></fieldset></li>
				<li><fieldset><label>NIM</label><span><?php echo $users['User']['nim']; ?></span></fieldset></li>
				<li><fieldset><label>Nama Lengkap</label><span><?php echo $users['User']['fullname']; ?></span></fieldset></li>
				<li><fieldset><label>Group</label><span><?php echo $users['Group']['groupname']; ?></span></fieldset></li>
				<li><fieldset><label>email</label><span><?php echo $users['User']['email']; ?></span></fieldset></li>
				<li><fieldset><label>No Telp.</label><span><?php echo $users['User']['telpno']; ?></span></fieldset></li>
			</ul>
		</td></tr></tbody></table>
		<?php endif; ?>
	</div> <!-- end of tabs-6 -->
	
	<div id="tabs-7">
		<div id="box-tgl-presensi">
			<form id="formsearchbox4" style="display:inline;" method="post" 
			            action="<?php echo $this->Html->url(array('controller'=>'admin', 'action'=>'home')); ?>">
				<input type="hidden" value="POST" name="_method">
				<input type="hidden" value="presensi" name="data[Cari][m]">
				<input type="hidden" value="1" name="data[Cari][page]">
				<input type="hidden" name="data[Cari][q]" value="-"/>
				<span style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Presensi: </span>
				<select name="data[Cari][t]" id="tipepresensi" style="vertical-align: middle;">
					<?php if ($tipepresensi == 0) { ?>
					<option value="0" selected="selected">Desk Evaluation</option>
					<option value="1">Kolokium</option>
					<?php } elseif ($tipepresensi == 1) { ?>
					<option value="0">Desk Evaluation</option>
					<option value="1" selected="selected">Kolokium</option>
					<?php } ?>
				</select>
				<span id="blok_kolokium">
					<span id="label_tglpresensi" style="display: inline; margin: 0 10px 0 0;vertical-align: middle;">Tanggal Kolokium</span>
					<select name="data[Cari][o]" id="tgl-presensi" style="vertical-align: middle;">
						<option value="" selected="selected"></option>
						<?php 
						if ($tipepresensi == 1) {
							foreach($all_jadwal as $j) {
								$strtglformat = $this->Tools->prnDate($j['Jadwal']['tanggal']);
								if (!empty($tglpresensi) && $tglpresensi['Jadwal']['id'] == $j['Jadwal']['id']) {
									echo '<option value="' . $j['Jadwal']['id'] . '" selected="selected">'. $strtglformat .'</option>';
								} else {
									echo '<option value="' . $j['Jadwal']['id'] . '">'. $strtglformat .'</option>';
								}
							}
						} elseif ($tipepresensi == 0) {
							foreach($all_jwdde as $j) {
								$strtglformat = $this->Tools->prnDate($j['De']['tanggal'],4) . ' (Kelompok: '. $j['De']['kelompok'] .')';
								if (!empty($tglpresensi) && $tglpresensi['De']['id'] == $j['De']['id']) {
									echo '<option value="' . $j['De']['id'] . '" selected="selected">'. $strtglformat .'</option>';
								} else {
									echo '<option value="' . $j['De']['id'] . '">'. $strtglformat .'</option>';
								}
							}
						}
						?>
					</select>
				</span>
				<input type="submit" value="Cari" style="vertical-align: middle;"/>
			</form>
		</div>
		<p style="text-align:center;">
			<?php if (!empty($tglpresensi)) { 
				if ($tipepresensi == 1) {
					$isbtncari = $tglpresensi['Jadwal']['id'];
					$strlabel = "Presensi Kolokium Periode: " . $this->Tools->prnDate($tglpresensi['Jadwal']['tanggal']);
				} elseif ($tipepresensi == 0) {
					$isbtncari = $tglpresensi['De']['id'];
					$strlabel = "Presensi Desk Evaluation Periode: " . 
								$this->Tools->prnDate($tglpresensi['De']['tanggal'], 4);
				} else {
					$isbtncari = "";
					$strlabel = "Tidak Valid";
				}
			?>
			<span id="isbtncari" value="<?php echo $isbtncari ?>"></span>
			<span style="display:inline; font-size: 1.5em; font-weight: bold;">
				<?php echo $strlabel; ?>
			</span>
			<?php } else { ?>
			<span id="isbtncari" value=""></span>
			<?php } ?>
		</p>
		<div id="data-presensi" style="margin-top: 30px;">
			<p>&nbsp;</p><p>&nbsp;</p>
			<table class="display" id="tabelpresensi">
				<thead>
					<tr>
						<th>iddata</th>
						<th>Dosen</th>
						<th>Jam Datang</th>
						<th>Jam Selesai</th>
						<th>Lama (jam)</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$i=0; 
					foreach($presensis as $d):  ?>
						<tr class="<?php echo ($i %2 == 0)?'even':'odd'; ?>">
							<td><?php echo $d['Presensi']['id']; ?></td>
							<td><?php echo $d['Presensi']['id_dosen'] . '-' . $d['Dosen']['nama']; ?></td>
							<td><?php echo date('d-m-Y H:i:s', strtotime($d['Presensi']['waktu_datang'])); ?></td>
							<td><?php echo date('d-m-Y H:i:s', strtotime($d['Presensi']['waktu_pulang'])); ?></td>
							<td><?php echo $d['Presensi']['lama']; ?></td>
						</tr>
					<?php  endforeach;  ?>
				</tbody>
			</table>
			<p>&nbsp;</p>
		</div>
	</div> <!-- end of tabs-7 -->
</div>

<div id="berita-acara" title="Berita Acara Kolokium">
<div>NIM/NAMA MAHASISWA: <span style="color: green; font-size: 1.25em;" id="baform-nim">Mohon ditunggu...</span> / <span style="color: green; font-size: 1.25em;" id="baform-nama"></span></div>
<form id="baform">
	<input type="hidden" name="data[Ba][id]" id="baform-id" value="" />
	<div><label for="baform-judulproposal">Judul Proposal</label>
		<textarea row="1" cols="80" style="width:100%" id="baform-judulproposal" name="data[Ba][judul_proposal]"></textarea>
	</div>
	<div><label for="baform-judul">Judul Skripsi yang Ditetapkan</label>
		<textarea row="1" cols="80" style="width:100%" id="baform-judul" name="data[Ba][judul]"></textarea>
	</div>
	<div>
		<label id="label-catatan" for="baformcatatan" style="cursor: pointer;"><span class="icon-collapse" style="background-position: -16px 0px;"></span>&nbsp;&nbsp;Catatan Hasil Kolokium</label>
		<span id="blokcatatan">
	    	<textarea row="3" cols="80" style="width:100%" id="baformcatatan" name="data[Ba][catatan]"></textarea>
		</span>
	</div>
	<div>
		<label id="label-internal" for="baforminternal" style="cursor: pointer;"><span  class="icon-collapse" style="background-position: 0px 0px;"></span>&nbsp;&nbsp;Catatan Internal Kolokium</label>
		<span id="blokinternal" style="display:none;">
	    	<textarea row="3" cols="80" style="width:100%" id="baforminternal" name="data[Ba][catatan_internal]"></textarea>
		</span>
	</div>
	<table>
	<tr><td>
	<?php echo $this->Form->input('dosen1',array(
			//'div' => array('style' => 'float:left; margin-top:11px;'),
			'label' => __('Dosen Pembimbing I:',true),
			'type' => 'select',
			'options' => $alldosens,
			'id' => 'baform-dosen1',
			'name' => 'data[Ba][dosen1]',
			'empty' => true
		));	?></td><td>
	<?php
		echo $this->Form->input('status', array(
			//'div' => array('style' => 'clear:both; float:left; margin-top:11px;'),
			'options' => array('L' => 'Lulus', 'S' => 'Lulus dengan Syarat', 'B' => 'Belum bisa diterima'),
			'type' => 'select',
			'id' => 'baform-status',
			'name' => 'data[Ba][status]',
			'default' => 'B'
		)); ?></td><td>
	<?php echo $this->Form->input('batas_kumpul', array(
			'id' => 'baform-batas_kumpul',
			'name' => 'data[Ba][batas_kumpul]'
		) );
	?></td>
	</tr></table>
</form>
</div>

<?php
endif;
?>

<div id="dialog" title="Confirmation Required">
  Are you sure about this?
</div>

<?php
// BLOK JAVASCRIPT
?>

<script type="text/javascript">
$(document).ready(function() {
	
	$('#please-wait > h3').text('Halaman siap...');
	$('#please-wait').hide(2000);

<?php 
// Blok JavaScript untuk MAHASISWA
//
if ($this->Session->read('User.group_id') == 2 ): ?>
	$("#kolodialog").dialog({
		autoOpen: false,
		width: 600,
		position: 'center',
		modal: true
	});
	
	$('#addkolo3').click(function(e){
		e.preventDefault();
		$.getJSON("<?php echo $this->Html->url(array('controller'=>'proposals', 'action'=>'getproposals')); ?>", function(data) {
			if (data.Status.return == 0) {
				var txt = '';
				$.each(data.Status.msg, function(i, item){
					txt = txt + '<option value="' + item['id'] + '">' + item['id'] + ' - ' + item['judul'] + '</option>';
				});
				$('#kolodialog-proposalid').html(txt);
				
				if (txt != '') {
					$("#kolodialog").dialog("open");
				} else {
					alert("MAAF, belum ada data proposal yang dinyatakan LOLOS!");
				}
			} else {
				alert(data.Status.msg);
			}
		});
		
		$("#kolodialog").dialog({
			buttons : {
				"Lanjutkan" : function() {
					$('#kolodialog').dialog("close");
					window.location.href = "<?php echo $this->Html->url(array('controller' => 'papers', 'action' => 'add') ); ?>/" + $('#kolodialog-proposalid').val();
				},
				"Batal" : function() {
					$(this).dialog("close");
				}
			}
		});
	});
	
	$('#prnextend').click(function(e){
		window.open("<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'prnperpanjangan')); ?>");
	});
	$('#prnusuldosen').click(function(e){
		window.open("<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'prnusuldosen')); ?>");
	});
	$('#prnubahjudul').click(function(e){
		window.open("<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'prnubahjudul')); ?>");
	});
	$('#prnsampul').click(function(e){
		window.open("<?php echo $this->Html->url(array('controller'=>'pendadarans', 'action'=>'prnsampul')); ?>");
	});		
	$('#prnrevisi').click(function(e){
		window.open("<?php echo $this->Html->url(array('controller'=>'pendadarans', 'action'=>'prnrevisi')); ?>");
	});		
    $('#dadarregisbtn').click(function(e){
		e.preventDefault();
		$.post("<?php echo $this->Html->url(array('controller' => 'pendadarans', 'action' => 'regis')); ?>", 
					$('#daftarpddrform').serialize(),
					function(data) {
						if (data.Status.return == 0) {
							location.reload();
						} else {
							alert(data.Status.msg);
						}
					}, "json"
				  );
		return false;
	});
	
	$('.tafile_cancel').live('click', function(e) {
		var idta = $(this).attr('idta');
		var row = $(this).parents('tr')[0];
		$(row).remove();
	});
	
	$('.addfileta-btn').live('click', function(e){
		var idta = $(this).attr('idta');
		var skrg = new Date();
		if ($('#tafileform-' + idta).length > 0) {
			alert('Form Untuk Upload file masih ada yang aktif. Silahkan ditutup terlebih dahulu dengan Batal atau Simpan.');
		} else {
			$('#files-' + idta + ' > tbody:last').append(
				'<tr><td colspan="4"><form method="post" action="<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'addtafile'));?>" enctype="multipart/form-data" id="tafileform-' + idta + '"><input type="hidden" name="data[Tafile][ta_id]" value="' +  idta+ '">' +
				'<p><span style="display: block; height: 25px; width: 100px; text-align: right; vertical-align: middle; float: left; padding-right:3px;">Isi File : </span><select style="display: inline;" name="data[Tafile][jenisfiles_id]">' +
				'<?php foreach($jenisfiles as $j){ echo '<option value="' . $j['Jenisfile']['id'] . '">' . $j['Jenisfile']['jenis'] . '</option>'; } ?>' +
				'</select></p>' +
				'<p style="clear: both;"><span style="display: block; height: 25px; width: 100px; text-align: right; vertical-align: middle; float: left; padding-right:3px;">Keterangan : </span><input type="text" name="data[Tafile][keterangan]" style="display: inline; width: 650px;"/></p>' + 
				'<p style="clear: both;"><input type="file" name="data[Tafile][filename]" style="padding-left: 103px;"></p>' +
				'<div class="action"><a href="#tabs-4" class="button tafile_cancel" idta="' + idta + '"><span>Batal</span></a>&nbsp;&nbsp;&nbsp;<input type="submit" idta="' + idta + '" class="tafile_save" value="Simpan"></div></tr>'
			);
		}
		return false;
	});
	
	$('.expand-tamhs-table').click(function(e){
		e.preventDefault();
		var idta = $(this).attr('idtamhs');
		var nRow = $(this).parents('tr')[0];
		var gb = $(this).children();
		
		if ($('#fileman-tamhs-' + idta).length > 0) {
			$('#fileman-tamhs-' + idta).slideToggle("normal", function(){
				var nmgb = gb.attr('src');
				if (nmgb.indexOf('up.png') != -1) {
					gb.attr('src', '<?php echo $this->Html->url('/img/down.png'); ?>');
				} else {
					gb.attr('src', '<?php echo $this->Html->url('/img/up.png'); ?>');
				}
			});
		} else {
			$.getJSON('<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'gettafile')); ?>/' + idta, function(data){
			    var newrow = 
					'<tr id="fileman-tamhs-' + idta + '"><td colspan="11">' +
					'	<div style="font-size:1.1em; font-weight:bold; ">' +
					'		Daftar Berkas-berkas Skripsi Terunggah' +
					'	</div>' +
					'<?php echo $this->Html->link("<span>+</span>", "#tabs-4", array("class" => "button addfileta-btn", "escape" => false, "idta"=>"' + idta + '") ); ?>' +
					'<div style="margin-left: 50px; width:80%;"><table id="files-' + idta + '" idta="' + idta +'" >' +
					'<thead><tr><th>File</th><th>Keterangan</th><th style="width: 150px;">Modified</th><th></th></tr></thead>' +
					'<tbody>';
				
				if (data.Status.return > 0) {
					newrow = newrow + '<tr>' +
					'	<td colspan="3">' + data.Status.msg + '</td>'+
					'</tr>';
				} else {
					$.each(data.Status.msg, function(i, item){
						newrow = newrow + '<tr>' +
						'	<td><a href="<?php echo $this->Html->url(array('controller' => 'tas', 'action' => 'download')); ?>/' + item.Tafile.id + '" target="_blank" title="Download file">' + item.Jenisfile.jenis + '</a></td><td>' + item.Tafile.keterangan + '</td><td>' + item.Tafile.modified + '</td>' +
						'   <td style="width:30px;"><a href="<?php echo $this->Html->url(array('controller' => 'tas', 'action' => 'delfile')); ?>/' + item.Tafile.id + '" class="confirmLink" title="Hapus file!"><span class="icon-actions" style="background-position: -96px 0px;"></span></a></td>'+
						'</tr>';
					});
				}
				newrow = newrow + '</tbody>' + 
					'</table>' +
					'</td></tr>';
				$(nRow).after(newrow).slideDown();
				gb.attr('src', '<?php echo $this->Html->url('/img/up.png'); ?>');
			});
		}
		return false;
	});
	
	$("#konfirmasikirim").dialog({
		autoOpen: false,
		width: 500,
		position: 'center',
		modal: true
	});

	$(".confirmKumpul").live('click', function(e) {
		e.preventDefault();
		$id = $(this).attr('kirimid');
		//var targetUrl = $(this).attr("href");
		$('#kirimform-id').val($id);
		
		$("#konfirmasikirim").dialog({
			buttons : {
				"Lanjutkan" : function() {
					//window.location.href = targetUrl;
					$.post("<?php echo $this->Html->url(array('controller' => 'proposals', 'action' => 'kumpul') ); ?>", $('#kirimform').serialize(),
							function(data) {
								if (data.Status.return == 0){
									$('#konfirmasikirim').dialog("close");
									location.reload();
								} else {
									alert("ADA KESALAHAN SISTEM: " + data.Status.msg);
								}
							}, "json"
						);
				},
				"Batal" : function() {
					$(this).dialog("close");
				}
			}
		});
		
		$("#konfirmasikirim").dialog("open");
	});
	
<?php 
// Blok JavaScript untuk DOSEN
//
elseif ($this->Session->read('User.group_id') == 3 || $this->Session->read('User.group_id') == 4): ?>
	$('#prnprolist').click(function(e){
		if ($('#seltglde').val() != '') {
			window.open("<?php echo $this->Html->url(array('controller'=>'proposals', 'action'=>'report')); ?>/" + $('#seltglde').val());
		}
	});

	$('#tipepresensi').change(function() {
		var v = $(this).val();
		var otglcombo = $('#tgl-presensi');
		var opsi = '<option value="" selected="selected"></option>';
		otglcombo.html("");
		otglcombo.html(opsi);
		if (v==0) {
			$('#label_tglpresensi').html(' Tanggal Desk Evaluation:');
			$.getJSON('<?php echo $this->Html->url(array("controller"=>"des", "action"=>"getalldates")); ?>', 
				function(data){
					if (data.Status.return == 0) {
						$.each(data.Status.msg, function(i, item){
							opsi = opsi + '<option value="' + item.id + '">' + item.tanggal  + ' (Kelompok: ' + item.kelompok + ')</option>';
						});
					}
					otglcombo.html("");
					otglcombo.html(opsi);
			}).error(function(){
				alert('Terjadi kesalahan saat melakukan permintaan data tanggal Desk Evaluation!');
			});
		} else {
			$('#label_tglpresensi').html(' Tanggal Kolokium:');
			$.getJSON('<?php echo $this->Html->url(array("controller"=>"jadwals", "action"=>"getalldates")); ?>', 
				function(data){
					if (data.Status.return == 0) {
						$.each(data.Status.msg, function(i, item){
							opsi = opsi + '<option value="' + item.id + '">' + item.tanggal  + '</option>';
						});
					}
					otglcombo.html("");
					otglcombo.html(opsi);
			}).error(function(){
				alert('Terjadi kesalahan saat melakukan permintaan data tanggal Kolokium!');
			});
		}
	});

	// untuk dataTable Presensi
	var oTable = $('#tabelpresensi').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
 				});
	
	$('.expand-tamhs-table').click(function(e){
		e.preventDefault();
		var idta = $(this).attr('idtamhs');
		var nRow = $(this).parents('tr')[0];
		var gb = $(this).children();
		
		if ($('#fileman-tamhs-' + idta).length > 0) {
			$('#fileman-tamhs-' + idta).slideToggle("normal", function(){
				var nmgb = gb.attr('src');
				if (nmgb.indexOf('up.png') != -1) {
					gb.attr('src', '<?php echo $this->Html->url('/img/down.png'); ?>');
				} else {
					gb.attr('src', '<?php echo $this->Html->url('/img/up.png'); ?>');
				}
			});
		} else {
			$.getJSON('<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'gettafile')); ?>/' + idta, function(data){
				var newrow = 
					'<tr id="fileman-tamhs-' + idta + '"><td colspan="11">' +
					'<div style="margin-left: 50px; width:80%;"><table id="files-' + idta + '" idta="' + idta +'" >' +
					'<?php echo $this->Html->tableHeaders(array('File', 'Keterangan', 'Modified')); ?>';
				
				if (data.Status.return > 0) {
					newrow = newrow + '<tr>' +
					'	<td colspan="3">' + data.Status.msg + '</td>'+
					'</tr>';
				} else {
					$.each(data.Status.msg, function(i, item){
						newrow = newrow + '<tr>' +
						'	<td style="width: 100px;"><a href="<?php echo $this->Html->url(array('controller' => 'tas', 'action' => 'download')); ?>/' + item.Tafile.id + '" target="_blank" title="Download file">' + item.Jenisfile.jenis + '</a></td><td>' + item.Tafile.keterangan + '</td><td style="width: 130px;">' + item.Tafile.modified + '</td>'+
						'</tr>';
					});
				}
				newrow = newrow + '</table></div>' +
					'</td></tr>';
				$(nRow).after(newrow).slideDown();
				gb.attr('src', '<?php echo $this->Html->url('/img/up.png'); ?>');
			});
		}
		return false;
	});
	
	$('.prn-ba').click(function(e){
		e.preventDefault();
		var nRow = $(this).parents('tr')[0];
		var stat =  $(nRow).children('td:nth-child(6)').text().trim();		
		var id = ($(this).attr("id").substr(2));
		if (stat == 'Lulus' || stat == 'Lulus Bersyarat' || stat == 'Belum Disetujui') {
			window.open("<?php echo $this->Html->url(array('controller'=>'papers', 'action'=>'prnba')); ?>/" + id);
		} else {
			alert('Tidak dapat mencetak Berita Acara, karena mahasiswa terpilih belum dinyatakan Lulus atau Belum Lulus!');
		}
	});
	
 	CKEDITOR.replace( 'baformcatatan',
	{
		//contentsCss : 'assets/output_xhtml.css',
		toolbar : 'MinimToolbar',
		height: 100,
		width: 750
	});

 	CKEDITOR.replace( 'baforminternal',
	{
		//contentsCss : 'assets/output_xhtml.css',
		toolbar : 'MinimToolbar',
		height: 100,
		width: 750
	});	

	
	$('#label-internal').click(function(){
		$('#blokcatatan').hide('fast', function(){
			$('#label-catatan > span').css('background-position', '0px 0px');
		});
		$('#blokinternal').show('fast', function() {
			$('#label-internal > span').css('background-position', '-16px 0px');
		});
	});
	$('#label-catatan').click(function(){
		$('#blokinternal').hide('fast', function(){
			$('#label-internal > span').css('background-position', '0px 0px');
		});
		$('#blokcatatan').show('fast', function() {
			$('#label-catatan > span').css('background-position', '-16px 0px');
		});
	});

	$("#berita-acara").dialog({
		autoOpen: false,
		width: 800,
		position: 'center',
		modal: true
	});
	
	$(".ba-icon").click(function(e) {
		e.preventDefault();
		var id = $(this).attr("idba");
		
		//var nRow = $(this).parents('tr')[0];
		//var stat =  $(nRow).children('td:nth-child(5)').text().trim();
		var stat = $(this).attr("status");
		
		$("#berita-acara").dialog({
			buttons: {
				"Simpan" : function() {
					CKEDITOR.instances.baformcatatan.updateElement();
					CKEDITOR.instances.baforminternal.updateElement();
					$.post("<?php echo $this->Html->url(array('controller' => 'papers', 'action' => 'saveba') ); ?>", $('#baform').serialize(),
								function(data) {
									if (data.Status.return == 0){
										$('#berita-acara').dialog("close");
										location.reload();
									} else {
										alert("ADA KESALAHAN SISTEM: " + data.Status.msg);
									}
								}, "json"
							);
				},
				"Batal" : function() {
				  $(this).dialog("close");
				}
			}
		});
		
		/*if (stat != 'Valid') {
			$("#berita-acara").dialog({
						buttons: {
							"Batal" : function() {
							  $(this).dialog("close");
							}
						}
					});
		}*/
		
		$("#berita-acara").dialog("open");

		$('#baform-batas_kumpul').datepicker({
				dateFormat: 'dd-mm-yy'
			});
		
		$.getJSON('<?php echo $this->Html->url(array('controller'=>'papers', 'action'=>'bakol')); ?>' + '/' + id, function(data) {
			if (data.Status.return == 0 || data.Status.return == 4) {
				$('#baform-id').val(data.Status.msg.Kolokium.id);
				$('#baform-nim').html('<strong>' + data.Status.msg.Kolokium.nim + '</strong>');
				$('#baform-nama').html('<strong>' + data.Status.msg.Kolokium.fullname + '</strong>');
				$('#baform-judul').val(data.Status.msg.Kolokium.judul);
				$('#baform-judulproposal').val(data.Status.msg.Kolokium.judul_proposal);
				CKEDITOR.instances.baformcatatan.setData(data.Status.msg.Kolokium.catatan);
				$('#baform-dosen1').val(data.Status.msg.Kolokium.dosen1);
				$('#baform-dosen2').val(data.Status.msg.Kolokium.dosen2);
				$('#baform-status').val(data.Status.msg.Kolokium.status);
				$('#baform-batas_kumpul').val(data.Status.msg.Kolokium.tglkumpul);
				CKEDITOR.instances.baforminternal.setData(data.Status.msg.Kolokium.internal);
				//$('#baforminternal').val(data.Status.msg.Kolokium.internal);
				$('#baform-judulproposal').attr('readonly', 'readonly');
				$('#baform-batas_kumpul').attr('readonly', 'readonly');
				if (stat != 'V' || data.Status.return == 4) {
					$('#baform-judul').attr('readonly', 'readonly');
					$('#baform-judulproposal').attr('disabled', 'disabled');
					$('#baform-batas_kumpul').attr('disabled', 'disabled');
					$('#baform-dosen1').attr('disabled', 'disabled');
					$('#baform-dosen2').attr('disabled', 'disabled');
					$('#baform-status').attr('disabled', 'disabled');
					$(":button:contains('Simpan')").prop("disabled", true).addClass("ui-state-disabled");
				} else {
					//$('#baform-batas_kumpul').removeAttr('disabled');
					$('#baform-judul').removeAttr('readonly');
					$('#baform-judulproposal').removeAttr('disabled');
					$('#baform-dosen1').removeAttr('disabled');
					$('#baform-dosen2').removeAttr('disabled');
					$('#baform-status').removeAttr('disabled');
				}
			} else {
				$("#berita-acara").dialog("close");
				alert(data.Status.msg);
			}
		});
	});

	$('.selectkolokium').buttonset();
	$('.selectkol').click(function(event){
		var sel = $(this);
		
		var valsel = $('input[name="data[select-kol]"]:checked', '#formselkol').val();
		
		if (valsel == 'a') {
			$('#select_title').text = 'Semua Usulan Judul';
		} else {
			$('#select_title').text = 'Usulan Judul dengan Dosen Pembimbing';
		}
		window.location = '<?php echo($this->Html->url(array("controller"=>"admin", "action"=>"home"))); ?>/' + valsel + '#tabs-3';
	});
	
	$('.radiota').buttonset();
	$('.selectta').click(function(event){
		var sel = $(this);
		
		var valsel = $('input[name="data[select-ta]"]:checked', '#formselta').val();
		
		if (valsel == 'a') {
			$('#select_title_ta').text = 'Semua Mahasiswa Tugas Akhir/Skripsi';
		} else if (valsel == 's') {
			$('#select_title_ta').text = 'Daftar Semua Mahasiswa Bimbingan Dosen';
		} else {
			$('#select_title_ta').text = 'Daftar Mahasiswa Bimbingan Dosen';
		}
		window.location = '<?php echo($this->Html->url(array("controller"=>"admin", "action"=>"home"))); ?>/' + $('input[name="data[select-kol]"]:checked', '#formselkol').val() + '/qta:' + valsel + '#tabs-4';
	});
<?php endif; ?>

<?php 
// Blok JavaScript untuk ADMINISTRATOR
// 
if ($this->Session->read('User.group_id') == 1) { ?>
	$('#prnkololist').click(function(e){
		if ($('#tglkol').val() != '') {
			window.open("<?php echo $this->Html->url(array('controller'=>'papers', 'action'=>'report')); ?>/" + $('#tglkol').val());
		}
	});
	$('#prnprolist').click(function(e){
		if ($('#seltglde').val() != '') {
			window.open("<?php echo $this->Html->url(array('controller'=>'proposals', 'action'=>'report')); ?>/" + $('#seltglde').val());
		}
	});
		
	// untuk dataTable Presensi
	var oTable = $('#tabelpresensi').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
 				});
	var nEditing = null;
	//oTable.fnSetColumnVis( 0, false );
	var isAddPresensi = false;

	$('#form-daftar-pendadaran').dialog({
		autoOpen: false,
		width: 800,
		position: 'center',
		modal: true
	});
	
	$('#cetak-presensi-dialog').dialog({
		autoOpen: false,
		width: 400,
		position: 'center',
		modal: true
	});
	
	$('#form-atur-pendadaran').dialog({
		autoOpen: false,
		width: 850,
		position: 'center',
		modal: true
	});
	
	$('#form-hasil-pendadaran').dialog({
		autoOpen: false,
		width: 800,
		position: 'center',
		modal: true
	});
	
	$('#form-edit-judul').dialog({
		autoOpen: false,
		width: 800,
		position: 'center',
		modal: true
	});
	
	$('.editjudul').click(function(e){
		e.preventDefault();
		var id = $(this).attr("idta");
		
		$("#form-edit-judul").dialog({
			buttons: {
				"Simpan" : function() {
					$.post("<?php echo $this->Html->url(array('controller' => 'tas', 'action' => 'savejudul')); ?>", 
						$('#editjudulform').serialize(),
						function(data) {
							if (data.Status.return == 0) {
								$(this).dialog("close");
								alert(data.Status.msg);
								location.reload();
							} else {
								alert(data.Status.msg);
							}
						}, "json"
					);
				},
				"Batal" : function() {
				  $(this).dialog("close");
				}
			}
		});
		
		$('#form-edit-judul').dialog("open");
		
		$.getJSON("<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'getjudul')); ?>"+ '/' + id, function(data) {
			if (data.Status.return == 0) {
				$('#editjudulform-id').val(data.Status.msg.Ta.id);
				$('#editjudulform-nim').html('<strong>' + data.Status.msg.Mahasiswa.nim + '</strong>');
				$('#editjudulform-nama').html('<strong>' + data.Status.msg.Mahasiswa.nama + '</strong>');
				$('#editjudulform-judul').val(data.Status.msg.Ta.judul); 
			} else {
				$("#form-daftar-pendadaran").dialog("close");
				alert(data.Status.msg);
			}
		});
	});
	
	$('.regis-dadar').click(function(e){
		e.preventDefault();
		var id = ($(this).attr("id").substr(2));
		
		$("#form-daftar-pendadaran").dialog({
			buttons: {
				"Daftarkan" : function() {
					$.post("<?php echo $this->Html->url(array('controller' => 'pendadarans', 'action' => 'regis')); ?>", 
						$('#regdadarform').serialize(),
						function(data) {
							if (data.Status.return == 0) {
								$(this).dialog("close");
								alert(data.Status.msg);
								location.reload();
							} else {
								alert(data.Status.msg);
							}
						}, "json"
					);
				},
				"Batal" : function() {
				  $(this).dialog("close");
				}
			}
		});
		
		$('#form-daftar-pendadaran').dialog("open");
		
		$.getJSON('<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'getta')); ?>' + '/' + id, function(data) {
			if (data.Status.return == 0) {
				$('#regdadarform-id').val(data.Status.msg.Ta.id);
				$('#regdadarform-nim').html('<strong>' + data.Status.msg.Mahasiswa.nim + '</strong>');
				$('#regdadarform-nama').html('<strong>' + data.Status.msg.Mahasiswa.nama + '</strong>');
				$('#regdadarform-judul').val(data.Status.msg.Ta.judul); 
				$('#regdadarform-judul').attr('readonly', 'readonly');
			} else {
				$("#form-daftar-pendadaran").dialog("close");
				alert(data.Status.msg);
			}
		});
	});
	
	$('.valdadar').click(function(e){
		e.preventDefault();
		var vid = ($(this).attr("id").substr(2));
		var idta = $(this).attr("idta");
		var nim = $(this).attr("nim");

		$("#dialog").dialog({
			buttons : {
				"Lanjutkan" : function() {
					$.post("<?php echo $this->Html->url(array('controller' => 'pendadarans', 'action' => 'validasi')); ?>", 
						{"id": vid, "nim": nim, "idta": idta},
						function(data) {
							$('#dialog').dialog("close");
							if (data.Status.return == 0) {
								alert(data.Status.msg);
								location.reload();
							} else {
								alert(data.Status.msg);
							}
						}, "json"
					);
				},
				"Batal" : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#dialog").dialog("open");
	});
	
	$('#hasildadarform-nilai1, #hasildadarform-nilai2, #hasildadarform-nilai3, #hasildadarform-nilai4, #hasildadarform-nilai5, #hasildadarform-nilai6').live('change', function(){
		var nilai1 = parseFloat($('#hasildadarform-nilai1').val());
		var nilai2 = parseFloat($('#hasildadarform-nilai2').val());
		var nilai3 = parseFloat($('#hasildadarform-nilai3').val());
		var nilai4 = parseFloat($('#hasildadarform-nilai4').val());
		var nilai5 = parseFloat($('#hasildadarform-nilai5').val());
		var nilai6 = parseFloat($('#hasildadarform-nilai6').val());
		
		var pembagi = 4;
		if (nilai3 <= 0 || nilai4 <= 0 || nilai5 <= 0 || nilai6 <= 0) {
		   pembagi = 3;
		}
		var nilaibimbingan = nilai1 + nilai2;
		var nilaiujian = nilai3 + nilai4 + nilai5 + nilai6;
		var nilaiakhir = 0.4 * nilaibimbingan/2 + 0.6 * nilaiujian/pembagi;
		nilaiakhir = nilaiakhir.toFixed(2);
		$('#hasildadarform-nilaiakhir').html('0.4 x (' + nilaibimbingan + '/2) + 0.6 x (' + nilaiujian + '/' + pembagi + ') = ' + nilaiakhir);
		
		if (nilaiakhir >= 85.0){
		   $('#hasildadarform-nilaihuruf').html('A');
		} else if (nilaiakhir >= 80){
		   $('#hasildadarform-nilaihuruf').html('A-');
		} else if (nilaiakhir >= 75){
		   $('#hasildadarform-nilaihuruf').html('B+');
		} else if (nilaiakhir >= 70){
		   $('#hasildadarform-nilaihuruf').html('B');
		} else if (nilaiakhir >= 65){
		   $('#hasildadarform-nilaihuruf').html('B-');
		} else if (nilaiakhir >= 60){
		   $('#hasildadarform-nilaihuruf').html('C+');
		} else if (nilaiakhir >= 55){
		   $('#hasildadarform-nilaihuruf').html('C');
		} else {
		   $('#hasildadarform-nilaihuruf').html('D');
		   $('#hasildadarform-lulus').val(1);
		}
		if (nilaiakhir >=55) {
		   $('#hasildadarform-lulus').val(2);
		}
	});
	
	$('.hasildadar').live('click', function(e){
		e.preventDefault();
		var iddadar = $(this).attr('iddadar');
		var stat = $(this).text();
		
		$("#form-hasil-pendadaran").dialog({
			buttons: {
				"Simpan" : function() {
					$.post("<?php echo $this->Html->url(array('controller' => 'pendadarans', 'action' => 'savehasildadar')); ?>", 
						$('#hasildadarform').serialize(),
						function(data) {
							if (data.Status.return == 0) {
								alert(data.Status.msg);
								$("#form-hasil-pendadaran").dialog("close");
								location.reload();
							} else {
								alert(data.Status.msg);
							}
						}, "json"
					);
				},
				"Batal" : function() {
				  $(this).dialog("close");
				}
			}
		});
		
		$('#form-hasil-pendadaran').dialog("open");
		$.getJSON('<?php echo $this->Html->url(array('controller'=>'pendadarans', 'action'=>'gethasildadar')); ?>' + '/' + iddadar, function(data) {
			if (data.Status.return == 0) {
				$('#hasildadarform-id').val(data.Status.msg.Dadar.id);
				$('#hasildadarform-nim').html('<strong>' + data.Status.msg.Dadar.nim + '</strong>');
				$('#hasildadarform-nama').html('<strong>' + data.Status.msg.Dadar.nama + '</strong>');
				$('#hasildadarform-judul').val(data.Status.msg.Dadar.judul); 
				$('#hasildadarform-judul').attr('readonly', 'readonly');
				$('#hasildadarform-ujianke').html(data.Status.msg.Dadar.ujianke); 

				$('#hasildadarform-pembimbing1').html(data.Status.msg.Dadar.dosen1); 
				$('#hasildadarform-nilai1').val(data.Status.msg.Dadar.nilai1); 
				if (stat == 'Sudah') {
				   $('#hasildadarform-nilai1').attr('readonly', 'readonly');
				} else {
				   $('#hasildadarform-nilai1').removeAttr('readonly');
				}

				$('#hasildadarform-pembimbing2').html(data.Status.msg.Dadar.dosen2); 
				$('#hasildadarform-nilai2').val(data.Status.msg.Dadar.nilai2); 
				if (stat == 'Sudah') {
				   $('#hasildadarform-nilai2').attr('readonly', 'readonly');
				} else {
				   $('#hasildadarform-nilai2').removeAttr('readonly');
				}
				
				$('#hasildadarform-penguji1').html('<strong>' + data.Status.msg.Dadar.penguji1 + ' *</strong>'); 
				$('#hasildadarform-nilai3').val(data.Status.msg.Dadar.nilai3); 
				if (stat == 'Sudah') {
				   $('#hasildadarform-nilai3').attr('readonly', 'readonly');
				} else {
				   $('#hasildadarform-nilai3').removeAttr('readonly');
				}

				$('#hasildadarform-penguji2').html(data.Status.msg.Dadar.penguji2); 
				$('#hasildadarform-nilai4').val(data.Status.msg.Dadar.nilai4); 
				if (stat == 'Sudah') {
				   $('#hasildadarform-nilai4').attr('readonly', 'readonly');
				} else {
				   $('#hasildadarform-nilai4').removeAttr('readonly');
				}

			    $('#hasildadarform-penguji3').html(data.Status.msg.Dadar.penguji3); 
				$('#hasildadarform-nilai5').val(data.Status.msg.Dadar.nilai5); 
				if (stat == 'Sudah') {
				   $('#hasildadarform-nilai5').attr('readonly', 'readonly');
				} else {
				   $('#hasildadarform-nilai5').removeAttr('readonly');
				}

				$('#hasildadarform-penguji4').html(data.Status.msg.Dadar.penguji4);
				$('#hasildadarform-nilai6').val(data.Status.msg.Dadar.nilai6); 
				if (stat == 'Sudah') {
				   $('#hasildadarform-nilai6').attr('readonly', 'readonly');
				} else {
				   $('#hasildadarform-nilai6').removeAttr('readonly');
				}
				$('#hasildadarform-jadwal').html(data.Status.msg.Dadar.jadwal); 
				if (stat == 'Sudah') {
				   $('#hasildadarform-lulus').attr('disabled', 'disabled');
				} else {
				   $('#hasildadarform-lulus').removeAttr('disabled');
				}
				
				$('#hasildadarform-nilai1').trigger('change'); 
				
				if (stat == 'Sudah') {
				   $(".ui-dialog-buttonpane button:contains('Simpan')").button("disable");
				} else {
				   $(".ui-dialog-buttonpane button:contains('Simpan')").button("enable");
				}
				
			} else {
				$("#form-hasil-pendadaran").dialog("close");
				alert(data.Status.msg);
			}
		});
	});
	
	$('.aturjadwaldadar').click(function(e){
		e.preventDefault();
		var dadarid = $(this).attr("dadarid");
		
		$("#form-atur-pendadaran").dialog({
			buttons: {
				"Simpan" : function() {
					$.post("<?php echo $this->Html->url(array('controller' => 'pendadarans', 'action' => 'aturjadwal')); ?>", 
						$('#aturdadarform').serialize(),
						function(data) {
							if (data.Status.return == 0) {
								alert(data.Status.msg);
								$("#form-atur-pendadaran").dialog("close");
								location.reload();
							} else {
								alert(data.Status.msg);
							}
						}, "json"
					);
				},
				"Batal" : function() {
				  $(this).dialog("close");
				}
			}
		});
		
		$('#form-atur-pendadaran').dialog("open");
		$('#aturdadarform-jadwal').datetimepicker({
				dateFormat: 'dd-mm-yy', 
				timeFormat: 'hh:mm:ss', 
				showSecond: true
			});
		$.getJSON('<?php echo $this->Html->url(array("controller"=>"pendadarans", "action"=>"getdadar")); ?>' + '/' + dadarid, function(data) {
			if (data.Status.return == 0) {
				$('#aturdadarform-id').val(data.Status.msg.Dadar.id);
				$('#aturdadarform-nim').html('<strong>' + data.Status.msg.Dadar.nim + '</strong>');
				$('#aturdadarform-nama').html('<strong>' + data.Status.msg.Dadar.nama + '</strong>');
				$('#aturdadarform-judul').val(data.Status.msg.Dadar.judul); 
				$('#aturdadarform-judul').attr('readonly', 'readonly');
				$('#aturdadarform-ujianke').val(data.Status.msg.Dadar.ujianke); 
				$('#aturdadarform-penguji1').val(data.Status.msg.Dadar.penguji1); 
				$('#aturdadarform-penguji1').attr('disabled', 'disabled');
				$('#aturdadarform-penguji2').val(data.Status.msg.Dadar.penguji2); 
				$('#aturdadarform-penguji2').attr('disabled', 'disabled');
				$('#aturdadarform-penguji3').val(data.Status.msg.Dadar.penguji3); 
				$('#aturdadarform-penguji4').val(data.Status.msg.Dadar.penguji4); 
				$('#aturdadarform-ketua').val(data.Status.msg.Dadar.ketua); 
				$('#aturdadarform-jadwal').val(data.Status.msg.Dadar.jadwal); 
			} else {
				$("#form-atur-pendadaran").dialog("close");
				alert(data.Status.msg);
			}
		});
	});
	
	$('.btnvalidate').click(function(e){
		e.preventDefault();
		var proposalid = $(this).attr("idproposal");
		var isvalid = $(this).attr("isvalid");
		var o = $(this);
		var baseurl = '<?php echo $this->Html->url(array('controller'=>'proposals','action'=>'evaluasi')); ?>/' + proposalid;
		
		$("#dialog").dialog({
			buttons : {
				"Lanjutkan" : function() {
					$.getJSON('<?php echo $this->Html->url(array('controller'=>'proposals', 'action'=>'validasi')); ?>' + '/' + proposalid + '/' + isvalid, 
						function(data) {
							if (data.Status.return == 0) {
								$('#dialog').dialog("close");
								var txtold = o.closest('tr').children('td:last').html();

								if (isvalid == 1) {
									o.html('<span class="icon-valid" style="background-position: -24px 0px;"></span>');
									o.attr('title', 'SUDAH tervalidasi. Klik untuk menonvalidasikan Pembayaran Desk Evaluation');
									o.attr('isvalid',0);
									if (data.Status.data.status_proposal == 1 && 
										!data.Status.data.kosong) {
										txtold = '<a href="' + baseurl + '">Evaluasi</a>' + txtold;
										o.closest('tr').children('td:last').html("");
										o.closest('tr').children('td:last').html(txtold);
									}
								} else {
									o.html('<span class="icon-valid" style="background-position: 0px 0px;"></span>');
									o.attr('title', 'BELUM tervalidasi. Klik untuk validasi Pembayaran Desk Evaluation');
									o.attr('isvalid',1);
									o.closest('tr').children('td:last').html("");
									o.closest('tr').children('td:last').html(
										txtold.replace('<a href="' + baseurl + '">Evaluasi</a>',""));
								}
							} else {
								$('#dialog').dialog("close");
								alert(data.Status.msg);
							}
					});
				},
				"Batal" : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#dialog").dialog("open");
	});
	
	//$( ".statuskolokium > .radio" ).buttonset();
	
	$('#mnuprint').change(function() {
		if ($(this).val() == 'j') {
			if ($('#tglkol').val() != '') {
				window.open("<?php echo $this->Html->url(array('controller'=>'papers', 'action'=>'report')); ?>/" + $('#tglkol').val());
			}
		} else if ($(this).val() == 'b') {
			if ($('#tglkol').val() != '') {
				window.open("<?php echo $this->Html->url(array('controller'=>'papers', 'action'=>'prnallba')); ?>/" + $('#tglkol').val());
			}
		} else if ($(this).val() == 's') {
			if ($('#tglkol').val() != '') {
				window.open("<?php echo $this->Html->url(array('controller'=>'papers', 'action'=>'prnletters')); ?>/" + $('#tglkol').val());
			}
		}
	});
	
	$('#prndadarform-batasdadar').change(function(){
		var id = $(this).val();
		if (id != '') {
			window.open("<?php echo $this->Html->url(array('controller'=>'pendadarans', 'action'=>'prnpeserta')); ?>/" + id);
		}
	});
	
	$('#dosenbimbing').change(function(){
		var id = $(this).val();
		if (id != '') {
			window.open("<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'prnbimbingan')); ?>/" + id);
		}
	});

	$('#btnlihatskripsi').click(function(e) {
		e.preventDefault();
		var id = $('#menuskripsi').val();
		if (id == 1) {
			window.open("<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'prnlist')); ?>");
		} else if (id == 2) {
			window.open("<?php echo $this->Html->url(array('controller'=>'tas', 'action'=>'prnjmlta')); ?>");
		}
	});
	
	//$('.kolstat').click(function(event){
	$('.kolstat').live('change', function(event){
		//event.preventDefault();
		var sel = $(this);
		
		$("#dialog").dialog({
		  buttons : {
			"Lanjutkan" : function() {
				$(this).dialog("close");
				//var s = sel.attr('id').substr(-1);
				var id = sel.attr('id');
				if (id) {
					//var tipe = $('#tipekolokium' + id).val();

					$.getJSON('<?php echo($this->Html->url(array("controller"=>"papers", "action"=>"status"))); ?>/' + id + sel.val(), 
						  function(data){
								alert(data.Status.msg);
								//location.reload();
						  });
				}
			},
			"Batal" : function() {
			  $(this).dialog("close");
			}
		  }
		});
		$("#dialog").dialog("open");
	});
	
	function restoreRow ( oTable, nRow )
	{
		var aData = oTable.fnGetData(nRow);
		var jqTds = $('>td', nRow);
		
		for ( var i=0, iLen=jqTds.length ; i<iLen ; i++ ) {
			oTable.fnUpdate( aData[i], nRow, i, false );
		}
		
		oTable.fnDraw();
	}

	function editRow ( oTable, nRow )
	{
		var aData = oTable.fnGetData(nRow);
		var jqTds = $('>td', nRow);
		jqTds[1].innerHTML =  '<select name="dosenkol" id="dosenkol">' +
								<?php foreach($alldosens as $d):?>
									'<option value="<?php echo $d['Dosen']['id']; ?>"><?php echo $d['Dosen']['nama']; ?></option>' +
								<?php endforeach; ?>
								'</select>';
		var i = aData[1].indexOf('-');
		if (i >= 0) {
			var splits = aData[1].split("-", 2);
			$("#dosenkol").val(splits[0]).attr('selected', true);
		} 
		jqTds[2].innerHTML = '<input type="text" id="timestart" value="'+aData[2]+'">';
		jqTds[3].innerHTML = '<input type="text" id="timeend" value="'+aData[3]+'">';
		jqTds[4].innerHTML = '<input type="text" id="lama" value="'+aData[4]+'">';
		jqTds[5].innerHTML = '<a class="edit" href="">Save</a>';
		jqTds[6].innerHTML = '<a class="delete" href="">Cancel</a>';
		
		<?php 
			if (!empty($tglpresensi)):
				if ($tipepresensi == 1) {
					list($thn, $bln, $hari) = split('-', $tglpresensi['Jadwal']['tanggal']);
				} elseif ($tipepresensi == 0) {
					list($thn, $bln, $hari) = split('-', date('Y-m-d', strtotime($tglpresensi['De']['tanggal'])));
				} else {
					list($thn, $bln, $hari) = split('-', date('Y-m-d'));
				}
			else:
				$skrg = getdate();
				$thn = $skrg['year'];
				$bln= $skrg['mon'];
				$hari = $skrg['mday'];
			endif;
		?>
		
		$('#timestart').datetimepicker({
				minDate: new Date(<?php echo $thn; ?>, <?php echo $bln; ?>-1, <?php echo $hari; ?>, 8, 00),
				maxDate: new Date(<?php echo $thn; ?>, <?php echo $bln; ?>-1, <?php echo $hari; ?>, 17, 30),
				dateFormat: 'dd-mm-yy', 
				timeFormat: 'hh:mm:ss', 
				showSecond: true
			});
		$('#timeend').datetimepicker({
				minDate: new Date(<?php echo $thn; ?>, <?php echo $bln; ?>-1, <?php echo $hari; ?>, 8, 00),
				maxDate: new Date(<?php echo $thn; ?>, <?php echo $bln; ?>-1, <?php echo $hari; ?>, 17, 30),
				dateFormat: 'dd-mm-yy', 
				timeFormat: 'hh:mm:ss', 
				showSecond: true
			});
		
		$('#timestart').change(function(){
			var mulai = $(this).val().split(' ', 2);
			var tglmulai = mulai[0].split('-',3);
			var jammulai = mulai[1].split(':', 3);
			if ($('#timeend').val() == '') {
				$('#lama').val('0');
			} else {
				var akhir = $('#timeend').val().split(' ', 2);
				var tglakhir = akhir[0].split('-',3);
				var jamakhir = akhir[1].split(':', 3);
				var waktuawal = new Date(tglmulai[2], parseInt(tglmulai[1])-1, tglmulai[0], jammulai[0], jammulai[1], jammulai[2]);
				var waktuakhir = new Date(tglakhir[2], parseInt(tglakhir[1])-1, tglakhir[0], jamakhir[0], jamakhir[1], jamakhir[2]);
				var lama = (waktuakhir - waktuawal)/3600000;
				$('#lama').val( lama.toFixed(2) );
			}
		});
			
		$('#timeend').change(function(){
			var akhir = $(this).val().split(' ', 2);
			var tglakhir = akhir[0].split('-',3);
			var jamakhir = akhir[1].split(':', 3);
			if ($('#timestart').val() == '') {
				$('#lama').val('0');
			} else {
				var mulai = $('#timestart').val().split(' ', 2);
				var tglmulai = mulai[0].split('-',3);
				var jammulai = mulai[1].split(':', 3);
				var waktuawal = new Date(tglmulai[2], parseInt(tglmulai[1])-1, tglmulai[0], jammulai[0], jammulai[1], jammulai[2]);
				var waktuakhir = new Date(tglakhir[2], parseInt(tglakhir[1])-1, tglakhir[0], jamakhir[0], jamakhir[1], jamakhir[2]);
				var lama = (waktuakhir - waktuawal)/3600000;
				$('#lama').val( lama.toFixed(2) );
			}
		});
	}

	function saveRow ( oTable, nRow )
	{
		var comboInput = $('select', nRow);
		oTable.fnUpdate( comboInput[0].value + '-' + $("#dosenkol option[value='" + comboInput[0].value + "']").text(), nRow, 1, false );
		
		var jqInputs = $('input', nRow);
		oTable.fnUpdate( jqInputs[0].value, nRow, 2, false );
		oTable.fnUpdate( jqInputs[1].value, nRow, 3, false );
		oTable.fnUpdate( jqInputs[2].value, nRow, 4, false );
		oTable.fnUpdate( '<a class="edit" href="">Edit</a>', nRow, 5, false );
		oTable.fnDraw();
		
		<?php
		if (!empty($tglpresensi)) {
			if ($tipepresensi == 1) {
				$idtglselected = $tglpresensi['Jadwal']['id'];
			} elseif ($tipepresensi == 0) {
				$idtglselected = $tglpresensi['De']['id'];
			} else {
				$idtglselected = " ";
			}
		} else {
			$idtglselected = " ";
		}
		?>
		
		var aData = oTable.fnGetData(nRow);
		$.post("<?php echo $this->Html->url(array('controller' => 'presensis', 'action' => 'save')); ?>", 
					{	idtglkol: "<?php echo $idtglselected; ?>", 
						tipepresensi: <?php echo $tipepresensi ?>,
						id_dosen: comboInput[0].value, 
						waktu_datang: jqInputs[0].value, 
						waktu_pulang: jqInputs[1].value, 
						lama: jqInputs[2].value,
						id: aData[0]},
					function(data){
						var d = jQuery.parseJSON(data);
						if (d.Status.return == 0) {
							alert(d.Status.msg);
							oTable.fnUpdate( d.Status.id, nRow, 0, false );
							oTable.fnDraw();
						} else {
							alert(d.Status.msg);
							restoreRow( oTable, nRow );
						}
					});
	}

	$('#tipepresensi').change(function() {
		var v = $(this).val();
		var otglcombo = $('#tgl-presensi');
		var opsi = '<option value="" selected="selected"></option>';
		otglcombo.html("");
		otglcombo.html(opsi);
		if (v==0) {
			$('#label_tglpresensi').html(' Tanggal Desk Evaluation:');
			$.getJSON('<?php echo $this->Html->url(array("controller"=>"des", "action"=>"getalldates")); ?>', 
				function(data){
					if (data.Status.return == 0) {
						$.each(data.Status.msg, function(i, item){
							opsi = opsi + '<option value="' + item.id + '">' + item.tanggal  + ' (Kelompok: ' + item.kelompok + ')</option>';
						});
					}
					otglcombo.html("");
					otglcombo.html(opsi);
			}).error(function(){
				alert('Terjadi kesalahan saat melakukan permintaan data tanggal Desk Evaluation!');
			});
		} else {
			$('#label_tglpresensi').html(' Tanggal Kolokium:');
			$.getJSON('<?php echo $this->Html->url(array("controller"=>"jadwals", "action"=>"getalldates")); ?>', 
				function(data){
					if (data.Status.return == 0) {
						$.each(data.Status.msg, function(i, item){
							opsi = opsi + '<option value="' + item.id + '">' + item.tanggal  + '</option>';
						});
					}
					otglcombo.html("");
					otglcombo.html(opsi);
			}).error(function(){
				alert('Terjadi kesalahan saat melakukan permintaan data tanggal Kolokium!');
			});
		}
	});
	
	$('#new-presensi').live('click', function(e) {
		e.preventDefault();
		
		if ($('#tgl-presensi').val() != '' && $('#isbtncari').attr('value') == $('#tgl-presensi').val() ){
			isAddPresensi = true;
			var arrRow = [ '', '', '', '', '',
				'<a class="edit" href="">Edit</a>', '<a class="delete" href="">Cancel</a>' ];
			var aiNew = oTable.fnAddData( arrRow );
			var nRow = oTable.fnGetNodes( aiNew[0] );
			editRow( oTable, nRow );
			nEditing = nRow;
		} else {
			isAddPresensi = false;
			alert('Belum ada tanggal kolokium yang terpilih!');
		}
		return false;
	} );
	
	$('#print-presensi').click( function(e) {
		e.preventDefault();
		if ($('#tgl-presensi').val() == '') {
			alert('Belum ada tanggal kolokium yang terpilih!');
		} else {
			window.open("<?php echo $this->Html->url(array('controller'=>'presensis', 'action'=>'cetak')); ?>/" + $('#tgl-presensi').val() +"/" + $('#tipepresensi').val());
		}
		return false;
	});
	
	/*$('#printall-presensi').click(function(e){
		e.preventDefault();
		$("#cetak-presensi-dialog").dialog({
		  buttons : {
			"Lanjutkan" : function() {
				$(this).dialog("close");
				var awal = $('#cetak-periode-form-awal').val();
				var akhir = $('#cetak-periode-form-akhir').val();
				if (awal && akhir) {
					window.open("<?php echo $this->Html->url(array('controller'=>'presensis', 'action'=>'cetakall')); ?>/" + awal + '/' + akhir);
				}
			},
			"Batal" : function() {
			  $(this).dialog("close");
			}
		  }
		});
		$("#cetak-presensi-dialog").dialog("open");
		return false;
	}); */
	
	$('#tabelpresensi a.delete').live('click', function (e) {
		e.preventDefault();
		
		var nRow = $(this).parents('tr')[0];
		var aData = oTable.fnGetData(nRow);
		if (this.innerHTML == 'Cancel') {
			if (!isAddPresensi) {
				restoreRow( oTable, nRow );
			} else {
				oTable.fnDeleteRow( nRow );
			}
		} else {
			$.getJSON('<?php echo($this->Html->url(array("controller"=>"presensis", "action"=>"delete"))); ?>/' + aData[0], 
						function(data){
							alert(data.Status.msg);
							oTable.fnDeleteRow( nRow );
						});
		}
		isAddPresensi = false;
	} );
	
	$('#tabelpresensi a.edit').live('click', function (e) {
		e.preventDefault();
		
		isAddPresensi = false;
		
		/* Get the row as a parent of the link that was clicked on */
		var nRow = $(this).parents('tr')[0];
		
		if ( nEditing !== null && nEditing != nRow ) {
			/* Currently editing - but not this row - restore the old before continuing to edit mode */
			restoreRow( oTable, nEditing );
			editRow( oTable, nRow );
			nEditing = nRow;
		}
		else if ( nEditing == nRow && this.innerHTML == "Save" ) {
			/* Editing this row and want to save it */
			saveRow( oTable, nEditing );
			nEditing = null;
		}
		else {
			/* No edit in progress - let's start one */
			editRow( oTable, nRow );
			nEditing = nRow;
		}
	} );

	// untuk form berita acara kolokium
	//
 	CKEDITOR.replace( 'baformcatatan',
	{
		toolbar : 'MinimToolbar',
		height: 100,
		width: 750
	});

 	CKEDITOR.replace( 'baforminternal',
	{
		toolbar : 'MinimToolbar',
		height: 100,
		width: 750
	});	

	
	$('#label-internal').click(function(){
		$('#blokcatatan').hide('fast', function(){
			$('#label-catatan > span').css('background-position', '0px 0px');
		});
		$('#blokinternal').show('fast', function() {
			$('#label-internal > span').css('background-position', '-16px 0px');
		});
	});
	$('#label-catatan').click(function(){
		$('#blokinternal').hide('fast', function(){
			$('#label-internal > span').css('background-position', '0px 0px');
		});
		$('#blokcatatan').show('fast', function() {
			$('#label-catatan > span').css('background-position', '-16px 0px');
		});
	});

	$("#berita-acara").dialog({
		autoOpen: false,
		width: 800,
		position: 'center',
		modal: true
	});
	
	$(".ba-icon").click(function(e) {
		e.preventDefault();
		var id = $(this).attr("idba");
		
		var stat = $(this).attr("status");
		
		$("#berita-acara").dialog({
			buttons: {
				"Simpan" : function() {
					CKEDITOR.instances.baformcatatan.updateElement();
					CKEDITOR.instances.baforminternal.updateElement();
					$.post("<?php echo $this->Html->url(array('controller' => 'papers', 'action' => 'modifba') ); ?>", 
						$('#baform').serialize(),
								function(data) {
									if (data.Status.return == 0){
										$('#berita-acara').dialog("close");
										location.reload();
									} else {
										alert("ADA KESALAHAN SISTEM: " + data.Status.msg);
									}
								}, "json"
							);
				},
				"Batal" : function() {
				  $(this).dialog("close");
				}
			}
		});
		
		$("#berita-acara").dialog("open");

		$('#baform-batas_kumpul').datepicker({
				dateFormat: 'dd-mm-yy'
			});
		
		$.getJSON('<?php echo $this->Html->url(array('controller'=>'papers', 'action'=>'bakol')); ?>' + '/' + id, function(data) {
			if (data.Status.return == 0 || data.Status.return == 4) {
				$('#baform-id').val(data.Status.msg.Kolokium.id);
				$('#baform-nim').html('<strong>' + data.Status.msg.Kolokium.nim + '</strong>');
				$('#baform-nama').html('<strong>' + data.Status.msg.Kolokium.fullname + '</strong>');
				$('#baform-judul').val(data.Status.msg.Kolokium.judul);
				$('#baform-judulproposal').val(data.Status.msg.Kolokium.judul_proposal);
				CKEDITOR.instances.baformcatatan.setData(data.Status.msg.Kolokium.catatan);
				$('#baform-dosen1').val(data.Status.msg.Kolokium.dosen1);
				$('#baform-dosen2').val(data.Status.msg.Kolokium.dosen2);
				$('#baform-status').val(data.Status.msg.Kolokium.status);
				$('#baform-batas_kumpul').val(data.Status.msg.Kolokium.tglkumpul);
				CKEDITOR.instances.baforminternal.setData(data.Status.msg.Kolokium.internal);
				$('#baform-judulproposal').attr('readonly', 'readonly');
				$('#baform-batas_kumpul').attr('readonly', 'readonly');
				$('#baform-judul').removeAttr('readonly');
				$('#baform-batas_kumpul').attr('disabled', 'disabled');
				$('#baform-dosen1').attr('disabled', 'disabled');
				$('#baform-dosen2').attr('disabled', 'disabled');
				$('#baform-status').attr('disabled', 'disabled');
			} else {
				$("#berita-acara").dialog("close");
				alert(data.Status.msg);
			}
		});
	});	
<?php } ?>

	$("#dialog").dialog({
		autoOpen: false,
		position: 'center',
		modal: true
	});

	$(".confirmLink").live('click', function(e) {
		e.preventDefault();
		var targetUrl = $(this).attr("href");
		
		$("#dialog").dialog({
			buttons : {
				"Lanjutkan" : function() {
					window.location.href = targetUrl;
				},
				"Batal" : function() {
					$(this).dialog("close");
				}
			}
		});

		$("#dialog").dialog("open");
	});
});
</script>