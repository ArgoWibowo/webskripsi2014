<?php
App::import('Vendor','tcpdf/tcpdf');

$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false); //PDF_PAGE_FORMAT
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripTI UKDW");
$tcpdf->SetTitle("Surat Pengantar Tugas Akhir - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setHeaderData('logoukdw90.png','',
		'             Fakultas Teknologi Informasi Program Studi Sistem Informasi',
		'              Universitas Kristen Duta Wacana - Yogyakarta');

$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(40);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);

if (!empty($data)) {
	$imgbox = $this->Html->image('/app/webroot/img/box.png');
	foreach($data as $d):
	$html = '<p>&nbsp;</p>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.25em;">LEMBAR PENGANTAR SKRIPSI</span><br/>
	<span style="font-size:0.8em">Dicetak tanggal: '. date('d-m-Y H:i:s') . '</span>
</div>
<p>&nbsp;</p>
<table cellpadding="5">
<tr><td style="width: 150px;">Nama Mahasiswa</td><td style="width:15px;">:</td><td style="width: 430px;"><strong>'. $d['Mahasiswa']['nama'] . 
'</strong></td></tr>
<tr><td>No. Induk Mahasiswa</td><td>:</td><td><strong>'. $d['Ta']['nim'] . '</strong></td></tr>
<tr><td>Pengambilan Tugas Akhir</td><td>:</td><td>';
$semester = 'UNKNOWN';
if ($d['Ta']['sem'] == 1) {
	$semester = 'GASAL';
} elseif ($d['Ta']['sem']) {
	$semester = 'GENAP';
}
$html = $html . 'Semester '. $semester.' Tahun Ajaran ' . $d['Ta']['tahun'] . '/' . ($d['Ta']['tahun']+1) . '</td></tr>
<tr><td></td><td></td><td></td></tr>
<tr><td>Judul Tugas Akhir</td><td>:</td><td>'. $d['Ta']['judul'] . '</td></tr>
<tr><td></td><td></td><td></td></tr>
<tr><td>Dosen Pembimbing I</td><td>:</td><td><strong>'. $d['Dosen']['nama'] . ', '. $d['Dosen']['gelar'] . '</strong></td></tr>
<tr><td>Dosen Pembimbing II</td><td>:</td><td><strong>'. $d['Dosen2']['nama'] . ', '. $d['Dosen2']['gelar'] . '</strong></td></tr>
</table>';
		$tcpdf->AddPage();
		// output the HTML content
		$tcpdf->writeHTML($html, true, false, true, false, '');
		$tcpdf->lastPage();
	endforeach;
} else {
	$html = '<h2>Belum ada peserta kolokium untuk tanggal</h2>';
	$tcpdf->AddPage();
	// output the HTML content
	$tcpdf->writeHTML($html, true, false, true, false, '');
	$tcpdf->lastPage();
}

$tcpdf->Output('suratpengantar-' . date('dmY') . '.pdf', 'I');
?>
