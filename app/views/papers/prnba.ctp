<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/proposalpdf');

$tcpdf = new PROPOSALPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Berita Acara Kolokium - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(20);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(20, 35, 20, 20);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',10);

$tcpdf->AddPage();

if (!empty($data)) {
	$teks = '<section>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.5em;">BERITA ACARA KOLOKIUM</span><br/>
	<span><small>Dicetak tanggal: '. date('d-m-Y H:i:s') .'</small></span>
</div>
<p>Dengan ini dinyatakan bahwa PROPOSAL SKRIPSI berjudul:</p>
  <table width="100%" style="border: 1px solid #c0c0c0" cellpadding="5">
  	<tr><td><strong>';
if (!empty($datata['Ta']['judul'])) {
  $teks = $teks . strtoupper($datata['Ta']['judul']);
} else {
  $teks = $teks . strtoupper($data['Kolokium']['judul']);
}
$teks = $teks . '</strong></td></tr>
  </table>
<p>&nbsp;</p>
<p>Yang diajukan oleh mahasiswa:</p>
<p>
<table width="100%">
<tr><td width="250" style="height:50px;">NIM/NAMA</td><td width="15">:</td><td width="580"><strong>'. $data['Kolokium']['nim'] . ' / ' . strtoupper($data['User']['fullname']) . '</strong></td></tr>
<tr><td style="height:50px;">Dosen Pengarah</td><td>:</td><td>';

if (!empty($data['Dosen']['nama'])) {
	$teks = $teks . $data['Dosen']['nama_dosen'];
} else {
	$teks = $teks . '.................................................................';
}

$teks = $teks . '</td></tr>
</table>
</p><p>
<table><tr><td style="width: 150px;">Dinyatakan: </td><td style="width: 150px;">';
if ($data['Kolokium']['status'] == 'L') {
	$teks = $teks . $this->Html->image('/app/webroot/img/boxcheck.png', array('style' => 'vertical-align: middle;'));
} else { 
	$teks = $teks . $this->Html->image('/app/webroot/img/box.png', array('style' => 'vertical-align: middle;'));
}
$teks = $teks . ' LULUS</td><td style="width: 300px;">';
if ($data['Kolokium']['status'] == 'S') {
	$teks = $teks . $this->Html->image('/app/webroot/img/boxcheck.png', array('style' => 'vertical-align: middle;'));
} else { 
	$teks = $teks . $this->Html->image('/app/webroot/img/box.png', array('style' => 'vertical-align: middle;'));
}
$teks = $teks . ' LULUS BERSYARAT</td><td style="width: 300px;">';
if ($data['Kolokium']['status'] == 'B'){
	$teks = $teks . $this->Html->image('/app/webroot/img/boxcheck.png', array('style' => 'vertical-align: middle;'));
} else {
	$teks = $teks . $this->Html->image('/app/webroot/img/box.png', array('style' => 'vertical-align: middle;'));
}
$teks = $teks . ' BELUM DAPAT DITERIMA</td></tr></table></p>
<p>Adapun beberapa catatan terkait dengan proposal skripsi yang disepakati dalam kolokium ini:</p>
<table style="width:100%; border: 1px solid #c0c0c0"  cellpadding="5">
<tr><td style="background-color: #cccccc;">' . 
$data['Kolokium']['catatan'] . 
'</td></tr>
</table>
<p>&nbsp;</p>
<p>Proposal final harus dikumpulkan selambat-lambatnya hari: <span><strong>' . $this->Tools->prnDate($data['Kolokium']['batas_kumpul']) . '</strong></span></p>
<p>Disepakati dalam KOLOKIUM tanggal: ' . $this->Tools->prnDate($data['Jadwal']['tanggal']) . '</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="width:100%;">
<tr><td style="width:50%; text-align:center; text-decoration: underline;">' . strtoupper($data['Dosen']['nama_dosen']) . '</td><td style="width:50%; text-align:center; text-decoration: underline;">' . strtoupper($data['User']['fullname']) . '</td></tr>
<tr><td style="width:50%; text-align:center;">Dosen Pengarah</td><td style="width:50%; text-align:center;">Mahasiswa ybs.</td></tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="width:100%;">
<tr><td style="text-align:center;">(_______________________________________)</td></tr>
<tr><td style="text-align:center;">TIM KOLOKIUM</td></tr>
</table>
<p>&nbsp;</p>
<p>Catatan:<br/><span style="font-size:0.8em;">Berita acara ini dan hasil Evaluasi Ringkasan Proposal harus dilampirkan di proposal final.</span></p>
</section>';
} else {
	$teks = '<h2>Belum ada peserta kolokium untuk tanggal</h2>';
}

// output the HTML content
$tcpdf->writeHTML($teks, true, false, true, false, '');
$tcpdf->Output('kolokium-' . $data['Kolokium']['nim'] . '.pdf', 'I');
?>
