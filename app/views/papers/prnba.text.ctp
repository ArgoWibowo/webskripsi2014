<header class="heading">
<?php //echo $this->Html->image('logoukdw90.png', array('class'=>'logo')); ?>
<div class="prodi"><span style="font-size: 1.00em;">Fakultas Teknologi Informasi Program Studi Sistem Informasi</span><br/>
<span><small>Universitas Kristen Duta Wacana</small></span><br/>
<span><small>Dr Wahidin Sudirahusada 5-25 Yogyakarta - 55224</small></span></div>
</header>
<div style="clear:both;"></div>
<p></p>
<section>
<?php if (!empty($data)): ?>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.25em;">BERKAS ACARA KOLOKIUM</span>
</div>
<div>
<p>Dengan ini dinyatakan bahwa PROPOSAL SKRIPSI dengan judul:</p>
<p style="text-transform:capitalize;">
<?php echo $data['Kolokium']['judul']; ?>
</p>
<p>Yang diajukan oleh mahasiswa:</p>
<table>
<tr><td>NIM/NAMA</td><td>:</td><td><?php echo $data['Kolokium']['nim']; ?> / <?php echo $data['User']['fullname']; ?></td></tr>
</table>
<p>Dinyatakan:</p>
<p><input type="checkbox"/>Diterima&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"/>Belum bisa diterima</p>
<p>dengan judul <input type="checkbox"/>sesuai dengan usulan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"/>disesuaikan dengan judul baru</p>
<p>Judul Skripsi baru yang disepakati (jika terdapat perubahan judul)</p>
<table style="width:100%">
<tr><td style="border-bottom: 1px dotted #000">&nbsp;</td></tr>
<tr><td style="border-bottom: 1px dotted #000">&nbsp;</td></tr>
<tr><td style="border-bottom: 1px dotted #000">&nbsp;</td></tr>
</table>
Adapun beberapa catatan terkait dengan proposal skripsi yang disepakati dalam kolokium ini:
<table style="width:100%; border: 1px solid #c0c0c0">
<tr><td style="height: 300px;">&nbsp;</td></tr>
</table>
<p>Proposal ini diajukan lewat jalur:</p>
<p><input type="checkbox"/>skripsi terarah, dengan dosen pembimbing: ........................................... dan ...........................................</p>
<p><input type="checkbox"/>skripsi studi literatur, dengan dosen pembimbing: ........................................... dan ...........................................</p>
<p><input type="checkbox"/>skripsi regular, dengan usulan dosen pembimbing: ........................................... dan ...........................................</p>
<p>Berita acara ini disepakati dalam kolokium tanggal: <span style="text-weight: bold;"><?php echo $data['Jadwal']['waktu']; ?></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="width:100%;">
<tr><td>Ketua TIM Kolokium</td><td>Mahasiswa ybs.</td></tr>
</table>
<p>&nbsp;</p>
<p><small>Berita acara ini harus dikumpulkan bersama dengan proposal resmi yang telah direvisi untuk pendaftaran skripsi sesuai dengan waktu pengambilan matakuliah skripsi dari mahasiswa yang bersangkutan.</small></p>
</div>
<?php else: ?>
<h3>Tidak ada peserta kolokium untuk id terpilih!</h3>
<?php endif; ?>
</section>