<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/proposalpdf');

$tcpdf = new PROPOSALPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
//$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Surat Pengantar Tugas Akhir - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(100);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(30, 50, 30, false);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);

if (!empty($data)) {
	$imgbox = $this->Html->image('/app/webroot/img/box.png');
	$judulta = nl2br($data['Ta']['judul']);

	$html = '<p>&nbsp;</p>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.25em;">LEMBAR PENGANTAR SKRIPSI</span><br/>
	<span style="font-size:0.8em">Dicetak tanggal: '. date('d-m-Y H:i:s') . '</span>
</div>
<p>&nbsp;</p>
<table cellpadding="5">
<tr><td style="width: 240px;">Nama Mahasiswa</td><td style="width:15px;">:</td><td style="width: 630px;"><strong>'. $data['Mahasiswa']['nama'] . 
'</strong></td></tr>
<tr><td>No. Induk Mahasiswa</td><td>:</td><td><strong>'. $data['Ta']['nim'] . '</strong></td></tr>
<tr><td>Pengambilan Tugas Akhir</td><td>:</td><td>';
$semester = 'UNKNOWN';
if ($data['Ta']['sem'] == 1) {
	$semester = 'GASAL';
} elseif ($data['Ta']['sem']) {
	$semester = 'GENAP';
}
$html = $html . 'Semester '. $semester.' Tahun Ajaran ' . $data['Ta']['tahun'] . '/' . ($data['Ta']['tahun']+1) . '</td></tr>
<tr><td></td><td></td><td></td></tr>
<tr><td>Judul Tugas Akhir</td><td>:</td><td>'. $judulta . '</td></tr>
<tr><td></td><td></td><td></td></tr>
<tr><td>Dosen Pembimbing I</td><td>:</td><td><strong>'. $data['Dosen']['nama'] . ', '. $data['Dosen']['gelar'] . '</strong></td></tr>
<tr><td>Dosen Pembimbing II</td><td>:</td><td><strong>'. $data['Dosen2']['nama'] . ', '. $data['Dosen2']['gelar'] . '</strong></td></tr>
</table>';
} else {
	$html = '<h2>Belum ada peserta tugas akhir atau mahasiswa yang bersangkutan tidak aktif dan atau belum registrasi tugas akhir untuk tanggal</h2>';
}
$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->lastPage();

$tcpdf->Output('suratpengantar-' . $data['Ta']['nim'] . '.pdf', 'I');
?>
