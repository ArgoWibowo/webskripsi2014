<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-3'), array('class'=>'button', 'escape' => false)); ?>
<h3>Pendaftaran Kolokium</h3>
<?php
	echo $this->Form->create('Paper', array('action' => 'edit', 'type' => 'file'));
	echo $this->Form->input('Kolokium.id', array('id' => 'id', 'type' => 'hidden', 'value'=>$data['Kolokium']['id']));
	if ($this->Session->read('User.group_id') == 1):
		echo $this->Form->input('Kolokium.nim', array('id' => 'nim', 'label' => 'NIM: ', 'value' => $data['Kolokium']['nim'], 'error' => false, 'style'=> 'width: 100px;'));
		echo ('<div class="ceknim">');
		echo $this->Html->link('<span>Cek NIM</span>', '#', array('id' => 'ceknim_lnk', 'class'=>'button', 'style'=>'display:inline; vertical-align:middle;', 'escape' => false));
		echo ('<div id="nmmhs" style="display: inline; vertical-align:middle; font-size:1.2em; color: green;">'. $data['User']['fullname'] . '</div>');
		echo ('</div>');
	else:
		echo $this->Form->input('Kolokium.nim', array('id' => 'nim', 'type' => 'hidden', 'value' => $data['Kolokium']['nim'] ));
		echo $this->Form->input('Kolokium.userid', array('id' => 'userid', 'type' => 'hidden', 'value' => $data['Kolokium']['userid'] ));
	endif;
	echo $this->Form->input('Kolokium.judul', array('id' => 'judul', 'value' => $data['Kolokium']['judul'], 'label' => 'Usulan Judul Tugas Akhir: ', 'error' => false, 'style'=> 'width: 100%;'));
	echo $this->Form->input('Kolokium.kolokium_ke', array('id' => 'kolokium_ke', 'readonly'=>'readonly', 'value' => $data['Kolokium']['kolokium_ke'], 'label' => 'Kolokium ke: ', 'error' => false, 'style'=> 'width: 100px;'));
	
	//echo $this->Form->input('Topik.Topik', array('style'=>'width: 400px;'));
	$selected = array();
	foreach($data['Topik'] as $t){
		array_push($selected, $t['id']);
	}
	echo $this->Form->input('Topik.Topik', array('type'=>'select', 'multiple'=>'false', 'options'=> $topiks, 'selected' => $selected, 'style'=>'width: 400px;'));
	
	echo $this->Form->input('Kolokium.usulan_dosen1',array(
			'label' => __('Dosen Pengarah:',true),
			'type' => 'select',
			'options' => $dosens,
			'empty' => true,
			'selected' => $data['Kolokium']['usulan_dosen1'],
		));
	/*echo $this->Form->input('Kolokium.usulan_dosen2',array(
			'label' => __('Usulan Dosen Pembimbing II:',true),
			'type' => 'select',
			'options' => $dosens,
			'empty' => true,
			'selected' => $data['Kolokium']['usulan_dosen2'],
		));*/
	echo $this->Form->input('Kolokium.tgl_kolokium',array(
			'label' => __('Tanggal Rencana Kolokium :',true),
			'type' => 'select',
			'options' => $jadwals,
			'selected' => $this->Html->value('Kolokium.tgl_kolokium'),
		));
	if ($this->Session->read('User.group_id') == 1):
	/*echo '<div id="statuskolokium">';
	echo $this->Form->input('Kolokium.status', array(
			'options' => array('N' => 'Baru', 'L' => 'Lolos', 'B' => 'Belum Lolos'),
			'type' => 'radio',
			'legend' => 'Status Kolokium',
			'default' => $data['Kolokium']['status']
		));
	echo '</div>';*/
		echo $this->Form->input( 'Kolokium.status', array('type'=>'hidden', 'value'=>$data['Kolokium']['status']) );
	endif;
	echo '<p>You can upload your paper only in these format: .doc, .docx, .odt. The maximum size of file is 3 MB. </p>';
	echo $this->Form->input('Kolokium.proposal', array('label' => 'File Proposal:', 'between'=>'<br />','type'=>'file'));
	echo $this->Form->input('dir', array('type' => 'hidden'));
    echo $this->Form->input('mimetype', array('type' => 'hidden'));
    echo $this->Form->input('filesize', array('type' => 'hidden'));	
    echo $this->Form->end( array('label' => 'Submit Pendaftaran Kolokium') ); 
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#statuskolokium').buttonset();
	/*$("#TopikTopik").multiselect(); 
	$("#KolokiumUsulanDosen1").multiselect({
	   multiple: false,
	   header: "Select an option",
	   noneSelectedText: "Select an Option",
	   selectedList: 1
	});
	$("#KolokiumUsulanDosen2").multiselect({
	   multiple: false,
	   header: "Select an option",
	   noneSelectedText: "Select an Option",
	   selectedList: 1
	});
	$("#KolokiumTglKolokium").multiselect({
	   multiple: false,
	   header: "Select an option",
	   noneSelectedText: "Select an Option",
	   selectedList: 1
	});*/
	
	$('#ceknim_lnk').click(function(event){
		$('#nmmhs').val('');
		$.getJSON('<?php echo($this->Html->url(array("controller"=>"papers", "action"=>"ceknim"))); ?>/' + $('#nim').val(), 
			  function(data){
					$('#nmmhs').text(data.Status.msg);
			  		if (data.Status.return != 0) {
						$('#nim').val('');
						$('#nmmhs').css('color', 'red');
					} else {
						$('#nmmhs').css('color', 'green');
					}
			  });
	});
});
</script>