<div style="font-size:1.2em;">
<?php echo $this->element('header_report'); ?>
<div style="clear:both;"></div>
<p>&nbsp;</p>
<section>
<?php if (!empty($data)): ?>
<div><span style="font-weight: bold; font-size: 1.4em;">Daftar Peserta Kolokium</span><br/>
<span style="font-size: 0.85em;">Periode Tanggal: <?php echo $tgl['Jadwal']['waktu']; ?></span></div>
<br/>
<table id="box-report">
<thead>
	<tr>
		<th scope="col">No.</th>
		<th scope="col"><small>NIM</small></th>
		<th scope="col"><small>Nama Mahasiswa</small></th>
		<th scope="col"><small>Usulan Judul Tugas Akhir</small></th>
		<th scope="col"><small>Dosen Pengarah</small></th>
		<th scope="col"><small>Kolokium<br/>Ke</small></th>
		<th scope="col"><small>Status</small></th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="8"><small>*) Beri tanda silang salah satu pilihan</small></td>
	</tr>
</tfoot>
<tbody>
<?php 
$i = 0;
foreach($data as $d): ?>
	<tr>
		<td style="width: 20px; text-align:center;"><?php echo ++$i; ?></td>
		<td style="width: 80px; text-align:center;"><small><?php echo $d['Kolokium']['nim'] ?></small></td>
		<td style="width: 200px; text-align:left;"><small><?php echo $d['User']['fullname'] ?></small></td>
		<td style="width: 400px; text-align:left;"><small><?php echo $d['Kolokium']['judul'] ?></small></td>
		<td style="width: 200px; font-size: 0.8em; text-align:left;"><small><?php echo $d['Dosen']['nama_dosen'] ?></small></td>
		<td style="width: 60px; text-align:center;"><?php echo $d['Kolokium']['kolokium_ke'] ?></td>
		<td style="width: 70px; text-align:center;">
		<?php if ($d['Kolokium']['status'] == 'N'):
			echo 'Baru'; 
		elseif ($d['Kolokium']['status'] == 'L'):
			echo 'Lulus';
		elseif ($d['Kolokium']['status'] == 'B'):
			echo 'Belum Disetujui';
		elseif ($d['Kolokium']['status'] == 'V'):
			echo 'Valid';
		elseif ($d['Kolokium']['status'] == 'S'):
			echo 'Lulus Bersyarat';
		endif;
		?>
		</td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else: ?>
<h2>Belum ada peserta kolokium</h2>
<?php endif; ?>
</section>
</div>