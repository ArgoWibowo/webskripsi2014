<?php echo $this->Html->link('<span>Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-3'), array('class'=>'button', 'escape' => false)); ?>
<h3>Pendaftaran Kolokium</h3>
<?php
	echo $this->Form->create('Paper', array('action' => 'add', 'type' => 'file'));
	/*if ($this->Session->read('User.group_id') == 1):
		echo $this->Form->input('Kolokium.nim', array('id' => 'nim', 'label' => 'NIM: ', 'style'=> 'width: 100px;'));
		echo ('<div class="ceknim">');
		echo $this->Html->link('<span>Cek NIM</span>', '#', array('id' => 'ceknim_lnk', 'class'=>'button', 'style'=>'display:inline; vertical-align:middle;', 'escape' => false));
		echo ('<div id="nmmhs" style="display: inline; vertical-align:middle; font-size:1.2em; color: red;"></div>');
		echo ('</div>');
	endif;*/
	echo $this->Form->input('Kolokium.proposal_id', array('type' => 'hidden', 'value' => $proposal['Proposal']['id']));
	echo $this->Form->input('Kolokium.judul', array('id' => 'judul', 'label' => 'Usulan Judul Tugas Akhir: ', 'style'=> 'width: 100%;', 'value' => $proposal['Proposal']['judul']));
	if (!empty($data['Kolokium']['kolokium_ke'])):
		echo $this->Form->input('Kolokium.kolokium_ke', array('id' => 'kolokium_ke', 'label' => 'Kolokium ke: ', 'readonly'=>'readonly', 'style'=> 'width: 100px;', 'value'=>$data['Kolokium']['kolokium_ke']));
	else:
		echo $this->Form->input('Kolokium.kolokium_ke', array('id' => 'kolokium_ke', 'label' => 'Kolokium ke: ', 'readonly'=>'readonly', 'style'=> 'width: 100px;', 'value'=>$numkolo));
	endif;
	echo $this->Form->input('Topik.Topik', array('style'=>'width: 400px;', 'label' => __('Kosentrasi:', true), 'type' => 'select', 'default' => $proposal['Proposal']['konsentrasi']));
	echo $this->Form->input('Kolokium.usulan_dosen1',array(
			'label' => __('Dosen Pengarah:',true),
			'type' => 'select',
			'options' => $dosens,
			'empty' => true,
			'default' => $proposal['Proposal']['dosen']
		));
	echo $this->Form->input('Kolokium.tgl_kolokium',array(
			'label' => __('Tanggal Rencana Kolokium :',true),
			'type' => 'select',
			'options' => $jadwals,
			'default' => $this->Html->value('Kolokium.tgl_kolokium'),
		));
	if ($this->Session->read('User.group_id') == 1):
		//echo '<div id="statuskolokium">';
		/* echo $this->Form->input('Kolokium.status', array(
				'options' => array('N' => 'Baru', 'L' => 'Lolos', 'B' => 'Belum Lolos'),
				'type' => 'radio',
				'legend' => 'Status Kolokium',
				'default' => 'N'
			)); */
		//echo '</div>';
		echo $this->Form->input( 'Kolokium.status', array('type'=>'hidden', 'value'=>'N') );
	endif;
	echo '<p>You can upload your paper only in these format: .doc, .docx, .odt. The maximum size of file is 3 MB. </p>';
	echo $this->Form->input('Kolokium.proposal', array('label' => 'File Proposal:', 'between'=>'<br />','type'=>'file'));
	echo $this->Form->input('Kolokium.dir', array('type' => 'hidden'));
    echo $this->Form->input('Kolokium.mimetype', array('type' => 'hidden'));
    echo $this->Form->input('Kolokium.filesize', array('type' => 'hidden'));	
    echo $this->Form->end( array('label' => 'Submit Pendaftaran Kolokium') ); 
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#statuskolokium').buttonset();
	$('#ceknim_lnk').click(function(event){
		$('#nmmhs').val('');
		$.getJSON('<?php echo($this->Html->url(array("controller"=>"papers", "action"=>"ceknim"))); ?>/' + $('#nim').val(), 
			  function(data){
					$('#nmmhs').text(data.Status.msg);
			  		if (data.Status.return != 0) {
						$('#nim').val('');
						$('#nmmhs').css('color', 'red');
					} else {
						$('#nmmhs').css('color', 'green');
					}
			  });
	});
});
</script>