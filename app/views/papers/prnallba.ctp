<?php
App::import('Vendor','tcpdf/tcpdf');

$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Berita Acara Kolokium - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setHeaderData('logoukdw90.png','',
		'             Fakultas Teknologi Informasi Program Studi Sistem Informasi',
		'              Universitas Kristen Duta Wacana - Yogyakarta');

$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(40);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);

if (!empty($data)) {
	$imgbox = $this->Html->image('/app/webroot/img/box.png');
	foreach($data as $d):
	$html = '<section>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.25em;">BERITA ACARA KOLOKIUM</span>
</div>
<div>
<p>Dengan ini dinyatakan bahwa PROPOSAL SKRIPSI dengan judul:</p>
  <table width="100%" style="border: 1px solid #c0c0c0" cellpadding="5">
  	<tr><td><strong>' . strtoupper($d['Kolokium']['judul']) . '</strong></td></tr>
  </table>
<p>&nbsp;</p>
<p>Yang diajukan oleh mahasiswa:</p>
<table width="100%">
<tr><td width="90">NIM/NAMA</td><td width="10">:</td><td width="540" style="border-bottom: 1px dotted #c0c0c0"><strong>'. $d['Kolokium']['nim'] . ' / ' . strtoupper($d['User']['fullname']) . '</strong></td></tr>
</table>
<p>&nbsp;</p>
<p>Dinyatakan:</p>
<p>' . $imgbox . ' Diterima&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $imgbox . ' Belum bisa diterima</p>
<p>Judul Skripsi baru yang disepakati (jika terdapat perubahan judul)</p>
<table style="width:100%" cellpadding="5">
<tr><td style="border-bottom: 1px dotted #c0c0c0">&nbsp;</td></tr>
<tr><td style="border-bottom: 1px dotted #c0c0c0">&nbsp;</td></tr>
</table>
<p>&nbsp;</p>
<p>Adapun beberapa catatan terkait dengan proposal skripsi yang disepakati dalam kolokium ini:</p>
<table style="width:100%; border: 1px solid #c0c0c0">
<tr><td style="height: 150px;">&nbsp;</td></tr>
</table>
<p>&nbsp;</p>
<p>Proposal ini diajukan lewat jalur:</p>
<p>' . $imgbox . ' skripsi terarah &nbsp;&nbsp;&nbsp;' . $imgbox . ' skripsi studi literatur &nbsp;&nbsp;&nbsp;' . $imgbox . ' skripsi regular</p>
<p>Perubahan dosen pembimbing <small>(jika ada)</small>: ......................................................... dan .................................................................</p>
<p>Berita acara ini disepakati dalam kolokium tanggal: <span><strong>' . $d['Jadwal']['waktu'] . '</strong></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="width:100%;">
<tr><td>Ketua TIM Kolokium</td><td>Mahasiswa ybs.</td></tr>
</table>
<p>&nbsp;</p>
<p><small>Berita acara ini harus dikumpulkan bersama dengan proposal resmi yang telah direvisi untuk pendaftaran skripsi sesuai dengan waktu pengambilan matakuliah skripsi dari mahasiswa yang bersangkutan.</small></p>
</div>
</section>';
		$tcpdf->AddPage();
		// output the HTML content
		$tcpdf->writeHTML($html, true, false, true, false, '');
		$tcpdf->lastPage();
	endforeach;
} else {
	$html = '<h2>Belum ada peserta kolokium untuk tanggal</h2>';
}

$tcpdf->Output('kolokium-' . $d['Jadwal']['tanggal'] . '.pdf', 'I');
?>