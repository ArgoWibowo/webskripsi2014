<div class="grid_1">
<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-6'), array('class'=>'button', 'escape' => false)); ?>
<h3>Add User</h3>
<?php
echo $this->Form->create('User', array('action' => 'tambah', 'type'=>'file'));
if (isset($data) && sizeof($data) > 0):
	echo $this->Form->input('User.username', array('label' => 'Username', 'value' => $data['User']['username']));
else:
	echo $this->Form->input('User.username', array('label' => 'Username', 'value' => ''));
endif; 
if (isset($data) && sizeof($data) > 0):
	echo $this->Form->input('User.nim', array('label' => 'NIM/NIDN', 'value' => $data['User']['nim']));
else:
	echo $this->Form->input('User.nim', array('label' => 'NIM/NIDN', 'value' => ''));
endif; 
if (isset($data) && sizeof($data) > 0):
	echo $this->Form->input('User.fullname', array('label' => 'Full Name', 'value' => $data['User']['fullname']));
else:
	echo $this->Form->input('User.fullname', array('label' => 'Full Name', 'value' => ''));
endif; 
echo $this->Form->input('User.password', array('label' => 'Password:', 'id' => 'pwd', 'value' => ''));
echo $this->Form->input('User.password2', array('label' => 'Re-type password:', 'id' => 'pwd2', 'type'=>'password', 'value' => ''));
echo '<div class="activeuser">';
echo $this->Form->input('User.active', array(
										'options' => array(1 => 'Aktif', '0' => 'Tidak Aktif'),
										'type' => 'radio',
										'legend' => '',
										'class' => 'useractive',
										'default' => 1
									));
echo '</div>';
echo $this->Form->input('User.group_id',array(
		'label' => __('User Group:',true),
		'type' => 'select',
		'options' => $groups,
		'selected' => 2,
	));
echo $this->Form->input('User.email', array('label' => 'email'));
echo $this->Form->input('User.telpno', array('label' => 'Telp no.'));
echo '</div>';
echo '<div class="grid_2" style="padding-top: 60px; margin-left:75px; display:block; width:500px">';
echo $this->Html->image('anonym.png');
echo '<p>Anda dapat upload foto profil Anda dengan ukuran maksimum 140 x 140 pixel.</p>';
echo $this->Form->input('User.foto', array('label' => 'File foto:', 'type'=>'file'));
echo '</div>';
echo '<div class="grid_3">';
echo $this->Form->end(array('label' => 'Save', 'id' => 'submit'));
?>
</div>
<style>
	span.error {
		margin-left: 10px;
		color: #ffffff;
		background-color: #ff0000;
		padding: 2px 6px;
	}
</style>
<script type="text/javascript">
jQuery(function(){
	$( ".activeuser" ).buttonset();
	
	$("#submit").click(function(){
		$(".error").hide();
		var hasError = false;
		var passwordVal = $("#pwd").val();
		var checkVal = $("#pwd2").val();
		
		if (passwordVal == '') {
			$("#pwd").after('<span class="error">Please type your password.</span>');
			hasError = true;
		} else if (checkVal == '') {
			$("#pwd2").after('<span class="error">Please re-type your password.</span>');
			hasError = true;
		} else if (passwordVal != checkVal ) {
			$("#pwd2").after('<span class="error">Sorry, your password are not exactly same.</span>');
			hasError = true;
		}
		if(hasError == true) {return false;}
	});
});
</script>