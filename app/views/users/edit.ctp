<div class="grid_1">
<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-6'), array('class'=>'button', 'escape' => false)); ?>
<h3>Edit User</h3>
<?php
if (isset($data) && sizeof($data) > 0):
echo $this->Form->create('User', array('action' => 'save', 'type'=>'file'));
echo $this->Form->input('User.id', array('type' => 'hidden', 'value' => $data['User']['id']));
echo $this->Form->input('User.username', array('label' => 'Username', 'readonly' => 'true', 'value' => $data['User']['username'], 'style'=>'width: 100px;'));
if ($this->Session->read('User.id')  == 1):
	echo $this->Form->input('User.nim', array('label' => 'NIM', 'value' => $data['User']['nim'], 'style'=>'width: 150px;'));
else:
	echo $this->Form->input('User.nim', array('label' => 'NIM', 'value' => $data['User']['nim'], 'style'=>'width: 150px;', 'readonly' => 'readonly'));
endif;
echo $this->Form->input('User.fullname', array('label' => 'Full Name', 'value' => $data['User']['fullname'], 'style'=>'width: 400px;'));
if ($this->Session->read('User.group_id')  == 1):
	echo '<div class="activeuser">';
	echo $this->Form->input('User.active', array(
											'options' => array(1 => 'Aktif', '0' => 'Tidak Aktif'),
											'type' => 'radio',
											'legend' => '',
											'class' => 'useractive',
											'default' => $data['User']['active']
										));
	echo '</div>';
	echo $this->Form->input('User.group_id',array(
			'label' => __('User Group:',true),
			'type' => 'select',
			'options' => $groups,
			'selected' => $data['User']['group_id'] // $this->Html->value('User.group_id'),
		));
endif;
echo $this->Form->input('User.email', array('label' => 'email', 'value' => $data['User']['email'], 'style'=>'width: 400px;'));
echo $this->Form->input('User.telpno', array('label' => 'Telp no.', 'value' => $data['User']['telpno'], 'style'=>'width: 200px;'));
echo '</div>';
echo '<div class="grid_2" style="padding-top: 60px; margin-left:75px; display:block; width:500px">';
if (!empty($data['User']['foto'])):
	echo '<img src="' . $this->Html->url(array('controller'=>'users', 'action'=>'foto', $data['User']['id'])) . '"/>';
else:
	echo $this->Html->image('anonym.png');
endif;
echo '<p>Anda dapat upload foto profil Anda dengan ukuran maksimum 140 x 140 pixel.</p>';
echo $this->Form->input('User.foto', array('label' => 'File foto:', 'type'=>'file'));
echo '</div>';
echo '<div class="grid_3">';
echo $this->Form->end(array('label' => 'Save', 'id' => 'submit'));
endif;
?>
</div>
<style>
	span.error {
		margin-left: 10px;
		color: #ffffff;
		background-color: #ff0000;
		padding: 2px 6px;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$( ".activeuser" ).buttonset();
});
</script>