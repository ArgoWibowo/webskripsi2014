<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-6'), array('class'=>'button', 'escape' => false)); ?>
<h3>Edit User</h3>
<?php
if (isset($data) && sizeof($data) > 0):

echo $this->Form->create('User', array('action' => 'savepwd'));
echo $this->Form->input('User.id', array('type' => 'hidden', 'value' => $data['User']['id']));
echo $this->Form->input('User.username', array('label' => 'Username', 'readonly' => 'true', 'value' => $data['User']['username'], 'style' => 'width: 100px'));
echo $this->Form->input('User.fullname', array('label' => 'Full Name', 'readonly' => 'true', 'value' => $data['User']['fullname'], 'style' => 'width: 400px'));
echo $this->Form->input('User.password', array('label' => 'Password:', 'id' => 'pwd', 'style' => 'width: 200px'));
echo $this->Form->input('User.password2', array('label' => 'Re-type password:', 'id' => 'pwd2', 'type'=>'password', 'style' => 'width: 200px'));
echo $this->Form->end(array('label' => 'Ubah Password', 'id' => 'submit'));

endif;
?>
<style>
	span.error {
		margin-left: 10px;
		color: #ffffff;
		background-color: #ff0000;
		padding: 2px 6px;
	}
</style>
<script type="text/javascript">
jQuery(function(){
        $("#submit").click(function(){
        $(".error").hide();
        var hasError = false;
        var passwordVal = $("#pwd").val();
        var checkVal = $("#pwd2").val();
        if (passwordVal == '') {
            $("#pwd").after('<span class="error">Please type your password.</span>');
            hasError = true;
        } else if (checkVal == '') {
            $("#pwd2").after('<span class="error">Please re-type your password.</span>');
            hasError = true;
        } else if (passwordVal != checkVal ) {
            $("#pwd2").after('<span class="error">Sorry, your password are not exactly same.</span>');
            hasError = true;
        }
        if(hasError == true) {return false;}
    });
});
</script>