<header class="heading">
<?php echo $this->Html->image('logoukdw90.png', array('class'=>'logo')); ?>
<div class="prodi"><span style="font-size: 1.00em;">Fakultas Teknologi Informasi Program Studi Sistem Informasi</span><br/>
<span><small>Universitas Kristen Duta Wacana</small></span><br/>
<span><small>Dr Wahidin Sudirahusada 5-25 Yogyakarta - 55224</small></span></div>
</header>
<div style="clear:both;"></div>
<p></p>
<section style=" font-size: 1.2em;">
<?php if (!empty($data)): ?>
<div style="text-align:left;"><span style="font-weight: bold; font-size: 1.2em;">
	<?php 
	echo $judul; 
	if ($data[0]['Presensi']['tipe'] == 0) {
		echo ' (Kelompok: ' . $data[0]['De']['kelompok'] . ')';
	}
	?>
</span><br/>
<span style="font-size: 0.85em;">
	Periode Tanggal: <?php 
		if ($data[0]['Presensi']['tipe'] == 1) {
			echo $this->Tools->prnDate($data[0]['Jadwal']['tanggal'], 2); 
		} else {
			echo $this->Tools->prnDate($data[0]['De']['tanggal'], 2); 
		}
	?>
</span>
</div>
<p>&nbsp;</p>
<table id="box-report">
<thead>
	<tr>
		<th scope="col">No.</th>
		<th scope="col"><small>Nama Dosen</small></th>
		<th scope="col"><small>Waktu Datang</small></th>
		<th scope="col"><small>Waktu Pulang</small></th>
		<th scope="col"><small>Lama (jam)</small></th>
	</tr>
</thead>
<tbody>
<?php 
$i = 0;
$total = 0;
foreach($data as $d): ?>
	<?php $total += $d['Presensi']['lama']?>
	<tr>
		<td style="width: 20px; text-align:center;"><?php echo ++$i; ?></td>
		<td style="width: 300px;"><small><?php echo $d['Dosen']['nama_dosen'] ?></small></td>
		<td style="width: 150px; text-align:center;"><small><?php echo $this->Tools->prnDate($d['Presensi']['waktu_datang'],3); ?></small></td>
		<td style="width: 150px; text-align:center;"><small><?php echo $this->Tools->prnDate($d['Presensi']['waktu_pulang'],3); ?></small></td>
		<td style="width: 60px; text-align:right;"><?php echo $d['Presensi']['lama'] ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
<tfoot>
	<tr>
		<td colspan="2" style="text-align: right; font-weight: bold;">Total Mahasiswa</td>
		<td style="text-align: right; font-weight: bold;">
			<?php
			if (!empty($mhs)) {
				echo ($mhs[0]['total']); 
			} else {
				echo 'NaN';
			}
			?>
		</td>
		<td style="text-align: right; font-weight: bold;">Total Waktu</td>
		<td style="width: 60px; text-align:right;"><?php echo $total; ?></td>
	</tr>
</tfoot>
</table>
<?php else: ?>
<h2>Belum ada presensi untuk tanggal terpilih!</h2>
<?php endif; ?>
</section>