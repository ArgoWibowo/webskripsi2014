<header class="heading">
<?php echo $this->Html->image('logoukdw90.png', array('class'=>'logo')); ?>
<div class="prodi"><span style="font-size: 1.00em;">Fakultas Teknologi Informasi Program Studi Sistem Informasi</span><br/>
<span><small>Universitas Kristen Duta Wacana</small></span><br/>
<span><small>Dr Wahidin Sudirahusada 5-25 Yogyakarta - 55224</small></span></div>
</header>
<div style="clear:both;"></div>
<p></p>
<section style=" font-size: 1.2em;">
<?php if (!empty($presensi)): ?>
<div style="text-align:left;"><span style="font-weight: bold; font-size: 1.2em;">Daftar Presensi Dosen Penguji Kolokium</span><br/>
<span style="font-size: 0.85em;">Periode Tanggal: <?php echo $this->Tools->prnDate($periode[0]['awal'], 2); ?> s/d <?php echo $this->Tools->prnDate($periode[0]['akhir'],2); ?></span></div>
<p>&nbsp;</p>
<table id="box-report">
<thead>
	<tr>
		<th scope="col">No.</th>
		<th scope="col"><small>Nama Dosen</small></th>
<?php
$totpertgl = array();
foreach($mhs as $m){
  echo '<th scope="col"><small>' . $this->Tools->prnDate($m['Jadwal']['tanggal'], 2) . '</small></th>';
  $totpertgl[$m['Kolokium']['tgl_kolokium']] = 0;
}
?>
		<th scope="col"><small>Sub Total</small></th>
	</tr>
</thead>
<tbody>
<?php 
$i = 0;
$total = 0;
while ($d = current($presensi)) { ?>
	<tr>
		<td style="width: 20px; text-align:center;"><?php echo ++$i; ?></td>
		<td style="width: 300px;"><small><?php echo key($presensi); ?></small></td>
<?php
$subtotal = 0;
foreach($mhs as $m){
	if (isset( $d[$m['Kolokium']['tgl_kolokium']] ) ) {
		$total += $d[$m['Kolokium']['tgl_kolokium']];
		$subtotal += $d[$m['Kolokium']['tgl_kolokium']];
		$totpertgl[$m['Kolokium']['tgl_kolokium']] += $d[$m['Kolokium']['tgl_kolokium']];
		echo '<td style="width: 150px; text-align:right;">' . $d[$m['Kolokium']['tgl_kolokium']] . '</td>';
	} else {
		echo '<td style="width: 150px; text-align:right;">-</td>';
	}
}
?>
		<td style="width: 60px; text-align:right;"><?php echo $subtotal; ?></td>
	</tr>
<?php 
   next($presensi);
} 
?>
</tbody>
<tfoot>
	<tr>
		<td colspan="2" style="text-align: right; font-weight: bold;">Total Waktu</td>		
<?php
foreach($mhs as $m) {
	echo '<td style="text-align: right; font-weight: bold;">' . $totpertgl[$m['Kolokium']['tgl_kolokium']] . '</td>';
}
?>
		<td style="width: 60px; text-align:right;"><?php echo $total; ?></td>
	</tr>
	<tr>
		<td colspan="2" style="text-align: right; font-weight: bold;">Total Mahasiswa</td>
<?php
$totmhs = 0;
foreach($mhs as $m) {
    $totmhs += $m[0]['total'];
	echo '<td style="text-align: right; font-weight: bold;">' . $m[0]['total'] . '</td>';
}
?>
		<td style="width: 60px; text-align:right;"><?php echo $totmhs; ?></td>
	</tr>
</tfoot>
</table>
<?php else: ?>
<h2>Belum ada presensi penguji kolokium untuk tanggal terpilih!</h2>
<?php endif; ?>
</section>