<?php
App::import('Vendor','tcpdf/tcpdf');

$tcpdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Label Berkas Pendadaran - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(40);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);

if (!empty($data)) {
	$logoukdw = $this->Html->image('/app/webroot/img/logoukdw90.png');

	$html = '<p>&nbsp;</p>
<p style="text-align:center;"><table width="100%" cellpadding="10" style="border-collapse:collapse;">
<tr><td width="48%" style="border:1px solid #c0c0c0; text-align:center; font-size: 0.8em;"><p>Fakultas Teknologi Informasi<br/>Program Studi Sistem Informasi<br/>
Universitas Kristen Duta Wacana - Yogyakarta</p>
<p style="font-size: 1.4em;"><strong>BERKAS MATERI UJIAN SKRIPSI</strong></p>
<p>&nbsp;</p>
<p style="font-size: 1.2em;"><strong>' . $data['Ta']['nim'] . '<br/>' . $data['Ta']['Mahasiswa']['nama'] . '</strong></p>
<p>&nbsp;</p>
<p>Jadwal Pendadaran:</p><p style="font-size:1.2em;"><strong>' . $this->Tools->prnDate($data['Pendadaran']['jadwal'], 4) .' WIB</strong></p>
</td>
<td width="4%">&nbsp;&nbsp;&nbsp;</td>
<td width="48%" style="border:1px solid #c0c0c0; text-align:center; font-size: 0.8em;"><p>Fakultas Teknologi Informasi<br/>Program Studi Sistem Informasi<br/>
Universitas Kristen Duta Wacana - Yogyakarta</p>
<p style="font-size: 1.4em;"><strong>BERKAS MATERI UJIAN SKRIPSI</strong></p>
<p>&nbsp;</p>
<p style="font-size: 1.2em;"><strong>' . $data['Ta']['nim'] . '<br/>' . $data['Ta']['Mahasiswa']['nama'] . '</strong></p>
<p>&nbsp;</p>
<p>Jadwal Pendadaran:</p><p style="font-size:1.2em;"><strong>' . $this->Tools->prnDate($data['Pendadaran']['jadwal'], 4) .' WIB</strong></p>
</td></tr>
<tr>
<td style="border:1px solid #c0c0c0; background-color: #c4c4c4; text-align:center;">Dosen Pembimbing:
<p><strong>' . $data['Dosen']['nama_dosen']. '</strong></p></td>
<td width="4%">&nbsp;&nbsp;&nbsp;</td>
<td style="border:1px solid #c0c0c0; background-color: #c4c4c4; text-align:center;">Dosen Pembimbing:
<p><strong>' . $data['Dosen2']['nama_dosen']. '</strong></p></td>
</tr>
</table></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align:center;"><table width="100%" cellpadding="10" style="border-collapse:collapse;">
<tr><td width="48%" style="border:1px solid #c0c0c0; text-align:center; font-size: 0.8em;"><p>Fakultas Teknologi Informasi<br/>Program Studi Sistem Informasi<br/>
Universitas Kristen Duta Wacana - Yogyakarta</p>
<p style="font-size: 1.4em;"><strong>BERKAS MATERI UJIAN SKRIPSI</strong></p>
<p>&nbsp;</p>
<p style="font-size: 1.2em;"><strong>' . $data['Ta']['nim'] . '<br/>' . $data['Ta']['Mahasiswa']['nama'] . '</strong></p>
<p>&nbsp;</p>
<p>Jadwal Pendadaran:</p><p style="font-size:1.2em;"><strong>' . $this->Tools->prnDate($data['Pendadaran']['jadwal'], 4) .' WIB</strong></p>
</td>
<td width="4%">&nbsp;&nbsp;&nbsp;</td>
<td width="48%" style="border:1px solid #c0c0c0; text-align:center; font-size: 0.8em;"><p>Fakultas Teknologi Informasi<br/>Program Studi Sistem Informasi<br/>
Universitas Kristen Duta Wacana - Yogyakarta</p>
<p style="font-size: 1.4em;"><strong>BERKAS MATERI UJIAN SKRIPSI</strong></p>
<p>&nbsp;</p>
<p style="font-size: 1.2em;"><strong>' . $data['Ta']['nim'] . '<br/>' . $data['Ta']['Mahasiswa']['nama'] . '</strong></p>
<p>&nbsp;</p>
<p>Jadwal Pendadaran:</p><p style="font-size:1.2em;"><strong>' . $this->Tools->prnDate($data['Pendadaran']['jadwal'], 4) .' WIB</strong></p>
</td></tr>
<tr>
<td style="border:1px solid #c0c0c0; background-color: #c4c4c4; text-align:center;">Dosen Penguji:
<p><strong>' . $data['Dosen3']['nama_dosen']. '</strong></p></td>
<td width="4%">&nbsp;&nbsp;&nbsp;</td>
<td style="border:1px solid #c0c0c0; background-color: #c4c4c4; text-align:center;">Dosen Penguji:
<p><strong>' . $data['Dosen4']['nama_dosen']. '</strong></p></td>
</tr>
</table></p>';
} else {
	$html = '<h2>Belum ada peserta pendadaran atau mahasiswa yang bersangkutan tidak aktif dan atau belum registrasi tugas akhir untuk tanggal</h2>';
}
$tcpdf->AddPage();
// output the HTML content
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->lastPage();

$tcpdf->Output('labeldadar-' . $data['Ta']['nim'] . '.pdf', 'I');
?>
