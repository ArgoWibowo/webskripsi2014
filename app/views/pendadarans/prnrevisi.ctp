<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/proposalpdf');

$tcpdf = new PROPOSALPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Form Bukti Revisi Skripsi - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(100);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(20, 30, 30, 30);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',10);

if (!empty($data)) {
    $saatini = date('Y-m-d H:i:s');
    $tgldadar = date('Y-m-d H:i:s', strtotime($data['Pendadaran']['jadwal']));
    $nmfile = 'revisi-' . $data['Ta']['nim'] . '.pdf';
    $judulta = nl2br($data['Ta']['judul']);
    
	$html = 
'<table cellpadding="2" style="font-size: 1.0em;" width="100%" border="0">
<tr><td colspan="2">
<div style="text-align: center;"><h2>FORMULIR PERBAIKAN (REVISI) SKRIPSI</h2>
<small>Dicetak tanggal: '. date('d-m-Y H:i:s') .'</small></div>
</td></tr>

<tr><td colspan="2">
<br/><br/><br/><br/>Yang bertanda tangan di bawah ini:
<p><table>
<tr style="line-height: 200%;"><td style="width:200px;">Nama</td><td style="width:15px;">:</td><td style="width:750px;">' . $mhs['Mahasiswa']['nama'] . '</td></tr>
<tr style="line-height: 200%;"><td>N I M</td><td>:</td><td>' . $mhs['Mahasiswa']['nim'] . '</td></tr>
<tr style="line-height: 200%;"><td>Judul Skripsi</td><td>:</td><td>' . strtoupper($judulta) . '</td></tr>
<tr style="line-height: 200%;"><td>Tanggal Pendadaran</td><td>:</td><td>' . $this->Tools->prnDate($tgldadar, 4) . ' WIB</td></tr>
</table></p>
</td></tr>

<tr><td colspan="2">
<p style="line-height: 200%;">Telah melakukan perbaikan tugas akhir dengan lengkap.</p>
<p>&nbsp;</p>
<p>Demikian pernyataan kami agar dapat dipergunakan sebagaimana mestinya.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</td></tr>

<tr><td></td><td><div style="text-align:center;">
Yogyakarta, ' .  $this->Tools->prnDate($saatini) . '<br/><br/>
</div></td></tr>

<tr style="font-size: 10pt;"><td style="padding: 4px;"><div style="text-align:center;"><br/><br/>
Dosen Pembimbing I<br/><br/><br/><br/>
<div>'. $data['Dosen']['nama_dosen'] . '</div>
</div></td><td><div style="text-align:center;">
<br/><br/>Dosen Pembimbing II<br/><br/><br/><br/>
<div>'. $data['Dosen2']['nama_dosen'] . '</div>
</div></td></tr>
</table>';

} else {
	$html = '<p>&nbsp;</p><h2>Status pendadaran Anda belum ada yang sudah lulus!</h2>';
	$nmfile = 'revisi-UNK.pdf';
}
$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->Line(20,30, 190, 30);
$tcpdf->lastPage();

$tcpdf->Output($nmfile, 'I');
?>
