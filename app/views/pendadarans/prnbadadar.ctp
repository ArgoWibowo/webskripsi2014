<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/badadar');

$tcpdf = new BADADAR("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Berita Acara Pendadaran - Skripsi Sistem Informasi UKDW");
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(10);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(20, 20, 20, true);
$tcpdf->setFooterMargin(5);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, 10);
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'', 10);

if (!empty($data)) {
	$teks = $this->Html->image('/app/webroot/img/box.png');
	$rumus = $this->Html->image('/app/webroot/img/rumusdadar.png');
	$judulta = nl2br($data['Ta']['judul']);

	// untuk PHP < 5.3
	$tgldadar = date('Y-m-d H:i:s', strtotime($data['Pendadaran']['jadwal']));
	$tglkumpul = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($data['Pendadaran']['jadwal']))); 
	$tglkumpul = $this->Tools->cekDate($tglkumpul);
	
	$ketua = "";
	$penguji1 = "";
	$penguji2 = "";
	$penguji3 = "";
	if ($data['Pendadaran']['ketua'] == 1) {
		$ketua = trim($data['Dosen']['nama_dosen']);
		$penguji1 = trim($data['Dosen2']['nama_dosen']);
		$penguji2 = trim($data['Dosen3']['nama_dosen']);
		$penguji3 = trim($data['Dosen4']['nama_dosen']);
	} elseif ($data['Pendadaran']['ketua'] == 2) {
		$ketua = trim($data['Dosen2']['nama_dosen']);
		$penguji1 = trim($data['Dosen']['nama_dosen']);
		$penguji2 = trim($data['Dosen3']['nama_dosen']);
		$penguji3 = trim($data['Dosen4']['nama_dosen']);
	} elseif ($data['Pendadaran']['ketua'] == 3) {
		$ketua = trim($data['Dosen3']['nama_dosen']);
		$penguji1 = trim($data['Dosen']['nama_dosen']);
		$penguji2 = trim($data['Dosen2']['nama_dosen']);
		$penguji3 = trim($data['Dosen4']['nama_dosen']);
	} elseif ($data['Pendadaran']['ketua'] == 4) {
		$ketua = trim($data['Dosen4']['nama_dosen']);
		$penguji1 = trim($data['Dosen']['nama_dosen']);
		$penguji2 = trim($data['Dosen2']['nama_dosen']);
		$penguji3 = trim($data['Dosen3']['nama_dosen']);
	}
	
// Halaman kedua: BERITA ACARA PENDADARAN #1
$html = '<p>&nbsp;</p>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.25em;">BERITA ACARA UJIAN SKRIPSI</span><br/>
	<span><small>(Diisi oleh Ketua Tim Penguji)</small></span>
</div>
Pada hari ini : <strong>'. $this->Tools->prnDate($data['Pendadaran']['jadwal']) .'</strong><br/>Telah dilakukan Ujian Skripsi untuk mahasiswa tersebut dibawah ini :
<p><table cellpadding="2">
<tr><td style="width: 220px;">Nama Mahasiswa</td><td style="width:20px;">:</td><td style="width: 700px;"><strong>'. $mhs['Mahasiswa']['nama'] . 
'</strong></td></tr>
<tr><td>No. Induk Mahasiswa</td><td>:</td><td><strong>'. $data['Ta']['nim'] . '</strong></td></tr>
<tr><td>Judul Skripsi</td><td>:</td><td>'. strtoupper($judulta) . '<br/></td></tr>
<tr><td>Dosen Pembimbing I</td><td>:</td><td><strong>'. trim($data['Dosen']['nama_dosen']) . '</strong></td></tr>
<tr><td>Dosen Pembimbing II</td><td>:</td><td><strong>'. trim($data['Dosen2']['nama_dosen']) . '</strong></td></tr>
<tr>
  <td style="font-size:1.1em;"><strong>N I L A I</strong><br/><small>(Lingkari yang dipilih)</small></td><td style="font-size:1.1em;">:</td>
  <td style="font-size:1.2em;">
	<table cellpadding="5" cellspacing="3">
		<tr style="text-align:center; font-weight:bold; line-height:150%;">
			<td style="border:1px solid #000; width:75px;">A</td>
			<td style="border:1px solid #000; width:75px;">A-</td>
			<td style="border:1px solid #000; width:75px;">B+</td>
			<td style="border:1px solid #000; width:75px;">B</td>
			<td style="border:1px solid #000; width:75px;">B-</td>
			<td style="border:1px solid #000; width:75px;">C+</td>
			<td style="border:1px solid #000; width:75px;">C</td>
			<td style="border:1px solid #000; width:75px;">D</td>
		</tr>
	</table>
  </td>
</tr>
<tr>
  <td style="font-size:1.1em;"><strong>K e t e r a n g a n</strong><br/><small>(Coret salah satu)</small></td><td style="font-size:1.1em;">:</td>
  <td style="font-size:1.1em; line-height: 150%;"><strong>L U L U S&nbsp;&nbsp;/&nbsp;&nbsp;TIDAK LULUS</strong></td>
</tr>
<tr>
  <td style="font-size:1.1em;" colspan="3">
	<table cellpadding="5">
		<tr><td style="border:1px solid #000; width:70px; font-weight: bold; text-align: center; background-color: black; color: #fff;">No.</td>
		<td style="border:1px solid #000; width:850px; font-weight: bold; text-align: center; background-color: black; color: #fff;">CATATAN PERBAIKAN</td></tr>
		<tr><td style="border:1px solid #000; line-height: 90px;"></td><td style="border:1px solid #000;"></td></tr>
	</table>
	<div style="font-size:0.9em; line-height: 150%;">Perubahan di atas harus sudah diselesaikan <strong>paling lambat tanggal ' . $this->Tools->prnDate($tglkumpul) . '</strong></div>
  </td>
</tr>
</table></p>
<table cellpadding="6">
<tr><td colspan="5" style="font-size:1.2em; line-height: 150%;">Dewan Penguji Skripsi :</td></tr>
<tr style="line-height: 150%;"><td style="width:30px;">1.</td><td style="width: 450px; border-bottom: 1px solid #4d4d4d;">'. $ketua . '</td><td style="width:20px;">&nbsp;</td><td style="width:200px;">1.</td><td style="width:100px;"></td></tr>
<tr style="line-height: 150%;"><td style="width:30px;">2.</td><td style="width: 450px; border-bottom: 1px solid #4d4d4d;">' . $penguji1 . '</td><td style="width:20px;">&nbsp;</td><td style="width:200px;"></td><td style="width:100px;">2.</td></tr>
<tr style="line-height: 150%;"><td style="width:30px;">3.</td><td style="width: 450px; border-bottom: 1px solid #4d4d4d;">' . $penguji2 . '</td><td style="width:20px;">&nbsp;</td><td style="width:200px;">3.</td><td style="width:100px;"></td></tr>
<tr style="line-height: 150%;"><td style="width:30px;">4.</td><td style="width: 450px; border-bottom: 1px solid #4d4d4d;">' . $penguji3 . '</td><td style="width:20px;">&nbsp;</td><td style="width:200px;"></td><td style="width:100px;">4.</td></tr>
</table>
<table><tr><td style="width: 400px; text-align:center;"><p>&nbsp;</p>
&nbsp;<br/>Mahasiswa yang diuji,<p>&nbsp;</p>
<strong>('. trim($mhs['Mahasiswa']['nama']) .')</strong></td><td style="text-align: center;"><p>&nbsp;</p>
Yogyakarta, ' . $this->Tools->prnDate($data['Pendadaran']['jadwal'], 2) . '<br/>Ketua Tim Penguji<p>&nbsp;</p>
<strong>(' . $ketua . ')</strong>
</td></tr></table>
<p>&nbsp;</p>
<table style="font-size:0.8em;"><tr><td style="width: 90px;"><strong>Catatan:</strong></td><td>
1 (satu) lembar untuk mahasiswa<br/>
1 (satu) lembar untuk arsip
</td></tr></table>';
$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->Line(20,28, 190, 28);
$tcpdf->SetXY(170, 25);
$tcpdf->SetFillColor(255);
$tcpdf->Cell(20, 5, 'F23.09.001', 1, 2, 'C', 1, '', 0);
$tcpdf->lastPage();
$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->Line(20,28, 190, 28);
$tcpdf->SetXY(170, 25);
$tcpdf->SetFillColor(255);
$tcpdf->Cell(20, 5, 'F23.09.001', 1, 2, 'C', 1, '', 0);
$tcpdf->lastPage();

// Halaman #3: FORM PENILAIAN PENDADARAN AKHIR
//
$html = '<p>&nbsp;</p>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.25em;">REKAP PENILAIAN UJIAN SKRIPSI</span><br/>
	<span><small>(Diisi oleh Ketua Tim Penguji)</small></span>
</div>
<p><table cellpadding="5">
<tr>
	<td style="width: 220px;">Tanggal</td>
	<td style="width:20px;">:</td>
	<td style="width: 700px;"><strong>'. $this->Tools->prnDate($data['Pendadaran']['jadwal']) . '</strong></td>
</tr>
<tr>
	<td style="width: 220px;">Nama Mahasiswa</td>
	<td style="width:20px;">:</td>
	<td style="width: 700px;"><strong>'. $mhs['Mahasiswa']['nama'] . '</strong></td>
</tr>
<tr><td>No. Induk Mahasiswa</td><td>:</td><td><strong>'. $data['Ta']['nim'] . '</strong></td></tr>
<tr><td>Judul Skripsi</td><td>:</td><td>'. strtoupper($judulta) . '</td></tr>
<tr><td>Dosen Pembimbing I</td><td>:</td><td><strong>'. $data['Dosen']['nama_dosen'] . '</strong></td></tr>
<tr><td>Dosen Pembimbing II</td><td>:</td><td><strong>'. $data['Dosen2']['nama_dosen'] . '</strong></td></tr>
<tr><td>Ujian Ke<br/><small>(Lingkari yang dipilih)</small></td><td>:</td><td><strong>1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</strong></td></tr>
</table></p>
<table cellpadding="4" style="width:950px;">
<tr><td style="width: 550px;">
<p><table cellpadding="3" style="border-collapse:collapse;">
<tr><td style="width: 200px;">&nbsp;</td><td style="line-height: 150%; width:150px; border: 1px solid #4d4d4d; background-color: #000; color: #fff; text-align:center;">SKRIPSI *)</td><td style="line-height: 150%; width:150px; text-align:center; border: 1px solid #4d4d4d; background-color: #000; color: #fff;">PENDADARAN</td></tr>
<tr><td style="line-height: 150%; border: 1px solid #4d4d4d;">Ketua Tim Penguji</td><td style="border: 1px solid #4d4d4d;">&nbsp;</td><td style="border: 1px solid #4d4d4d;">&nbsp;</td></tr>
<tr><td style="line-height: 150%; border: 1px solid #4d4d4d;">Anggota I</td><td style="border: 1px solid #4d4d4d;">&nbsp;</td><td style="border: 1px solid #4d4d4d;">&nbsp;</td></tr>
<tr><td style="line-height: 150%; border: 1px solid #4d4d4d;">Anggota II</td><td style="border: 1px solid #4d4d4d;">&nbsp;</td><td style="border: 1px solid #4d4d4d;">&nbsp;</td></tr>
<tr><td style="line-height: 150%; border: 1px solid #4d4d4d;">Anggota III</td><td style="border: 1px solid #4d4d4d;">&nbsp;</td><td style="border: 1px solid #4d4d4d;">&nbsp;</td></tr>
<tr><td style="line-height: 150%; border: 1px solid #4d4d4d;"><strong>Nilai Akhir (NA)</strong></td><td style="border: 1px solid #4d4d4d;" colspan="2">&nbsp;</td></tr>
</table></p>
<p style="font-size: 0.9em; width: 80%;"><strong>Catatan:</strong><br/>
Bilamana didapati perbedaan menyolok didalam penilaian antara Para Dosen Penguji Skripsi, Ketua Tim Penguji WAJIB untuk melakukan klarifikasi terhadap perbedaan nilai tersebut.
<br/><br/><small>*) Nilai Skripsi hanya dari Dosen Pembimbing</small></p>
</td><td style="width: 400px;" >
<div style="padding-left: 20px;"><p style="text-align:center;">';
$html = $html . $rumus;
$html = $html . '</p>
<p style="font-size:0.8em;">
<strong>Keterangan :</strong><br/>
<table>
<tr><td style="width: 140px;">85.0 &lt;&#61; NA &lt;&#61; 100</td><td>&ndash;&raquo;  A</td></tr>
<tr><td>80.0 &lt;&#61; NA &lt; 85.0</td><td>&ndash;&raquo;  A-</td></tr>
<tr><td>75.0 &lt;&#61; NA &lt; 80.0</td><td>&ndash;&raquo;  B+</td></tr>
<tr><td>70.0 &lt;&#61; NA &lt; 75.0</td><td>&ndash;&raquo;  B</td></tr>
<tr><td>65.0 &lt;&#61; NA &lt; 70.0</td><td>&ndash;&raquo;  B-</td></tr>
<tr><td>60.0 &lt;&#61; NA &lt; 65.0</td><td>&ndash;&raquo;  C+</td></tr>
<tr><td>55.0 &lt;&#61; NA &lt; 60.0</td><td>&ndash;&raquo;  C</td></tr>
<tr><td>NA &lt; 55.0</td><td>&ndash;&raquo;  D  ( Tidak Lulus )</td></tr>
</table><br/>
NA = Nilai Akhir<br/>
Si = Nilai Skripsi dari Pembimbing<br/>
Pi = Nilai Pendadaran dari Pembimbing <br/>
m = Jml. Dosen Pembimbing Skripsi<br/>
k = Jml. Dosen Penguji Skripsi
</p></div>
</td></tr>
</table>
<table>
<tr>
  <td style="font-size:1.1em; width: 250px;"><strong>Equivalent Nilai Akhir</strong><br/><small>(Lingkari yang dipilih)</small></td>
  <td style="font-size:1.2em; width:700px;">:
	<table cellpadding="5" cellspacing="3">
		<tr style="text-align:center; font-weight:bold; line-height:150%;">
			<td style="border:1px solid #000; width:75px;">A</td>
			<td style="border:1px solid #000; width:75px;">A-</td>
			<td style="border:1px solid #000; width:75px;">B+</td>
			<td style="border:1px solid #000; width:75px;">B</td>
			<td style="border:1px solid #000; width:75px;">B-</td>
			<td style="border:1px solid #000; width:75px;">C+</td>
			<td style="border:1px solid #000; width:75px;">C</td>
			<td style="border:1px solid #000; width:75px;">D</td>
		</tr>
	</table>
  </td>
</tr>
</table>
<p>Berdasarkan nilai tersebut di atas mahasiswa yang bersangkutan dinyatakan<strong> LULUS / TIDAK LULUS</strong> **)<br/><small>**) Coret yang tidak terpilih</small></p>
<p><table>
<tr><td cellpadding="4" style="width:80px; background-color: #000; color: #fff;">Catatan:</td><td style="width: 825px; border-bottom: 1px solid #4d4d4d;">&nbsp;</td></tr>
<tr><td>&nbsp;</td><td style="line-height: 250%; width: 825px; border-bottom: 1px solid #4d4d4d;">&nbsp;</td></tr>
</table></p>
<p><table><tr><td style="width: 350px;"></td><td style="text-align: center;"><br/>
Yogyakarta, ' . $this->Tools->prnDate($data['Pendadaran']['jadwal'], 2) . '<br/>Ketua Tim Penguji<p>&nbsp;</p>
(<strong>' . $ketua . '</strong>)
</td></tr></table></p>';
$tcpdf->AddPage();
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->Line(20,28, 190, 28);
$tcpdf->SetXY(170, 25);
$tcpdf->SetFillColor(255);
$tcpdf->Cell(20, 5, 'F23.09.002', 1, 2, 'C', 1, '', 0);
$tcpdf->lastPage();

// Halaman #4: FORM PENILAIAN PENDADARAN DARI SETIAP DOSEN PENGUJI
//
$html = '<p>&nbsp;</p>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.25em;">PENILAIAN AKHIR UJIAN SKRIPSI</span><br/>
	<span style="font-size:0.8em">( Diisi oleh Ketua / Anggota Tim Penguji )</span>
</div>
<p><table cellpadding="5">
<tr><td style="width: 220px;">Tanggal</td><td style="width:20px;">:</td><td style="width: 700px;"><strong>'. $this->Tools->prnDate($data['Pendadaran']['jadwal']) . 
'</strong></td></tr>
<tr><td>Nama Mahasiswa</td><td>:</td><td><strong>'. $mhs['Mahasiswa']['nama'] . 
'</strong></td></tr>
<tr><td>No. Induk Mahasiswa</td><td>:</td><td><strong>'. $data['Ta']['nim'] . '</strong></td></tr>
<tr><td>Judul Skripsi</td><td>:</td><td>'. strtoupper($judulta) . '</td></tr>
<tr><td>Dosen Pembimbing I</td><td>:</td><td><strong>'. $data['Dosen']['nama_dosen'] . '</strong></td></tr>
<tr><td>Dosen Pembimbing II</td><td>:</td><td><strong>'. $data['Dosen2']['nama_dosen'] . '</strong></td></tr>
<tr><td>Ujian Ke</td><td>:</td><td><strong>1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*)
</strong></td></tr>
</table></p>
<table cellpadding="6">
<tr><td style="width: 360px;">
<p><table cellpadding="5" style="border-collapse:collapse;">
<tr><td style="line-height: 200%; border: 1px solid #4d4d4d; background-color:#000; color:#fff;"><strong>JENIS</strong></td><td style="line-height: 200%; text-align: center; border: 1px solid #4d4d4d; background-color:#000; color:#fff;"><strong>NILAI</strong></td></tr>
<tr><td style="line-height: 200%; border: 1px solid #4d4d4d;">Skripsi *)</td><td style="border: 1px solid #4d4d4d;"></td></tr>
<tr><td style="line-height: 200%; border: 1px solid #4d4d4d;">Pendadaran</td><td style="border: 1px solid #4d4d4d;"></td></tr>
</table></p>
</td><td><p style="margin-left: 10px; font-size:0.8em;">
<strong>Keterangan :</strong>
<p><table>
<tr><td style="width: 190px;">85.0 &lt;&#61; NA &lt;&#61; 100</td><td>&ndash;&raquo;  A</td></tr>
<tr><td>80.0 &lt;&#61; NA &lt; 85.0</td><td>&ndash;&raquo;  A-</td></tr>
<tr><td>75.0 &lt;&#61; NA &lt; 80.0</td><td>&ndash;&raquo;  B+</td></tr>
<tr><td>70.0 &lt;&#61; NA &lt; 75.0</td><td>&ndash;&raquo;  B</td></tr>
<tr><td>65.0 &lt;&#61; NA &lt; 70.0</td><td>&ndash;&raquo;  B-</td></tr>
<tr><td>60.0 &lt;&#61; NA &lt; 65.0</td><td>&ndash;&raquo;  C+</td></tr>
<tr><td>55.0 &lt;&#61; NA &lt; 60.0</td><td>&ndash;&raquo;  C</td></tr>
<tr><td>NA &lt; 55.0</td><td>&ndash;&raquo;  D  ( Tidak Lulus )</td></tr>
</table></p>
<p>NA = Nilai Akhir</p></p>
</td></tr>
</table>
<span style="font-size:0.8em;"><strong>PENJELASAN :</strong><br/>
<strong>Penilaian Skripsi meliputi :</strong>
<ul><li>Relevansi judul dan isi.</li>
<li>Relevansi antara permasalahan dengan pembahasan dan kesimpulan.</li>
<li>Kedalaman pembahasan.</li>
<li>Kualitas Program.</li>
<li>Penggunaan Tata Bahasa Indonesia dengan baik dan benar.</li>
<li>Tingkat keseriusan mahasiswa dalam proses pembuatan skripsi.</li>
</ul>
<strong>Penilaian Pendadaran meliputi :</strong>
<ul><li>Kemampuan mahasiswa mengungkapkan ide pada saat presentasi.</li>
<li>Kemampuan mahasiswa mempertahankan hasil penelitian.</li>
</ul>
</span>
<table><tr><td style="width: 450px;"></td><td style="text-align: center;">
Yogyakarta, ' . $this->Tools->prnDate($data['Pendadaran']['jadwal'], 2) . '<br/>Anggota Tim Penguji<p>&nbsp;</p>(';

$tcpdf->AddPage();
$html1 = $html . $ketua . ')</td></tr></table>
<p style="font-size:0.8em; list-style-type:none;">
<strong>Catatan:</strong><br/>
*) Nilai Skripsi hanya dari Dosen Pembimbing</p>';
$tcpdf->writeHTML($html1, true, false, true, false, '');
$tcpdf->Line(20,28, 190, 28);
$tcpdf->SetXY(170, 25);
$tcpdf->SetFillColor(255);
$tcpdf->Cell(20, 5, 'F23.09.003', 1, 2, 'C', 1, '', 0);
$tcpdf->lastPage();

$tcpdf->AddPage();
$html2 = $html . $penguji1 . ')</td></tr></table>
<p style="font-size:0.8em; list-style-type:none;">
<strong>Catatan:</strong><br/>
*) Nilai Skripsi hanya dari Dosen Pembimbing</p>';
$tcpdf->writeHTML($html2, true, false, true, false, '');
$tcpdf->Line(20,28, 190, 28);
$tcpdf->SetXY(170, 25);
$tcpdf->SetFillColor(255);
$tcpdf->Cell(20, 5, 'F23.09.003', 1, 2, 'C', 1, '', 0);
$tcpdf->lastPage();

$tcpdf->AddPage();
$html3 = $html . $penguji2 . ')</td></tr></table>
<p style="font-size:0.8em; list-style-type:none;">
<strong>Catatan:</strong><br/>
*) Nilai Skripsi hanya dari Dosen Pembimbing</p>';
$tcpdf->writeHTML($html3, true, false, true, false, '');
$tcpdf->Line(20,28, 190, 28);
$tcpdf->SetXY(170, 25);
$tcpdf->SetFillColor(255);
$tcpdf->Cell(20, 5, 'F23.09.003', 1, 2, 'C', 1, '', 0);
$tcpdf->lastPage();

$tcpdf->AddPage();
$html4 = $html . $penguji3 . ')</td></tr></table>
<p style="font-size:0.8em; list-style-type:none;">
<strong>Catatan:</strong><br/>
*) Nilai Skripsi hanya dari Dosen Pembimbing</p>';
$tcpdf->writeHTML($html4, true, false, true, false, '');
$tcpdf->Line(20,28, 190, 28);
$tcpdf->SetXY(170, 25);
$tcpdf->SetFillColor(255);
$tcpdf->Cell(20, 5, 'F23.09.003', 1, 2, 'C', 1, '', 0);
$tcpdf->lastPage();
} else {
	$html = '<h2>Tidak ada peserta pendadaran untuk ID tersebut!</h2>';
}

$tcpdf->Output('bapendadaran-' . $data['Ta']['nim'] . '.pdf', 'I');
?>
