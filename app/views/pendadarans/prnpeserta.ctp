<header class="heading">
<?php echo $this->Html->image('logoukdw90.png', array('class'=>'logo')); ?>
<div class="prodi"><span style="font-size: 1.00em;">Fakultas Teknologi Informasi Program Studi Sistem Informasi</span><br/>
<span><small>Universitas Kristen Duta Wacana</small></span><br/>
<span><small>Dr Wahidin Sudirahusada 5-25 Yogyakarta - 55224</small></span></div>
</header>
<div style="clear:both;"></div>
<p></p>
<section>
<?php 
if (!empty($dadar)){ 
  $min = date('Y-m-d', strtotime($periode[0][0]['awal']));
  $max = date('Y-m-d', strtotime($periode[0][0]['akhir']));
  if ($min != '1970-01-01') {
	$per = $this->Tools->prnDate($periode[0][0]['awal'], 2);
	if ($min != $max) {
		$per = $per . ' s/d ' . $this->Tools->prnDate($periode[0][0]['akhir'], 2);
	}
  } else {
	$per = "(Belum di setup)";
  }
?>
<div style="text-align:center;"><span style="font-weight: bold; font-size: 1.0em;">Daftar Peserta Pendadaran</span><br/>
<span style="font-size: 0.85em;">Periode Tanggal: <?php echo $per; ?></span></div>
<br/>
<table id="box-report">
<thead>
	<tr>
		<th scope="col" rowspan="2">No.</th>
		<th scope="col" rowspan="2">Waktu Pendadaran</th>
		<th scope="col" rowspan="2"><small>NIM</small></th>
		<th scope="col" rowspan="2"><small>Nama Mahasiswa</small></th>
		<th scope="col" rowspan="2"><small>Judul Tugas Akhir</small></th>
		<th scope="col" colspan="4"><small>Penguji</small></th>
	</tr>
	<tr>
		<th>I</th>
		<th>II</th>
		<th>III</th>
		<th>IV</th>	</tr>
</thead>
<tbody>
<?php 
$i = 0;
foreach($dadar as $d): ?>
	<tr>
		<td style="width: 20px; text-align:center;"><?php echo ++$i; ?></td>
		<td style="width: 120px; text-align:center;"><small><?php echo $this->Tools->prnDate($d['Pendadaran']['jadwal'],4) ?></small></td>
		<td style="width: 60px; text-align:center;"><small><?php echo $d['Ta']['nim'] ?></small></td>
		<td style="width: 200px; text-align:left;"><small><?php echo $d['Ta']['Mahasiswa']['nama'] ?></small></td>
		<td style="width: 400px; text-align:left;"><small><?php echo $d['Ta']['judul'] ?></small></td>
		<td style="width: 110px; font-size: 0.8em; text-align:left;"><small>
			<?php
				if ($d['Pendadaran']['ketua'] == 1) {
					echo '<strong>' . $d['Dosen']['nama_dosen'] . ' *</strong>';
				} else {
					echo $d['Dosen']['nama_dosen'];
				}
			?>
		</small></td>
		<td style="width: 110px; font-size: 0.8em; text-align:left;"><small>
			<?php
				if ($d['Pendadaran']['ketua'] == 2) {
					echo '<strong>' . $d['Dosen2']['nama_dosen'] . ' *</strong>';
				} else {
					echo $d['Dosen2']['nama_dosen'];
				}
			?>
		</small></td>
		<td style="width: 110px; font-size: 0.8em; text-align:left;"><small>
			<?php
				if ($d['Pendadaran']['ketua'] == 3) {
					echo '<strong>' . $d['Dosen3']['nama_dosen'] . ' *</strong>';
				} else {
					echo $d['Dosen3']['nama_dosen'];
				}
			?>
		</small></td>
		<td style="width: 110px; font-size: 0.8em; text-align:left;"><small>
			<?php
				if ($d['Pendadaran']['ketua'] == 4) {
					echo '<strong>' . $d['Dosen4']['nama_dosen'] . ' *</strong>';
				} else {
					echo $d['Dosen4']['nama_dosen'];
				}
			?>
		</small></td>
	</tr>
<?php endforeach; ?>
</tbody>
<tfoot>
	<tr>
		<td colspan="8"><small><strong>*) Ketua Tim Penguji</strong></small></td>
	</tr>
</tfoot>
</table>
<p>&nbsp;</p>
<p style="text-align:center;">
Yogyakarta, <?php echo $this->Tools->prnDate( date('Y-m-d'), 2); ?><br/>
Koordinator Skripsi SI UKDW
<p>&nbsp;</p>
<p style="text-align: center;">(...........................................)</div>
</p>

<?php } else { ?>
<h2>Belum ada peserta pendadaran untuk periode tanggal pengumpulan terakhir tersebut</h2>
<?php } ?>
</section>