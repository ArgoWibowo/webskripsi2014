<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/sampul');

$tcpdf = new SAMPUL("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'times';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Halaman Sampul Skripsi - Skripsi Sistem Informasi UKDW");
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(true);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(40, 30, 30, true);
$tcpdf->SetFooterMargin(40);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(2.0);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',12);

if (!empty($data)) {
    $saatini = date('Y-m-d H:i:s');
    $nmfile = 'sampul-' . $data['Ta']['nim'] . '.pdf';
    $judulta = nl2br($data['Ta']['judul']);

    $dewan = array();
	if ($data['Pendadaran']['ketua'] == 1) {
		$dewan[0] = $data['Dosen']['nama_dosen'];
		$dewan[1] = $data['Dosen2']['nama_dosen'];
		$dewan[2] = $data['Dosen3']['nama_dosen'];
		$dewan[3] = $data['Dosen4']['nama_dosen'];
	} else if ($data['Pendadaran']['ketua'] == 2) {
		$dewan[0] = $data['Dosen2']['nama_dosen'];
		$dewan[1] = $data['Dosen']['nama_dosen'];
		$dewan[2] = $data['Dosen3']['nama_dosen'];
		$dewan[3] = $data['Dosen4']['nama_dosen'];
	} else if ($data['Pendadaran']['ketua'] == 3) {
		$dewan[0] = $data['Dosen3']['nama_dosen'];
		$dewan[1] = $data['Dosen']['nama_dosen'];
		$dewan[2] = $data['Dosen2']['nama_dosen'];
		$dewan[3] = $data['Dosen4']['nama_dosen'];
	} else {
		$dewan[0] = $data['Dosen4']['nama_dosen'];
		$dewan[1] = $data['Dosen']['nama_dosen'];
		$dewan[2] = $data['Dosen2']['nama_dosen'];
		$dewan[3] = $data['Dosen3']['nama_dosen'];
	}
    
	if ($data['Pendadaran']['nilai4'] <= 0) {
		$dewan[1] = '';
	} else if ($data['Pendadaran']['nilai5'] <= 0) {
		$dewan[2] = '';
	} else if ($data['Pendadaran']['nilai6'] <= 0) {
		$dewan[3] = '';
	}

    $teks = 
'<p style="text-align: center;"><strong>' . strtoupper($judulta) . '</strong></p>
<p>&nbsp;</p>
<p style="text-align: center;">Skripsi</p>
<p>&nbsp;</p><p>&nbsp;</p>
<p>&nbsp;</p><p style="text-align: center;">
';
	$teks = $teks . $this->Html->image('/app/webroot/img/logoukdw_sampul.png', array('style' => 'text-align: center;'));
	$teks = $teks . '</p><p>&nbsp;</p><p style="text-align: center;">
oleh<br/><strong>' . $mhs['Mahasiswa']['nama'] . '<br/>' . $mhs['Mahasiswa']['nim'] . '</strong>
</p>';
	
	$tcpdf->AddPage();
	$tcpdf->writeHTML($teks, true, false, true, false, '');
	$tcpdf->lastPage();
	
    $teks = 
'<p style="text-align: center;"><strong>' . strtoupper($judulta) . '</strong></p>
<p>&nbsp;</p>
<p style="text-align: center;">Skripsi</p>
<p>&nbsp;</p><p>&nbsp;</p>
<p>&nbsp;</p><p style="text-align: center;">
';
	$teks = $teks . $this->Html->image('/app/webroot/img/logoukdw_sampul.png');
	$teks = $teks . '</p><p>&nbsp;</p><p style="text-align: center;">
Diajukan kepada Program Studi Sistem Informasi Fakultas Teknologi Informasi  <br/>
Universitas Kristen Duta Wacana<br/>
Sebagai Salah Satu Syarat dalam Memperoleh Gelar<br/>
Sarjana Komputer<br/><br/><br/>
Disusun oleh<br/><br/><strong>' . $mhs['Mahasiswa']['nama'] . '<br/>' . $mhs['Mahasiswa']['nim'] . '</strong>
</p>';
	
	$tcpdf->AddPage();
	$tcpdf->writeHTML($teks, true, false, true, false, '');
	$tcpdf->lastPage();
	
    $teks = '
<p style="text-align: center;"><strong>PERNYATAAN KEASLIAN SKRIPSI</strong></p>
<p>&nbsp;</p>
<p style="text-align: justify;">Saya menyatakan dengan sesungguhnya bahwa skripsi dengan judul:</p>
<p style="text-align: center;"><strong>' . $judulta . '</strong></p>
<p style="text-align: justify;">yang saya kerjakan untuk melengkapi sebagian persyaratan menjadi Sarjana Komputer pada 
pendidikan Sarjana Program Studi Sistem Informasi Fakultas Teknologi Informasi 
Universitas Kristen Duta Wacana, bukan merupakan tiruan atau duplikasi dari skripsi kesarjanaan di 
lingkungan Universitas Kristen Duta Wacana maupun di Perguruan Tinggi atau instansi manapun, 
kecuali bagian yang sumber informasinya dicantumkan sebagaimana mestinya.</p>

<p style="text-align: justify;">
Jika dikemudian hari didapati bahwa hasil skripsi ini adalah hasil plagiasi atau tiruan dari skripsi lain, 
saya bersedia dikenai sanksi yakni pencabutan gelar kesarjanaan saya.
</p>
<p>&nbsp;</p>
<p>
<table>
<tr><td>&nbsp;</td><td style="text-align: center;">
Yogyakarta, ' . $this->Tools->prnDate($saatini, 2) . '<br/><br/><br/><br/><br/>
<div style="border-bottom:1px solid #000;">'. $mhs['Mahasiswa']['nama'] . '</div>'. $data['Ta']['nim'].'
</td></tr>
</table>
</p>
';
	
	$tcpdf->AddPage();
	$tcpdf->writeHTML($teks, true, false, true, false, '');
	$tcpdf->lastPage();
	$tcpdf->setPrintFooter(false);
	
	$time = strtotime($data['Jwdpddr']['batas']);
	$arrtime = getdate($time);
	
	if ($arrtime["mon"] >= 2 && $arrtime["mon"] <= 8) {
		$semester = 'Genap';
		$tahun = ($arrtime['year'] - 1) . '/' . $arrtime['year'];
	} else if ($arrtime["mon"] == 1) {
		$semester = 'Gasal';
		$tahun = ($arrtime['year'] - 1) . '/' . $arrtime['year'];
	} else {
		$semester = 'Gasal';
		$tahun = $arrtime['year'] . '/' . ($arrtime['year'] + 1);
	}
	
	if ($data['Pendadaran']['jadwal'] != '0000-00-00 00:00:00') {
		$tgldadar = $this->Tools->prnDate(date('Y-m-d H:i:s', strtotime($data['Pendadaran']['jadwal'])), 2);
	} else {
		$tgldadar = "....";
	}
	
	$teks = 
'<table cellpadding="2" style="font-size: 11pt;" width="100%" border="0">
<tr><td colspan="2">
<div style="text-align: center; font-size: 12pt;"><strong>HALAMAN PERSETUJUAN</strong></div>
<p>&nbsp;</p>
</td></tr>

<tr><td colspan="2">
<p><table>
<tr style="line-height: 200%;"><td style="width:200px;">Judul Skripsi</td><td style="width:15px;">:</td><td style="width:500px;">' . $judulta . '</td></tr>
<tr style="line-height: 200%;"><td>Nama Mahasiswa</td><td>:</td><td>' . $mhs['Mahasiswa']['nama'] . '</td></tr>
<tr style="line-height: 200%;"><td>N I M</td><td>:</td><td>' . $mhs['Mahasiswa']['nim'] . '</td></tr>
<tr style="line-height: 200%;"><td>Matakuliah</td><td>:</td><td>Skripsi</td></tr>
<tr style="line-height: 200%;"><td>Kode</td><td>:</td><td>SI4046</td></tr>
<tr style="line-height: 200%;"><td>Semester</td><td>:</td><td>' . $semester . '</td></tr>
<tr style="line-height: 200%;"><td>Tahun Akademik</td><td>:</td><td>' . $tahun . '</td></tr>
</table></p>
</td></tr>

<tr><td>&nbsp;</td><td><p>&nbsp;</p><p>&nbsp;</p><div style="text-align:center;">
Telah diperiksa dan disetujui di Yogyakarta,<br/>Pada tanggal ' .  $this->Tools->prnDate($saatini, 2) . '
<p>&nbsp;</p><p>&nbsp;</p>
</div></td></tr>

<tr style="font-size: 10pt;"><td style="padding: 4px;"><div style="text-align:center;"><br/><br/>
Dosen Pembimbing I<br/><br/><br/><br/>
<div style="font-size:0.8em;">'. trim($data['Dosen']['nama_dosen']) . '</div>
</div></td><td><div style="text-align:center;">
<br/><br/>Dosen Pembimbing II<br/><br/><br/><br/>
<div style="font-size:0.8em;">'. trim($data['Dosen2']['nama_dosen']) . '</div>
</div></td></tr>
</table>';
	$tcpdf->AddPage();
	$tcpdf->writeHTML($teks, true, false, true, false, '');
	$tcpdf->lastPage();
	
	$teks = 
'<table cellpadding="2" style="font-size: 11pt;" width="100%" border="0">
<tr>
<td colspan="2">
  <div style="text-align: center; font-size: 12pt;">
  	<strong>HALAMAN PENGESAHAN</strong>
  </div>
  <p>&nbsp;</p>
  <p style="text-align: center;"><strong>' . strtoupper($judulta) . '</strong></p>
<p style="text-align: center;">Oleh: ' . $mhs['Mahasiswa']['nama'] . ' / ' . $mhs['Mahasiswa']['nim']. '</p>
<p>&nbsp;</p>
<p style="text-align: center; font-size: 11pt;">
Dipertahankan di depan Dewan Penguji Skripsi<br/>
Program Studi Sistem Informasi Fakultas Teknologi Informasi<br/>
Universitas Kristen Duta Wacana - Yogyakarta<br/>
Dan dinyatakan diterima untuk memenuhi salah satu syarat memperoleh gelar<br/>
Sarjana Komputer<br/>
pada tanggal<br/>' . $tgldadar . '
</p>
</td></tr>

<tr><td>&nbsp;</td><td><p>&nbsp;</p><div style="text-align:center;">
Yogyakarta, ' .  $this->Tools->prnDate($saatini, 2) . '
<br/>Mengesahkan,<br/>
</div></td></tr>

<tr><td colspan="2" style="font-size: 10pt;">
Dewan Penguji:
<p><table style="font-size: 10pt;">';
	$bariske = 0;
	for($k=0; $k < sizeof($dewan); $k++) {
		if ($dewan[$k] <> '') {
			$bariske++;
			if ($bariske % 2 != 0 ) {
				$teks = $teks . '<tr style="line-height: 200%;"><td style="width: 20px;">' . $bariske . '.</td><td style="width:420px;">' . trim($dewan[$k]) . 
				'</td><td style="width:10px;">&nbsp;</td><td style="width:180px; border-bottom: 1px dotted #c0c0c0;">&nbsp;</td>'.
				'<td style="width:180px;">&nbsp;</td></tr>';
			} else {
				$teks = $teks . '<tr style="line-height: 200%;"><td style="width: 20px;">' . $bariske . '.</td><td style="width:420px;">' . trim($dewan[$k]) . 
				'</td><td style="width:10px;">&nbsp;</td><td style="width:180px;">&nbsp;</td>'.
				'<td style="width:180px; border-bottom: 1px dotted #c0c0c0;">&nbsp;</td></tr>';
			}
		}
	}
	$teks = $teks . '</table></p><br/>
</td></tr>

<tr style="font-size: 10pt;">
<td style="padding: 0 10px; text-align:center;"><br/><br/>
Dekan<br/><br/><br/>
<div style="text-decoration: underline;">(' . $data['Pendadaran']['dekan'] . ')</div>
</td>
<td style="padding: 0 4px; text-align:center;"><br/><br/>
Ketua Program Studi<br/><br/><br/>
<div style="text-decoration: underline;">(' . $data['Pendadaran']['kaprodi'] . ')</div>
</td>
</tr>
</table>';
} else {
	$teks = '<p>&nbsp;</p><h2>Status pendadaran Anda belum ada yang aktif atau belum ditentukan dosen pembimbing untuk Anda!</h2>';
	$nmfile = 'sampul-UNK.pdf';
}
$tcpdf->AddPage();
$tcpdf->writeHTML($teks, true, false, true, false, '');
$tcpdf->lastPage();

$tcpdf->Output($nmfile, 'I');
?>
