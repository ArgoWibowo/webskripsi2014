<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/proposalpdf');

$tcpdf = new PROPOSALPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Bukti Tanda Terima Berkas Pendadaran - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(false);
$tcpdf->SetTopMargin(30);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',10);

if (!empty($data)) {
	$judulta = nl2br($data['Ta']['judul']);

	$html = '<p>&nbsp;</p>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.25em;">LEMBAR BUKTI PENERIMAAN BERKAS PENDADARAN</span><br/>
	<span style="font-size:0.8em">Dicetak tanggal: '. date('d-m-Y H:i:s') . '</span>
</div>
<p>Melalui lembar bukti ini menyatakan bahwa mahasiswa yang disebutkan di bawah ini: </p>
<table cellpadding="5">
<tr><td style="width: 250px;">Nama Mahasiswa</td><td style="width:25px;">:</td><td style="width: 630px;"><strong>'. $mhs['Mahasiswa']['nama'] . 
'</strong></td></tr>
<tr><td>No. Induk Mahasiswa</td><td>:</td><td><strong>'. $data['Ta']['nim'] . '</strong></td></tr>
<tr><td>Judul Tugas Akhir</td><td>:</td><td>'. strtoupper($judulta) . '</td></tr>
<tr><td>Dosen Pembimbing I</td><td>:</td><td><strong>'. $data['Dosen']['nama_dosen']. '</strong></td></tr>
<tr><td>Dosen Pembimbing II</td><td>:</td><td><strong>'. $data['Dosen2']['nama_dosen'] . '</strong></td></tr>
</table>
<p>&nbsp;</p>
telah menyerahkan berkas materi skripsi untuk diproses pada pendadaran selanjutnya.
<p>Yogyakarta, ' . $this->Tools->prnDate(date('d-m-Y'), 2) . 
'</p><p>&nbsp;</p>
Petugas';
} else {
	$html = '<h2>Belum ada peserta kolokium untuk tanggal</h2>';
}
$tcpdf->AddPage();
// output the HTML content
$tcpdf->writeHTML($html, true, false, true, false, '');
$tcpdf->lastPage();

$tcpdf->Output('buktikumpuldadar-' . $data['Ta']['nim'] . '.pdf', 'I');
?>
