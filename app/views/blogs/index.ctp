<h3>News Management</h3>
<div>	
	<?php 
	echo $this->Html->link($this->Html->image("add.png", array("alt" => "Add new news.")), '/blogs/add', array('escape' => false, 'title' => 'Add new News.'));
	
	if (isset( $posts ) && is_array( $posts )): ?>
	<table>
		<?php echo $this->Html->tableHeaders(array('Title', 'Last modified', 'Published', 'Action')); ?>
		<?php if (sizeof( $posts ) == 0) : ?>
		<tr>
			<td colspan="6">There are no news right now.</td>
		</tr>
		<?php else: ?>
		<?php foreach ($posts as $post) : ?>
		<tr>			
			<td><?php echo $post['Blog']['title']; ?></td>
			<td><?php echo date('D, d M Y h:i:s a', strtotime($post['Blog']['modified'])); ?></td>
			<td class="actions"><?php echo $this->Html->link(($post['Blog']['published'] == 1? 'Published' : 'Unpublished'),
				'/blogs/'. ($post['Blog']['published'] == 1? 'disable' : 'enable').'/'.$post['Blog']['id']); ?></td>
			
			<td class="actions"><?php echo $this->Html->link('Edit', '/blogs/edit/'.$post['Blog']['id']); ?> | 			
			 <?php echo $this->Html->link('Delete', '/blogs/delete/'.$post['Blog']['id'], array('class' => 'confirmLink') ); ?></td>
		</tr>
		
		<?php endforeach; ?>
		<?php endif; ?>
	</table>
	<?php endif; ?>
	
	<p>	<?php echo $this->Paginator->counter('Page {:page} of {:pages}');?>	</p>
	<div class="paging">
		<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>
		<?php echo $this->Paginator->numbers(); ?>
		<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled'));?>
	</div>
</div>
<div id="dialog" title="Confirmation Required">
  Are you sure about this?
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $("#dialog").dialog({
      autoOpen: false,
      modal: true
    });
  });

  $(".confirmLink").click(function(e) {
    e.preventDefault();
    var targetUrl = $(this).attr("href");

    $("#dialog").dialog({
      buttons : {
        "Confirm" : function() {
          window.location.href = targetUrl;
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $("#dialog").dialog("open");
  });
</script>