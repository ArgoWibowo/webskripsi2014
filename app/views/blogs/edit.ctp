<?php //echo $this->Html->script(array('ckeditor/ckeditor.js'));?>
<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home'), array('class'=>'button', 'escape' => false)); ?>
<?php
	echo $this->Form->create('Blog', array('action' => 'edit'));
	echo $this->Form->input('Blog.id', array('type' => 'hidden', 'value' => $data['Blog']['id']));
	echo $this->Form->error('Blog.title');
	echo $this->Form->input('Blog.title', array('id' => 'posttitle', 'label' => 'Title: ', 'error' => false, 'value' => $data['Blog']['title'], 'style'=> 'width: 100%;'));
	echo $this->Form->error('Blog.content');
	echo $this->Form->input('Blog.content', array('id'=> 'postcontent', 'type' => 'textarea',  'value' => $data['Blog']['content'], 'style'=> 'width: 100%;', 'class' => 'ckeditor', 'label' => 'Content: ', 'rows' => '10', 'error' => false));
	echo $this->Form->input('Blog.introtext', array('id'=> 'postintrotext', 'type' => 'textarea',  'value' => $data['Blog']['introtext'], 'style'=> 'width: 100%;', 'label' => 'Intro text: ', 'rows' => '5', 'error' => false));
	echo $this->Form->end( array('label' => 'Submit Post') ); 
?>
<script type="text/javascript">
	//<![CDATA[
	CKEDITOR.replace( 'postcontent',
	{
		//contentsCss : 'assets/output_xhtml.css',
		extraPlugins : 'tableresize'
	});
	//]]>
</script>	