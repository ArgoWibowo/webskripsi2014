<section id="content" class="body">
<?php echo $this->Html->link('<span><< Home</span>', array('controller'=>'main', 'action'=>'index'), array('class'=>'button', 'escape' => false)); ?>
<p></p>
<?php if (!isset($smtp_errors)): ?>
<div class="pesan_regis"><p>Terima kasih Anda telah mendaftar di Skripsi SI UKDW. Untuk melanjutkan proses pembuatan akun Anda, silahkan cek inbox email Anda dan verifikasi registrasi Anda dengan mengunjungi url yang telah dikirimkan ke email Anda.</p>
<p></p>
<p></p>
<p>Admin Skripsi SI UKDW</p>
</div>
<?php else: ?>
<div class="error_message"><?php echo $smtp_errors; ?></div>
<?php endif; ?>
</section>
