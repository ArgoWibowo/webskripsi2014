<section id="content" class="body">
<?php echo $this->Html->link('<span><< Home</span>', array('controller'=>'main', 'action'=>'index'), array('class'=>'button', 'escape' => false)); ?>
<p></p>
<?php
echo '<div class="pesan_regis">';
echo $msg;
echo '</div>';
echo '<p>&nbsp;</p>';
if (isset($isError) && $isError):
	if ($isValid):
		echo $this->Form->create(null, array('url' => array('controller'=>'registers', 'action'=>'reconfirm')));
		echo $this->Form->input('User.email', array('label' => 'email', 'style' => 'width: 500px;'));
		//echo $this->Form->error('recaptcha_response_field');
		echo $this->Recaptcha->show('white');
		echo $this->Recaptcha->error();
		echo $this->Form->end(array('label' => 'Resend notification', 'id' => 'submit'));
	endif;
endif;
?>
</section>