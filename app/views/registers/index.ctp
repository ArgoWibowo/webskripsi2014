<section id="content" class="body">
<?php echo $this->Html->link('<span>Batal dan kembali ke menu</span>', array('controller'=>'main', 'action'=>'index'), array('class'=>'button', 'escape' => false)); ?>

<h3>Pendaftaran Peserta Skripsi SI UKDW</h3>
<p style="font-size: 1.25em;">Isikanlah ke dalam form di bawah ini data Anda dengan benar. Pastikan alamat email dan nomor telepon yang Anda masukkan dapat dihubungi oleh sistem dan atau petugas. <strong>Password Anda akan tersimpan dalam bentuk tersandikan.</strong></p>
<?php
echo $this->Form->create('Register', array('action' => 'regis', 'type'=>'file'));
echo $this->Form->input('group_id', array('type' => 'hidden', 'value' => '2'));
if (isset($data) && sizeof($data) > 0):
	echo $this->Form->input('User.username', array('label' => 'Username (5-15 karakter)', 'value' => $data['User']['username'], 'style' => 'width: 300px;'));
else:
	echo $this->Form->input('User.username', array('label' => 'Username (5-15 karakter)', 'value' => '', 'style' => 'width: 300px;'));
endif;
if (isset($data) && sizeof($data) > 0):
	echo $this->Form->input('User.nim', array('label' => 'NIM/NIDN', 'id' => 'nim', 'value' => $data['User']['nim'], 'style' => 'width: 300px;'));
else:
	echo $this->Form->input('User.nim', array('label' => 'NIM/NIDN', 'id' => 'nim', 'value' => '', 'style' => 'width: 300px;'));
endif;
//if (isset($data) && sizeof($data) > 0):
//	echo $this->Form->input('User.fullname', array('label' => 'Full Name', 'id' => 'nmmhs', 'value' => $data['User']['fullname'], 'readonly' => 'readonly'));
//else:
echo $this->Form->input('User.fullname', array('label' => 'Full Name', 'id' => 'nmmhs', 'value' => '', 'readonly' => 'readonly'));
//endif;
echo ('<div class="ceknim" style="left: 325px; top: -100px;">');
echo $this->Html->link('<span>Cek NIM/NIDN</span>', '#', array('id' => 'ceknim_lnk', 'class'=>'button', 'style'=>'display:inline; vertical-align:middle;', 'escape' => false));
echo ('<div id="errnmmhs" style="display: inline; vertical-align:middle; font-size:1.2em; color: red;"></div>');
echo ('</div>');

echo $this->Form->input('User.password', array('label' => 'Password (min. 8 karakter):', 'id' => 'pwd', 'value' => '', 'style' => 'width: 300px;'));
echo $this->Form->input('User.password2', array('label' => 'Re-type password:', 'id' => 'pwd2', 'type'=>'password', 'value' => '', 'style' => 'width: 300px;'));
if (isset($data) && sizeof($data) > 0):
	echo $this->Form->input('User.email', array('label' => 'email', 'value' => $data['User']['email'], 'style' => 'width: 500px;'));
	echo $this->Form->input('User.telpno', array('label' => 'Telp no.', 'value' => $data['User']['telpno'], 'style' => 'width: 300px;'));
else:
	echo $this->Form->input('User.email', array('label' => 'email', 'style' => 'width: 500px;'));
	echo $this->Form->input('User.telpno', array('label' => 'Telp no.', 'style' => 'width: 300px;'));
endif;
echo '<p>Anda dapat upload foto profil Anda dengan ukuran maksimum 140 x 140 pixel.</p>';
echo $this->Form->input('User.foto', array('label' => 'File foto:', 'type'=>'file'));
/*echo $this->Form->input('User.dir', array('type' => 'hidden'));
echo $this->Form->input('User.mimetype', array('type' => 'hidden'));
echo $this->Form->input('User.filesize', array('type' => 'hidden'));*/
// echo $this->Form->error('User.recaptcha_response_field');
// echo $this->Recaptcha->show();
// echo $this->Recaptcha->error();

echo $this->Form->end(array('label' => 'Daftar', 'id' => 'submit'));
?>
</section>
<style>
	span.error {
		margin-left: 10px;
		color: #ffffff;
		background-color: #ff0000;
		padding: 2px 6px;
	}
</style>
<script type="text/javascript">
jQuery(function(){
	$('#ceknim_lnk').click(function(event){
		$('#nmmhs').val('');
		$.getJSON('<?php echo($this->Html->url(array("controller"=>"registers", "action"=>"ceknim"))); ?>/' + $('#nim').val(), 
			  function(data){
			  		if (data.Status.return != 0) {
						$('#nim').val('');
						$('#nmmhs').val('');
						$('#errnmmhs').text(data.Status.msg);
						$('#nmmhs').css('color', 'red');
					} else {
						$('#nmmhs').val(data.Status.msg);
						$('#nmmhs').css('color', 'green');
					}
			  });
	});

 	$("#submit").click(function(){
        $(".error").hide();
        var hasError = false;
        var passwordVal = $("#pwd").val();
        var checkVal = $("#pwd2").val();
        if (passwordVal == '') {
            $("#pwd").after('<span class="error">Please type your password.</span>');
            hasError = true;
        } else if (checkVal == '') {
            $("#pwd2").after('<span class="error">Please re-type your password.</span>');
            hasError = true;
        } else if (passwordVal != checkVal ) {
            $("#pwd2").after('<span class="error">Sorry, your password are not exactly same.</span>');
            hasError = true;
        }
        if(hasError == true) {return false;}
    });
});
</script>
