<section id="content" class="body">
<?php echo $this->Html->link('<span><< Home</span>', array('controller'=>'main', 'action'=>'index'), array('class'=>'button', 'escape' => false)); ?>
<p>&nbsp;</p>
<?php
echo '<div class="pesan_regis">';
echo 'Masukkan email Anda yang pernah didaftarkan!';
echo '</div>';
echo '<p>&nbsp;</p>';
echo $this->Form->create(null, array('url' => array('controller'=>'registers', 'action'=>'lostpasswd')));
echo $this->Form->input('User.email', array('label' => 'email', 'style' => 'width: 500px;'));
echo $this->Recaptcha->show('white');
echo $this->Recaptcha->error();
echo $this->Form->end(array('label' => 'Reset Password', 'id' => 'submit'));
?>
</section>