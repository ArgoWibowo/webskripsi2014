<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->element('metas'); ?>
		<link href='http://fonts.googleapis.com/css?family=Asul' rel='stylesheet' type='text/css'>
		<?php echo $this->Html->css(array('scripti.css', 'fluid_grid.css', 'form.cake.generic.css', 'jquery-ui-1.8.16.custom.css')); ?>
		<?php echo $this->Html->script(array('jquery-1.6.4.min.js', 'jquery-ui-1.8.16.custom.min.js')); ?>
		<style type="text/css">
			body { background-color: #fff; font-family: arial,helvetica; font-size:0.75em;}
			.heading { padding-top: 10px; background-color:#fff; }
			.heading .logo { float: right; padding-right: 20px; padding-left: 20px; padding-bottom: 20px;}
			.heading .prodi { padding-top: 30px; font-family: arial,helvetica; text-align: right;}
			.footing { font-family: arial,helvetica; }
			h3 { font-family: 'Asul', cursive; Arial, serif; margin-top: 0px; font-size: 28px;}
			.pesan_regis { font-size:1.2em; padding: 20px; text-align: center; background-color: #FFF8E7;border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; }
			.pengumuman .news-list { list-style: none; margin: 0; padding-left: 0;}
			.pengumuman .news-list .entry-title { font-size: 1.2em; margin-bottom: 0;}
			.pengumuman .news-list footer { font-size: 0.8em; margin-bottom: 10px;}
			.pengumuman .news-list article { padding-bottom: 10px; border-bottom: 1px black dotted; }
			.paginating { padding-top: 40px; }
			.searchbox  { display: block; width: 500px; float: right; text-align: right;}
		</style>
		<script language="javascript" type="text/javascript">
			$(document).ready(function() {
					if ( $('#flashMessage').text() != '') {
						setTimeout(function() {
								$('#flashMessage').slideUp(400);
							}, 3000);
					};
				});
		</script>
	</head>
	<body>
		<?php echo $this->element('header'); ?>
		<div class="clear">&nbsp;</div>
		<div class="container_3">
			<?php echo $this->Html->link('<span>Kembali ke Home</span>', array('controller'=>'main', 'action'=>'index'), array('class'=>'button', 'escape' => false)); ?>
			<p>&nbsp;</p>
			<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>
		</div>
		<footer id="bawah" class="footing container_3">
			<div class="grid_3" style="padding-top: 30px;">&copy;2012 Sistem Informasi UKDW Yogyakarta</div>
		</footer>
	</body>
</html>
