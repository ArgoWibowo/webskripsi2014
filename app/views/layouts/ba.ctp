<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->element('metas', array('judul' => $judul) ); ?>
		<link href='http://fonts.googleapis.com/css?family=Asul' rel='stylesheet' type='text/css'/>
		<style type="text/css">
			@font-face {
				font-family: Calibri;
				src: url("<?php echo $this->Html->url('/fonts/calibri.eot'); ?>");
				src: url("<?php echo $this->Html->url('/fonts/calibri.ttf'); ?>") format('truetype');
			}
			body { background-color: #fff; font-family: calibri,arial,helvetica; font-size: 10pt;}
			.heading { font-family: calibri,arial,helvetica; padding-top: 10px; background-color:#fff; }
			.heading .logo { float: left; padding-right: 20px; padding-left: 20px; }
			.heading .prodi { padding-top: 4px; text-align: left;}
			#box-report { border: 0.75px solid #c0c0c0; border-collapse: collapse; }
			#box-report td, #box-report th {border: 0.75px solid #c0c0c0; padding: 4px;}
		</style>
	</head>
	<body>
		<?php echo $content_for_layout; ?>
		<footer id="bawah" class="footing">
			<div style="padding-top: 20px;"><small>&copy;2012 Sistem Informasi UKDW Yogyakarta</small>
			<!-- span style="float: right;"><small>##PAGE## dari ##PAGES##</small></span -->
			</div>
		</footer>
	</body>
</html>
