<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->element('metas', array('judul' => $judul)); ?>
		<link href='http://fonts.googleapis.com/css?family=Asul' rel='stylesheet' type='text/css'>
		<?php echo $this->Html->css(array('fluid_grid.css', 'form.cake.generic.css')); ?>
		<?php echo $this->Html->script(array('jquery-1.6.4.min.js')); ?>
		<style type="text/css">
			body { background-color: #fff; font-family: arial,helvetica; font-size:0.75em;}
			.heading { padding-top: 10px; background-color:#fff; }
			.heading .logo { float: right; padding-right: 20px; padding-left: 20px; padding-bottom: 20px;}
			.heading .prodi { padding-top: 30px; font-family: arial,helvetica; text-align: right;}
			.news .imgfront { float: left; padding: 10px; }
			.loginarea { background-color:#D7EEF4; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px;}
			.loginarea form { padding: 5px; } 
			.footing div { font-family: arial,helvetica; text-align: right; margin-top: 20px;}
			.news h3 { background: url(<?php echo $this->Html->url('/img/newsicon.png'); ?>) no-repeat 0 0; display: block; height: 48px; padding-left: 60px; font-family: 'Asul', cursive; Arial, serif; margin-top: 0px; font-size: 28px;}
			.news .news-list { list-style: none; margin: 0; padding-left: 0;}
			.news .news-list .entry-title { font-size: 1.2em; margin-bottom: 0; color:#FF6600;}
			.news .news-list footer { font-size: 0.8em; margin-bottom: 10px;}
			.news .news-list article { padding-bottom: 10px; border-bottom: 1px black dotted; }
		</style>
		<script language="javascript" type="text/javascript">
			$(document).ready(function() {
				if ( $('#flashMessage').text() != '') {
					setTimeout(function() {
							$('#flashMessage').slideUp(400);
						}, 3000);
				};
				$('#infojadwal > div').hide();
				$('#infojadwal > h4').click(function(){
					$('#infojadwal > div').slideToggle('slow');
				});
			});
		</script>
	</head>
	<body>
		<?php echo $this->element('header'); ?>
		<div class="clear">&nbsp;</div>
		<div class="container_3">
            <div class='news grid_2'>
                <h3>Pengumuman</h3>
                <ul class='news-list'>
                	<?php foreach($news as $post): ?>
                	<li>
                		<article>
						<header><h4 class="entry-title"><?php echo $post['Blog']['title']; ?></h4></header>
						<footer><?php echo date('D, d M Y h:i:s a', strtotime($post['Blog']['modified'])); ?></footer>
						<div class="news-content"><?php echo $post['Blog']['content']; ?></div>
						</article>
                	</li>
                	<?php endforeach; ?>
                </ul>
            </div>
            <div class='loginarea grid_1'>
<?php
	echo $this->Session->flash();
	echo $this->Form->create('Login', array('url' => array('controller' => 'main', 'action' => 'login')));
	echo $this->Form->input('username', array('label' => 'Username:', 'style' => 'width:200px;'));
	echo $this->Form->input('password', array('label' => 'Password:', 'style' => 'width:200px;'));
	//echo $this->Form->error('User.recaptcha_response_field');
	//echo $this->Recaptcha->show('white');
	//echo $this->Recaptcha->error();
	echo $this->Form->end(array('label' => 'Login', 'id' => 'submit'));
	echo '<div style="text-align: center; padding: 0 10px">';
	echo $this->Html->link('Pendaftaran Akun Baru', array('controller' => 'registers', 'action' => 'index') );
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link('Lupa password?', array('controller' => 'registers', 'action' => 'lostpasswd') );
	//echo '&nbsp;&nbsp;&nbsp;';
	//echo $this->Html->link('Reconfirm', array('controller' => 'registers', 'action' => 'notification', 'reconfirm') );
	echo '</div>';
?>
	<div>
		<?php echo $this->Html->image('doc.png', array('style'=> 'vertical-align:middle; padding: 12px 0 8px 8px;') ); ?>
		<?php echo $this->Html->link('Template Format Jurnal Skripsi', '/files/template_jurnal_jutei.docx', array('style' => 'padding: 4px; text-decoration: none; font-size: 1.2em; color:#FF6600;') ); ?>
	</div>
				<div id="infojadwal" style="padding: 10px 5px 0;">
				<h4 style="cursor: pointer; text-decoration: underline; font-size: 1.3em; margin-top: 10px; margin-bottom: 0; padding-left: 5px; padding-bottom: 10px; color: #FF6600;">
					<?php echo $this->Html->image("cal.png", array('style'=> 'vertical-align: middle;') ); ?>
					Jadwal Kolokium
				</h4>
				<div><table>
				<thead style="font-weight: bold;"><tr><td style="width: 100px;">Pelaksanaan</td><td>Batas Akhir Pengumpulan</td></tr></thead>
				<?php foreach($jadwals as $j): ?>
				<tr><td style="width: 150px;"><?php echo $this->Tools->prnDate(date('Y-m-d H:i:s', strtotime($j['Jadwal']['tanggal']))); ?></td><td><?php echo $this->Tools->prnDate(date('Y-m-d H:i:s', strtotime($j['Jadwal']['batas'])), 3); ?> WIB</td></tr>
				<?php endforeach; ?>
				</table></div>
				</div>
            </div>
		</div>
		<footer id="bawah" class="footing container_3">
			<div class="grid_3">&copy;2012 Sistem Informasi UKDW Yogyakarta</div>
<?php //debug($this->Session->read('Message.email')); ?>
		</footer>
	</body>
</html>
