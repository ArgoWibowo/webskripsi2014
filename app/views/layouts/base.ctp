<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->element('metas'); ?>
		<?php echo $this->Html->css(array('scripti.css', 'fluid_grid.css', 'form.cake.generic.css', 'jquery-ui-1.8.16.custom.css', 'demo_table_jui.css', 'jquery.multiselect.css', 'style.css', 'prettify.css')); ?>
		<?php echo $this->Html->script(array('jquery-1.6.4.min.js', 'jquery-ui-1.8.16.custom.min.js', 'jquery.dataTables.min.js', 'jquery-ui-timepicker-addon.js', 'prettify.js', 'jquery.multiselect.js', 'jquery.form.js')); ?>
		<?php 
			echo $this->Html->script(array('ckeditor/ckeditor.js'));
		?>
		<style type="text/css">
			body { background-color: #fff; font-family: arial,helvetica; font-size:0.75em;}
			.heading { padding-top: 10px; background-color:#fff; }
			.heading .logo { float: right; padding-right: 20px; padding-left: 20px; padding-bottom: 20px;}
			.heading .prodi { padding-top: 30px; font-family: arial,helvetica; text-align: right;}
			.footing { font-family: arial,helvetica; }
			h3 { font-family: Arial, serif; margin-top: 0px; font-size: 28px;}
			.pesan_regis { font-size:1.2em; padding: 20px; text-align: center; background-color: #FFF8E7;border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; }
			.pengumuman .news-list { list-style: none; margin: 0; padding-left: 0;}
			.pengumuman .news-list .entry-title { font-size: 1.2em; margin-bottom: 0;}
			.pengumuman .news-list footer { font-size: 0.8em; margin-bottom: 10px;}
			.pengumuman .news-list article { padding-bottom: 10px; border-bottom: 1px black dotted; }
			.paginating { padding-top: 40px; }
			.searchbox  { display: block; width: 550px; float: right; text-align: right;}
			.link-kol-box { display: block; width: 800px; float: left; }
			.isi { font-size: 0.85em; }
			.profil { list-style: none;  }
			.profil fieldset {display: block; margin:0; width: 600px; padding: 5px;}
			.profil fieldset label { display: block; float: left; text-align: right; width: 120px;}
			.profil fieldset span { display: block; float: right; text-align: left; width: 460px; background-color: #BFAFAE; padding: 4px; font-weight: bold;}
			form label { font-size: 1.5em; }
			tr.sorot:hover{ background-color: #D7EEF4;}
			.dataTables_filter > label > input { width: 200px; }
			.icon-actions { background: url(<?php echo ($this->Html->url('/img/opicon24.png')); ?>); width: 24px; height: 24px; float: left; }
			.icon-valid { background: url(<?php echo ($this->Html->url('/img/newvalid.png')); ?>); width: 24px; height: 24px; float: left; }
			.icon-collapse { background: url(<?php echo ($this->Html->url('/img/arrow.png')); ?>); width: 16px; height: 16px; float: left;}
			#baform label, #baform legend { font-size: 1.0em; }
		</style>
		<script language="javascript" type="text/javascript">
			$(document).ready(function() {
					$( "#tabs" ).tabs();
					$('#tabs ul li a').click(function () {location.hash = $(this).attr('href');});
					if ( $('#flashMessage').text() != '') {
						setTimeout(function() {
								$('#flashMessage').slideUp(400);
							}, 3000);
					};
				});
		</script>
	</head>
	<body>
		<?php echo $this->element('header'); ?>
		<div class="clear">&nbsp;</div>
		<div class="container_3 isi">
			<p style="font-size: 1.25em;">Selamat datang, <span style="font-weight: bold;"><?php echo $this->Session->read('User.fullname'); ?></span></p>
			<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>
		</div>
		<footer id="bawah" class="footing container_3">
			<div class="grid_3" style="padding-top: 30px;">&copy;2012 Sistem Informasi UKDW Yogyakarta</div>
		</footer>
	</body>
</html>
