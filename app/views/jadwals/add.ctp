<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-8'), array('class'=>'button', 'escape' => false)); ?>
<h3>Tambah Jadwal Kolokium</h3>
<?php
	echo $this->Form->create('Jadwal', array('action' => 'add'));
	echo $this->Form->input('Jadwal.tanggal', array('id' => 'tanggal', 'label' => 'Tanggal Kolokium: ', 'dateFormat' => 'DMY'));
	echo $this->Form->input('Jadwal.batas', array('id' => 'batas', 'label' => 'Batas Akhir Pendaftaran: ', 'dateFormat' => 'DMY'));
	echo $this->Form->input('Jadwal.tglkumpul', array('id' => 'status', 'label' => 'Batas Akhir Pengumpulkan Koreksi: ', 'dateFormat' => 'DMY'));
	echo $this->Form->input('Jadwal.status', array('id' => 'status', 'type' => 'hidden', 'value' => 'N'));
	echo $this->Form->end( array('label' => 'Save') ); 
?>