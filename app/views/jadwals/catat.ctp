<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-8'), array('class'=>'button', 'escape' => false)); ?>
<p>&nbsp;</p>
<h3>Dosen Pencatat Kolokium</h3>
<?php
echo $this->Form->create('Jadwal', array('action' => 'simpancatat'));
echo $this->Form->input('Jadwal.idjwd', array('id' => 'jadwal_id', 'type' => 'hidden', 'value' => $data['Jadwal']['id']));
$tgl = $data['Jadwal']['expired'];
if ($data['Jadwal']['expired'] == 0) {
	$tgl = $data['Jadwal']['tanggal'] . ' 17:00:00';
}
echo $this->Form->input('Jadwal.expired', array('label' => 'Waktu Expired hak akses dosen pencatat:', 'value' => $tgl, 'dateFormat' => 'DMY'));

$i = 0;
$selected = array();
echo '<p style="font-weight:bold;">Daftar Pembagian Dosen Penguji Kolokium:</p>';
echo '<table style="width:500px;">';
foreach($data['Dosen'] as $d) {
	array_push($selected, $d['id']);
	echo '<tr><td style="width:300px;">';
	echo $this->Form->input('Dosen.Dosen.'.$i.'.dosen_id', 
			array('label' => $d['nama_dosen'], 'type'=>'checkbox', 'value' => $d['id'], 'checked'=>true));
	echo '</td><td style="width:100px;">';
	echo $this->Form->input('Dosen.Dosen.'.$i.'.kelompok',
			array('label' => false, 'type'=>'select', 
				  'options'=>array(0=>'(none)', 1=>'Kelompok A',2=>'Kelompok B',3=>'Kelompok C'),
				  'default' => $d['JadwalsDosens']['kelompok']));
	echo '</td></tr>';
	$i++;
}
foreach($dosens as $d) {
	if (!in_array($d['Dosen']['id'], $selected)) {
		echo '<tr><td>';
		echo $this->Form->input('Dosen.Dosen.'.$i.'.dosen_id', 
				array('label' => $d['Dosen']['nama_dosen'], 'type'=>'checkbox', 'value' => $d['Dosen']['id']));
		echo '</td><td>';
		echo $this->Form->input('Dosen.Dosen.'.$i.'.kelompok',
				array('label' => false, 'type'=>'select', 
					  'options'=>array(0=>'(none)', 1=>'Kelompok A',2=>'Kelompok B',3=>'Kelompok C'),
					  'default' => 0));
		echo '</td></tr>';
		$i++;
	}
}
echo '</table>';

/*echo $this->Form->input('Dosen.Dosen', array('type'=>'select', 'multiple'=>'checkbox', 'options'=> $dosens, 'selected' => $selected, 'style'=>'width: 400px;', 'label' => 'Pilih Dosen yang akan mencatat kolokium:'));*/
echo $this->Form->end( array('label' => 'Save') );
?>