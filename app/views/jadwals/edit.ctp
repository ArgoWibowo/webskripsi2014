<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-8'), array('class'=>'button', 'escape' => false)); ?>
<h3>Ubah Jadwal Kolokium</h3>
<?php
	echo $this->Form->create('Jadwal', array('action' => 'edit'));
	echo $this->Form->input('Jadwal.id', array('id' => 'id', 'type' => 'hidden', 'value' => $data['Jadwal']['id']));
	echo $this->Form->input('Jadwal.tanggal', array('id' => 'tanggal', 'value'=> $data['Jadwal']['tanggal'], 'label' => 'Tanggal Kolokium: ', 'dateFormat' => 'DMY'));
	echo $this->Form->input('Jadwal.batas', array('id' => 'batas', 'value'=> $data['Jadwal']['batas'], 'label' => 'Batas Akhir Pendaftaran: ', 'dateFormat' => 'DMY'));
	echo $this->Form->input('Jadwal.tglkumpul', array('id' => 'tglkumpul', 'value'=> $data['Jadwal']['tglkumpul'], 'label' => 'Batas Akhir Pengumpulan Perbaikan: ', 'dateFormat' => 'DMY'));	
	echo $this->Form->end( array('label' => 'Save') ); 
?>