Halo <?php echo $fullname ?>,

Berikut adalah alamat tautan verifikasi akun Anda di Skripsi SI UKDW.
Silahkan aktifasi akun Anda dengan cara klik alamat tautan berikut:

http://skripsi.ukdw.ac.id/registers/notification/<?php echo $hash; ?>

Terima kasih atas partisipasi Anda.

Salam,


Admin Skripsi SI UKDW