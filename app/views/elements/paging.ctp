<?php
if (!isset($paginator->params['paging'])) {
  return;
}
if (!isset($model) || $paginator->params['paging'][$model]['pageCount'] < 2) {
  return;
}
if (!isset($options)) {
  $options = array();
}

$options['model'] = $model;
if (isset($opsi)) {
	foreach(array_keys($opsi) as $op) {
		$options['url'][$op] = $opsi[$op];
	}
/*if (isset($m) && isset($o) && isset($q)) {
	$options['url']['m'] = $m;
	$options['url']['q'] = $q;
	$options['url']['o'] = $o;
}*/
}
$options['url']['model'] = $model . '#' . $tabname;

$paginator->__defaultModel = $model;
?>
<div class="paging">
  <?php
  echo $paginator->prev('<< Previous', array_merge(array('escape' => false, 'class' => 'prev'), $options), null, array('class' => 'disabled'));
  echo $paginator->numbers(am($options, array('before' => false, 'after' => false, 'separator' => false)));
  echo $paginator->next('Next >>', array_merge(array('escape' => false, 'class' => 'next'), $options), null, array('class' => 'disabled'));
  ?>
</div>