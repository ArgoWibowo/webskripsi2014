<?php 
//debug($data);
//debug($kolokium);
//debug($tas);
//debug($pendadaran);
?>
<header>
	<h1>History Skripsi Mahasiswa</h1>
</header>
<section>
	<div style="font-size: 14pt;">
		<?php if (!empty($mhs)) {?>
		<strong>NIM</strong>: <?php echo $mhs['Mahasiswa']['nim'] ?> <br/>
		<strong>NAMA MAHASISWA</strong>: <?php echo $mhs['Mahasiswa']['nama'] ?>	
		<?php } ?>
	</div>
	<ul id="atap"> 
		<li style="background-color:#EBF8A4; padding: 5px; border-radius: 15px;-moz-border-radius: 15px;"><span style="cursor:pointer; font-size:14pt;">[DESK EVALUATION]</span>
			<ul>
				<?php
				if (!empty($data)) {
					foreach($data as $d) { ?>
						<li style="font-size: 1.2em;">
							<?php if (!empty($d['De'])) { ?>
							<span style="font-weight: bold;"><?php echo $d['Proposal']['judul'] ?></span> (Waktu Desk Evaluation:  <?php echo $this->Tools->prnDate($d['De'][0]['tanggal']) ?>)
							<?php } else {?>
							<span style="font-weight: bold;"><?php echo $d['Proposal']['judul'] ?></span> (Waktu Pengumpulan Terakhir:  <?php echo $this->Tools->prnDate($d['Jwddeskevaluation']['batasakhir']) ?>)
							<?php } ?>
						<?php
						$kolo = $kolokium[$d['Proposal']['id']];
						if (!empty($kolo)) {
							echo '<ul class="kolo"><li style="background-color:#FCEAAB; padding:5px; border-radius: 15px;-moz-border-radius: 15px;"><span style="cursor:pointer; font-size: 14pt;">[KOLOKIUM]</span>';
							echo '<ul class="kolokium">';
							foreach($kolo as $k) { ?>
								<li><strong>Tanggal Kolokium:</strong> <?php echo $this->Tools->prnDate($k['Jadwal']['tanggal']); ?> <br/><strong>Judul:</strong> <?php echo $k['Kolokium']['judul']; ?><br/><strong>Dosen:</strong> <?php echo $k['Dosen']['nama_dosen']; ?><br/><strong>Status: </strong>
								<?php if ($k['Kolokium']['status'] = 'L') {
									echo 'Lulus';
								} else {
									echo 'Belum Lulus';
								} ?>
								
								<?php
								$ta = $tas[$k['Kolokium']['id']];
								if (!empty($ta)) {
									echo '<ul class="ta"><li style="background-color:#A5E2F7; padding:5px; border-radius: 15px;-moz-border-radius: 15px;"><span style="cursor:pointer; font-size: 14pt;">[SKRIPSI]</span>';
									echo '<ul>';
									foreach($ta as $t) { ?>
										<li><strong>Judul:</strong> <?php echo $t['Ta']['judul']; ?> <br/>
											<strong>Dosen Pembimbing I: </strong> <?php echo $t['Dosen']['nama_dosen']; ?> <br/><strong>Dosen Pembimbing II:</strong><?php echo $t['Dosen2']['nama_dosen']; ?>
										<?php
										$dadar = $pendadaran[$t['Ta']['id']];
										if (!empty($dadar)) {
											echo '<ul class="dadar"><li style="background-color:#B3D4F9; padding:5px; border-radius: 15px;-moz-border-radius: 15px;"><span style="cursor:pointer; font-size: 14pt;">[PENDADARAN]</span>';
											echo '<ul>';
											foreach($dadar as $r) { ?>
												<li><strong>Tanggal: </strong> <?php echo $this->Tools->prnDate($r['Pendadaran']['jadwal'],4); ?><br/><strong>Dosen Penguji 1:</strong> <?php echo $r['Dosen']['nama_dosen']; ?><br/><strong>Dosen Penguji 2:</strong> <?php echo $r['Dosen2']['nama_dosen']; ?> <br/><strong>Dosen Penguji 3:</strong> <?php echo $r['Dosen3']['nama_dosen']; ?> <br/><strong>Dosen Penguji 4:</strong> <?php echo $r['Dosen4']['nama_dosen']; ?><br/>
												<strong>Status: </strong>
												<?php
												if ($r['Pendadaran']['status'] == 2){
													echo 'Lulus';
												} else {
													echo 'Belum Lulus';
												}
												?>
												, <strong>Nilai:</strong>
												<?php
												$nilai1 = $r['Pendadaran']['nilai1'];
												$nilai2 = $r['Pendadaran']['nilai2'];
												$nilai3 = $r['Pendadaran']['nilai3'];
												$nilai4 = $r['Pendadaran']['nilai4'];
												$nilai5 = $r['Pendadaran']['nilai5'];
												$nilai6 = $r['Pendadaran']['nilai6'];

												$pembagi=4;
												if ($nilai3 <= 0 || $nilai4 <= 0 || $nilai5 <= 0 || $nilai6 <= 0) {
						   							$pembagi = 3;
												}
												$nilaibimbingan = $nilai1 + $nilai2;
												$nilaiujian = $nilai3 + $nilai4 + $nilai5 + $nilai6;
												$nilaiakhir = 0.4 * ($nilaibimbingan/2) + 0.6 * ($nilaiujian/$pembagi);
												echo $nilaiakhir . ' (';
												if ($nilaiakhir >= 85.0){
													echo 'A';
												} else if ($nilaiakhir >= 80){
													echo 'A-';
												} else if ($nilaiakhir >= 75){
													echo 'B+';
												} else if ($nilaiakhir >= 70){
													echo 'B';
												} else if ($nilaiakhir >= 65){
													echo 'B-';
												} else if ($nilaiakhir >= 60){
													echo 'C+';
												} else if ($nilaiakhir >= 55){
													echo 'C';
												} else {
													echo 'D';
												}
												echo ')';
												?>
												</li>

				<?php
											}
											echo '</ul></li></ul>';
										}
										echo '</li>';
									}
									echo '</ul></li></ul>';
								}
								echo '</li>';
							}
							echo '</ul></li></ul>';
						}
						?>
						</li>
					<?php 
					}
				} else { ?>
					<li style="font-size: 1.2em;">[BELUM ADA DATA DESK EVALUATION]
						<ul>
							<li>[BELUM ADA DATA KOLOKIUM]
								<ul class="ta"><li style="background-color:#A5E2F7; padding:5px; border-radius: 15px;-moz-border-radius: 15px;"><span style="cursor:pointer; font-size: 14pt;">[SKRIPSI]</span>
									<ul>
								<?php
								$ta = $tas[0];
								foreach($ta as $t) { ?>
										<li><strong>Judul:</strong> <?php echo $t['Ta']['judul']; ?> <br/>
											<strong>Dosen Pembimbing I: </strong> <?php echo $t['Dosen']['nama_dosen']; ?> <br/><strong>Dosen Pembimbing II:</strong><?php echo $t['Dosen2']['nama_dosen']; ?>		
									<?php $dadar = $pendadaran[$t['Ta']['id']];
									if (!empty($dadar)) { ?>
											<ul class="dadar"><li style="background-color:#B3D4F9; padding:5px; border-radius: 15px;-moz-border-radius: 15px;"><span style="cursor:pointer; font-size: 14pt;">[PENDADARAN]</span>
												<ul>
										<?php foreach($dadar as $r) { ?>
												<li><strong>Tanggal: </strong> <?php echo $this->Tools->prnDate($r['Pendadaran']['jadwal'],4); ?><br/><strong>Dosen Penguji 1:</strong> <?php echo $r['Dosen']['nama_dosen']; ?><br/><strong>Dosen Penguji 2:</strong> <?php echo $r['Dosen2']['nama_dosen']; ?> <br/><strong>Dosen Penguji 3:</strong> <?php echo $r['Dosen3']['nama_dosen']; ?> <br/><strong>Dosen Penguji 4:</strong> <?php echo $r['Dosen4']['nama_dosen']; ?><br/>
												<strong>Status: </strong>
												<?php
												if ($r['Pendadaran']['status'] == 2){
													echo 'Lulus';
												} else {
													echo 'Belum Lulus';
												}
												?>
												, <strong>Nilai:</strong>
												<?php
												$nilai1 = $r['Pendadaran']['nilai1'];
												$nilai2 = $r['Pendadaran']['nilai2'];
												$nilai3 = $r['Pendadaran']['nilai3'];
												$nilai4 = $r['Pendadaran']['nilai4'];
												$nilai5 = $r['Pendadaran']['nilai5'];
												$nilai6 = $r['Pendadaran']['nilai6'];

												$pembagi=4;
												if ($nilai3 <= 0 || $nilai4 <= 0 || $nilai5 <= 0 || $nilai6 <= 0) {
						   							$pembagi = 3;
												}
												$nilaibimbingan = $nilai1 + $nilai2;
												$nilaiujian = $nilai3 + $nilai4 + $nilai5 + $nilai6;
												$nilaiakhir = 0.4 * ($nilaibimbingan/2) + 0.6 * ($nilaiujian/$pembagi);
												echo $nilaiakhir . ' (';
												if ($nilaiakhir >= 85.0){
													echo 'A';
												} else if ($nilaiakhir >= 80){
													echo 'A-';
												} else if ($nilaiakhir >= 75){
													echo 'B+';
												} else if ($nilaiakhir >= 70){
													echo 'B';
												} else if ($nilaiakhir >= 65){
													echo 'B-';
												} else if ($nilaiakhir >= 60){
													echo 'C+';
												} else if ($nilaiakhir >= 55){
													echo 'C';
												} else {
													echo 'D';
												}
												echo ')';
												?>
												</li>
										<?php } ?>
												</ul>
											</li></ul>
									<?php 
									} 
									?>
										</li>
								<?php 
								}
								?>
									</ul>
								</li></ul>
							</li>
						</ul>
					</li>
				<?php } ?>
			</ul>
		</li>
	</ul>
</section>

<script type="text/javascript">
	$('#atap > li > span').bind('click', function(e){
		$('#atap > li > ul').slideToggle('slow');
	});
	$('.kolo > li > span').bind('click', function(e){
		$('.kolo > li > ul').slideToggle('slow');
	});
	$('.ta > li > span').bind('click', function(e){
		$('.ta > li > ul').slideToggle('slow');
	});
	$('.dadar > li > span').bind('click', function(e){
		$('.dadar > li > ul').slideToggle('slow');
	});
</script>