<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-4'), array('class'=>'button', 'escape' => false)); ?>
<h3>Unggah PDF Skripsi</h3>
<?php
	echo $this->Form->create('Turnitin', array('action' => 'add', 'type' => 'file'));
	//echo $this->Form->input('Ta.aktif', array('id' => 'aktif', 'value'=>1, 'type' => 'hidden'));
	echo $this->Form->input('Turnitin.id_ta', array('type' => 'hidden', 'value' => $id_ta));
	echo $this->Form->input('Turnitin.url_turnitin', array('type' => 'hidden', 'value' => ''));
	echo $this->Form->input('Turnitin.url_skripsi', array('label' => 'File PDF Skripsi:', 'between'=>'<br />','type'=>'file'));
    echo $this->Form->end( array('label' => 'Simpan PDF Skripsi') ); 
?>