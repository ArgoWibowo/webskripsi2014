<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-7'), array('class'=>'button', 'escape' => false)); ?>
<?php
	echo $this->Form->create('Dosen', array('action' => 'edit'));
	echo $this->Form->input('Dosen.id', array('type' => 'hidden', 'value' => $data['Dosen']['id']));
	echo $this->Form->input('Dosen.nidn', array('id' => 'nidn', 'label' => 'NIDN: ', 'value' => $data['Dosen']['nidn'], 'style'=> 'width: 20%;'));
	echo $this->Form->input('Dosen.gelar_depan', array('id' => 'gelar', 'label' => 'Gelar Depan Dosen: ', 'value' => $data['Dosen']['gelar_depan'], 'style'=> 'width: 20%;'));
	echo $this->Form->input('Dosen.nama', array('id' => 'nama', 'label' => 'Nama Dosen: ', 'value' => $data['Dosen']['nama'], 'style'=> 'width: 100%;'));
	echo $this->Form->input('Dosen.gelar', array('id' => 'gelar', 'label' => 'Gelar Dosen: ', 'value' => $data['Dosen']['gelar'], 'style'=> 'width: 20%;'));
	echo $this->Form->input('Dosen.email', array('id' => 'email', 'label' => 'Email Dosen: ', 'value' => $data['Dosen']['email'], 'style'=> 'width: 60%;'));
	echo $this->Form->input('Dosen.telpno', array('id' => 'telpno', 'label' => 'No Telepon Dosen: ', 'value' => $data['Dosen']['telpno'], 'style'=> 'width: 20%;'));
	echo $this->Form->input('Dosen.boleh',array(
			'label' => __('Status Membimbing:',true),
			'type' => 'select',
			'options' => array('B' => 'Boleh Membimbing', 'M' => 'Belum Boleh Membimbing'),
			'selected' => $data['Dosen']['boleh'],
		));	
	echo $this->Form->input('Dosen.status',array(
			'label' => __('Skala Pembimbingan:',true),
			'type' => 'select',
			'options' => array('K' => 'Kolokium dan Skripsi', 'S' => 'Skripsi'),
			'selected' => $data['Dosen']['status'],
		));	
	echo $this->Form->end( array('label' => 'Save Changes') ); 
?>