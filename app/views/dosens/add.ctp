<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-7'), array('class'=>'button', 'escape' => false)); ?>
<?php
	echo $this->Form->create('Dosen', array('action' => 'add'));
	echo $this->Form->input('Dosen.nidn', array('id' => 'nidn', 'label' => 'NIDN: ', 'error' => false, 'style'=> 'width: 20%;'));
	echo $this->Form->input('Dosen.gelar_depan', array('id' => 'gelar', 'label' => 'Gelar Depan Dosen: ', 'style'=> 'width: 20%;'));
	echo $this->Form->input('Dosen.nama', array('id' => 'nama', 'label' => 'Nama: ', 'error' => false, 'style'=> 'width: 100%;'));
	echo $this->Form->input('Dosen.gelar', array('id' => 'gelar', 'label' => 'Gelar: ', 'error' => false, 'style'=> 'width: 20%;'));
	echo $this->Form->input('Dosen.email', array('id' => 'email', 'label' => 'Email: ', 'error' => false, 'style'=> 'width: 60%;'));
	echo $this->Form->input('Dosen.telpno', array('id' => 'telpno', 'label' => 'Telpno: ', 'error' => false, 'style'=> 'width: 20%;'));
	echo $this->Form->input('Dosen.boleh',array(
			'label' => __('Status Membimbing:',true),
			'type' => 'select',
			'options' => array('B' => 'Boleh Membimbing', 'M' => 'Belum Boleh Membimbing'),
			'selected' => $this->Html->value('Dosen.boleh'),
		));
	echo $this->Form->input('Dosen.status',array(
			'label' => __('Skala Pembimbingan:',true),
			'type' => 'select',
			'options' => array('K' => 'Kolokium dan Skripsi', 'S' => 'Skripsi'),
			'selected' => $this->Html->value('Dosen.status'),
		));	
	echo $this->Form->end( array('label' => 'Save') ); 
?>