<?php
// created by BUDI SUSANTO
class ToolsHelper extends AppHelper {
	function prnDate($tgl = null, $opsi = null){
		if (!$tgl) {
			return "";
		}
		$time = strtotime($tgl);
		$arrtime = getdate($time);
		$nmhari = '';
		switch($arrtime["wday"]){
			case 0:
				$nmhari = 'Minggu'; break;
			case 1:
				$nmhari = 'Senin'; break;
			case 2:
				$nmhari = 'Selasa'; break;
			case 3:
				$nmhari = 'Rabu'; break;
			case 4:
				$nmhari = 'Kamis'; break;
			case 5:
				$nmhari = 'Jumat'; break;
			case 6:
				$nmhari = 'Sabtu'; break;
		}
		$nmbulan = '';
		switch($arrtime["mon"]){
			case 1:
				$nmbulan = 'Januari'; break;
			case 2:
				$nmbulan = 'Februari'; break;
			case 3:
				$nmbulan = 'Maret'; break;
			case 4:
				$nmbulan = 'April'; break;
			case 5:
				$nmbulan = 'Mei'; break;
			case 6:
				$nmbulan = 'Juni'; break;
			case 7:
				$nmbulan = 'Juli'; break;
			case 8:
				$nmbulan = 'Agustus'; break;
			case 9:
				$nmbulan = 'September'; break;
			case 10:
				$nmbulan = 'Oktober'; break;
			case 11:
				$nmbulan = 'November'; break;
			case 12:
				$nmbulan = 'Desember'; break;
		}
		if ($opsi == 2) {
			return $arrtime["mday"] . ' ' . $nmbulan . ' ' . $arrtime["year"];
		} elseif ($opsi == 3) {
			return $arrtime["mday"] . ' ' . $nmbulan . ' ' . $arrtime["year"]. ' ' . sprintf("%02d", $arrtime['hours']) . ':' . sprintf("%02d", $arrtime['minutes']);
		} elseif ($opsi == 4) {
			return $nmhari . ', ' . $arrtime["mday"] . ' ' . $nmbulan . ' ' . $arrtime["year"]. ' pukul ' . sprintf("%02d", $arrtime['hours']) . ':' . sprintf("%02d", $arrtime['minutes']);
		} else {
			return $nmhari . ', ' . $arrtime["mday"] . ' ' . $nmbulan . ' ' . $arrtime["year"];
		}
	}
	
	function cekDate($tgl){
		if (!$tgl) {
			return "";
		}
		$time = strtotime($tgl);
		$arrtime = getdate($time);
		//$date = new DateTime($tgl);
		if ($arrtime["wday"] == 0) {
			$t = date("Y-m-d H:i:s", strtotime("-2 day", $time));
			// $date->sub(new DateInterval('P2D'));
		} else if ($arrtime["wday"] == 6) {
			$t = date("Y-m-d H:i:s", strtotime("-1 day", $time));
			//$date->sub(new DateInterval('P1D'));
		} else {
			$t = date("Y-m-d H:i:s", $time);
		}
		//return $date->format('Y-m-d H:i:s');
		return $t;
	}
}
?>