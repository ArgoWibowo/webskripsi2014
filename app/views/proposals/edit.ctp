<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-2'), array('class'=>'button', 'escape' => false)); ?>
<p>&nbsp;</p><h3>Ringkasan Proposal</h3>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Identitas</p>
<?php 
echo $this->Form->create('Proposal', array('action' => 'save')); 
echo $this->Form->input( 'Proposal.id', array( 'value' => $data['Proposal']['id'], 'type' => 'hidden') ); 
echo $this->Form->input( 'Proposal.userid', array( 'value' => $this->Session->read('User.id'), 'type' => 'hidden') ); 
?>
<table width="80%">
<tr>
	<td>NIM</td><td style="width:10px;">:</td><td style="font-weight:bold;"><?php echo $this->Session->read('User.nim') ?></td>
	<td stlye="width: 50px;"></td>
	<td>Nama</td><td style="width:10px;">:</td><td style="font-weight: bold;"><?php echo $this->Session->read('User.fullname'); ?></td>
	<td stlye="width: 50px;"></td>
	<td>No. Telepon</td><td style="width:10px;">:</td><td style="font-weight: bold;"><?php echo $this->Session->read('User.telpno'); ?></td>
</tr>
</table>
<table width="80%">
<tr>
	<td><?php echo $this->Form->input('Proposal.konsentrasi',array(
		'label' => __('Konsentrasi:',true),
		'type' => 'select',
		'options' => $topiks,
		'selected' => $data['Proposal']['konsentrasi'],
	)); ?></td>
	<td stlye="width: 50px;"></td>
	<td><?php echo $this->Form->input('Proposal.ipk', array('label' => 'IPK', 'value' => $data['Proposal']['ipk'])); ?></td>
	<td stlye="width: 50px;"></td>
	<td><?php echo $this->Form->input('Proposal.ipro', array('label' => 'Ipro', 'value' => $data['Proposal']['ipro'])); ?></td>
</tr>
<tr>
	<td colspan="3">
		<?php echo $this->Form->input('Proposal.judul', array('label' => 'Judul (tanpa diakhiri tanda titik (.))', 'value' => $data['Proposal']['judul'])); ?>
	</td>
	<td stlye="width: 50px;"></td>
	<td><?php echo $this->Form->input('Proposal.dosen',array(
		'label' => __('Dosen Pengarah:',true),
		'type' => 'select',
		'empty'=>'None',
		'options' => $dosens,
		'default' => $data['Proposal']['dosen'],
	)); ?>
	<div style="padding: 5px 0 0 7px;">Kelanjutan KP:
	<?php 
		echo $this->Form->input('Proposal.lanjutan_kp', array(
										'options' => array(0 => 'Tidak', 1 => 'Ya'),
										'type' => 'radio',
										'legend' => '',
										'div' => array('id' => 'isLanjutKP'),
										'default' => $data['Proposal']['lanjutan_kp']
									));?>
	</div>
	<?php 
	if ($data['Proposal']['dosen_kp']) {
		$dsnkp = $data['Proposal']['dosen_kp'];
	} else {
		$dsnkp = "";
	}
	echo $this->Form->input('Proposal.dosen_kp',array(
		'label' => __('Pembimbing KP:',true),
		'type' => 'select',
		'id'=>'dosenKP',
		'empty'=>'None',
		'options' => $dosens,
		'default' => $dsnkp
	)); ?>
	</td>
</tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Gambaran Skripsi yang akan dibuat</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.gambaran');
	echo $this->Form->input('Proposal.gambaran', array('id'=> 'proposal_gambaran', 'type' => 'textarea', 'value' => $data['Proposal']['gambaran'], 'class' => 'ckeditor', 'label' => "Deskripsi Langkah-langkah", 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Spesifikasi Input-output</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.input_output');
	echo $this->Form->input('Proposal.input_output', array('id'=> 'proposal_input_output', 'value' => $data['Proposal']['input_output'], 'type' => 'textarea', 'class' => 'ckeditor', 'label' => 'Masukan-keluaran sistem:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Pengguna</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.pengguna');
	echo $this->Form->input('Proposal.pengguna', array('id'=> 'proposal_pengguna', 'type' => 'textarea', 'value' => $data['Proposal']['pengguna'], 'class' => 'ckeditor', 'label' => 'Deskripsi Pengguna Sistem:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Metode</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.metode');
	echo $this->Form->input('Proposal.metode', array('id'=> 'proposal_metode', 'type' => 'textarea', 'value' => $data['Proposal']['metode'], 'class' => 'ckeditor', 'label' => 'Deskripsi Metode-metode yang dilakukan dalam penelitian:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Data</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.data');
	echo $this->Form->input('Proposal.data', array('id'=> 'proposal_data', 'type' => 'textarea', 'value' => $data['Proposal']['data'], 'class' => 'ckeditor', 'label' => 'Deskripsi data yang dibutuhkan:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<?php echo $this->Form->end(array('label' => 'Save', 'id' => 'submit')); ?>
<style>
	span.error {
		margin-left: 10px;
		color: #ffffff;
		background-color: #ff0000;
		padding: 2px 6px;
	}
</style>
<script type="text/javascript">
	//<![CDATA[
	CKEDITOR.replace( 'proposal_gambaran',
	{
		//contentsCss : 'assets/output_xhtml.css',
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_input_output',
	{
		//contentsCss : 'assets/output_xhtml.css',
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_pengguna',
	{
		//contentsCss : 'assets/output_xhtml.css',
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_metode',
	{
		//contentsCss : 'assets/output_xhtml.css',
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_data',
	{
		//contentsCss : 'assets/output_xhtml.css',
		toolbar : 'MinimToolbar'
	});

	$(function() {
		<?php if ($data['Proposal']['lanjutan_kp'] <> 0) { ?>
			$('#dosenKP').removeAttr('disabled');
		<?php } else { ?>
			$('#dosenKP').attr('disabled', 'disabled');
		<?php } ?>

		$('input[name="data[Proposal][lanjutan_kp]"]').change(function(){
			var isKP = $(this).val();

			if (isKP == 0) {
				$('#dosenKP').attr("selectedIndex", 0);
				var before= $('#dosenKP').html();
				$('#dosenKP').html("");
				$('#dosenKP').html(before);
				$('#dosenKP').attr('disabled', 'disabled');
			} else {
				$('#dosenKP').removeAttr('disabled');
			}
		});
	});
	//]]>
</script>