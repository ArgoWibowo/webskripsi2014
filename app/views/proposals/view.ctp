<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-2'), array('class'=>'button', 'escape' => false)); ?>
<p>&nbsp;</p><h3>Evaluasi Ringkasan Proposal</h3>
<?php
if (!empty($data)) {
	if ($data['Proposal']['status_proposal'] == 0) {
		$stat = 'Baru';
	} elseif ($data['Proposal']['status_proposal'] == 1) {
		$stat = 'Terkirim';
	} elseif ($data['Proposal']['status_proposal'] == 2) {
		$stat = 'Blm. Diterima';
	} else {
		$stat = 'Diterima';
	}

	$lanjutankp = 'Tidak';
	if ($data['Proposal']['lanjutan_kp'] == 1) {
		$lanjutankp = 'Ya';
	}
	if ($data['De']) {
		$tglde = $this->Tools->prnDate($data['De'][0]['tanggal'],5);
	} else {
		$tglde = '-';
	}

	$teks = '<section>
<p>&nbsp;</p>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.5em;">RINGKASAN PROPOSAL</span><br/>
	<span><small>Dicetak tanggal: '. date('d-m-Y H:i:s') .'</small></span>
</div>
<table>
	<tr><td colspan="7" style="line-height: 150%; font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Identitas</td></tr>
	<tr><td style="width:250px;">NIM</td><td style="width:15px;">:</td><td style="width:350px;">' . $data['User']['nim'] . '</td><td style="width:25px;">&nbsp;</td><td style="width:50px;">IPK</td><td style="width:15px;">:</td><td style="width:150px;">' . $data['Proposal']['ipk'] . '</td></tr>
	<tr><td>Nama</td><td>:</td><td>' . strtoupper($data['User']['fullname']) . '</td><td>&nbsp;</td><td>IPro</td><td>:</td><td>' . $data['Proposal']['ipro'] . '</td></tr>
	<tr><td style="line-height: 200%;">Konsentrasi</td><td>:</td><td>' . strtoupper($data['Topik']['name']) . '</td><td>&nbsp;</td><td>HP</td><td>:</td><td>' . $data['User']['telpno'] . '</td></tr>
	<tr><td style="line-height: 200%;">Judul</td><td>:</td><td colspan="5">' . strtoupper($data['Proposal']['judul']) . '</td></tr>
	<tr><td>Dosen Pengarah (jika ada)</td><td>:</td><td colspan="5">' . strtoupper($data['Dosen']['nama_dosen']) . '</td></tr>
	<tr><td>Apakah Skripsi ini kelanjutan dari KP?</td><td>:</td><td colspan="5">' . $lanjutankp . '</td></tr>
	<tr><td>Pembimbing KP</td><td>:</td><td colspan="5">' . $data['Dosen2']['nama_dosen'] . '</td></tr>
</table>
<br/>
<table cellpadding="5">
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Gambaran Skripsi yang akan dibuat</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['gambaran'] . '</td></tr>
</table>
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Spesifikasi Input-Output</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['input_output'] . '</td></tr>
</table cellpadding="5">
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Pengguna</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['pengguna'] . '</td></tr>
</table>
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Metode</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['metode'] . '</td></tr>
</table>
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Data</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['data'] . '</td></tr>
</table>
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Evaluasi</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['evaluasi'] . '<br/></td></tr>';

if ($this->Session->read('User.group_id')==1 || $this->Session->read('User.group_id')==3 || $this->Session->read('User.group_id')==4) {
	$teks = $teks . 
'<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Catatan Internal</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['catatan'] . '<br/></td></tr>';
}

$teks = $teks . '</table>
</td></tr>
<tr><td style="line-height: 150%;">&nbsp;</td></tr>
<tr><td style="font-weight:bold;  border:1px solid #000;font-size:1.4em; background-color:#eFeFeF;">&nbsp;&nbsp;Status Ringkasan Proposal: ' . $stat . '</td></tr>
</table>
<p>Ringkasan Proposal ini disepakati dalam <i>desk evaluation</i> tanggal: <span><strong>' . $tglde . '</strong></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="width:100%;">
<tr><td style="width:50%; text-align:center; text-decoration: underline;">(Tim <i>Desk Evaluation</i>)</td><td style="width:50%; text-align:center; text-decoration: underline;">&nbsp;</td></tr>
</table>
</section>';
	echo $teks;
} else {
	echo '<h3>Data tidak ditemukan!</h3>';
}
?>