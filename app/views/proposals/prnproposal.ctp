<?php
App::import('Vendor','tcpdf/tcpdf');
App::import('Vendor','tcpdf/proposalpdf');

$tcpdf = new PROPOSALPDF("P", PDF_UNIT, 'A4', true, 'UTF-8', false);
$textfont = 'helvetica';

$tcpdf->SetAuthor("SkripSI UKDW");
$tcpdf->SetTitle("Ringkasan Proposal - Skripsi Sistem Informasi UKDW");
$tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 10));
$tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$tcpdf->setPrintHeader(true);
$tcpdf->setPrintFooter(true);
$tcpdf->SetTopMargin(30);

// set default monospaced font
$tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$tcpdf->SetMargins(20, 30, 20, 20);
$tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',10);

$tcpdf->AddPage();

if (!empty($data)) {
	if ($data['Proposal']['status_proposal'] == 0) {
		$stat = 'Baru';
	} elseif ($data['Proposal']['status_proposal'] == 1) {
		$stat = 'Terkirim';
	} elseif ($data['Proposal']['status_proposal'] == 2) {
		$stat = 'Blm. Diterima';
	} else {
		$stat = 'Diterima';
	}

	$lanjutankp = 'Tidak';
	if ($data['Proposal']['lanjutan_kp'] == 1) {
		$lanjutankp = 'Ya';
	}
	if ($data['De']) {
		$tglde = $this->Tools->prnDate($data['De'][0]['tanggal'],5);
	} else {
		$tglde = '-';
	}
	$teks = '<section>
<p>&nbsp;</p>
<div style="text-align:center;">
	<span style="font-weight: bold; font-size: 1.5em;">RINGKASAN PROPOSAL</span><br/>
	<span><small>Dicetak tanggal: '. date('d-m-Y H:i:s') .'</small></span>
</div>
<table>
	<tr><td colspan="7" style="line-height: 150%; font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Identitas</td></tr>
	<tr><td style="width:250px;">NIM</td><td style="width:15px;">:</td><td style="width:350px;">' . $data['User']['nim'] . '</td><td style="width:25px;">&nbsp;</td><td style="width:50px;">IPK</td><td style="width:15px;">:</td><td style="width:150px;">' . $data['Proposal']['ipk'] . '</td></tr>
	<tr><td>Nama</td><td>:</td><td>' . strtoupper($data['User']['fullname']) . '</td><td>&nbsp;</td><td>IPro</td><td>:</td><td>' . $data['Proposal']['ipro'] . '</td></tr>
	<tr><td style="line-height: 200%;">Konsentrasi</td><td>:</td><td>' . strtoupper($data['Topik']['name']) . '</td><td>&nbsp;</td><td>HP</td><td>:</td><td>' . $data['User']['telpno'] . '</td></tr>
	<tr><td style="line-height: 200%;">Judul</td><td>:</td><td colspan="5">' . strtoupper($data['Proposal']['judul']) . '</td></tr>
	<tr><td>Dosen Pengarah (jika ada)</td><td>:</td><td colspan="5">' . strtoupper($data['Dosen']['nama_dosen']) . '</td></tr>
	<tr><td>Apakah Skripsi ini kelanjutan dari KP?</td><td>:</td><td colspan="5">' . $lanjutankp . '</td></tr>
	<tr><td>Pembimbing KP</td><td>:</td><td colspan="5">' . $data['Dosen2']['nama_dosen'] . '</td></tr>
</table>
<br/>
<table cellpadding="5">
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Gambaran Skripsi yang akan dibuat</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['gambaran'] . '</td></tr>
</table>
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Spesifikasi Input-Output</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['input_output'] . '</td></tr>
</table cellpadding="5">
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Pengguna</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['pengguna'] . '</td></tr>
</table>
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Metode</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['metode'] . '</td></tr>
</table>
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Data</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['data'] . '</td></tr>
</table>
</td></tr>
<tr nobr="true"><td>
<table cellpadding="5">
<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Evaluasi</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['evaluasi'] . '<br/></td></tr>';

/*if ($this->Session->read('User.group_id')==1 || $this->Session->read('User.group_id')==3 || $this->Session->read('User.group_id')==4) {
	$teks = $teks . 
'<tr><td style="font-weight:bold; font-size:1.2em; background-color:#c0c0c0;">&nbsp;&nbsp;Catatan Internal</td></tr>
<tr><td style="line-height: 150%; border:1px solid #000;">' . $data['Proposal']['catatan'] . '<br/></td></tr>';
} */

$teks = $teks . '</table>
</td></tr>
<tr><td style="line-height: 150%;">&nbsp;</td></tr>
<tr><td style="font-weight:bold;  border:1px solid #000;font-size:1.4em; background-color:#eFeFeF;">&nbsp;&nbsp;Status Ringkasan Proposal: ' . $stat . '</td></tr>
</table>
<p>Ringkasan Proposal ini disepakati dalam <i>desk evaluation</i> tanggal: <span><strong>' . $tglde . '</strong></span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="width:100%;">
<tr><td style="width:50%; text-align:center; text-decoration: underline;">(Tim <i>Desk Evaluation</i>)</td><td style="width:50%; text-align:center; text-decoration: underline;">&nbsp;</td></tr>
</table>
</section>';
} else {
	$teks = '<h2>Data tidak ditemukan!</h2>';
}

// output the HTML content
$tcpdf->writeHTML($teks, true, false, true, false, '');
$tcpdf->Output('deskevaluation-' . $data['User']['nim'] . '.pdf', 'I');
?>
