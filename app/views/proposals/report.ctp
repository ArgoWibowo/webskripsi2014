<div style="font-size:1.2em;">
<?php echo $this->element('header_report'); ?>
<div style="clear:both;"></div>
<div style="border-bottom:1px solid #000; width:1080px;"></div><br/>
<section>
<?php if (!empty($data)): ?>
<div style="text-align:left;"><span style="font-weight: bold; font-size: 1.4em;">Daftar Peserta Desk Evaluation</span><br/>
<span style="font-size: 0.85em;">Periode Tanggal: <?php echo $this->Tools->prnDate($data['De']['tanggal']); ?></span></div>
<p></p>
<p>Daftar Dosen Evaluator:
<ul>
<?php 
$dsn = array();
foreach($data['Dosen'] as $d) {
	array_push($dsn, $d['id']);
	echo '<li>' . $d['nidn'] . ' ' . $d['nama_dosen'] . '</li>';
}
?>
</ul>
</p>

<table id="box-report">
<thead>
	<tr>
		<th scope="col">No.</th>
		<th scope="col"><small>NIM</small></th>
		<th scope="col"><small>Nama Mahasiswa</small></th>
		<th scope="col"><small>Usulan Judul Skripsi</small></th>
		<th scope="col"><small>Dosen Pengarah</small></th>
		<th scope="col"><small>Status</small></th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="8"></td>
	</tr>
</tfoot>
<tbody>
<?php 
$i = 0;
foreach($data['Proposal'] as $d): ?>
	<tr>
		<td style="width: 20px; text-align:center;"><?php echo ++$i; ?></td>
		<td style="width: 80px; text-align:center;"><small><?php echo $d['User']['nim'] ?></small></td>
		<td style="width: 200px; text-align:left;"><small><?php echo $d['User']['fullname'] ?></small></td>
		<td style="width: 450px; text-align:left;"><small><?php echo $d['judul'] ?></small></td>
		<td style="width: 200px; font-size: 0.8em; text-align:left;">
			<?php
			if (!empty($d['Dosen'])) { 
				echo $d['Dosen']['nama_dosen'];
			} else {
				echo '-';
			}
			?>
		</td>
		<td style="width: 70px; text-align:center;">
		<?php if ($d['status_proposal'] == 0) { ?>
		<span style="background-color:#5CAAFF; padding: 2px;">Baru</span>
		<?php } elseif ($d['status_proposal'] == 1) { ?>
		<span style="background-color:#FFE43E; padding: 2px;">Terkirim</span>
		<?php } elseif ($d['status_proposal'] == 2) { ?>
		<span style="background-color:#FF5228; padding: 2px;">Blm Diterima</span>
		<?php } else { ?>
		<span style="background-color:#79FF54; padding: 2px;">Diterima</span>
		<?php } ?>
		</td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php else: ?>
<h2>Belum ada peserta Desk Evaluation!</h2>
<?php endif; ?>
</section>
</div>