<?php echo $this->Html->link('<span><< Batal dan kembali ke menu</span>', array('controller'=>'admin', 'action'=>'home', '#tabs-2'), array('class'=>'button', 'escape' => false)); ?>
<p>&nbsp;</p><h3>Evaluasi Ringkasan Proposal</h3>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 95%; background-color:#c0c0c0; padding: 4px;">Identitas</p>
<?php 
echo $this->Form->create('Proposal', array('action' => 'savevaluasi')); 
echo $this->Form->input( 'Proposal.id', array( 'value' => $data['Proposal']['id'], 'type' => 'hidden') ); 
echo $this->Form->input( 'Proposal.evaluator', array( 'value' => $this->Session->read('User.id'), 'type' => 'hidden') ); 
?>
<table width="80%">
<tr style="font-size: 1.2em;">
	<td>NIM</td><td style="width:10px;">:</td><td style="font-weight:bold;"><?php echo $data['User']['nim'] ?></td>
	<td stlye="width: 50px;"></td>
	<td>Nama</td><td style="width:10px;">:</td><td style="font-weight: bold;"><?php echo $data['User']['fullname']; ?></td>
	<td stlye="width: 50px;"></td>
	<td>No. Telepon</td><td style="width:10px;">:</td><td style="font-weight: bold;"><?php echo $data['User']['telpno']; ?></td>
</tr>
</table>
<table width="80%">
<tr>
	<td><?php echo $this->Form->input('Proposal.konsentrasi',array(
		'label' => __('Konsentrasi:',true),
		'type' => 'select',
		'options' => $topiks,
		'selected' => $data['Proposal']['konsentrasi'],
	)); ?></td>
	<td stlye="width: 50px;"></td>
	<td style="font-size: 1.5em;">IPK : <strong><?php echo $data['Proposal']['ipk']; ?></strong></td>
	<td stlye="width: 50px;"></td>
	<td style="font-size: 1.5em;">IPro : <strong><?php echo $data['Proposal']['ipro']; ?></strong></td>
</tr>
<tr>
	<td colspan="3">
		<?php echo $this->Form->input('Proposal.judul', array('label' => 'Judul (tanpa diakhiri tanda titik (.))', 'value' => $data['Proposal']['judul'])); ?>
	</td>
	<td stlye="width: 50px;"></td>
	<td><?php echo $this->Form->input('Proposal.dosen',array(
		'label' => __('Dosen Pengarah:',true),
		'type' => 'select',
		'empty'=>'None',
		'options' => $dosens,
		'selected' => $data['Proposal']['dosen'],
	)); ?>
	<div style="padding: 5px 0 0 7px;">Apakah Skripsi ini kelanjutan dari KP?
	<?php 
		echo $this->Form->input('Proposal.lanjutan_kp', array(
										'options' => array(0 => 'Tidak', 1 => 'Ya'),
										'type' => 'radio',
										'legend' => '',
										'default' => $data['Proposal']['lanjutan_kp']
									));?>
	</div>
	<?php echo $this->Form->input('Proposal.dosen_kp',array(
		'label' => __('Pembimbing KP:',true),
		'type' => 'select',
		'empty'=>'None',
		'options' => $dosens,
		'selected' => $data['Proposal']['dosen_kp'],
	)); ?>
	</td>
</tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Gambaran Skripsi yang akan dibuat</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.gambaran');
	echo $this->Form->input('Proposal.gambaran', array('id'=> 'proposal_gambaran', 'type' => 'textarea', 'value' => $data['Proposal']['gambaran'], 'class' => 'ckeditor', 'label' => null, 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Spesifikasi Input-output</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.input_output');
	echo $this->Form->input('Proposal.input_output', array('id'=> 'proposal_input_output', 'value' => $data['Proposal']['input_output'], 'type' => 'textarea', 'class' => 'ckeditor', 'label' => 'Masukan-keluaran sistem:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Pengguna</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.pengguna');
	echo $this->Form->input('Proposal.pengguna', array('id'=> 'proposal_pengguna', 'type' => 'textarea', 'value' => $data['Proposal']['pengguna'], 'class' => 'ckeditor', 'label' => 'Deskripsi Pengguna Sistem:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Metode</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.metode');
	echo $this->Form->input('Proposal.metode', array('id'=> 'proposal_metode', 'type' => 'textarea', 'value' => $data['Proposal']['metode'], 'class' => 'ckeditor', 'label' => 'Deskripsi Metode-metode yang dilakukan dalam penelitian:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Data</p>
<table>
<tr><td>
<?php
	echo $this->Form->error('Proposal.data');
	echo $this->Form->input('Proposal.data', array('id'=> 'proposal_data', 'type' => 'textarea', 'value' => $data['Proposal']['data'], 'class' => 'ckeditor', 'label' => 'Deskripsi data yang dibutuhkan:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<p style="font-weight:bold; font-size:1.4em; display:block; width: 100%; background-color:#c0c0c0; padding: 4px;">Evaluasi</p>
<table>
<tr>
	<td><?php 
	$stat = 1;
	if ($data['Proposal']['status_proposal'] > 1) {
		$stat = $data['Proposal']['status_proposal'];
	}
	echo $this->Form->input('Proposal.status_proposal',array(
		'label' => __('Status Proposal:',true),
		'type' => 'select',
		'options' => array('1' => 'Terkirim', '2' => 'Belum Diterima', '3'=>'Diterima'),
		'selected' => $stat,
	)); ?></td>
</tr>
<tr><td>
<?php
	echo $this->Form->error('Proposal.evaluasi');
	echo $this->Form->input('Proposal.evaluasi', array('id'=> 'proposal_evaluasi', 'type' => 'textarea', 'value' => $data['Proposal']['evaluasi'], 'class' => 'ckeditor', 'label' => 'Catatan evaluasi:', 'rows' => '10', 'error' => false));
?>
</td></tr>
<tr><td>
<?php
	echo $this->Form->error('Proposal.catatan');
	echo $this->Form->input('Proposal.catatan', array('id'=> 'proposal_catatan', 'type' => 'textarea', 'value' => $data['Proposal']['catatan'], 'class' => 'ckeditor', 'label' => 'Catatan-catatan internal tim:', 'rows' => '10', 'error' => false));
?>
</td></tr>
</table>
<?php echo $this->Form->end(array('label' => 'Save', 'id' => 'submit')); ?>
<style>
	span.error {
		margin-left: 10px;
		color: #ffffff;
		background-color: #ff0000;
		padding: 2px 6px;
	}
</style>
<script type="text/javascript">
	//<![CDATA[
	CKEDITOR.replace( 'proposal_gambaran',
	{
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_input_output',
	{
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_pengguna',
	{
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_metode',
	{
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_data',
	{
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_evaluasi',
	{
		toolbar : 'MinimToolbar'
	});
	CKEDITOR.replace( 'proposal_catatan',
	{
		toolbar : 'MinimToolbar'
	});
	//]]>
</script>