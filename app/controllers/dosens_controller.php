<?php
/**
 * 
 * @author budi susanto
 *
 */
class DosensController extends AppController 
{
	public $name = 'Dosens';
	public $layout = 'baseform';
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
			//exit();
		} else {
			if(($this->Session->read('User.group_id') != 1)) {
				$this->Session->setFlash('Sorry, you don\'t have any privileges to access this page.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home'));
				//exit();
			}
		}
	}
	
	function add() {
		$this->set('judul', 'Tambah Dosen');
		if (!empty($this->data)) {
			$this->Dosen->create();
			if ($this->Dosen->save($this->data)) {
				$this->Session->setFlash('Your dosen has been saved and published!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-7'));
			} else {
				$this->Session->setFlash('Sorry, there are any error that can not be handled by system in order to save the new Dosen!');
				$this->set('data', $this->data);
				$this->render('add');
			}
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update Dosen');
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-7'));
		}
		
		if (!empty($this->data)) {
			if ($this->Dosen->save($this->data)) {
				$this->Session->setFlash('The Dosen has been updated!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-7'));
			} else {
				$this->Session->setFlash('Sorry, there are any error that can not be handled by system in order to save your news!');
				$this->set('data', $this->data);
				$this->render('edit');
			}
		} else {
			$data = $this->Dosen->find('first', array('conditions' => array('Dosen.id' => $id)));
			$this->set('data', $data);
		}
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-7'));
		}
		
		if ($this->Dosen->delete($id)) {
			$this->Session->setFlash('The selected dosen has been deleted!', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash('The selected dosen could not be deleted!', 'default');
		}
		$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-7'));
	}
}
?>