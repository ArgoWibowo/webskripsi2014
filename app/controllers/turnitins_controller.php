<?php
//App::import('Sanitize');
class TurnitinsController extends AppController {
	public $name = 'Turnitins';
	public $layout = 'baseform';
	public $uses = array('Turnitin');
	var $components = array('RequestHandler');
	var $helpers = array('Html','Form','Tools');
	
	function add($id_ta = null) {
		$this->set('judul', 'Unggah PDF Skripsi');
		if (!empty($this->data)) {
			$this->Turnitin->create();
			$tmp_name_file = $this->data['Turnitin']['id_ta'].'.pdf';
			move_uploaded_file($this->data['Turnitin']['url_skripsi']['tmp_name'], WWW_ROOT . 'files/scanskripsi/' . $tmp_name_file);
			$this->data['Turnitin']['url_skripsi'] = $tmp_name_file;
			if ($this->Turnitin->save($this->data)) {
				$this->Session->setFlash('Data PDF Skripsi telah tersimpan!', 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
			} else {
				$this->Session->setFlash('Data PDF Skripsi tidak dapat disimpan!');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
			}
		} else {
			//$dosens = $this->Ta->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => array('K', 'S') ) ));
			$this->set(compact('id_ta'));
		}
	}

	function addhasil($id_ta = null) {
		$this->set('judul', 'Unggah PDF Hasil Turnitin');
		if (!empty($this->data)) {
			$this->Turnitin->create();
			$tmp_name_file = $this->data['Turnitin']['id_ta'].'.pdf';
			move_uploaded_file($this->data['Turnitin']['url_turnitin']['tmp_name'], WWW_ROOT . 'files/turnitin/' . $tmp_name_file);
			$this->data['Turnitin']['url_turnitin'] = $tmp_name_file;
			if ($this->Turnitin->save($this->data)) {
				$this->Session->setFlash('Data PDF Hasil Turnitin telah tersimpan!', 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
			} else {
				$this->Session->setFlash('Data PDF Hasil Turnitin tidak dapat disimpan!');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
			}
		} else {
			$this->set(compact('id_ta'));
		}
	}
	
	// function edit($id = null) {
	// 	$this->set('judul', 'Update Skripsi');
	// 	if (!empty($this->data)) {
	// 		$isOK = false;
	// 		$msg = 'Maaf sistem SkripSI belum dapat menyimpan perubahan data TA dari Anda!';
	// 		$this->data['Ta']['modified'] = date( 'Y-m-d H:i:s' );
	// 		if ($this->Ta->save($this->data)):
	// 			$this->Session->setFlash('Data Skripsi terpilih telah diupdate!', 'default', array('class' => 'success'));
	// 			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// 			$isOK = true;
	// 		endif;
			
	// 		if (!$isOK) {
	// 			$this->Session->setFlash($msg);
	// 			$dosens = $this->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => array('K', 'S') ) ));
	// 			$this->set(compact('dosens'));
				
	// 			$this->set('data', $this->data);
	// 			$this->set('err', $this->Ta->invalidFields());
	// 			$this->render('edit');
	// 		}
	// 	} else {
	// 		if (!$id) {
	// 			$this->Session->setFlash('Your request is not valid!');
	// 			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
	// 		}
			
	// 		//if(($this->Session->read('User.group_id') == 1)) {
	// 		$data = $this->Ta->find('first', array('conditions' => array('Ta.id' => $id)));
	// 		//} else {
	// 		//	$data = $this->Ta->find('first', array('conditions' => array('Ta.id' => $id, 'Ta.nim' => $this->Session->read('User.nim'))));
	// 		//}
	// 		if (!empty($data) && $data['Ta']['lulus'] == 0):
	// 			$dosens = $this->Ta->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => array('K', 'S') ) ));
	// 			$this->set(compact('dosens'));
	// 			$this->set('data', $data);
	// 		else:
	// 			$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani perubahan data TA yang Anda minta!');
	// 			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// 		endif;
	// 	}
	// }
	
	// function delete($id = null) {
	// 	if (!$id) {
	// 		$this->Session->setFlash('Your request is not valid!');
	// 		$this->redirect(array('controller' => 'admin', 'action' => 'home'));
	// 	}
		
	// 	if ($this->Session->read('User.group_id') == 1) {
	// 		$data = $this->Ta->find('first', array('conditions' => array('Ta.id' => $id)));
	// 	}
	// 	if (!empty($data)):
	// 		if ($this->Ta->delete($id)) {
	// 			$this->Session->setFlash('Data Skripsi terpilih sudah dihapus!', 'default', array('class' => 'success'));
	// 			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// 		} else {
	// 			$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani penghapusan data Skripsi yang Anda minta!');
	// 			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// 		}
	// 	else:
	// 		$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani penghapusan data Skripsi yang Anda minta!');
	// 		$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// 	endif;
	// }
	
	// function getta($id = null) {
	// 	$this->layout = 'ajax';
	// 	if ($id == null):
	// 		$d['Status']['return'] = 1;
	// 		$d['Status']['msg'] = 'Request tidak valid!';
	// 	else:
	// 		$ta = $this->Ta->find('first', array('fields' =>array('Ta.id', 'Mahasiswa.nim', 'Mahasiswa.nama', 'Ta.judul'), 'conditions' => array('Ta.id' => $id) ));
	// 		if (!empty($ta)):
	// 			$d['Status']['return'] = 0;
	// 			//$ta['Ta']['judul'] = Sanitize::clean($ta['Ta']['judul'], array('encode' => false));
	// 			$d['Status']['msg'] = $ta;
	// 		else:
	// 			$d['Status']['return'] = 1;
	// 			$d['Status']['msg'] = 'Mahasiswa dengan NIM tersebut tidak terdaftar!';
	// 		endif;
	// 	endif;
	// 	$this->set(compact('d'));
	// }
	
	// function getjudul($id=null){
	// 	$this->layout = 'ajax';
	// 	if ($id == null):
	// 		$d['Status']['return'] = 1;
	// 		$d['Status']['msg'] = 'Request tidak valid!';
	// 	else:
	// 		$ta = $this->Ta->find('first', array('fields' =>array('Ta.id', 'Mahasiswa.nim', 'Mahasiswa.nama', 'Ta.judul'), 'conditions' => array('Ta.id' => $id) ));
	// 		if (!empty($ta)):
	// 			$d['Status']['return'] = 0;
	// 			$d['Status']['msg'] = $ta;
	// 		else:
	// 			$d['Status']['return'] = 1;
	// 			$d['Status']['msg'] = 'Skripsi dengan id yang diminta tidak ada!';
	// 		endif;
	// 	endif;
	// 	$this->set(compact('d'));
	// }
	
	// function savejudul() {
	// 	$this->layout = 'ajax';
	// 	if ($this->params['isAjax']) {
	// 		$fdata = $this->params['form'];
	// 		if (!$fdata['id'] && !$fdata['judul']) {
	// 			$d['Status']['return'] = 1;
	// 			$d['Status']['msg'] = 'Request tidak valid!';
	// 		} else {
	// 			$data['Ta']['id'] = $fdata['id'];
	// 			$data['Ta']['judul'] = $fdata['judul'];
	// 			if ($this->Ta->save($data)) {
	// 				$d['Status']['return'] = 0;
	// 				$d['Status']['msg'] = 'Perubahan judul skripsi telah tersimpan!';
	// 			} else {
	// 				$d['Status']['return'] = 3;
	// 				$d['Status']['msg'] = 'Perubahan judul skripsi tidak dapat dilakukan oleh sistem saat ini!';
	// 			}
	// 		}
	// 	} else {
	// 		$d['Status']['return'] = 1;
	// 		$d['Status']['msg'] = 'Request tidak valid!';
	// 	}
	// 	$this->set('d', $d);
	// }
	
	// function prnkartu($id = null) {
	// 	if (!$id) {
	// 		$this->Session->setFlash('Maaf, tidak ada ID yang diberikan!');
	// 		$this->redirect(array('action'=>'index', null, true));
	// 	}
	// 	$data = $this->Ta->find('first', array(
	// 							'conditions' => array('Ta.id' => $id, 'Ta.aktif' => 1, 'Ta.lulus' => 0 ) 
	// 						)
	// 					);
	// 	$this->set('data', $data);
		
	// 	Configure::write('debug',0); // Otherwise we cannot use this method while developing
	// 	$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
	// 	$this->render();
	// }
	
	// function gettafile($id = null) {
	// 	$this->layout = 'ajax';
	// 	if ($id == null):
	// 		$d['Status']['return'] = 1;
	// 		$d['Status']['msg'] = 'Request tidak valid!';
	// 	else:
	// 		$tafile = $this->Ta->Tafile->find('all', array('conditions' => array('Tafile.ta_id' => $id) ));
	// 		if (!empty($tafile)):
	// 			$d['Status']['return'] = 0;
	// 			$d['Status']['msg'] = $tafile;
	// 		else:
	// 			$d['Status']['return'] = 1;
	// 			$d['Status']['msg'] = 'Belum ada file yang terunggah!';
	// 		endif;
	// 	endif;
	// 	$this->set(compact('d'));
	// }
	
	// function addtafile() {
	// 	if (!empty($this->data)) {
	// 		$this->Ta->Tafile->Behaviors->detach('MeioUpload.MeioUpload');
	// 		$this->Ta->Tafile->Behaviors->attach('MeioUpload.MeioUpload', 
	// 					array('filename' => array(
	// 								'dir' => '../files{DS}artikel{DS}files{DS}skripsi/' . $this->data['Tafile']['ta_id'],
	// 								'createDirectory' => true,
	// 								'allowedMime' => array(
	// 													'application/vnd.oasis.opendocument.text', 
	// 													'application/msword', 
	// 													'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	// 												),
	// 								'allowedExt' => array('.odt', '.doc', '.docx'),
	// 								'default' => false,
	// 								'maxSize' => 5242880
	// 							)
	// 						)
	// 				);
	// 		$this->Ta->Tafile->create();
	// 		if ($this->Ta->Tafile->save($this->data)) {
	// 			$this->Session->setFlash('Data File Skripsi telah tersimpan!', 'default', array('class' => 'success'));
	// 		} else {
	// 			$this->Session->setFlash('Terdapat kesalahan sistem saat menyimpan!');
	// 		}
	// 	} else {
	// 		$this->Session->setFlash('Request tidak valid!');
	// 	}
	// 	$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// }
	
	// /**
	//  * Sends a file to the client
	//  *
	//  * @param string $id UUID
	//  * @access public
	//  * Source: http://cakedc.com/florian_kraemer/2010/01/25/file-uploading-file-storage-and-cakephp-mediaview-class
	//  */
	// function download($id = null) {
	// 	if (!$id) {
	// 		$this->Session->setFlash('Your request is not valid!');
	// 		$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// 	}
		
	// 	$groupid = $this->Session->read('User.group_id');
		
	// 	if ($groupid == 1 || $groupid == 3 || $groupid == 4):
	// 		$media = $this->Ta->query('SELECT Tafile.* FROM tafiles Tafile WHERE Tafile.id = ' . $id );
	// 	else:
	// 		$media = $this->Ta->query('SELECT Tafile.* FROM tafiles Tafile, tas Ta WHERE Tafile.id = ' . $id . ' AND Ta.id = Tafile.ta_id AND Ta.nim = \'' . $this->Session->read('User.nim') . '\'');
	// 	endif;
		
	// 	if (empty($media)) {
	// 		$this->Session->setFlash('File tidak ditemukan!');
	// 		if ($groupid == 1 || $groupid == 2 || $groupid == 3):
	// 			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-4'));
	// 		else:
	// 			$this->redirect(array('controller'=>'admin', 'action' => 'home'));
	// 		endif;
	// 	}
		
	// 	$path_info = pathinfo($media[0]['Tafile']['filename']);
		
	// 	$params = array(
	// 		'cache' => '3 days',
	// 		'download' => true,
	// 		'name' => $path_info['filename'],
	// 		'id' => $media[0]['Tafile']['filename'],
	// 		'download' => true,
	// 		'path' => $media[0]['Tafile']['dir'] . DS,
	// 		'modified' => $media[0]['Tafile']['modified'],
	// 		'mimeType' => $media[0]['Tafile']['mimetype'],
	// 		'extension' => $path_info['extension']
	// 	);
		
	// 	$this->view = 'Media';
	// 	$this->set($params);
	// 	$this->autoLayout = false;
	// }	
	
	// function delfile($id = null) {
	// 	if (!$id) {
	// 		$this->Session->setFlash('Your request is not valid!');
	// 		$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// 	}
	// 	$tafiles = $this->Ta->Tafile->find('first', array('recursive' => 0, 'conditions' => array('Tafile.id' => $id) ));
	// 	if (!empty($tafiles)) {
	// 		$this->Ta->Tafile->Behaviors->detach('MeioUpload.MeioUpload');
	// 		$this->Ta->Tafile->Behaviors->attach('MeioUpload.MeioUpload', 
	// 					array('filename' => array(
	// 								'dir' => '../files{DS}artikel{DS}files{DS}skripsi/' . $tafiles['Tafile']['ta_id'],
	// 								'createDirectory' => true,
	// 								'allowedMime' => array(
	// 													'application/vnd.oasis.opendocument.text', 
	// 													'application/msword', 
	// 													'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	// 												),
	// 								'allowedExt' => array('.odt', '.doc', '.docx'),
	// 								'default' => false,
	// 								'maxSize' => 3145728
	// 							)
	// 						)
	// 				);
			
	// 		$this->Ta->Tafile->id = $id;
	// 		if ($this->Ta->Tafile->delete()) {
	// 			$this->Session->setFlash('File terpilih sudah terhapus.', 'default', array('class' => 'success'));
	// 		} else {
	// 			$this->Session->setFlash('Sistem saat ini tidak dapat menghapus file terpilih!');
	// 		}
	// 	} else {
	// 		$this->Session->setFlash('Sistem saat ini tidak dapat menghapus file terpilih!');
	// 	}
	// 	$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-4'));
	// }
	
	// function prnperpanjangan() {
	// 	$data = $this->Ta->find('first', array(
	// 							'conditions' => array('Ta.nim' => $this->Session->read('User.nim')),
	// 							'order' => array('Ta.id DESC')
	// 						));
		
	// 	$this->set('data', $data);
	// 	Configure::write('debug',0); // Otherwise we cannot use this method while developing
	// 	$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
	// 	$this->render();
	// }
	
	// function prnusuldosen() {
	// 	$data = $this->Ta->find('first', array(
	// 							'conditions' => array('Ta.nim' => $this->Session->read('User.nim')),
	// 							'order' => array('Ta.id DESC')
	// 						));
		
	// 	$this->set('data', $data);
	// 	Configure::write('debug',0); // Otherwise we cannot use this method while developing
	// 	$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
	// 	$this->render();
	// }
	
	// function prnubahjudul() {
	// 	$data = $this->Ta->find('first', array(
	// 							'conditions' => array('Ta.nim' => $this->Session->read('User.nim')),
	// 							'order' => array('Ta.id DESC')
	// 						));
		
	// 	$this->set('data', $data);
	// 	Configure::write('debug', 0); // Otherwise we cannot use this method while developing
	// 	$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
	// 	$this->render();
	// }

	// function prnlist() {
	// 	$this->set('judul', 'Daftar Skripsi');
	// 	$data = $this->Ta->find('all', array(
	// 						'conditions' => array('Ta.aktif' => 1),
	// 						'order' => array('take DESC')
	// 					)
	// 				);
	// 	$this->set('data', $data);
	// 	$this->layout = 'report';
	// }

	// function prnjmlta() {
	// 	$this->set('judul', 'Laporan Jumlah Bimbingan Skripsi');
	// 	$data = $this->Ta->getstat();
	// 	$this->set('data', $data);
	// 	$this->layout = 'report';
	// }
}
?>