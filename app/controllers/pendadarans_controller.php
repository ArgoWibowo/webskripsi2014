<?php
class PendadaransController extends AppController {
	public $name = 'Pendadarans';
	public $uses = array('Pendadaran', 'Mahasiswa');
	var $components = array('RequestHandler');
	var $helpers = array('Html', 'Tools');
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		}
		$groupid = $this->Session->read('User.group_id');
		if ($this->action == 'regis' || $this->action == 'delete' || $this->action == 'prnrevisi' || $this->action != 'prnsampul') {
			if ($groupid != 1 && $groupid != 2) {
				$this->Session->setFlash('Anda tidak memiliki hak akses untuk fungsi ini.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-5'));
			}
		}
		if ($this->action == 'validasi' || $this->action == 'prnletter') {
			if ($groupid != 1) {
				$this->Session->setFlash('Anda tidak memiliki hak akses untuk fungsi ini.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-5'));
			}
		}
	}
	
	function regis(){
		$this->layout = 'ajax';
		if ($this->params['isAjax']) {
			$fdata = $this->params['form'];
			if (!$fdata['batas']) {
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Request tidak valid!';
			} else {
				$isok = false;
				if ($this->Session->read('User.group_id') == 2) {
					$cekta = $this->Pendadaran->Ta->find('first', array('conditions' => array(
																			'Ta.nim' => $this->Session->read('User.nim'),
																			'Ta.aktif' => 1,
																			'Ta.lulus' => 0
																		),
																		'recursive' => -1
																	)
														);
				} elseif ($this->Session->read('User.group_id') == 1)  {
					$cekta = $this->Pendadaran->Ta->find('first', array('conditions' => array(
																			'Ta.id' => $fdata['id'],
																			'Ta.aktif' => 1,
																			'Ta.lulus' => 0
																			),
																		  'recursive' => -1 
																	)
														);
				}
				if (!empty($cekta) && (!empty($cekta['Ta']['dosen2']) && !empty($cekta['Ta']['dosen1']))) {
					// cek apakah ada data pendadaran berstatus BARU atau BELUM BERJALAN dan status kelulusan SUDAH LULUS?
					$cek = $this->Pendadaran->find('first', array(
								'conditions' => array(
												'Pendadaran.id_ta' => $cekta['Ta']['id'],
												'or' => array(array('Pendadaran.status' => 2,
																	'Pendadaran.lulus' => 2), 
															  array('Pendadaran.status' => array(0,1)))
													),
													'order' => 'modified desc'
										  ) );
					$isok = empty($cek);
				}
				
				if ($isok){
					$data['Pendadaran']['id_ta'] = $cekta['Ta']['id'];
					$data['Pendadaran']['penguji1'] = $cekta['Ta']['dosen1'];
					$data['Pendadaran']['penguji2'] = $cekta['Ta']['dosen2'];
					$data['Pendadaran']['id_tgldaftar'] = $fdata['batas'];
					$data['Pendadaran']['lulus'] = 0;
					$data['Pendadaran']['status'] = 0;
					$this->Pendadaran->create();
					if ($this->Pendadaran->save($data)) {
						$d['Status']['return'] = 0;
						$d['Status']['msg'] = 'Pendaftaran pendadaran Anda telah tercatat!';
					} else {
						$d['Status']['return'] = 3;
						$d['Status']['msg'] = 'Pendaftaran pendadaran tidak dapat dilakukan oleh sistem saat ini!';
					}
				} else {
					$d['Status']['return'] = 2;
					$d['Status']['msg'] = 'Anda tidak memiliki data Skripsi terakhir yang valid untuk pendadaran!';
				}
			}
		} else {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		}
		$this->set('d', $d);
	}
	
	function delete($id = null){
		if (!$id) {
			$this->Session->setFlash('Permintaan Anda tidak valid.');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-5'));
		}
		if ($this->Session->read('User.group_id') == 1) {
			$cek = $this->Pendadaran->find('first', array('conditions'=>array('Pendadaran.id' => $id)));
		} else {
			$cek = $this->Pendadaran->find('first', array('conditions'=>array('Pendadaran.id' => $id, 'Ta.nim' => $this->Session->read('User.nim') )));
		}
		
		if (!empty($cek) && ($cek['Pendadaran']['status'] == 0 || $cek['Pendadaran']['status'] == 1) ) {
			$this->Pendadaran->id = $id;
			if ($this->Pendadaran->delete()){
				$this->Session->setFlash('Data pendadaran telah dihapus.', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('Maaf, untuk saat ini sistem tidak dapat menghapus data pendadaran terpilih.');
			}
		}
		$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-5'));
	}
	
	function validasi(){
		$this->layout = 'ajax';
		if ($this->params['isAjax']) {
			$fdata = $this->params['form'];
			if (!$fdata['id'] || !$fdata['nim'] || !$fdata['idta']) {
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Request tidak valid!';
			} else {
				// hitung ujian ke berapa?
				$dadar = $this->Pendadaran->find('all', array('conditions' => array('Ta.id' => $fdata['idta'],
																	'Pendadaran.status' => 2) ));
				$ujianke = count($dadar)+1;
				$this->Pendadaran->id = $fdata['id'];
				if ($this->Pendadaran->saveField('status', 1) && 
					$this->Pendadaran->saveField('ujianke', $ujianke)) {
					$d['Status']['return'] = 0;
					$d['Status']['msg'] = 'Pendadaran telah berstatus valid!';
				} else {
					$d['Status']['return'] = 2;
					$d['Status']['msg'] = 'Proses validasi tidak dapat dilakukan saat ini! Silahkan hubungi administrator.';
				}
			}
		} else {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		}
		$this->set('d', $d);
	}
	
	function prnletter($id = null) {
        if (!$id) {
            $this->Session->setFlash('Sorry, there was no property ID submitted.');
            $this->redirect(array('action'=>'index'), null, true);
        }
		$data = $this->Pendadaran->find('first', array(
								'conditions' => array('Pendadaran.id' => $id ) 
							)
						);
		$this->set('data', $data);
		$mhs = $this->Mahasiswa->find('first', array('conditions' => array('Mahasiswa.nim' => $data['Ta']['nim']) ));
		$this->set('mhs', $mhs);
		
		Configure::write('debug',0); // Otherwise we cannot use this method while developing
		$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
		$this->render();
    }
	
	function prnbadadar($id = null) {
		if (!$id) {
			$this->Session->setFlash('Maaf, tidak ada ID yang diberikan!');
			$this->redirect(array('action'=>'index', null, true));
		}
		$data = $this->Pendadaran->find('first', array(
								'conditions' => array('Pendadaran.id' => $id, 'Pendadaran.status' => 1 ), 'recursive' => 2
							)
						);
		$this->set('data', $data);
		$mhs = $this->Mahasiswa->find('first', array('conditions' => array('Mahasiswa.nim' => $data['Ta']['nim']) ));
		$this->set('mhs', $mhs);
		Configure::write('debug',0); // Otherwise we cannot use this method while developing
		$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
		$this->render();
	}
	
	function getdadar($id=null) {
		$this->layout = 'ajax';
		if (!$id) {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		} else {
			$dadar = $this->Pendadaran->find('first', array('conditions' => array('Pendadaran.id' => $id,
																'Pendadaran.status' => 1) ));
			$mhs = $this->Mahasiswa->find('first', array('conditions' => array('Mahasiswa.nim' => $dadar['Ta']['nim']) ));
			
			if (!empty($dadar) && !empty($mhs)) {
				$hasil['Dadar']['id'] = $dadar['Pendadaran']['id'];
				$hasil['Dadar']['nim'] = $mhs['Mahasiswa']['nim'];
				$hasil['Dadar']['nama'] = $mhs['Mahasiswa']['nama'];
				$hasil['Dadar']['judul'] = $dadar['Ta']['judul'];
				$hasil['Dadar']['penguji1'] = $dadar['Pendadaran']['penguji1'];
				$hasil['Dadar']['penguji2'] = $dadar['Pendadaran']['penguji2'];
				$hasil['Dadar']['penguji3'] = $dadar['Pendadaran']['penguji3'];
				$hasil['Dadar']['penguji4'] = $dadar['Pendadaran']['penguji4'];
				$hasil['Dadar']['ujianke'] = $dadar['Pendadaran']['ujianke'];
				$hasil['Dadar']['ketua'] = $dadar['Pendadaran']['ketua'];
				$hasil['Dadar']['jadwal'] = date('d-m-Y H:i:s', strtotime($dadar['Pendadaran']['jadwal']));
				
				$d['Status']['return'] = 0;
				$d['Status']['msg'] = $hasil;
			} else {
				$d['Status']['return'] = 2;
				$d['Status']['msg'] = 'Data pendadaran tidak ditemukan/tidak ada data mahasiswa untuk pendadaran tersebut!';
			}
		}
		$this->set('d', $d);
	}
	
	function gethasildadar($id = null) {
		$this->layout = 'ajax';
		if (!$id) {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		} else {
			$dadar = $this->Pendadaran->find('first', array('conditions' => array('Pendadaran.id' => $id,
															'Pendadaran.status' => array(1,2)), 'recursive'=> 4 ));
			if (!empty($dadar)) {
				$hasil['Dadar']['id'] = $dadar['Pendadaran']['id'];
				$hasil['Dadar']['nim'] = $dadar['Ta']['Mahasiswa']['nim'];
				$hasil['Dadar']['nama'] = $dadar['Ta']['Mahasiswa']['nama'];
				$hasil['Dadar']['judul'] = $dadar['Ta']['judul'];
				$hasil['Dadar']['ketua'] = $dadar['Pendadaran']['ketua'];
				$hasil['Dadar']['ujianke'] = $dadar['Pendadaran']['ujianke'];
				$hasil['Dadar']['jadwal'] = date('d-m-Y H:i:s', strtotime($dadar['Pendadaran']['jadwal']));
				$hasil['Dadar']['status'] = $dadar['Pendadaran']['status'];
				$hasil['Dadar']['lulus'] = $dadar['Pendadaran']['lulus'];
				$hasil['Dadar']['nilai1'] = $dadar['Pendadaran']['nilai1'];
				$hasil['Dadar']['nilai2'] = $dadar['Pendadaran']['nilai2'];
				$hasil['Dadar']['dosen1'] = $dadar['Dosen']['nama_dosen'];
				$hasil['Dadar']['dosen2'] = $dadar['Dosen2']['nama_dosen'];
				$hasil['Dadar']['nilai3'] = $dadar['Pendadaran']['nilai3'];
				$hasil['Dadar']['nilai4'] = $dadar['Pendadaran']['nilai4'];
				$hasil['Dadar']['nilai5'] = $dadar['Pendadaran']['nilai5'];
				$hasil['Dadar']['nilai6'] = $dadar['Pendadaran']['nilai6'];
				if ($dadar['Pendadaran']['ketua'] == 1) {
					$hasil['Dadar']['penguji1'] = $dadar['Dosen']['nama_dosen'];
					$hasil['Dadar']['penguji2'] = $dadar['Dosen2']['nama_dosen'];
					$hasil['Dadar']['penguji3'] = $dadar['Dosen3']['nama_dosen'];
					$hasil['Dadar']['penguji4'] = $dadar['Dosen4']['nama_dosen'];
				} else if ($dadar['Pendadaran']['ketua'] == 2) {
					$hasil['Dadar']['penguji2'] = $dadar['Dosen']['nama_dosen'];
					$hasil['Dadar']['penguji1'] = $dadar['Dosen2']['nama_dosen'];
					$hasil['Dadar']['penguji3'] = $dadar['Dosen3']['nama_dosen'];
					$hasil['Dadar']['penguji4'] = $dadar['Dosen4']['nama_dosen'];
				} else if ($dadar['Pendadaran']['ketua'] == 3) {
					$hasil['Dadar']['penguji2'] = $dadar['Dosen']['nama_dosen'];
					$hasil['Dadar']['penguji3'] = $dadar['Dosen2']['nama_dosen'];
					$hasil['Dadar']['penguji1'] = $dadar['Dosen3']['nama_dosen'];
					$hasil['Dadar']['penguji4'] = $dadar['Dosen4']['nama_dosen'];
				} else {
					$hasil['Dadar']['penguji2'] = $dadar['Dosen']['nama_dosen'];
					$hasil['Dadar']['penguji3'] = $dadar['Dosen2']['nama_dosen'];
					$hasil['Dadar']['penguji4'] = $dadar['Dosen3']['nama_dosen'];
					$hasil['Dadar']['penguji1'] = $dadar['Dosen4']['nama_dosen'];
				}
				
				$d['Status']['return'] = 0;
				$d['Status']['msg'] = $hasil;
			} else {
				$d['Status']['return'] = 2;
				$d['Status']['msg'] = 'Data pendadaran tidak ditemukan/tidak ada data mahasiswa untuk pendadaran tersebut!';
			}
		}
		$this->set('d', $d);
	}
	
	function savehasildadar(){
		$this->layout = 'ajax';
		if ($this->params['isAjax']) {
			$fdata = $this->params['form'];
			if (!$fdata['id'] && !$fdata['nilai1'] && !$fdata['nilai2'] && !$fdata['nilai3'] && !$fdata['nilai4'] && !$fdata['nilai5'] && !$fdata['nilai6'] && !$fdata['lulus']) {
				$fdata['Status']['return'] = 1;
				$fdata['Status']['msg'] = 'invalid request';
			} else {
				$this->Pendadaran->id = $fdata['id'];
				if ($this->Pendadaran->saveField('nilai1', $fdata['nilai1']) &&
					$this->Pendadaran->saveField('nilai2', $fdata['nilai2']) &&
					$this->Pendadaran->saveField('nilai3', $fdata['nilai3']) &&
					$this->Pendadaran->saveField('nilai4', $fdata['nilai4']) &&
					$this->Pendadaran->saveField('nilai5', $fdata['nilai5']) &&
					$this->Pendadaran->saveField('nilai6', $fdata['nilai6']) &&
					$this->Pendadaran->saveField('lulus', $fdata['lulus']) &&
					$this->Pendadaran->saveField('status', 2) ) {
					
					if ($fdata['lulus'] == 2) {
						$datadadar = $this->Pendadaran->find('first', array('conditions'=>array('Pendadaran.id' => $fdata['id'] )));
						if ($datadadar){
							$this->Pendadaran->Ta->id = $datadadar['Pendadaran']['id_ta'];
							if ($this->Pendadaran->Ta->saveField('lulus', 1)) {
								$d['Status']['return'] = 0;
								$d['Status']['msg'] = "Sistem berhasil menyimpan hasil pendadaran!";
							} else {
								$d['Status']['return'] = 2;
								$d['Status']['msg'] = "Sistem tidak dapat mengupdate hasil pendadaran saat ini!";
							}
						}
					} else {
						$d['Status']['return'] = 0;
						$d['Status']['msg'] = "Sistem berhasil menyimpan hasil pendadaran!";
					}
				} else {
					$d['Status']['return'] = 2;
					$d['Status']['msg'] = "Sistem tidak dapat mengupdate hasil pendadaran saat ini!";
				}
			}
		} else {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'invalid request';
		}
		$this->set('d', $d);
	}
	
	function aturjadwal() {
		$this->layout = 'ajax';
		if ($this->params['isAjax']) {
			$fdata = $this->params['form'];
			if (!$fdata['id'] && !$fdata['penguji3'] && !$fdata['penguji4'] && !$fdata['jadwal']) {
				$fdata['Status']['return'] = 1;
				$fdata['Status']['msg'] = 'invalid request';
			} else {
				list($tgl, $jam) = split(" ", $fdata['jadwal']);
				list($day, $m, $y) = split("-", $tgl);
				list($h, $i, $s) = split(":", $jam);
				$jadwal = $y . '-' . $m . '-' . $day . ' ' . $h . ':' .$i . ':' . $s;
				
				$this->Pendadaran->id = $fdata['id'];
				if ($this->Pendadaran->saveField('penguji3', $fdata['penguji3']) &&
					$this->Pendadaran->saveField('penguji4', $fdata['penguji4']) &&
					$this->Pendadaran->saveField('ketua', $fdata['ketua']) &&
					$this->Pendadaran->saveField('jadwal', $jadwal ) ) {
					$d['Status']['return'] = 0;
					$d['Status']['msg'] = "Sistem berhasil menyimpan jadwal pendadaran!";
				} else {
					$d['Status']['return'] = 2;
					$d['Status']['msg'] = "Sistem tidak dapat mengupdate jadwal pendadaran saat ini!";
				}
			}
		} else {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'invalid request';
		}
		$this->set('d', $d);
	}
	
	function prnpeserta($id = null){
		$this->layout = "report";
		$this->set('judul', 'Peserta Pendadaran Program Studi Sistem Informasi UKDW');
		if (!$id) {
			$this->set('status', 'URL tidak valid!');
		} else {
			$periode = $this->Pendadaran->find('all', 
							array('conditions' => array('Pendadaran.id_tgldaftar' => $id,
														'Pendadaran.status' => array(1,2)),
								  'recursive' => -1,
								  'fields' => array('min(jadwal) as awal', 'max(jadwal) as akhir') ));
			$this->set(compact('periode'));
			$dadar = $this->Pendadaran->find('all', 
							array('conditions' => array('Pendadaran.id_tgldaftar' => $id,
														'Pendadaran.status' => array(1,2)),
								  'recursive' => 5,
								  'order' => 'Pendadaran.jadwal'));
			$this->set('dadar', $dadar);
		}
	}
	
	function prnlabel($id = null) {
		if (!$id) {
			$this->Session->setFlash('Maaf, tidak ada ID yang diberikan!');
			$this->redirect(array('action'=>'index', null, true));
		}
		$data = $this->Pendadaran->find('first', array(
								'conditions' => array('Pendadaran.id' => $id, 'Pendadaran.status' => 1 ),
								'recursive' => 5
							)
						);
		$this->set('data', $data);
		
		Configure::write('debug',0); // Otherwise we cannot use this method while developing
		$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
		$this->render();
	}
	
	function prnrevisi() {
		$data = $this->Pendadaran->find('first', array(
								'conditions' => array('Ta.nim' => $this->Session->read('User.nim'), 
													  	'and' => array('Pendadaran.lulus' => 2, 'Pendadaran.status' => 2)
													),
								'order' => array('Pendadaran.id DESC')
							));
		$mhs = $this->Mahasiswa->find('first', array('conditions'=> array('Mahasiswa.nim' => $this->Session->read('User.nim')) ));
		
		$this->set('data', $data);
		$this->set('mhs', $mhs);
		Configure::write('debug', 2); // Otherwise we cannot use this method while developing
		$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
		$this->render();
	}
	
	function prnsampul() {
		$data = $this->Pendadaran->find('first', array(
								'conditions' => array('Ta.nim' => $this->Session->read('User.nim'), 
													  'or' => array(
													  	'and' => array('Pendadaran.lulus' => 2, 'Pendadaran.status' => 2),
													  	'Pendadaran.status' => array(0,1)
													  )
													),
								'order' => array('Pendadaran.id DESC')
							));
		$mhs = $this->Mahasiswa->find('first', array('conditions'=> array('Mahasiswa.nim' => $this->Session->read('User.nim')) ));
		$this->set('data', $data);
		$this->set('mhs', $mhs);
		
		Configure::write('debug', 0); // Otherwise we cannot use this method while developing
		//debug($data);
		$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
		$this->render();
	}
}
?>