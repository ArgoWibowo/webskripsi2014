<?php
/**
 * 
 * @author budi susanto
 *
 */
class JwdpddrsController extends AppController {
	public $name = 'Jwdpddrs';
	public $layout = 'baseform';
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		} else {
			if(($this->Session->read('User.group_id') != 1)) {
				$this->Session->setFlash('Sorry, you don\'t have any privileges to access this page.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			}
		}
	}
	
	function add() {
		$this->set('judul', 'Tambah Jadwal Pendadaran');
		if (!empty($this->data)) {
			$this->Jwdpddr->create();
			if ($this->Jwdpddr->save($this->data)) {
				$this->Session->setFlash('Jadwal Tenggal Pendadaran telah tersimpan!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
			} else {
				$this->Session->setFlash('Maaf, sistem tidak dapat menyimpan Jadwal Tenggat Pendadaran saat ini!');
				$this->set('data', $this->data);
				$this->render('add');
			}
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update Jadwal Pendadaran');
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		}
		
		if (!empty($this->data)) {
			if ($this->Jwdpddr->save($this->data)) {
				$this->Session->setFlash('Jadwal Tenggat Pendadaran telah diupdate!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
			} else {
				$this->Session->setFlash('Maaf, saat ini sistem belum dapat menyimpan update tanggal tenggat pendadaran!');
				$this->set('data', $this->data);
				$this->render('edit');
			}
		} else {
			$data = $this->Jwdpddr->find('first', array('conditions' => array('Jwdpddr.id' => $id)));
			$this->set('data', $data);
		}
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		}
		
		if ($this->Jwdpddr->delete($id)) {
			$this->Session->setFlash('Jadwal tenggat pendadaran telah dihapus!', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash('Maaf, sistem belum dapat menyimpan jadwal tenggat pendadaran saat ini!', 'default');
		}
		$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
	}
}
?>