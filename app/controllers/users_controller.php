<?php
class UsersController extends AppController {
	public $helpers = array('Html', 'Form', 'Session');
	public $name = 'Users';
	public $uses = array('User', 'Log');
	public $layout = "baseform";
	public $components = array('RequestHandler');
	//public $paginate = array('limit' => 10, 'page' => 1, 'order' => array('User.id'=>'desc'));
	
	function beforeFilter() {
		//cek session dan hak akses
		if($this->Session->check('User') == false ) {
			//redirect ke halaman login dengan error message
			$this->Session->setFlash('You must login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
			exit();
		} else {
			if ($this->action != 'foto' && $this->action != 'edit' && $this->action != 'save' && $this->action != 'chpwd' && $this->action != 'savepwd'):
				//sudah login, periksa hak akses. Untuk mengakses modul ssb perlu privilege 1(admin) 
				if(($this->Session->read('User.group_id') != 1)) {
					//redirect ke halaman home dengan error message : anda tidak diizinkan mengakses modul ini
					$this->Session->setFlash('Sorry, you don\'t have privilege to access user management.');
					$this->redirect(array('controller' => 'admin', 'action' => 'home'));
					exit();
				}
			endif;
		}
	}
	
	//function index() {
	//	$this->set('data', $this->paginate());
	//	$this->set('judul', 'Administration Tools');
	//}
	
	function tambah() {
		$this->set('judul', 'Tambah User Akun');
		if (!empty($this->data)) {
			$this->User->create();
			if ($this->User->save($this->data)) {
				$this->Session->setFlash('The new user have been saved!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
			} else {
				$groups = $this->User->Group->find('list', array('fields' => array('id', 'groupname')));
				$this->set(compact('groups'));
				$this->Session->setFlash('Sorry, system can not save the new user!');
				$this->set('data', $this->data);
				$this->render('tambah');
			}
		} else {
			$groups = $this->User->Group->find('list', array('fields' => array('id', 'groupname')));
			$this->set(compact('groups'));
			$this->render('tambah');
		}
	}
	
	function del($id = null) {
		if (!$id) {
			$this->Session->setFlash('The URL is not valid!');
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		}
		
		if ($this->User->delete($id)) {
			$this->Session->setFlash('The user has been deleted!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));
		}
	}
	
	function save() {
		if (empty($this->data)) {
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		} else {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash('User has been updated.', 'default', array('class' => 'success'));
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
			} else {
				$groups = $this->User->Group->find('list', array('fields' => array('id', 'groupname')));
				$this->set(compact('groups'));
				$this->Session->setFlash('Sorry, system can not save data.', true);
				$this->set('data', $this->data);
				$this->set('err', $this->User->invalidFields());
				$this->render('edit');
			}
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update User Akun');
		if ($id != null):
			if ($this->Session->read('User.group_id') == 1) {
				$data = $this->User->find('first', array('conditions' => array('User.id' => $id)));
			} else {
				$data = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
			}
			$this->set('data', $data);
			$groups = $this->User->Group->find('list', array('fields' => array('id', 'groupname')));
			$this->set(compact('groups'));
		else:
			$this->Session->setFlash('URL is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		endif;
	}
	
	function savepwd() {
		if (empty($this->data)) {
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		} else {
			if ($this->Session->read('User.group_id') == 1) {
				$userid = $this->data['User']['id'];
			} else {
				$userid = $this->Session->read('User.id');
			}
			$user = $this->User->find('first', array('conditions' => array('User.id' => $userid)));
			if (!empty($user)):
				$this->User->id = $user['User']['id'];
				if ($this->User->saveField('password', $this->data['User']['password'])):
					$this->Session->setFlash('User\'s password has been updated.', 'default', array('class' => 'success'));
					$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
				else:
					$this->Session->setFlash('Sorry, system can not save the new password data.', true);
					$this->set('data', $this->data);
					$this->set('err', $this->User->invalidFields());
					$this->render('chpwd');
				endif;
			else:
				$this->Session->setFlash('Sorry, system can not save the new password data.', true);
				$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
			endif;
		}
	}
	
	function chpwd($id = null){
		$this->set('judul', 'Ubah Password');
		if ($id != null):
			if ($this->Session->read('User.group_id') == 1) {
				$data = $this->User->find('first', array('conditions' => array('User.id' => $id)));
			} else {
				$data = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id'))));
			}
			$this->set('data', $data);
		else:
			$this->Session->setFlash('URL is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		endif;
	}
	
	function enable($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		}
		
		$this->User->id = $id;
		$this->User->read();
		$data = $this->User->data;
		if (!empty($data)) {
			if ($this->User->saveField('active', 1)) {
				if (!$data['User']['group_id'] || $data['User']['group_id'] == 0):
					if ($this->User->saveField('group_id', 2)):
						$this->Session->setFlash('The user is activated!', 'default', array('class' => 'success'));
					else:
						$this->Session->setFlash('User ' . $id . ' can not be activated right now!');
					endif;
				else:
					$this->Session->setFlash('The user is activated!', 'default', array('class' => 'success'));
				endif;
			} else {
				$this->Session->setFlash('User ' . $id . ' can not be activated right now!');
			}
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		} else {
			$this->Session->setFlash('User data is not found!');
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		}
	}
	
	function disable($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		}
		
		$this->User->id = $id;
		$this->User->read();
		$data = $this->User->data;
		if (!empty($data)) {
			if ($this->User->saveField('active', 0)) {
				$this->Session->setFlash('The selected user has been disabled!', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('User ' . $id . ' can not be updated!');
			}
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		} else {
			$this->Session->setFlash('The user data is not found!');
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-6'));				
		}
	}
	
	/**
	 * Sends a file to the client
	 *
	 * @param string $id UUID
	 * @access public
	 * Source: http://cakedc.com/florian_kraemer/2010/01/25/file-uploading-file-storage-and-cakephp-mediaview-class
	 */
	function foto($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-2'));
		}
	
		$groupid = $this->Session->read('User.group_id');
		
		$this->User->recursive = -1;
		
		if ($groupid == 1 || $groupid == 3 || $groupid == 4):
			$media = $this->User->find('first', array( 'conditions' => array('User.id' => $id) ) );
		else:
			$media = $this->User->find('first', array( 'conditions' => array('User.id' => $id, 'User.id' => $this->Session->read('User.id')) ) );
		endif;
		
		if (empty($media)) {
			$groupid = $this->Session->read('User.group_id');
			$this->Session->setFlash('File is not found!');
			$this->redirect(array('controller'=>'admin', 'action' => 'home'));
		}
		
		$path_info = pathinfo($media['User']['foto']);
		$params = array(
			'cache' => '3 days',
			'download' => true,
			//'name' => $path_info['foto'],
			'id' => $media['User']['foto'],
			'download' => true,
			'path' => $media['User']['dir'] . DS,
			'modified' => $media['User']['modified'],
			'mimeType' => $media['User']['mimetype'],
			'extension' => $path_info['extension']
		);
		
		$this->view = 'Media';
		$this->set($params);
		$this->autoLayout = false;
		//if ($this->render() !== false) {
		//	$this->Kolokium->updateAll(
		//		array('Kolokium.downloads' => 'Kolokium.downloads + 1'),
		//		array('Kolokium.id' => $id));
		//}
	}
}
?>