<?php
//
// created by Budi Susanto
//
class AdminController extends AppController {
	public $name = 'Admin';
	public $layout = 'base';
	public $uses = array('User', 'Blog', 'Dosen', 'Proposal', 'Kolokium', 'Jadwal','Presensi','Ta', 'Jwdpddr', 'Pendadaran', 'Log', 'Jwddeskevaluation', 'De');
	public $helpers = array('Html', 'Form', 'Tools');
	public $paginate = array(
						 'Blog' => array('limit' => 10, 'page' => 1, 'order' => array('Blog.created'=>'desc')),
						 'Proposal' => array('limit' => 15, 'page' => 1, 'order' => array('Proposal.modified'=>'desc')),
						 'User' => array('limit' => 15, 'page' => 1, 'order' => array('User.id'=>'desc')),
						 'Dosen' => array('limit' => 15, 'page' => 1, 'order' => array('Dosen.nidn'=>'asc')),
						 'Kolokium' => array('limit' => 15, 'page' => 1, 'order' => array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim'=>'asc')),
						 'Jadwal' => array('limit' => 10, 'page' => 1, 'order' => array('Jadwal.id'=>'desc')),
						 'Jwdpddr' => array('limit' => 10, 'page' => 1, 'order' => array('Jwdpddr.id'=>'desc')),
						 'Jwddeskevaluation' => array('limit' => 10, 'page' => 1, 'order' => array('Jwdldeskevaluation.id'=>'desc')),
						 'De' => array('limit' => 10, 'page' => 1, 'order' => array('De.tanggal'=>'desc')),
						 'Ta' => array('limit' => 15, 'page' => 1, 'order' => array('Ta.nim' => 'asc')),
						 'Pendadaran' => array('limit' => 15, 'page' => 1, 'order' => array('Pendadaran.jadwal' => 'asc')),
					   );
	
	/*function __dopaging($model="", $limit=0, $order="", $ordermode="", $where=null) {
		$page = $this->pageForPagination($model);
		$this->paginate[$model] = array(
			'limit' => $limit, 
			'order' => array($order => $ordermode),
			'page' => $page
		);
		if ($where) {
			return $this->paginate($model, $where);
		} else {
			return $this->paginate($model);
		}
	}*/

	function __dopaging($model="", $limit=0, $orderby=null, $where=null, $recursive=1) {
		$page = $this->pageForPagination($model);
		$this->paginate[$model] = array(
			'limit' => $limit, 
			'order' => $orderby,
			'page' => $page,
			'recursive' => $recursive
		);
		if ($where) {
			return $this->paginate($model, $where);
		} else {
			return $this->paginate($model);
		}
	}

	function index() {
		if($this->Session->check('User') == true) {
			$this->Session->setFlash('You are already login.', 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			exit();
		}
		$this->redirect(array('controller' => 'main', 'action' => 'index'));
	}
	
	function home($selkol='a') {
		if($this->Session->check('User') == false):
			$this->redirect(array('controller' => 'admin', 'action' => 'index'));
		endif;
		
		$this->set('group', $this->Session->read('User.group_id'));
		$this->set('nimaktif', $this->Session->read('User.nim'));
		
		if ($this->Session->read('User.group_id') == 1):
			$this->set('judul', 'Administration Tools');
			
			$this->set('blogs', $this->__dopaging('Blog', 10, array('Blog.created' => 'desc')) );
			$this->set('jadwals',  $this->__dopaging('Jadwal', 10, array('Jadwal.id' => 'desc')) );
			$this->set('jwdpddrs',  $this->__dopaging('Jwdpddr', 10, array('Jwdpddr.id' => 'desc')) );
			$this->set('jwddeskevaluations',  $this->__dopaging('Jwddeskevaluation', 10, array('Jwddeskevaluation.id' => 'desc')) );
			$this->set('des',  $this->__dopaging('De', 10, array('De.tanggal' => 'desc')) );
			
			$jdwls = $this->Jadwal->find('all', 
						array('fields' => array('id', 'tanggal'), 
							   'conditions'=>array('Jadwal.batas >= ' => date('Y-m-d') ), 
							   'limit' => 6, 'order' => 'tanggal asc' ) );
			$this->set('jdwls', $jdwls);
			
			$all_jadwal = $this->Jadwal->find('all', array('fields' => array('id', 'tanggal'), 'limit' => 10, 'order' => 'tanggal desc' ) );
			$this->set('all_jadwal', $all_jadwal);
			
			$all_jwdde = $this->De->find('all', array('fields' => array('id', 'tanggal', 'kelompok'), 'limit' => 10, 'order' => 'tanggal desc' ) );
			$this->set('all_jwdde', $all_jwdde);
			
			$jdwlpendadaran = $this->Jwdpddr->find('list', 
					array('fields' => array('id', 'batasakhir'), 
						'conditions'=>array('Jwdpddr.batas >= ' => date('Y-m-d H:i:s') ), 
						'limit' => 10, 'order' => 'Jwdpddr.batas desc' ) );
			$this->set('jdwlpendadaran', $jdwlpendadaran);
			
			$alljdwldadar = $this->Jwdpddr->find('all', 
					array('fields' => array('id', 'batasakhir'), 
						  'limit' => 10, 'order' => 'Jwdpddr.batas desc' ) );
			$this->set('alljdwldadar', $alljdwldadar);
			
			$this->set('presensis', array());
			
			$alldosens = $this->Dosen->find('all', array('order' => array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc')) );
			$this->set(compact('alldosens'));
			
			$listdosens = $this->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('status' => array('K', 'S') ) ));
			$this->set(compact('listdosens'));
			
			$this->set('tipepresensi', 1);

			// menangani dosen, users, kolokium
			if (empty($this->data)):
				if (empty($this->params['named'])):
					$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0) ) );
					$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc') ) );
					$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc') ) );
					$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc') ) );
					$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc') ) );
					$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc')) );
				else:
					if (!empty($this->params['named']['q']) && 
					    !empty($this->params['named']['o']) && 
					    !empty($this->params['named']['m'])):
					    
						$this->set('o', $this->params['named']['o']);
						$this->set('q', $this->params['named']['q']);
						$this->set('m', $this->params['named']['m']);
						
						if ($this->params['named']['m'] == 'proposal'):
							$this->set('pilihproposal', $this->params['named']['o']);
							if ($this->params['named']['o'] == 'nim'):
								$where = array('User.nim like' => '%' . $this->params['named']['q'] . '%', 'Proposal.status_proposal <> ' => 0);
							elseif ($this->params['named']['o'] == 'judul'):
								$where = array('Proposal.judul like' => '%' . $this->params['named']['q'] . '%', 'Proposal.status_proposal <> ' => 0);
							elseif ($this->params['named']['o'] == 'nama'):
								$where = array('User.fullname like' => '%' . $this->params['named']['q'] . '%', 'Proposal.status_proposal <> ' => 0);
							elseif ($this->params['named']['o'] == 'dosen'):
								$where = array('lower(Dosen.nama) like' => '%' . strtolower($this->params['named']['q']) . '%', 'Proposal.status_proposal <> ' => 0);
							/*elseif ($this->params['named']['o'] == 'tglde'):
								$where = array('De.tanggal' => date('Y-m-d', strtotime($this->params['named']['q'])), 'Proposal.status_proposal <> ' => 0);*/
							else:
								$where = array();
							endif;

							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), $where,1 ) );
							$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc') ) );
							$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc')) );
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc')) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc') ) );
							$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc') ) );
							
					    elseif ($this->params['named']['m'] == 'dosen'):
							if ($this->params['named']['o'] == 'nidn'):
								$where = array('Dosen.nidn like' => '%' . $this->params['named']['q'] . '%');
							else:
								$where = array('Dosen.nama like' => '%' . $this->params['named']['q'] . '%');
							endif;
							
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0) ) );
							$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc'), $where) );
							$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc')) );
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc')) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc') ) );
							$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc') ) );
							
						elseif ($this->params['named']['m'] == 'user'):
							if ($this->params['named']['o'] == 'nim'):
								$where = array('User.nim like' => '%' . $this->params['named']['q'] . '%');
							else:
								$where = array('User.fullname like' => '%' . $this->params['named']['q'] . '%');
							endif;
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0) ) );
							$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc') ) );
							$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc'), $where) );
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc')) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc')) );
							$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc')) );
							
						elseif ($this->params['named']['m'] == 'kolokium'):
							$this->set('pilihan', $this->params['named']['o']);
							$this->set('pencarian', $this->params['named']['q']);
							
							if ($this->params['named']['o'] == 'nim'):
								$where = array('Kolokium.nim like' => '%' . $this->params['named']['q'] . '%');
							elseif ($this->params['named']['o'] == 'tglkol'):
								$where = array('Jadwal.tanggal like' => '%' . $this->params['named']['q'] . '%');
							else:
								$where = array('Kolokium.judul like' => '%' . $this->params['named']['q'] . '%');
							endif;
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0) ) );
							$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc')) );
							$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc')) );
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc') ) );
							$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc') ) );
							
						elseif ($this->params['named']['m'] == 'presensi'):
							$t = -1;
							if (isset($this->params['named']['t'])) {
								$t = $this->params['named']['t'];
							}
							if ($t == 0) {
								$presensis = $this->Presensi->find('all', 
												array('fields' => array('Presensi.id', 'Presensi.id_dosen', 'Dosen.nama', 
																		 'Presensi.waktu_datang', 'Presensi.waktu_pulang', 'Presensi.lama'), 
													  'conditions' => array('Presensi.id_tglde' => $this->params['named']['o'],
													  						'Presensi.tipe' => $t) ));
							} else {
								$presensis = $this->Presensi->find('all', 
												array('fields' => array('Presensi.id', 'Presensi.id_dosen', 'Dosen.nama', 
																		 'Presensi.waktu_datang', 'Presensi.waktu_pulang', 'Presensi.lama'), 
													  'conditions' => array('Presensi.idtglkol' => $this->params['named']['o'],
													  						'Presensi.tipe' => $t) ));
							}
							$this->set('presensis', $presensis);
							if ($t == 1) {
								$tglpresensi = $this->Jadwal->find('first', array('conditions' => array('Jadwal.id' => $this->params['named']['o']) ));
								$this->set('tglpresensi', $tglpresensi);
							} elseif ($t == 0) {
								$tglpresensi = $this->De->find('first', array('conditions' => array('De.id' => $this->params['named']['o']) ));
								$this->set('tglpresensi', $tglpresensi);
							} else {
								$this->set('tglpresensi', array());
							}
							$this->set('tipepresensi', $t);
							
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0) ) );
							$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc')) );
							$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc')) );
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc')) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc')) );
							$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc')) );
							
						elseif ($this->params['named']['m'] == 'ta'):
							if ($this->params['named']['o'] == 'nim'):
								$where = array('Ta.nim like' => '%' . $this->params['named']['q'] . '%');
							elseif ($this->params['named']['o'] == 'nmmhs'):
								$where = array('Mahasiswa.nama like' => '%' . $this->params['named']['q'] . '%');
							elseif ($this->params['named']['o'] == 'nmdsn'):
								$where = array('Or'=>array('Dosen.nama like' => '%' . $this->params['named']['q'] . '%', 
																									   'Dosen2.nama like' => '%' . $this->params['named']['q'] . '%') 
																				 );
							else:
								$where = array('Ta.judul like' => '%' . $this->params['named']['q'] . '%');
							endif;
							
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0) ) );
							$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc')) );
							$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc')) );
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc')) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'), $where) );
							$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc')) );
						
						elseif ($this->params['named']['m'] == 'dadar'):
							if ($this->params['named']['o'] == 'nim'):
								$where = array('Ta.nim like ' => '%' . $this->params['named']['q'] . '%');
								$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc'), $where) );
							else:
								$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc')) );
							endif;
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0) ) );
							$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc')) );
							$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc')) );
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc')) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc')) );
							
						else:
							$this->Session->setFlash('Permintaan Anda tidak valid!');
							$this->redirect(array('controller' => 'admin', 'action' => 'home'));
						endif;
					else:
						$this->set('presensis', array());
						$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0) ) );
						$this->set('dosens', $this->__dopaging('Dosen', 15, array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc')) );
						$this->set('users', $this->__dopaging('User', 15, array('User.id' => 'asc')) );
						$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc')) );
						$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc')) );
						$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.status' => 'asc', 'Pendadaran.jadwal' => 'asc')) );
						
						/*$page = $this->pageForPagination('User');
						$this->paginate['User'] = array(
							'limit' => 10, 
							'order' => array('User.id'=>'desc'),
							'page' => $page
						);
						$this->set('users', $this->paginate('User') );*/
					endif;
				endif;
			else:
				// bentuk url untuk redirect
				$url = array('controller' => 'admin', 'action' => 'home');
				$page = 'tabs-1';
				if( is_array($this->data['Cari']) ) {
					if ($this->data['Cari']['m'] == 'dosen'):
						$page = 'tabs-7';
					elseif ($this->data['Cari']['m'] == 'user'):
						$page = 'tabs-6';
					elseif ($this->data['Cari']['m'] == 'kolokium'):
						$page = 'tabs-3';
					elseif ($this->data['Cari']['m'] == 'presensi'):
						$page = 'tabs-9';
					elseif ($this->data['Cari']['m'] == 'ta'):
						$page = 'tabs-4';
					elseif ($this->data['Cari']['m'] == 'dadar'):
						$page = 'tabs-5';
					elseif ($this->data['Cari']['m'] == 'proposal'):
						$page = 'tabs-2';
					endif;
					foreach($this->data['Cari'] as &$cari) {
						if( is_array($cari) ) {
							$cari = base64_encode(serialize($cari));
						} else {
							$cari = urlencode($cari);
						}
					}
				}
				$params = array_merge($url, $this->data['Cari'], array('#' => $page));
				$this->redirect($params);
			endif;
		
		// untuk MAHASISWA
		//
		elseif ($this->Session->read('User.group_id') == 2):
			$this->set('judul', 'Mahasiswa Tools');
			$this->set('blogs', $this->paginate('Blog', array('Blog.published' => '1')) );
			
			// setup untuk profil
			$users = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id') )));
			$this->set('users', $users);
			$groups = $this->User->Group->find('list', array('fields' => array('id', 'groupname')));
			$this->set(compact('groups'));
			$jdwlpendadaran = $this->Jwdpddr->find('list', array('fields' => array('id', 'batasakhir'), 
																 'conditions'=>array('Jwdpddr.batas >= ' => date('Y-m-d H:i:s') ), 
							   								     'limit' => 6, 'order' => 'Jwdpddr.batas asc' ) );
			$this->set('jdwlpendadaran', $jdwlpendadaran);
			
			$jwddeskevaluationlist = $this->Jwddeskevaluation->find('list', array('fields' => array('id', 'batasakhir'), 'conditions' => array('Jwddeskevaluation.batas >= ' => date('Y-m-d H:i:s')) ));
			$this->set(compact('jwddeskevaluationlist'));
			
			$dadars = $this->Pendadaran->find('all', array('conditions' => array('Ta.nim' => $this->Session->read('User.nim') ) ));
			$this->set('dadars', $dadars);
			
			$this->set('tas', $this->paginate('Ta', array('Ta.nim' => $this->Session->read('User.nim') ) ));
			$this->set('last_ta', $this->Ta->find('first', array(
																'conditions' => array('Ta.nim' => $this->Session->read('User.nim')),
																'order' => array('Ta.id DESC')
															)));
			
			$this->set('jenisfiles', $this->Ta->Tafile->Jenisfile->find('all'));
			
			// menangani dosen, users, kolokium
			if (empty($this->data)):
				$this->set('proposals', $this->paginate('Proposal', array('Proposal.userid' => $this->Session->read('User.id') ) ) );
				$this->set('kolokiums', $this->paginate('Kolokium', array('Kolokium.nim' => $this->Session->read('User.nim') ) ) );
			else:
				// bentuk url untuk redirect
				$url = array('controller' => 'admin', 'action' => 'home');
				$page = 'tabs-1';
				if( is_array($this->data['Cari']) ) {
					if ($this->data['Cari']['m'] == 'kolokium'):
						$page = 'tabs-3';
					elseif ($this->data['Cari']['m'] == 'ta'):
						$page = 'tabs-4';
					elseif ($this->data['Cari']['m'] == 'proposal'):
						$page = 'tabs-2';
					endif;
					foreach($this->data['Cari'] as &$cari) {
						if( is_array($cari) ) {
							$cari = base64_encode(serialize($cari));
						} else {
							$cari = urlencode($cari);
						}
					}
				}
				$params = array_merge($url, $this->data['Cari'], array('#' => $page));
				$this->redirect($params);
			endif;
		
		// untuk DOSEN dan KETUA TIM KOLOKIUM
		//
		elseif ($this->Session->read('User.group_id') == 3 || $this->Session->read('User.group_id') == 4):
			$this->set('judul', 'Dosen Tools');
			$this->set('blogs', $this->paginate('Blog', array('Blog.published' => '1') ) );
			
			// setup untuk profil
			$users = $this->User->find('first', array('conditions' => array('User.id' => $this->Session->read('User.id') )));
			$this->set('users', $users);
			$groups = $this->User->Group->find('list', array('fields' => array('id', 'groupname')));
			$this->set(compact('groups'));
			$this->set('qkol', $selkol);
			if (array_key_exists('qta', $this->params['named'])) {
				$this->set('qta', $this->params['named']['qta']);
				$selta = $this->params['named']['qta'];
			} else {
				$this->set('qta', 'u');
				$selta = 'u';
			}
			$currdosen = $this->Dosen->find('first', array('conditions' => array('Dosen.nidn' => $this->Session->read('User.nim') )));
			$this->set(compact('currdosen'));
			$alldosens = $this->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => array('K', 'S') ),
											'order' => array('Dosen.status' => 'asc', 'Dosen.boleh' => 'asc', 'Dosen.nama' => 'asc') ));
			$this->set(compact('alldosens'));
			
			$this->set('dadars', $this->__dopaging('Pendadaran', 15, array('Pendadaran.jadwal' => 'asc'),
							array(
									'or' => array(
												'Dosen.nidn' => $this->Session->read('User.nim'),
												'Dosen2.nidn' => $this->Session->read('User.nim'),
												'Dosen3.nidn' => $this->Session->read('User.nim'),
												'Dosen4.nidn' => $this->Session->read('User.nim'),
											),
									'Pendadaran.status' => 1
								) 
						) );
			
			$this->set('presensis', array());
			$all_jadwal = $this->Jadwal->find('all', 
							array('fields' => array('id', 'tanggal'), 'limit' => 10, 'order' => 'tanggal desc' ) );
			$this->set('all_jadwal', $all_jadwal);
			
			$all_jwdde = $this->De->find('all', array('fields' => array('id', 'tanggal', 'kelompok'), 'limit' => 10, 'order' => 'tanggal desc' ) );
			$this->set('all_jwdde', $all_jwdde);
			
			$this->set('tipepresensi', 1);

			// menangani dosen, users, kolokium
			if (empty($this->data)) {
				if (empty($this->params['named'])) {
					// untuk kolokium
					if ($selkol == 'u'):
						$where = array('or' => array('Dosen.nidn' => $this->Session->read('User.nim'), 
													'Dosen2.nidn' => $this->Session->read('User.nim') ), 
										 'Kolokium.status' => array('L', 'V', 'B', 'S') 
										);
					else:
						$where = array('Kolokium.status' => array('L', 'V', 'B', 'S'));
					endif;
					$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where, 2) );
					
					// untuk skripsi
					if ($selta == 'u') {
						$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'),
									array('Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
														 'Dosen2.nidn' => $this->Session->read('User.nim') ),
											      'aktif' => 1
										)
								) );
					} else if ($selta == 's') {
						$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'),
									array('Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
														 'Dosen2.nidn' => $this->Session->read('User.nim') ),
											'aktif' => array(0, 1),
											'sem <>' => 0
										)
								) );
					} else {
						$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'), array('aktif' => 1)) );
					}
					
					// untuk lainnya
					$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0), 2 ) );
				} else { 
					if (!empty($this->params['named']['q']) && 
					    !empty($this->params['named']['o']) && 
					    !empty($this->params['named']['m'])) {
					    $this->set('m', $this->params['named']['m']);
					    $this->set('q', $this->params['named']['q']);
					    $this->set('o', $this->params['named']['o']);
					    
						if ($this->params['named']['m'] == 'kolokium'):
							$this->set('pilihan', $this->params['named']['o']);
							$this->set('pencarian', $this->params['named']['q']);
							$this->set('qkol', $this->params['named']['qkol']);
							$selkol = $this->params['named']['qkol'];
							
							if ($this->params['named']['o'] == 'nim'):
								if ($selkol == 'u') {
									$where = array('Kolokium.nim like' => '%' . $this->params['named']['q'] . '%',
																								'or' => array('Dosen.nidn' => $this->Session->read('User.nim'), 
																													  'Dosen2.nidn' => $this->Session->read('User.nim') ) , 
																								'Kolokium.status' => array('L', 'V', 'B', 'S')
																							);
								} else {
									$where = array('Kolokium.nim like' => '%' . $this->params['named']['q'] . '%', 
													 'Kolokium.status' => array('L', 'V', 'B', 'S')
												);
								}
								$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where, 2) );
							elseif ($this->params['named']['o'] == 'tglkol'):
								if ($selkol == 'u') {
									$where = array('Jadwal.tanggal like' => '%' . $this->params['named']['q'] . '%',
														'or' => array('Dosen.nidn' => $this->Session->read('User.nim'), 
																			  'Dosen2.nidn' => $this->Session->read('User.nim') ), 
														'Kolokium.status' => array('L', 'V','B', 'S')
													);
								} else {
									$where = array('Jadwal.tanggal like' => '%' . $this->params['named']['q'] . '%', 
														 'Kolokium.status' => array('L', 'V', 'B', 'S')
													);
								}
								$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where, 2) );
							else:
								if ($selkol == 'u') {
									$where = array('Kolokium.judul like' => '%' . $this->params['named']['q'] . '%',
														'or' => array('Dosen.nidn' => $this->Session->read('User.nim'), 
																			  'Dosen2.nidn' => $this->Session->read('User.nim') ), 
														'Kolokium.status' => array('L', 'V', 'B', 'S')
													);
								} else {
									$where = array('Kolokium.judul like' => '%' . $this->params['named']['q'] . '%', 
												 'Kolokium.status' => array('L', 'V', 'B', 'S')
												);
								}
								$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where, 2) );
							endif;
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'), array('Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
																						'Dosen2.nidn' => $this->Session->read('User.nim') ), 
																						'aktif'=>1 ) ) );
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0), 2 ) );
						elseif ($this->params['named']['m'] == 'ta'):
							if (array_key_exists('qta', $this->params['named'])) {
								$this->set('qta', $this->params['named']['qta']);
								$selta = $this->params['named']['qta'];
							} else {
								$this->set('qta', 'u');
								$selta = 'u';
							}
							
							if ($this->params['named']['o'] == 'nim'):
								if ($selta == 'u') {
									$where = array('Ta.nim like' => '%' . $this->params['named']['q'] . '%',
													'Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
																			'Dosen2.nidn' => $this->Session->read('User.nim') ), 'aktif'=>1
												);
								} else if ($selta == 's') {
									$where = array('Ta.nim like' => '%' . $this->params['named']['q'] . '%',
													'Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
																	'Dosen2.nidn' => $this->Session->read('User.nim') ),
													'aktif' => array(0, 1),
													'sem <>' => 0
												);
								} else {
									$where = array('Ta.nim like' => '%' . $this->params['named']['q'] . '%', 'aktif'=>1 );
								}
							else:
								if ($selta == 'u') {
									$where = array('Ta.judul like' => '%' . $this->params['named']['q'] . '%',
													'Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
																   'Dosen2.nidn' => $this->Session->read('User.nim') ), 'aktif'=>1
												);
								} else if ($selta == 's') {
									$where = array('Ta.judul like' => '%' . $this->params['named']['q'] . '%',
														'Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
																	 'Dosen2.nidn' => $this->Session->read('User.nim') ),
														'aktif' => array(0, 1),
														'sem <>' => 0
													);
								} else {
									$where = array('Ta.judul like' => '%' . $this->params['named']['q'] . '%', 'aktif'=>1);
								}
							endif;
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'), $where ));
							if ($selkol == 'u'):
								$where = array('or' => array('Dosen.nidn' => $this->Session->read('User.nim'), 
															'Dosen2.nidn' => $this->Session->read('User.nim') ), 
												 'Kolokium.status' => array('L', 'V', 'B', 'S') 
												);
							else:
								$where = array('Kolokium.status' => array('L', 'V', 'B', 'S'));
							endif;
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where,2) );
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0), 2 ) );
							
						elseif ($this->params['named']['m'] == 'presensi'):
							$t = -1;
							if (isset($this->params['named']['t'])) {
								$t = $this->params['named']['t'];
							}
							if ($t == 0) {
								$presensis = $this->Presensi->find('all', 
												array('fields' => array('Presensi.id', 'Presensi.id_dosen', 'Dosen.nama', 
																		 'Presensi.waktu_datang', 'Presensi.waktu_pulang', 'Presensi.lama'), 
													  'conditions' => array('Presensi.id_tglde' => $this->params['named']['o'],
													  						'Presensi.tipe' => $t) ));
							} else {
								$presensis = $this->Presensi->find('all', 
												array('fields' => array('Presensi.id', 'Presensi.id_dosen', 'Dosen.nama', 
																		 'Presensi.waktu_datang', 'Presensi.waktu_pulang', 'Presensi.lama'), 
													  'conditions' => array('Presensi.idtglkol' => $this->params['named']['o'],
													  						'Presensi.tipe' => $t) ));
							}
							$this->set('presensis', $presensis);
							if ($t == 1) {
								$tglpresensi = $this->Jadwal->find('first', array('conditions' => array('Jadwal.id' => $this->params['named']['o']) ));
								$this->set('tglpresensi', $tglpresensi);
							} elseif ($t == 0) {
								$tglpresensi = $this->De->find('first', array('conditions' => array('De.id' => $this->params['named']['o']) ));
								$this->set('tglpresensi', $tglpresensi);
							} else {
								$this->set('tglpresensi', array());
							}
							$this->set('tipepresensi', $t);

							/*$presensis = $this->Presensi->find('all', 
															array('fields' => array('Presensi.id', 'Presensi.id_dosen', 'Dosen.nama', 
																					'Presensi.waktu_datang', 'Presensi.waktu_pulang', 'Presensi.lama'), 
																  'conditions' => array('Presensi.idtglkol' => $this->params['named']['o']) ));
							$this->set('presensis', $presensis);
							$tglpresensi = $this->Jadwal->find('first', array('conditions' => array('Jadwal.id' => $this->params['named']['o']) ));
							$this->set('tglpresensi', $tglpresensi); */
							
							if ($selkol == 'u'):
								$where = array('or' => array('Dosen.nidn' => $this->Session->read('User.nim'), 
															'Dosen2.nidn' => $this->Session->read('User.nim') ), 
												 'Kolokium.status' => array('L', 'V', 'B', 'S') 
												);
							else:
								$where = array('Kolokium.status' => array('L', 'V', 'B', 'S'));
							endif;
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0), 2 ) );
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where, 2) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'),
											array('Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
																 'Dosen2.nidn' => $this->Session->read('User.nim') ),
														  'aktif' => 1
												)
										) );
						
						elseif ($this->params['named']['m'] == 'proposal'):
							if ($this->params['named']['o'] == 'nim'):
								$where = array('User.nim like' => '%' . $this->params['named']['q'] . '%', 'Proposal.status_proposal <> ' => 0);
							elseif ($this->params['named']['o'] == 'judul'):
								$where = array('Proposal.judul like' => '%' . $this->params['named']['q'] . '%', 'Proposal.status_proposal <> ' => 0);
							else:
								$where = array('User.fullname like' => '%' . $this->params['named']['q'] . '%', 'Proposal.status_proposal <> ' => 0);
							endif;
							$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), $where, 2 ) );
							
							if ($selkol == 'u'):
								$where = array('or' => array('Dosen.nidn' => $this->Session->read('User.nim'), 
															'Dosen2.nidn' => $this->Session->read('User.nim') ), 
												 'Kolokium.status' => array('L', 'V', 'B', 'S') 
												);
							else:
								$where = array('Kolokium.status' => array('L', 'V', 'B', 'S'));
							endif;
							$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where, 2) );
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'),
											array('Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
																 'Dosen2.nidn' => $this->Session->read('User.nim') ),
														  'aktif' => 1
												)
										) );
						else:
							$this->Session->setFlash('Permintaan Anda tidak valid!');
							$this->redirect(array('controller' => 'admin', 'action' => 'home'));
						endif;
						
					} else {
						if ($selkol == 'u'):
							$where = array('or' => array('Dosen.nidn' => $this->Session->read('User.nim'), 
														'Dosen2.nidn' => $this->Session->read('User.nim') ), 
											 'Kolokium.status' => array('L', 'V', 'B', 'S') 
											);
						else:
							$where = array('Kolokium.status' => array('L', 'V', 'B', 'S'));
						endif;
						$this->set('kolokiums', $this->__dopaging('Kolokium', 15, array('Kolokium.tgl_kolokium' => 'desc', 'Kolokium.nim' => 'asc'), $where, 2) );
						$this->set('proposals', $this->__dopaging('Proposal', 15, array('Proposal.modified' => 'desc'), array('Proposal.status_proposal <> ' => 0), 2 ) );
						if ($selta == 'u') {
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'),
									array('Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
														 'Dosen2.nidn' => $this->Session->read('User.nim') ),
											      'aktif' => 1
										)
								) );
						} else if ($selta == 's') {
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'),
										array('Or' => array( 'Dosen.nidn' => $this->Session->read('User.nim'), 
															 'Dosen2.nidn' => $this->Session->read('User.nim') ),
												'aktif' => array(0, 1),
												'sem <>' => 0
											)
									) );
						} else {
							$this->set('tas', $this->__dopaging('Ta', 15, array('Ta.nim' => 'asc'), array('aktif' => 1)) );
						}
					}
				}
			} else {
				// bentuk url untuk redirect
				$url = array('controller' => 'admin', 'action' => 'home');
				$page = 'tabs-1';
				if( is_array($this->data['Cari']) ) {
					if ($this->data['Cari']['m'] == 'kolokium'):
						$page = 'tabs-3';
					elseif ($this->data['Cari']['m'] == 'ta'):
						$page = 'tabs-4';
					elseif ($this->data['Cari']['m'] == 'presensi'):
						$page = 'tabs-7';
					elseif ($this->data['Cari']['m'] == 'proposal'):
						$page = 'tabs-2';
					endif;
					foreach($this->data['Cari'] as &$cari) {
						if( is_array($cari) ) {
							$cari = base64_encode(serialize($cari));
						} else {
							$cari = urlencode($cari);
						}
					}
				}
				$params = array_merge($url, $this->data['Cari'], array('#' => $page));
				$this->redirect($params);
			}
		else:
			$this->set('judul', 'Unknown');
			$this->redirect(array('controller' => 'main', 'action' => 'logout'));
		endif;
	}
}
?>