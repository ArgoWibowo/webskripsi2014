<?php
/**
 * 
 * @author budi susanto
 *
 */
class DesController extends AppController 
{
	public $name = 'Des';
	public $layout = 'baseform';
	public $helpers = array('Html', 'Form', 'Tools');
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		} else {
			if(($this->Session->read('User.group_id') != 1)) {
				if ($this->Session->read('User.group_id') == 3 && $this->action == 'getalldates') {
					return true;
				}
				$this->Session->setFlash('Sorry, you don\'t have any privileges to access this page.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			}
		}
	}
	
	function add() {
		$this->set('judul', 'Tambah Desk Evaluation');
		$jwddeskevaluationlist = $this->De->Jwddeskevaluation->find('list', array('fields' => array('id', 'batasakhir'), 'limit'=>5, 'order' => array('Jwddeskevaluation.batas desc') ));
		$this->set(compact('jwddeskevaluationlist'));
		$dosens = $this->De->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
		$this->set(compact('dosens'));

		if (!empty($this->data)) {
			$this->De->create();
			if ($this->De->save($this->data)) {
				$this->Session->setFlash('Jadwal Pelaksanaan Desk Evaluation telah ditambahkan!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
			} else {
				$this->Session->setFlash('Maaf, sistem tidak dapat menambahkan data Jadwal Pelaksanaan Desk Evaluation!');
				$this->set('data', $this->data);
				$this->render('add');
			}
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update Desk Evaluation');		
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		}
		
		$jwddeskevaluationlist = $this->De->Jwddeskevaluation->find('list', array('fields' => array('id', 'batasakhir'), 'limit'=>5, 'order' => array('Jwddeskevaluation.batas desc') ));
		$this->set(compact('jwddeskevaluationlist'));
		$dosens = $this->De->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
		$this->set(compact('dosens'));

		if (!empty($this->data)) {
			if ($this->De->save($this->data)) {
				$this->Session->setFlash('Jadwal Pelaksanaan Desk Evaluation telah terupdate!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
			} else {
				$this->Session->setFlash('Maaf sistem tidak dapat menyimpan perubahan data Jadwal Pelaksanaan Desk Evaluation yang Anda lakukan!');
				$this->set('data', $this->data);
				$this->render('edit');
			}
		} else {
			$data = $this->De->find('first', array('conditions' => array('De.id' => $id)));
			$this->set('data', $data);
		}
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		}
		
		if ($this->De->delete($id)) {
			$this->Session->setFlash('Jadwal Pelaksanaan Desk Evaluation terpilih telah terhapus!', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash('Jadwal Pelaksanaan  Desk Evaluation terpilih tidak dapat dihapus!', 'default');
		}
		$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
	}
	
	function catat($id=null) {
		$this->set('judul', 'Catatan Desk Evaluation');
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		} else {
			$data = $this->De->find('first', array('conditions' => array('De.id' => $id), 'recursive' => 2 ));
			$this->set(compact('data'));
			$dataproposal = array();
			foreach($data['Proposal'] as $p){
				array_push($dataproposal, $p['id']);
			}
			$proposals = $this->De->Proposal->find('all', array(
									'recursive' => 2,
									//'conditions' => array('Proposal.status_proposal' => 1, array('NOT' => array('Proposal.id' => $dataproposal))) 
									'conditions' => array('Proposal.status_proposal' => 1) 
								));
			$this->set(compact('proposals'));
		}
	}
	
	function simpancatat() {
		if ($this->data) {
			$this->De->id = $this->data['De']['id'];
			if ($this->De->save($this->data)) {
				$this->Session->setFlash('Setup Data Kelompok Desk Evaluation telah disimpan!', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('Setup Data Kelompok Desk Evaluation tidak dapat disimpan saat ini!');
			}
		} else {
			$this->Session->setFlash('Your request is not valid!');
		}
		$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
	}
	
	function getdates() {
		$this->layout = 'ajax';
		$data = $this->Jwddeskevaluation->find('list', array('fields' => array('id', 'tanggal'), 'conditions' => array('Jwddeskevaluation.batas >= ' => date('Y-m-d H:i:s')) ));
		if (!empty($data)):
			$d['Status']['return'] = 0;
			$d['Status']['msg'] = $data;
		else:
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Tidak ada tenggat waktu untuk Desk Evaluation!';
		endif;
		$this->set(compact('d'));
	}

	function getalldates() {
		$this->layout = 'ajax';
		$data = $this->De->find('all', array('recursive' => 2, 'order' => array('De.tanggal DESC') ));
		$res = array();
		foreach($data as $dt) {
			$temp = array();
			$temp['id'] = $dt['De']['id'];
			$temp['kelompok'] = $dt['De']['kelompok'];
			$temp['tanggal'] = $dt['De']['pelaksanaan'];
			if (!empty($dt['Dosen'])) {
				$i = 0;
				foreach($dt['Dosen'] as $dosen) {
					$temp['Dosen'][$i]['id'] = $dosen['id'];
					$temp['Dosen'][$i]['nidn'] = $dosen['nidn'];
					$temp['Dosen'][$i]['nama'] = $dosen['nama_dosen'];
					$i++;
				}
			} else {
				$temp['Dosen'] = array();
			}
			array_push($res, $temp);
		}
		$d = array();
		if (!empty($data)):
			$d['Status']['return'] = 0;
			$d['Status']['msg'] = $res;
		else:
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Tidak ada waktu untuk Pelaksanaan Desk Evaluation!';
		endif;
		$this->set(compact('d'));
	}
}
?>