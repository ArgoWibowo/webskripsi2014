<?php
class PapersController extends AppController {
	public $name = 'Papers';
	public $uses = array('Proposal', 'Kolokium', 'Jadwal', 'Dosen', 'User', 'Ta', 'Mahasiswa');
	public $layout = 'baseform';
	public $helpers = array('Html', 'Form', 'Tools');
	var $components = array('RequestHandler');
	
	function beforeFilter() {
		if($this->Session->check('User') == false && ($this->action != 'viewpdf' && $this->action != 'report' )) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		}
		$groupid = $this->Session->read('User.group_id');
		if ($this->action == 'status' || $this->action == 'prnletters' || $this->action == 'prnletter' || $this->action == 'modifba') {
			if ($groupid != 1):
				$this->Session->setFlash('Anda tidak memiliki hak akses untuk fungsi ini.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
			endif;
		}
		if ($this->action == 'ceknim' || $this->action == 'prnba') {
			if ($groupid != 1 && $groupid != 3):
				$this->Session->setFlash('Anda tidak memiliki hak akses untuk fungsi ini.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
			endif;
		}
		if ($this->action == 'bakol') {
			if ($groupid != 4 && $groupid != 3 && $groupid != 1):
				$this->Session->setFlash('Anda tidak memiliki hak akses untuk fungsi ini.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
			endif;
		}
	}
	
	protected function __randomPath($string, $level = 3) {
		if (!$string) {
			throw new Exception(__('First argument is not a string!', true));
		}
 
		$string = crc32($string);
		$decrement = 0;
		$path = null;
 
		for ($i = 0; $i < $level; $i++) {
			$decrement = $decrement -2;
			$path .= sprintf("%02d" . DS, substr('000000' . $string, $decrement, 2));
		}
 
		return $path;
	}
	
	function add($id=null) {
		$this->set('judul', 'Tambah Kolokium');
		if (!empty($this->data)) {
			$ok = false;
			$msg = 'Maaf, saat ini sistem tidak dapat menyimpan proposal Anda! Silahkan dicoba kembali beberapa saat lagi.';
			if (!empty($this->data['Topik']['Topik'])) {
				if ($this->Session->read('User.group_id') == 2):
					$this->data['Kolokium']['nim'] = $this->Session->read('User.nim');
				endif;
				if ($this->Session->read('User.group_id') == 2 || $this->Session->read('User.group_id') == 1):
					$this->data['Kolokium']['userid'] = $this->Session->read('User.id');
				endif;
				$this->data['Kolokium']['status'] = 'N';
				$this->data['Kolokium']['tglentry'] = date( 'Y-m-d H:i:s' );
				$this->data['Kolokium']['modified'] = date( 'Y-m-d H:i:s' );
				
				if (is_uploaded_file($this->data['Kolokium']['proposal']['tmp_name'])):
					$this->Kolokium->create();
					if ($this->Kolokium->save($this->data)) {
						$ok = true;
					}
				endif;
			} else {
				$msg = 'Silahkan pilih satu atau lebih topik yang sesuai dengan usulan judul TA Anda!';
			}
			
			if ($ok) {
				$this->Session->setFlash('Proposal TA Anda sudah tersimpan. Terima kasih.', 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
			} else {
				$this->Session->setFlash($msg);
				$jadwals = $this->Jadwal->find('list', array('fields' => array('id', 'waktu'), 'conditions' => array('date_format(batas, "%Y-%m-%d %H:%i") >=' => date('Y-m-d H:i') ) ));
				//$jadwals = $this->Jadwal->find('list', array('fields' => array('id', 'waktu'), 'conditions' => array('status' => 'N') ));
				$this->set(compact('jadwals'));
				$dosens = $this->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
				$this->set(compact('dosens'));
				$topiks = $this->Kolokium->Topik->find('list');
				$this->set(compact('topiks'));
				
				$this->set('data', $this->data);
				$this->set('errors', $this->Kolokium->invalidFields());
				$this->render('add');
			}
		} else {
			if ($id == null) {
				$this->Session->setFlash('Maaf Anda tidak diperkenankan untuk mendaftar Kolokium');
				$this->redirect(array('controller'=>'admin', 'action'=>'home', '#tabs-3'));
			}
			$proposal = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id, 'Proposal.userid' => $this->Session->read('User.id') )));
			$this->set(compact('proposal'));
			$this->set('numkolo',  1);
			if(($this->Session->read('User.group_id') == 2)):
				$isaktif = $this->Mahasiswa->find('first', array('conditions' => array('Mahasiswa.nim' => $this->Session->read('User.nim'), 'Mahasiswa.status' => 1 ) ) );
				if (empty($isaktif)) {
					$this->Session->setFlash('Maaf Anda tidak diijinkan untuk menambah data usulan judul kolokium baru, karena status Mahasiswa Anda tidak aktif!');
					$this->redirect(array('controller'=>'admin', 'action'=>'home', '#tabs-3'));
				}
				
				$isok = $this->Kolokium->find('all', array(	'fields' => array('Kolokium.id, Kolokium.nim', 'Kolokium.status, Kolokium.kolokium_ke'), 
																					'recursive' => -1,
																					'order' => array('Kolokium.nim, Kolokium.tglentry'),
																					'conditions' => array('Kolokium.nim' => $this->Session->read('User.nim') ) 
																				));
				$numrows = $this->Kolokium->getNumRows();
				if ($numrows > 0):
					$lastkolo = $isok[$numrows-1];
					// cek apakah status terakhir adalah baru?
					if ($lastkolo['Kolokium']['status'] == 'N' || $lastkolo['Kolokium']['status'] == 'V'):
						$this->Session->setFlash('Maaf Anda tidak diijinkan untuk menambah data usulan judul kolokium baru, karena sebelumnya masih ada berstatus BARU atau VALID!');
						$this->redirect(array('controller'=>'admin', 'action'=>'home', '#tabs-3'));
					else:
						// cek apakah status terakhir adalah sudah lolos?
						if ($lastkolo['Kolokium']['status'] == 'L' ):
							$this->Session->setFlash('Dengan pendaftaran ini, Anda diwajibkan untuk membayar uang kolokium!', 'default', array('class' => 'success'));
							$this->set('numkolo', '1');
						else:
							$kolke = $lastkolo['Kolokium']['kolokium_ke'] + 1;
							if ($kolke > 3):
								$this->Session->setFlash('Dengan pendaftaran ini, Anda diwajibkan untuk membayar uang kolokium!', 'default', array('class' => 'success'));
								$this->set('numkolo',  1);
							else:
								$this->set('numkolo',  $kolke);
							endif;
						endif;
					endif;
				else:
					$this->Session->setFlash('Dengan pendaftaran ini, Anda diwajibkan untuk membayar uang kolokium!', 'default', array('class' => 'success'));
					$this->set('numkolo',  1);
				endif;
			endif;
			
			$jadwals = $this->Jadwal->find('list', array('fields' => array('id', 'waktu'), 'conditions' => array('date_format(batas, "%Y-%m-%d %H:%i") >=' => date('Y-m-d H:i') ) ));
			$this->set(compact('jadwals'));
			$dosens = $this->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
			$this->set(compact('dosens'));
			$topiks = $this->Kolokium->Topik->find('list');
			$this->set(compact('topiks'));
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update Kolokium');
		if (!empty($this->data)) {
			$isOK = false;
			$msg = 'Maaf sistem SkripSI belum dapat menyimpan perubahan data dari Anda!';
			if (!empty($this->data['Topik']['Topik'])):
				$this->data['Kolokium']['modified'] = date( 'Y-m-d H:i:s' );
				if ($this->Kolokium->save($this->data)):
					$this->Session->setFlash('Data Kolokium terpilih telah diupdate!', 'default', array('class' => 'success'));
					$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
					$isOK = true;
				endif;
			else:
				$msg = "Silahkan pilih topik sesuai dengan usulan judul TA Anda!";
			endif;
			if (!$isOK) {
				$this->Session->setFlash($msg);
				if ($this->Session->read('User.group_id') == 2) {
					$jadwals = $this->Jadwal->find('list', array('fields' => array('id', 'waktu'), 'conditions' => array('date_format(batas, "%Y-%m-%d %H:%i") >=' => date('Y-m-d H:i') ) ));
				} else {
					$jadwals = $this->Jadwal->find('list', array('fields' => array('id', 'waktu'), 'limit' => 10, 'order' => 'batas DESC' ));
				}
				//$jadwals = $this->Jadwal->find('list', array('fields' => array('id', 'tanggal'), 'conditions' => array('status' => 'N') ));
				$this->set(compact('jadwals'));
				$dosens = $this->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
				$this->set(compact('dosens'));
				$topiks = $this->Kolokium->Topik->find('list');
				$this->set(compact('topiks'));
				
				$this->set('data', $this->data);
				$this->set('err', $this->Kolokium->invalidFields());
				$this->render('edit');
			}
		} else {
			if (!$id) {
				$this->Session->setFlash('Your request is not valid!');
				$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			}
			
			if(($this->Session->read('User.group_id') == 1)) {
				$data = $this->Kolokium->find('first', array('conditions' => array('Kolokium.id' => $id)));
			} else {
				$data = $this->Kolokium->find('first', array('conditions' => array('Kolokium.id' => $id, 'Kolokium.userid' => $this->Session->read('User.id'))));
			}
			if ($this->Session->read('User.group_id') == 2) {
				if (!empty($data) && ($data['Kolokium']['status'] == 'N' || $data['Kolokium']['status'] == 'V')):
					$jadwals = $this->Jadwal->find('list', array('fields' => array('id', 'waktu'), 'conditions' => array('date_format(batas, "%Y-%m-%d %H:%i") >=' => date('Y-m-d H:i') ) ));
					$this->set(compact('jadwals'));
					$dosens = $this->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
					$this->set(compact('dosens'));
					$topiks = $this->Kolokium->Topik->find('list');
					$this->set(compact('topiks'));
					
					$this->set('err', $this->Kolokium->invalidFields());
					$this->set('data', $data);
				else:
					$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani perubahan data kolokium yang Anda minta!');
					$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
				endif;
			} elseif ($this->Session->read('User.group_id') == 1) {
				if (!empty($data) && ($data['Kolokium']['status'] != 'N')):
					$jadwals = $this->Jadwal->find('list', array('fields' => array('id', 'waktu'), 'limit' => 10, 'order' => 'batas DESC' ));
					$this->set(compact('jadwals'));
					$dosens = $this->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
					$this->set(compact('dosens'));
					$topiks = $this->Kolokium->Topik->find('list');
					$this->set(compact('topiks'));
					
					$this->set('err', $this->Kolokium->invalidFields());
					$this->set('data', $data);
				else:
					$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani perubahan data kolokium yang Anda minta!');
					$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
				endif;
			}
		}
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		}
		
		if ($this->Session->read('User.group_id') == 1) {
			$data = $this->Kolokium->find('first', array('conditions' => array('Kolokium.id' => $id)));
		} else {
			$data = $this->Kolokium->find('first', array('conditions' => array('Kolokium.id' => $id, 'Kolokium.userid' => $this->Session->read('User.id'))));
		}
		if (!empty($data)):
			$boleh = false;
			if ($this->Session->read('User.group_id') == 1) {
				$boleh = true;
			} elseif ($this->Sesion->read('User.group_id') == 2 && $data['Kolokium']['status'] == 'N') {
				$boleh = true;
			}
			if ($boleh && $this->Kolokium->delete($id)) {
				$this->Session->setFlash('The selected paper has been deleted!', 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
			} else {
				$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani penghapusan data kolokium yang Anda minta!');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
			}
		else:
			$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani penghapusan data kolokium yang Anda minta!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
		endif;
	}
	
	/**
	 * Sends a file to the client
	 *
	 * @param string $id UUID
	 * @access public
	 * Source: http://cakedc.com/florian_kraemer/2010/01/25/file-uploading-file-storage-and-cakephp-mediaview-class
	 */
	function download($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
		}
	
		$groupid = $this->Session->read('User.group_id');
		
		$this->Kolokium->recursive = -1;
		
		if ($groupid == 1 || $groupid == 3):
			$media = $this->Kolokium->find('first', array( 'conditions' => array('Kolokium.id' => $id) ) );
		else:
			$media = $this->Kolokium->find('first', array( 'conditions' => array('Kolokium.id' => $id, 'Kolokium.userid' => $this->Session->read('User.id')) ) );
		endif;
		
		if (empty($media)) {
			$groupid = $this->Session->read('User.group_id');
			$this->Session->setFlash('File is not found!');
			if ($groupid == 1 || $groupid == 2 || $groupid == 3):
				$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-3'));
			else:
				$this->redirect(array('controller'=>'admin', 'action' => 'home'));
			endif;
		}
		
		$path_info = pathinfo($media['Kolokium']['proposal']);
		
		$params = array(
			'cache' => '3 days',
			'download' => true,
			'name' => $path_info['filename'],
			'id' => $media['Kolokium']['proposal'],
			'download' => true,
			'path' => $media['Kolokium']['dir'] . DS,
			'modified' => $media['Kolokium']['modified'],
			'mimeType' => $media['Kolokium']['mimetype'],
			'extension' => $path_info['extension']
		);
		
		$this->view = 'Media';
		$this->set($params);
		$this->autoLayout = false;
		if ($this->render() !== false) {
			$this->Kolokium->updateAll(
				array('Kolokium.downloads' => 'Kolokium.downloads + 1'),
				array('Kolokium.id' => $id));
		}
	}
	
	function status($val=null) {
		$this->layout = 'ajax';
		if ($val == null):
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		else:
			//if ($tipe == 'L' || $tipe == 'R' || $tipe == 'T') {
				$status = substr($val,-1);
				$id = substr($val, 0, -1);
				$kols = $this->Kolokium->find('first', array('conditions'=>array('Kolokium.id'=> $id)) );
				
				if (!empty($kols)) {
					$this->Kolokium->id = $id;
					if ($this->Kolokium->saveField('status', $status) && $this->Kolokium->saveField('modified', date( 'Y-m-d H:i:s' )) ):
						// jika dinyatakan lolos kolokium
						if ($status == 'L') {
							// ambil data mahasiswa yang bersangkutan
							$mhs = $this->Mahasiswa->find('first', array('conditions' => array('Mahasiswa.nim' => $kols['Kolokium']['nim']) ) );
							if (!empty($mhs)) {
								// cek apakah pernah mengambil TA sebelumnya
								$tas = $this->Ta->find('all', array('conditions' => array('Ta.nim' => $kols['Kolokium']['nim']), 'recursive'=>-1 ) );
								foreach($tas as $t):
									if ($t['Ta']['aktif'] != 2) {
										$this->Ta->id = $t['Ta']['id'];
										$this->Ta->saveField('aktif', 2);
									}
								endforeach;
								
								// jika mahasiswa registrasi dan mengambil TA
								if ($mhs['Mahasiswa']['status'] == 1 && $mhs['Mahasiswa']['ta'] == 1) {
									$data['Ta']['aktif'] = 1;
									// jika bulan tanggal kolokium adalah 1 - 6 maka genap, 7-12 maka ganjil
									//
									$blnkol = date('m', strtotime($kols['Jadwal']['tanggal']));
									$thnkol = date('Y', strtotime($kols['Jadwal']['tanggal']));
									if ($blnkol >= 1 && $blnkol <=6) {
										$data['Ta']['sem'] = 2;
										$data['Ta']['tahun'] = $thnkol-1;
									} else {
										$data['Ta']['sem'] = 1;
										$data['Ta']['tahun'] = $thnkol;
									}
									$data['Ta']['take'] = 1;
								} else {
									$data['Ta']['aktif'] = 0;
								}
							} else {
								$data['Ta']['aktif'] = 0;
							}
							$data['Ta']['nim'] = $kols['Kolokium']['nim'];
							$data['Ta']['judul'] = $kols['Kolokium']['judul'];
							$data['Ta']['dosen1'] = $kols['Kolokium']['usulan_dosen1'];
							$data['Ta']['dosen2'] = $kols['Kolokium']['usulan_dosen2'];
							$data['Ta']['thnkolokium'] = date('Y', strtotime($kols['Jadwal']['tanggal']));
							$data['Ta']['blnkolokium'] = date('m', strtotime($kols['Jadwal']['tanggal']));
							$data['Ta']['kolokium_id'] = $kols['Kolokium']['id'];
							
							$this->Ta->create();
							if ($this->Ta->save($data)){
								$d['Status']['return'] = 0;
								$d['Status']['msg'] = 'Perubahan status kolokium telah tersimpan!';
							} else {
								$d['Status']['return'] = 1;
								$d['Status']['msg'] = 'Perubahan status kolokium tidak dapat menyimpan data Tugas Akhirnya. Silahkan diulangi!';
							}
						} else {
							$d['Status']['return'] = 0;
							$d['Status']['msg'] = 'Perubahan status kolokium telah tersimpan!';
						}
					else:
						$d['Status']['return'] = 1;
						$d['Status']['msg'] = 'Perubahan status kolokium tidak dapat menyimpan data Tugas Akhirnya. Silahkan diulangi!';
					endif;
				} else {
					$d['Status']['return'] = 1;
					$d['Status']['msg'] = 'Request tidak valid!';
				}
			/*} else {
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Request tidak valid!';
			}*/
		endif;
		$this->set(compact('d'));
	}
	
	function ceknim($nim=null) {
		$this->layout = 'ajax';
		if ($nim == null):
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		else:
			$mhs = $this->User->find('first', array('fields' =>array('fullname'), 'recursive' => -1, 'conditions' => array('User.nim' => $nim) ));
			if (!empty($mhs)):
				$d['Status']['return'] = 0;
				$d['Status']['msg'] = $mhs['User']['fullname'];
			else:
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Tidak ada mahasiswa dengan NIM tersebut!';
			endif;
		endif;
		$this->set(compact('d'));
	}
	
	function bakol($id=null) {
		$this->layout = 'ajax';
		if ($id == null):
			$d['Status']['return'] = 2;
			$d['Status']['msg'] = 'Request tidak valid!';
		else:
			if ($this->Session->read('User.group_id') == 3 || $this->Session->read('User.group_id') == 4) {
				$currdosen = $this->Kolokium->Dosen->find('first', array('conditions' => array('Dosen.nidn' => $this->Session->read('User.nim') )));
				
				$data = $this->Kolokium->find('first', array('conditions' => array('Kolokium.id' => $id), 'recursive' => 2 ) );
				if (!empty($data)) {
					$isok = false;
					if ($data['Jadwal']['Dosen']) {
						$tglakhir = date('Y-m-d H:i:s', strtotime($data['Jadwal']['expired']));
						$tglsistem = date('Y-m-d H:i:s');
						$tglawal = date('Y-m-d H:i:s', strtotime( date('Y-m-d', strtotime($data['Jadwal']['expired'])) . '07:30:00'));

						foreach($data['Jadwal']['Dosen'] as $t){
							if ($t['id'] == $currdosen['Dosen']['id'] && 
								($tglsistem >= $tglawal && $tglsistem <= $tglakhir )) {
								$isok = true; break;
							}
						}
					}
					
					if (!$isok) {
						$d['Status']['return'] = 4;
					} else {
						$d['Status']['return'] = 0;
					}
					//if (($data['Kolokium']['status'] == 'V' || $data['Kolokium']['status'] == 'N')) { //&& $this->Session->read('User.group_id') == 3) {
					if ($data['Kolokium']['status'] == 'N') {
						$d['Status']['return'] = 3;
						$d['Status']['msg'] = 'Data Kolokium belum dinyatakan valid!';
					} else {
						$datata = $this->Ta->find('first', array('conditions' => array('Ta.kolokium_id' => $data['Kolokium']['id']) ) );
						
						$k['Kolokium']['id'] = $data['Kolokium']['id'];
						$k['Kolokium']['nim'] = $data['Kolokium']['nim'];
						$k['Kolokium']['fullname'] = $data['User']['fullname'];
						$k['Kolokium']['judul'] = $data['Kolokium']['judul'];
						$k['Kolokium']['judul_proposal'] = $data['Proposal']['judul'];
						$k['Kolokium']['status'] = $data['Kolokium']['status'];
						$k['Kolokium']['catatan'] = $data['Kolokium']['catatan'];
						$k['Kolokium']['internal'] = $data['Kolokium']['catatan_internal'];
						$k['Kolokium']['tglkumpul'] = date('d-m-Y', strtotime($data['Jadwal']['tglkumpul']));
						if (!empty($datata)) {
							$k['Kolokium']['dosen1'] = $datata['Ta']['dosen1'];
							$k['Kolokium']['dosen2'] = $datata['Ta']['dosen2'];
						} else {
							$k['Kolokium']['dosen1'] = $data['Kolokium']['usulan_dosen1'];
							$k['Kolokium']['dosen2'] = $data['Kolokium']['usulan_dosen2'];
						}
						$d['Status']['msg'] = $k;
					}
				} else {
					$d['Status']['return'] = 1;
					$d['Status']['msg'] = 'Tidak ada data Kolokium dengan NIM tersebut!';
				}
			} else {
				$data = $this->Kolokium->find('first', array('conditions' => array('Kolokium.id' => $id), 'recursive' => 2 ) );
				if (!empty($data)) {
					if ($data['Kolokium']['status'] == 'N') {
						$d['Status']['return'] = 3;
						$d['Status']['msg'] = 'Data Kolokium belum dinyatakan valid!';
					} else {
						$datata = $this->Ta->find('first', array('conditions' => array('Ta.kolokium_id' => $data['Kolokium']['id']) ) );
						
						$k['Kolokium']['id'] = $data['Kolokium']['id'];
						$k['Kolokium']['nim'] = $data['Kolokium']['nim'];
						$k['Kolokium']['fullname'] = $data['User']['fullname'];
						$k['Kolokium']['judul'] = $data['Kolokium']['judul'];
						$k['Kolokium']['judul_proposal'] = $data['Proposal']['judul'];
						$k['Kolokium']['status'] = $data['Kolokium']['status'];
						$k['Kolokium']['catatan'] = $data['Kolokium']['catatan'];
						$k['Kolokium']['internal'] = $data['Kolokium']['catatan_internal'];
						$k['Kolokium']['tglkumpul'] = date('d-m-Y', strtotime($data['Jadwal']['tglkumpul']));
						if (!empty($datata)) {
							$k['Kolokium']['dosen1'] = $datata['Ta']['dosen1'];
							$k['Kolokium']['dosen2'] = $datata['Ta']['dosen2'];
						} else {
							$k['Kolokium']['dosen1'] = $data['Kolokium']['usulan_dosen1'];
							$k['Kolokium']['dosen2'] = $data['Kolokium']['usulan_dosen2'];
						}
						$d['Status']['msg'] = $k;
						$d['Status']['return'] = 0;
					}
				} else {
					$d['Status']['return'] = 1;
					$d['Status']['msg'] = 'Tidak ada data Kolokium dengan NIM tersebut!';
				}
			}
		endif;
		$this->set(compact('d'));
	}
	
	function report($tglkol=null) {
		$this->set('judul', 'Daftar Peserta Kolokium');
		if (!$tglkol) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
		}
		$data = $this->Kolokium->find('all', array('conditions' => array('Kolokium.tgl_kolokium' => $tglkol, 'Kolokium.status <> ' => 'N'), 'order' => 'Kolokium.nim ASC' ) );
		$this->set('data', $data);
		$tgl = $this->Jadwal->find('first', array('conditions' => array('Jadwal.id' => $tglkol) ));
		$this->set('tgl', $tgl);
		$this->layout = "report";
	}
	
    function viewpdf($id = null) {
        if (!$id) {
            $this->Session->setFlash('Sorry, there was no property ID submitted.');
            $this->redirect(array('action'=>'index'), null, true);
        }
		$tgl = $this->Jadwal->find('first', array('conditions' => array('Jadwal.id' => $id) ));
		$this->set('tgl', $tgl);
		/*$data = $this->Kolokium->find('all', array('conditions' => array('tgl_kolokium' => $id), 'order' => 'Kolokium.nim ASC' ) );
		$this->set('data', $data);*/
		
        // using html2pdf
        App::import('Component', 'Pdf');
        $Pdf = new PdfComponent();
        $Pdf->filename = 'Kolokium' . $tgl['Jadwal']['waktu']; // Without .pdf
        $Pdf->output = 'download';
        $Pdf->init();
        $Pdf->media->set_landscape(true);
		$Pdf->p->fetchers[] = new FetcherURL;
		$Pdf->p->data_filters[] = new DataFilterHTML2XHTML;
		$header_html = "";
		$footer_html = "<small>##PAGE## dari ##PAGES##</small>";
		$Pdf->p->pre_tree_filters = array();
		$Pdf->p->pre_tree_filters[] = new PreTreeFilterHeaderFooter($header_html, $footer_html);
		$Pdf->p->pre_tree_filters[] = new PreTreeFilterHTML2PSFields();
		
        $Pdf->process(Router::url('/', true) . 'papers/report/'. $id);
        $this->render(false);
    }
    
    function prnba($id = null) {
        if (!$id) {
            $this->Session->setFlash('Maaf, URL yang Anda minta tidak valid!');
            $this->redirect(array('action'=>'index'), null, true);
        }
        $currdosen = $this->Kolokium->Dosen->find('first', array('conditions' => array('Dosen.nidn' => $this->Session->read('User.nim') )));

		$data = $this->Kolokium->find('first', array('conditions' => array('Kolokium.id' => $id), 'recursive' => 2 ) );
		$this->set('data', $data);
		if ($data) {
			$isok = false;
			if ($this->Session->read('User.group_id') == 3) {
				if (isset($data['Jadwal']['Dosen'])) {
					$tglakhir = date('Y-m-d H:i:s', strtotime($data['Jadwal']['expired']));
					$tglsistem = date('Y-m-d H:i:s');
					$tglawal = date('Y-m-d H:i:s', strtotime( date('Y-m-d', strtotime($data['Jadwal']['expired'])) . '07:30:00'));
					foreach($data['Jadwal']['Dosen'] as $t){
						if ($t['id'] == $currdosen['Dosen']['id'] && 
							($tglsistem >= $tglawal && $tglsistem <= $tglakhir )) {
							$isok = true; break;
						}
					}
				}
			} elseif ($this->Session->read('User.group_id') == 1) { 
				$isok = true;
			}
			
			if ($isok) {
				$datata = $this->Ta->find('first', array('conditions' => array('Ta.kolokium_id' => $id) ) );
				$this->set(compact('datata'));
				
				Configure::write('debug',0);
				$this->layout = 'tcpdf';
				$this->render();
			} else {
				$this->Session->setFlash('Sesi Anda sudah kadaluwarsa untuk mencetak berita acara Kolokium!');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
			}
		} else {
			$this->Session->setFlash('Tidak ditemukan data kolokium terpilih!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-3'));
		}
    }
	
    function prnallba($id = null) {
        if (!$id) {
            $this->Session->setFlash('Sorry, there was no property ID submitted.');
            $this->redirect(array('action'=>'index'), null, true);
        }
		$data = $this->Kolokium->find('all', array('conditions' => array('tgl_kolokium' => $id, 'Kolokium.status'=> 'V'), 'order' => 'Kolokium.nim ASC' ) );
		$this->set('data', $data);
		
		Configure::write('debug',0); // Otherwise we cannot use this method while developing
		$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
		$this->render();
    }
    
    function prnletter($id = null) {
        if (!$id) {
            $this->Session->setFlash('Maaf, URL yang Anda minta tidak valid!');
            $this->redirect(array('action'=>'index'), null, true);
        }
		$data = $this->Ta->find('first', array('conditions' => array('Ta.id' => $id, 'Mahasiswa.status' => 1, 'Mahasiswa.ta' => 1 ) ));
		$this->set('data', $data);
		
		Configure::write('debug',0); 
		$this->layout = 'tcpdf'; 
		$this->render();
    }
	
    function prnletters($id = null) {
        if (!$id) {
            $this->Session->setFlash('Sorry, there was no property ID submitted.');
            $this->redirect(array('action'=>'index'), null, true);
        }
		$kols = $this->Kolokium->find('all', array('fields' => array('id','nim'), 'recursive'=>-1, 'conditions' => array('tgl_kolokium' => $id, 'Kolokium.status'=> 'L'), 'order' => 'Kolokium.nim ASC' ) );
		$kolids = array();
		foreach($kols as $k) {
			array_push($kolids, $k['Kolokium']['id']);
		}
		$data = $this->Ta->find('all', array('conditions' => array('Ta.kolokium_id' => $kolids ) ));
		$this->set('data', $data);
		
		Configure::write('debug',0); // Otherwise we cannot use this method while developing
		$this->layout = 'tcpdf'; //this will use the pdf.ctp layout
		$this->render();
    }

    function modifba() {
		$this->layout = 'ajax';
		$df = $this->data;
		
		$kols = $this->Kolokium->find('first', array('conditions'=>array('Kolokium.id'=> $df['Ba']['id'])) );
		
		if (!empty($kols)) {
			$this->Kolokium->id = $df['Ba']['id'];
			if ($this->Kolokium->saveField('judul', $df['Ba']['judul']) &&
				$this->Kolokium->saveField('catatan', $df['Ba']['catatan']) &&
				$this->Kolokium->saveField('evaluator', $this->Session->read('User.id')) &&
				$this->Kolokium->saveField('batas_kumpul', date('Y-m-d', strtotime($df['Ba']['batas_kumpul']))) &&
				$this->Kolokium->saveField('catatan_internal', $df['Ba']['catatan_internal']) &&
				$this->Kolokium->saveField('modified', date( 'Y-m-d H:i:s' )) ) {
				
				$d['Status']['return'] = 0;
				$d['Status']['msg'] = 'Perubahan status kolokium telah tersimpan!';
			} else {
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Perubahan status kolokium tidak dapat menyimpan data Tugas Akhirnya. Silahkan diulangi!';
			}
		} else {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Pencatatan Tugas Akhir tidak berhasil!';
		}
		
		$this->set(compact('d'));
    }
	
	function saveba() {
		$this->layout = 'ajax';
		$df = $this->data;
		
		$kols = $this->Kolokium->find('first', array('conditions'=>array('Kolokium.id'=> $df['Ba']['id'])) );
		
		if (!empty($kols)) {
			$this->Kolokium->id = $df['Ba']['id'];
			if ($this->Kolokium->saveField('status', $df['Ba']['status']) && 
				$this->Kolokium->saveField('judul', $df['Ba']['judul']) &&
				$this->Kolokium->saveField('catatan', $df['Ba']['catatan']) &&
				$this->Kolokium->saveField('evaluator', $this->Session->read('User.id')) &&
				$this->Kolokium->saveField('batas_kumpul', date('Y-m-d', strtotime($df['Ba']['batas_kumpul']))) &&
				$this->Kolokium->saveField('catatan_internal', $df['Ba']['catatan_internal']) &&
				$this->Kolokium->saveField('modified', date( 'Y-m-d H:i:s' )) ) {
				
				// jika dinyatakan lolos kolokium
				if ($df['Ba']['status'] == 'L' || $df['Ba']['status'] == 'S') {
					// ambil data mahasiswa yang bersangkutan
					$mhs = $this->Mahasiswa->find('first', array('conditions' => array('Mahasiswa.nim' => $kols['Kolokium']['nim']) ) );
					if (!empty($mhs)) {
						// cek apakah pernah mengambil TA sebelumnya
						$tas = $this->Ta->find('all', array('conditions' => array('Ta.nim' => $kols['Kolokium']['nim']), 'recursive'=>-1 ) );
						$ta_ke = 0;
						foreach($tas as $t):
							$ta_ke++;
							if ($t['Ta']['aktif'] != 2) {
								$this->Ta->id = $t['Ta']['id'];
								$this->Ta->saveField('aktif', 2);
							}
						endforeach;
						
						// jika mahasiswa registrasi dan mengambil TA
						if ($mhs['Mahasiswa']['status'] == 1 && $mhs['Mahasiswa']['ta'] == 1) {
							$data['Ta']['aktif'] = 1;
							// jika bulan tanggal kolokium adalah 1 - 6 maka genap, 7-12 maka ganjil
							//
							$blnkol = date('m', strtotime($kols['Jadwal']['tanggal']));
							$thnkol = date('Y', strtotime($kols['Jadwal']['tanggal']));
							if ($blnkol >= 1 && $blnkol <=6) {
								$data['Ta']['sem'] = 2;
								$data['Ta']['tahun'] = $thnkol-1;
								$data['Ta']['periode_awal'] = strval($thnkol) . '-01-01';
								$data['Ta']['periode_akhir'] = strval($thnkol) . '-06-31';
							} else {
								$data['Ta']['sem'] = 1;
								$data['Ta']['tahun'] = $thnkol;
								$data['Ta']['periode_awal'] = strval($thnkol) . '-07-01';
								$data['Ta']['periode_akhir'] = strval($thnkol) . '-12-31';
							}
							$data['Ta']['take'] = 1;
						} else {
							$data['Ta']['sem'] = 0;
							$data['Ta']['aktif'] = 0;
						}
					} else {
						$data['Ta']['sem'] = 0;
						$data['Ta']['aktif'] = 0;
					}
					$data['Ta']['nim'] = $kols['Kolokium']['nim'];
					$data['Ta']['judul'] = $df['Ba']['judul'];
					$data['Ta']['dosen1'] = $df['Ba']['dosen1'];
					//$data['Ta']['dosen2'] = $df['Ba']['dosen2'];
					$data['Ta']['thnkolokium'] = date('Y', strtotime($kols['Jadwal']['tanggal']));
					$data['Ta']['blnkolokium'] = date('m', strtotime($kols['Jadwal']['tanggal']));
					$data['Ta']['kolokium_id'] = $df['Ba']['id'];
					$data['Ta']['lulus'] = 0;
					
					$this->Ta->create();
					if ($this->Ta->save($data)){
						$d['Status']['return'] = 0;
						$d['Status']['msg'] = 'Perubahan status kolokium telah tersimpan!';
					} else {
						$d['Status']['return'] = 1;
						$d['Status']['msg'] = 'Perubahan status kolokium tidak dapat menyimpan data Tugas Akhirnya. Silahkan diulangi!';
					}
				} else {
					$d['Status']['return'] = 0;
					$d['Status']['msg'] = 'Perubahan status kolokium telah tersimpan!';
				}
			} else {
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Perubahan status kolokium tidak dapat menyimpan data Tugas Akhirnya. Silahkan diulangi!';
			}
		} else {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Pencatatan Tugas Akhir tidak berhasil!';
		}
		
		$this->set(compact('d'));
	}
	
	/*function setkolid() {
		$kol = $this->Kolokium->find('all', array('recursive' => -1, 'conditions' => array('Kolokium.status' => 'L')));
		if (!empty($kol)) {
			foreach ($kol as $k):
				$ta = $this->Ta->find('first', array('recursive' => -1, 'conditions' => array('Ta.aktif' => array(0,1), 'nim' => $k['Kolokium']['nim'] ) ));
				
				if (!empty($ta)) {
					$this->Ta->id = $ta['Ta']['id'];
					$this->Ta->saveField('kolokium_id', $k['Kolokium']['id']);
				}
			endforeach;
		}
		$this->redirect(array('controller' => 'admin', 'action' => 'home'));
	}*/
}
?>