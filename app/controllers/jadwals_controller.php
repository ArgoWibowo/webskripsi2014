<?php
/**
 * 
 * @author budi susanto
 *
 */
class JadwalsController extends AppController 
{
	public $name = 'Jadwals';
	public $layout = 'baseform';
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		} else {
			if(($this->Session->read('User.group_id') != 1)) {
				if ($this->Session->read('User.group_id') == 3 && $this->action == 'getalldates') {
					return true;
				}
				$this->Session->setFlash('Sorry, you don\'t have any privileges to access this page.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			}
		}
	}
	
	function add() {
		$this->set('judul', 'Tambah Jadwal Kolokium');
		if (!empty($this->data)) {
			$this->Jadwal->create();
			if ($this->Jadwal->save($this->data)) {
				$this->Session->setFlash('Your Jadwal has been saved and active!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
			} else {
				$this->Session->setFlash('Sorry, there are any error that can not be handled by system in order to save the new Jadwal Kolokium!');
				$this->set('data', $this->data);
				$this->render('add');
			}
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update Jadwal Kolokium');
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		}
		
		if (!empty($this->data)) {
			if ($this->Jadwal->save($this->data)) {
				$this->Session->setFlash('The Jadwal has been updated!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
			} else {
				$this->Session->setFlash('Sorry, there are any error that can not be handled by system in order to save your Jadwal Kolokium!');
				$this->set('data', $this->data);
				$this->render('edit');
			}
		} else {
			$data = $this->Jadwal->find('first', array('conditions' => array('Jadwal.id' => $id)));
			$this->set('data', $data);
		}
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		}
		
		if ($this->Jadwal->delete($id)) {
			$this->Session->setFlash('The selected Jadwal has been deleted!', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash('The selected Jadwal could not be deleted!', 'default');
		}
		$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
	}
	
	function catat($id=null) {
		$this->set('judul', 'Setup Tim Kolokium');
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		} else {
			$data = $this->Jadwal->find('first', array('conditions' => array('Jadwal.id' => $id) ));
			$this->set(compact('data'));
			$dosens = $this->Jadwal->Dosen->find('all', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
			$this->set(compact('dosens'));
		}
	}
	
	function simpancatat() {
		if ($this->data) {
			for($i=0; $i < count($this->data['Dosen']['Dosen']); $i++) {
				if ($this->data['Dosen']['Dosen'][$i]['dosen_id']==0) {
					unset($this->data['Dosen']['Dosen'][$i]['dosen_id']);
					unset($this->data['Dosen']['Dosen'][$i]['kelompok']);
				}
			}
			$this->Jadwal->id = $this->data['Jadwal']['idjwd'];
			if ($this->Jadwal->save($this->data)) {
				$this->Session->setFlash('Setup Data Dosen pencatat telah disimpan!', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('Setup Data Dosen pencatat tidak dapat disimpan saat ini!');
			}
		} else {
			$this->Session->setFlash('Your request is not valid!');
		}
		$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
	}

	function getalldates() {
		$this->layout = 'ajax';
		$data = $this->Jadwal->find('all', array('recursive' => 2, 'order' => array('Jadwal.tanggal DESC') ));
		$res = array();
		foreach($data as $dt) {
			$temp = array();
			$temp['id'] = $dt['Jadwal']['id'];
			$temp['tanggal'] = $dt['Jadwal']['waktu'];
			if (!empty($dt['Dosen'])) {
				$i = 0;
				foreach($dt['Dosen'] as $dosen) {
					$temp['Dosen'][$i]['id'] = $dosen['id'];
					$temp['Dosen'][$i]['nidn'] = $dosen['nidn'];
					$temp['Dosen'][$i]['nama'] = $dosen['nama_dosen'];
					$i++;
				}
			} else {
				$temp['Dosen'] = array();
			}
			array_push($res, $temp);
		}
		$d = array();
		if (!empty($data)):
			$d['Status']['return'] = 0;
			$d['Status']['msg'] = $res;
		else:
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Tidak ada waktu untuk Pelaksanaan Desk Evaluation!';
		endif;
		$this->set(compact('d'));
	}
}
?>