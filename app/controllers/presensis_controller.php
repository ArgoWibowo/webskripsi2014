<?php
class PresensisController extends AppController {
	public $name = 'Presensis';
	public $uses = array('Presensi', 'Kolokium', 'Proposal');
	public $components = array('RequestHandler');
	var $helpers = array('Html', 'Tools');
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		} else {
			if(($this->Session->read('User.group_id') != 1)) {
				$this->Session->setFlash('Sorry, you don\'t have any privileges to access this page.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			}
		}
	}
	
	function save() {
		$this->layout = 'ajax';
		if ($this->params['isAjax']):
			$data = $this->params['form'];
			
			if ($data['id'] != ''):
				$this->data['Presensi']['id'] = $data['id'];
			endif;
			if ($data['tipepresensi'] == 1) {
				$this->data['Presensi']['idtglkol'] = $data['idtglkol'];
				$this->data['Presensi']['id_tglde'] = 0;
			} else {
				$this->data['Presensi']['idtglkol'] = 0;
				$this->data['Presensi']['id_tglde'] = $data['idtglkol'];
			}
			$this->data['Presensi']['tipe'] = $data['tipepresensi'];
			$this->data['Presensi']['id_dosen'] = $data['id_dosen'];
			list($tgl, $jam) = split(" ", $data['waktu_datang']);
			list($day, $m, $y) = split("-", $tgl);
			list($h, $i, $s) = split(":", $jam);
			$this->data['Presensi']['waktu_datang'] = $y . '-' . $m . '-' . $day . ' ' . $h . ':' .$i . ':' . $s;
			list($tgl, $jam) = split(" ", $data['waktu_pulang']);
			list($day, $m, $y) = split("-", $tgl);
			list($h, $i, $s) = split(":", $jam);
			$this->data['Presensi']['waktu_pulang'] = $y . '-' . $m . '-' . $day . ' ' . $h . ':' .$i . ':' . $s;
			$this->data['Presensi']['lama'] = $data['lama'];
			
			if ($this->Presensi->save($this->data)):
				$d['Status']['return'] = 0;
				if ($data['id'] == ''):
					$d['Status']['id'] = $this->Presensi->getLastInsertId();
				else:
					$d['Status']['id'] = $data['id'];
				endif;
				$d['Status']['msg'] = 'Data presensi telah tersimpan!';
			else:
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Maaf, server tidak dapat menyimpan saat ini!';
			endif;
			$this->set('d', $d);
		else:
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
			$this->set('d', $d);
		endif;
	}
	
	function delete($id = null){
		$this->layout = 'ajax';
		if ($this->params['isAjax']):
			if (!$id):
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Request tidak valid!';
			else:
				if ($this->Presensi->delete($id)):
					$d['Status']['return'] = 0;
					$d['Status']['msg'] = 'data presensi terpilih telah dihapus!';
				else:
					$d['Status']['return'] = 1;
					$d['Status']['msg'] = 'Maaf, sistem belum dapat menghapus data terpilih!';
				endif;
			endif;
		else:
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		endif;
		$this->set('d', $d);
	}
	
	function cetak($id = null, $t=null) {
		if (!$id && !$t) {
			$this->Session->setFlash('Maaf, URL yang diminta tidak valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-9'));
		} else {
			if ($t == 1) {
				$this->set('judul', 'Daftar Presensi Penguji Kolokium');
				$data = $this->Presensi->find('all', array('conditions' => array('Presensi.idtglkol' => $id)));
				$mhs = $this->Kolokium->find('first', array(
							'recursive' => 0, 
							'fields' => array('COUNT(Kolokium.id) AS total'),
							'conditions' => 'Kolokium.tgl_kolokium = ' . $id . ' AND Kolokium.status NOT IN (\'N\')'
						)); 
			} elseif ($t == 0) {
				$this->set('judul', 'Daftar Presensi Tim Desk Evaluation');
				$data = $this->Presensi->find('all', array('conditions' => array('Presensi.id_tglde' => $id)));
				$mhs = $this->Proposal->find('first', array(
							'recursive' => 0, 
							'fields' => array('COUNT(Proposal.id) AS total'),
							'conditions' => 'Proposal.tglde = ' . $id . ' AND Proposal.valid = 1'
						)); 
			} else {
				$this->set('judul', 'Tidak ada catatan untuk tipe presensi yang dimasukkan!');
				$data = array();
				$mhs = array();
			}
			$this->set(compact('mhs'));
			$this->set(compact('data'));
			$this->layout = "report";
		}
	}
	
	function cetakall($id = null, $idakhir = null) {
		if (!$id || !$idakhir) {
			$this->Session->setFlash('Maaf, URL yang diminta tidak valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-9'));
		} else {
			$this->set('judul', 'Daftar Presensi Penguji Kolokium');
			$periode = $this->Presensi->Jadwal->find('first', array(
								'fields' => array('MIN(Jadwal.tanggal) AS awal', 'MAX(Jadwal.tanggal) as akhir'),
								'conditions' => array('Jadwal.id BETWEEN ? AND ?' => array($id, $idakhir) )
							));
			$data = $this->Presensi->find('all', array('conditions' => array('Presensi.idtglkol BETWEEN ? AND ?' => array($id, $idakhir)), 'order'=>'Presensi.idtglkol'));
			$mhs = $this->Kolokium->find('all', array(
						'recursive' => 2, 
						'fields' => array('Kolokium.tgl_kolokium', 'Jadwal.tanggal', 'COUNT(Kolokium.id) AS total'),
						'conditions' => 'Kolokium.tgl_kolokium BETWEEN ' . $id . ' AND ' . $idakhir . '  AND Kolokium.status NOT IN (\'N\') GROUP BY Kolokium.tgl_kolokium, Jadwal.tanggal'
					));
			
			// buat crosstab
			$presensi = array();
			foreach($data as $d){
				$nmdosen = $d['Dosen']['nama_dosen'];
				$tglkol = $d['Presensi']['idtglkol'];
				if (array_key_exists($nmdosen, $presensi)) {
					if (isset($presensi[$nmdosen][$tglkol])) {
						$presensi[$nmdosen][$tglkol] = $presensi[$nmdosen][$tglkol] + $d['Presensi']['lama'];
					} else {
						$presensi[$nmdosen][$tglkol] = $d['Presensi']['lama'];
					}
				} else {
					$presensi[$nmdosen] = array($tglkol => $d['Presensi']['lama']);
				}
			}
			
			$this->set(compact('presensi'));
			$this->set(compact('periode'));
			$this->set(compact('mhs'));
			$this->layout = "report";
		}
	}	
}
?>