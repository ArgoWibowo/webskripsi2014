<?php
// created by Budi Susanto
class RegistersController extends AppController {
	public $name = 'Registers';
	public $uses = array('User', 'Mahasiswa', 'Dosen', 'Log');
	public $layout = "baseform";
	// public $components = array('RecaptchaPlugin.Recaptcha', 'Email', 'RequestHandler');
	// public $helpers = array('RecaptchaPlugin.Recaptcha', 'Html');
	public $components = array('Email','RequestHandler');
	public $helpers = array('Html', 'Tools');

    /*
     * resource: http://www.laughing-buddha.net/php/password
     */
	function __generatePassword($length = 8) {
		// start with a blank password
		$password = "";
		
		// define possible characters - any character in this string can be
		// picked for use in the password, so if you want to put vowels back in
		// or add special characters such as exclamation marks, this is where
		// you should do it
		$possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
		
		// we refer to the length of $possible a few times, so let's grab it now
		$maxlength = strlen($possible);
	  	
		// check for length overflow and truncate if necessary
		if ($length > $maxlength) {
			$length = $maxlength;
		}
		
		// set up a counter for how many characters are in the password so far
		$i = 0; 
		
		// add random characters to $password until $length is reached
		while ($i < $length) { 
			// pick a random character from the possible ones
			$char = substr($possible, mt_rand(0, $maxlength-1), 1);
			// have we already used this character in $password?
			if (!strstr($password, $char)) { 
				// no, so it's OK to add it onto the end of whatever we've already got...
				$password .= $char;
				// ... and increase the counter by one
				$i++;
			}
		}
	
		// done!
		return $password;
	}
	 	
	function index() {
		$this->set('judul', 'Pendaftaran User Baru');
	}
	
    function __sendNotifUserMail($username, $fullname, $email, $hash) {
    	// gmail SMTP configuration
		$this->Email->smtpOptions = array(
			'port'=>'465', 
			'timeout'=>'30',
			'host' => 'ssl://smtp.gmail.com',
			'username'=>'skripsi.ukdw@gmail.com',
			'password'=>'SkripsiSIUKDW!@#$%',
		);
    	
    	$this->Email->delivery = 'smtp';
		//$this->Email->delivery = 'debug';
		
		/* Check for SMTP errors. */
		$this->set('smtp_errors', $this->Email->smtpError);
		
		$this->Email->to = $email;
		$this->Email->bcc = array('budsus@ti.ukdw.ac.id');
		$this->Email->subject = 'Konfirmasi Pendaftaran Peserta Skripsi SI UKDW';
		$this->Email->replyTo = 'skripsi.ukdw@gmail.com';
		$this->Email->from = 'Admin Skripsi SI UKDW <skripsi.ukdw@gmail.com>';
		$this->Email->template = 'notification'; // note no '.ctp'
		
		//Send as 'html', 'text' or 'both' (default is 'text')
		$this->Email->sendAs = 'text'; // because we like to send pretty mail
		
		// set data that will be sent to user by email
		$this->set('username', $username);
		$this->set('fullname', $fullname);
		$this->set('email', $email);
		$this->set('hash', $hash);
		
		//Do not pass any args to send()
		$this->Email->send();
    }
    
    function __sendResetPwdUserMail($username, $fullname, $email, $newpasswd) {
    	// gmail SMTP configuration
		$this->Email->smtpOptions = array(
			'port'=>'465', 
			'timeout'=>'30',
			'host' => 'ssl://smtp.gmail.com',
			'username'=>'skripsi.ukdw@gmail.com',
			'password'=>'SkripsiSIUKDW!@#$%',
		);
    	
    	$this->Email->delivery = 'smtp';
		//$this->Email->delivery = 'debug';
		
		/* Check for SMTP errors. */
		$this->set('smtp_errors', $this->Email->smtpError);
		
		$this->Email->to = $email;
		$this->Email->bcc = array('budsus@ti.ukdw.ac.id');
		$this->Email->subject = 'Reset Kata Sandi untuk Skripsi SI UKDW';
		$this->Email->replyTo = 'skripsi.ukdw@gmail.com';
		$this->Email->from = 'Admin Skripsi SI UKDW <skripsi.ukdw@gmail.com>';
		$this->Email->template = 'resetpasswd'; // note no '.ctp'
		
		//Send as 'html', 'text' or 'both' (default is 'text')
		$this->Email->sendAs = 'text'; // because we like to send pretty mail
		
		// set data that will be sent to user by email
		$this->set('username', $username);
		$this->set('fullname', $fullname);
		$this->set('email', $email);
		$this->set('newpasswd', $newpasswd);
		
		//Do not pass any args to send()
		$this->Email->send();
    }
    	
	function regis(){
		$this->set('judul', 'Pendaftaran User Baru');
		if (!empty($this->data)) {
			// create hash value for verification that will be sent to user
			$this->data['User']['hash_validation'] = md5($this->data['User']['username'] . date('dMYHis') . 'Kn4st1K' );
			if (strlen($this->data['User']['nim']) == 10):
				$this->data['User']['group_id'] = 3;
			elseif (strlen($this->data['User']['nim']) == 8):
				$this->data['User']['group_id'] = 2;
			else:
				$this->data['User']['group_id'] = 0;
			endif;

			$this->User->create();
			
			if ($this->User->save($this->data)) {
				//$this->__sendNotifUserMail($this->data['User']['username'], $this->data['User']['fullname'], $this->data['User']['email'], $this->data['User']['hash_validation']);
				$this->Session->setFlash('The new user have been saved!', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('Sorry, system can not save the new user!');
				$this->set('data', $this->data);
				$this->render('index');
			}
		} else {
			$this->redirect(array('action' => 'index'));
		}
	}
	
	function reconfirm() {
		if (!empty($this->data)):
			// for dummy validation
			$this->data['User']['username'] = 'testingTESTING';
			$this->data['User']['password'] = 'testingTESTING';
			$this->data['User']['nim'] = '12345678';
			$this->data['User']['fullname'] = 'testing';
			$this->data['User']['telpno'] = '12345678';
			$this->User->set($this->data['User']);
			if ($this->User->validates()):
				$user = $this->User->find('first', array('conditions' => array('email' => $this->data['User']['email'], 'active' => 0) ));
				if (!empty($user)) {
					//$this->User->set($this->data);
					$this->User->id = $user['User']['id'];
					//$this->User->saveField('group_id', 2);
					$this->User->saveField('active', 0);
					$hashkey = md5($user['User']['username'] . date('dMYHis') . 'Kn4st1K' );
					$this->User->saveField('hash_validation', $hashkey);
					
					//$this->__sendNotifUserMail($user['User']['username'], $user['User']['fullname'], $user['User']['email'], $hashkey);
					$this->Session->setFlash('Your notification has been sent to your email.', 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash('Your notification has been sent to your email.', 'default', array('class' => 'success'));
					//$this->Session->setFlash('Sorry, our system can not find your email address! Please register it first.');
					//$this->set('msg', 'You can renotification again by click Resend notification button below.');
					//$this->set('isValid', true);
					//$this->set('isError', true);
					//$this->render('notification');
				}
				$this->redirect(array('controller'=>'main', 'action' => 'index'));
			else:
				$this->Session->setFlash('Sorry, there are some invalid data.');
				$this->set('msg', 'You can renotification again by click Resend notification button below.');
				$this->set('isValid', true);
				$this->set('isError', true);
				$this->set('data', $this->data);
				$this->render('notification');
			endif;
		else:
			$this->redirect(array('action' => 'index'));
		endif;
	}
	
	function notification($hash = null) {
		//App::import('Helper', 'Html');
		$this->set('isValid', true);
		$this->set('isError', true);
		if (!$hash):
			$this->Session->setFlash('Sorry, your request is not valid!');
			$this->set('isValid', false);
			$this->set('msg', 'Your confirmation request is not valid! Please be sure you have received a notification email from us related with your registration in Skripsi SI UKDW!');
		else:
			$user = $this->User->find('first', array('conditions' => array( 'hash_validation' => $hash ) ));
			if (!empty($user)):
				$this->User->id = $user['User']['id'];
				//$this->User->saveField('group_id', 2);
				$this->User->saveField('active', 1);
				$this->User->saveField('hash_validation', '');
				$this->set('isError', false);
				$this->set('msg', 'Your account is active now. You can make a new session to Skripsi SI UKDW system by login using your username and password. From there, you can proceed to next process. Thank you.');
			else:
				//$html = new HtmlHelper();
				$this->set('msg', 'Sorry, your request code is not accepted by our system. You can renotification again by click Resend notification button below.');
			endif;
		endif;
	}
	
	function ceknim($nim=null) {
		$this->layout = 'ajax';
		if ($this->RequestHandler->isAjax() && $nim == null):
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		else:
			if (strlen($nim) > 8):
				$mhs = $this->Dosen->find('first', array('fields'=>array('nama'), 'recursive' => -1, 'conditions'=>array('Dosen.nidn' => $nim) ));
			else:
				$mhs = $this->Mahasiswa->find('first', array('fields' =>array('nama'), 'recursive' => -1, 'conditions' => array('Mahasiswa.nim' => $nim, 'Mahasiswa.status' => "1") ));
			endif;
			if (!empty($mhs)):
				$d['Status']['return'] = 0;
				if (strlen($nim) > 8):
					$d['Status']['msg'] = $mhs['Dosen']['nama'];
				else:
					$d['Status']['msg'] = $mhs['Mahasiswa']['nama'];
				endif;
			else:
				$d['Status']['return'] = 1;
				if (strlen($nim) > 8):
					$d['Status']['msg'] = 'Tidak ada Dosen dengan NIDN tersebut!';
				else:
					$d['Status']['msg'] = 'Tidak ada Mahasiswa dengan NIM tersebut!';
				endif;
			endif;
		endif;
		$this->set(compact('d'));
	}
	
	function lostpasswd() {
		$this->set('judul', 'Reset Password');
		if($this->Session->check('User') == true) {
			// jika sudah login, tidak perlu lostpasswd
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			exit();
		} else {
			// jika belum login, cek apakah ada data dari form yang dikirim
			if (!empty($this->data)):
				// for dummy validation
				$this->data['User']['username'] = 'testingTESTING';
				$this->data['User']['nim'] = '12345678';
				$this->data['User']['password'] = 'testingTESTING';
				$this->data['User']['fullname'] = 'testingTESTING';
				$this->data['User']['telpno'] = '1234567890';
				
				$this->User->set($this->data);
				if ($this->User->validates()):
					$user = $this->User->find('first', array('conditions' => array('email' => $this->data['User']['email']) ));
					if (!empty($user)) {
						// jika emailnya ada di database
						$newpwd = $this->__generatePassword(10);
						$this->User->id = $user['User']['id'];
						$this->User->saveField('password', $newpwd);
						$this->User->saveField('modified', date( 'Y-m-d H:i:s' ));
						$this->__sendResetPwdUserMail($user['User']['username'], $user['User']['fullname'], $user['User']['email'], $newpwd);
					}
					$this->data['User']['username'] = '';
					$this->data['User']['password'] = '';
					$this->Session->setFlash('The new password has been sent to your email.', 'default', array('class' => 'success'));
					$this->redirect(array('controller'=>'main','action' => 'index'));
				endif;
			else:
				$this->layout = 'base';
			endif;
		}
	}
}
?>