<?php
class ProposalsController extends AppController {
	public $helpers = array('Html', 'Form', 'Session', 'Tools');
	public $name = 'Proposals';
	public $uses = array('Proposal', 'Log');
	public $layout = "baseform";
	public $components = array('RequestHandler');
	
	function beforeFilter() {
		//cek session dan hak akses
		if($this->Session->check('User') == false ) {
			//redirect ke halaman login dengan error message
			$this->Session->setFlash('You must login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
			exit();
		} else {
			if ($this->action == 'tambah' || $this->action == 'kumpul' || $this->action == 'edit' || $this->action == 'save' || $this->action == 'getproposals') {
				if ($this->Session->read('User.group_id') != 2) {
					$this->Session->setFlash('Maaf, untuk menambah proposal baru hanya dapat dilakukan oleh Mahasiswa.');
					$this->redirect(array('controller' => 'admin', 'action' => 'home'));
					exit();
				}
			}elseif ($this->action == 'delete') {
				if ($this->Session->read('User.group_id') != 2 && $this->Session->read('User.group_id') != 1) {
					$this->Session->setFlash('Maaf, menghapus ringkasan proposal hanya dapat dilakukan oleh Admin atau Mahasiswa yang bersangkutan.');
					$this->redirect(array('controller' => 'admin', 'action' => 'home'));
					exit();
				}
			}elseif ($this->action == 'evaluasi' && $this->action == 'savevaluasi'  || $this->action == 'report') {
				if(($this->Session->read('User.group_id') != 1) && ($this->Session->read('User.group_id') != 3)) {
					$this->Session->setFlash('Sorry, you don\'t have privilege to access user management.');
					$this->redirect(array('controller' => 'admin', 'action' => 'home'));
					exit();
				}
			}
		}
	}
	
	//function index() {
	//	$this->set('data', $this->paginate());
	//	$this->set('judul', 'Administration Tools');
	//}
	
	function tambah() {
		$this->set('judul', 'Tambah Ringkasan Proposal');
		if (!empty($this->data)) {
			$this->Proposal->create();
			if ($this->Proposal->save($this->data)) {
				$this->Session->setFlash('Data Ringkasan Proposal Anda telah disimpan. Silahkan lanjutkan dengan mencetaknya!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-2'));
			} else {
				$this->Session->setFlash('Maaf, saat ini sistem tidak dapat menyimpan data Ringkasan Proposal Anda!');
				$this->set('data', $this->data);
				$dosens = $this->Proposal->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('Dosen.boleh'=>'B', 'Dosen.status' => 'K')));
				$this->set(compact('dosens'));
				$topiks = $this->Proposal->Topik->find('list');
				$this->set(compact('topiks'));
				$this->render('tambah');
			}
		} else {
			$dosens = $this->Proposal->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('Dosen.boleh'=>'B', 'Dosen.status' => 'K')));
			$this->set(compact('dosens'));
			$topiks = $this->Proposal->Topik->find('list');
			$this->set(compact('topiks'));
			$this->render('tambah');
		}
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		}
		
		if ($this->Session->read('User.group_id') == 1) {
			$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id)));
		} else {
			$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id, 'Proposal.userid' => $this->Session->read('User.id'))));
		}
		if (!empty($data)):
			$boleh = false;
			if ($this->Session->read('User.group_id') == 1) {
				$boleh = true;
			} elseif ($this->Session->read('User.group_id') == 2 && $data['Proposal']['status'] == 0) {
				$boleh = true;
			}
			if ($boleh && $this->Proposal->delete($id)) {
				$this->Session->setFlash('Ringkasan Proposal terpilih telah dihapus!', 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-2'));
			} else {
				$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani penghapusan data ringkasan proposal yang Anda minta!');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-2'));
			}
		else:
			$this->Session->setFlash('Maaf, sistem kami tidak dapat melayani penghapusan data data ringkasan proposal yang Anda minta!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-2'));
		endif;
	}
	
	function save() {
		if (empty($this->data)) {
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-2'));
		} else {
			if ($this->Proposal->save($this->data)) {
				$this->Session->setFlash('Data Ringkasan Proposal telah diupdate.', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-2'));				
			} else {
				$dosens = $this->Proposal->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('Dosen.boleh'=>'B', 'Dosen.status' => 'K')));
				$this->set(compact('dosens'));
				$topiks = $this->Proposal->Topik->find('list');
				$this->set(compact('topiks'));
				$this->Session->setFlash('Maaf, sistem tidak dapat menyimpan perubahan data Anda.', true);
				$this->set('data', $this->data);
				$this->set('err', $this->Proposal->invalidFields());
				$this->render('edit');
			}
		}
	}
	
	function savevaluasi() {
		if (empty($this->data)) {
			$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-2'));
		} else {
			//$this->Proposal->id = $this->data['Proposal']['id'];
			//if ($this->Proposal->saveField('status_proposal', $this->data['Proposal']['status_proposal'])) {
			if ($this->Proposal->save($this->data)) {
				$this->Session->setFlash('Evaluasi Data Ringkasan Proposal telah diupdate.', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-2'));				
			} else {
				$dosens = $this->Proposal->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('Dosen.boleh'=>'B', 'Dosen.status' => 'K')));
				$this->set(compact('dosens'));
				$topiks = $this->Proposal->Topik->find('list');
				$this->set(compact('topiks'));
				$this->Session->setFlash('Maaf, sistem tidak dapat menyimpan perubahan data Anda.', true);
				$this->set('data', $this->data);
				$this->set('err', $this->Proposal->invalidFields());
				$this->render('evaluasi');
			}
		}
	}
	
	function evaluasi($id = null) {
		$this->set('judul', 'Catatan Desk Evaluation');
		if ($id != null) {
			if ($this->Session->read('User.group_id') == 3) {
				$currdosen = $this->Proposal->Dosen->find('first', 
					array('conditions' => array('Dosen.nidn' => $this->Session->read('User.nim') )));
			}

			$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id), 'recursive' => 2));
			if ($data) {
				$isok = false;
				if ($this->Session->read('User.group_id') == 3) {
					if ($data['De']) {
						foreach($data['De'][0]['Dosen'] as $t){
							$tgl = date('Y-m-d', strtotime($data['De'][0]['tanggal']));
							$waktuawal = date('Y-m-d H:i:s', strtotime($tgl . ' 08:00:00'));
							$waktuakhir = date('Y-m-d H:i:s', strtotime($tgl . ' 23:00:00'));
							$tglsistem = date('Y-m-d H:i:s');
							if ($t['id'] == $currdosen['Dosen']['id'] && 
							    ($tglsistem >= $waktuawal && $tglsistem <= $waktuakhir) ) {
								$isok = true; break;
							}
						}
					}
				} elseif ($this->Session->read('User.group_id') == 1) {
					$isok = true;
				}
				if ($isok) {  
					if ($data['Proposal']['status_proposal'] == 1 && $data['Proposal']['valid'] == 1) {
						$this->set(compact('data'));
						$dosens = $this->Proposal->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('Dosen.boleh'=>'B', 'Dosen.status' => 'K')));
						$this->set(compact('dosens'));
						$topiks = $this->Proposal->Topik->find('list');
						$this->set(compact('topiks'));
					} else {
						$this->Session->setFlash('Maaf, Penilaian Desk Evaluasi untuk mahasiswa terpilih telah selesai!');
						$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-2'));
					}
				} else {
					$this->Session->setFlash('Maaf Jadwal Penilaian Desk Evaluasi Anda untuk saat ini sudah selesai!');
					$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-2'));
				}
			} else {
				$this->Session->setFlash('Maaf sistem tidak menemukan data ringkasan proposal yang terpilih!');
				$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-2'));
			}
		} else {
			$this->Session->setFlash('URL is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update Ringkasan Proposal');
		if ($id != null):
			if ($this->Session->read('User.group_id') == 1) {
				$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id)));
			} else {
				$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id, 'Proposal.userid' => $this->Session->read('User.id'))));
			}
			$this->set('data', $data);
			$dosens = $this->Proposal->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('Dosen.boleh'=>'B', 'Dosen.status' => 'K')));
			$this->set(compact('dosens'));
			$topiks = $this->Proposal->Topik->find('list');
			$this->set(compact('topiks'));
		else:
			$this->Session->setFlash('URL is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		endif;
	}
	
	function kumpul($id=null) {
		$this->layout = 'ajax';
		$df = $this->data;
		
		$cekdata = $this->Proposal->find('first', array('conditions' => array('Proposal.status_proposal' => '1', 'Proposal.userid' => $this->Session->read('User.id'))));
		if ($cekdata) {
			$d['Status']['return'] = 3;
			$d['Status']['msg'] = 'Anda TIDAK diperkenankan mengirimkan Ringkasan Proposal, karena masih terdapat Ringkasan Proposal Anda yang berstatus TERKIRIM!';
		} else {
			$pro = $this->Proposal->find('first', array('conditions'=>array('Proposal.id'=> $df['Kirim']['id'])) );
			
			if (!empty($pro) && !empty($df['Kirim']['idtgl'])) {
				$this->Proposal->id = $df['Kirim']['id'];
				if ($this->Proposal->saveField('status_proposal', 1) && 
					$this->Proposal->saveField('tglkumpul', date( 'Y-m-d H:i:s' )) &&
					$this->Proposal->saveField('tglde', $df['Kirim']['idtgl']) ) {
					
					$d['Status']['return'] = 0;
					$d['Status']['msg'] = 'Pencatatan Kiriman Ringkasan Proposal berhasil!';
				} else {
					$d['Status']['return'] = 2;
					$d['Status']['msg'] = 'Pengiriman Ringkasan Proposal tidak berhasil!';
				}
			} else {
				$d['Status']['return'] = 1;
				$d['Status']['msg'] = 'Permintaan Pengiriman Ringkasan Proposal tidak valid!';
			}
		}
		
		$this->set(compact('d'));
		
		/*if ($id != null) {
			$cekdata = $this->Proposal->find('first', array('conditions' => array('Proposal.status_proposal' => '1', 'Proposal.userid' => $this->Session->read('User.id'))));
			if ($cekdata) {
				$this->Session->setFlash('Anda TIDAK diperkenankan mengirimkan Ringkasan Proposal, karena masih terdapat Ringkasan Proposal Anda yang berstatus TERKIRIM!');
				$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-2'));
			} else {
				$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id, 'Proposal.userid' => $this->Session->read('User.id'))));
				if ($data) {
					$this->Proposal->id = $data['Proposal']['id'];
					if ($this->Proposal->saveField('status_proposal', 1) && $this->Proposal->saveField('tglkumpul', date( 'Y-m-d H:i:s' )) ) {
						$this->Session->setFlash('Data Ringkasan Proposal BERHASIL terkirim ke koordinator Skripsi SI.', 'default', array('class' => 'success'));
					} else {
						$this->Session->setFlash('Data Ringkasan Proposal GAGAL terkirim ke koordinator Skripsi SI.', 'default', array('class' => 'success'));
					}
					$this->redirect(array('controller'=>'admin', 'action' => 'home', '#tabs-2'));
				}
			}
		} else {
			$this->Session->setFlash('URL is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		}*/
	}
	
	function prnproposal($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		} else {
			if ($this->Session->read('User.group_id') == 2) {
				$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id, 'Proposal.userid' => $this->Session->read('User.id') ) ) );
			} else {
				$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id) ) );
			}
			$this->set(compact('data'));
			
			Configure::write('debug',2); 
			$this->layout = 'tcpdf'; 
			$this->render();
		}
	}
	
	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
		} else {
			if ($this->Session->read('User.group_id') == 2) {
				$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id, 'Proposal.userid' => $this->Session->read('User.id') ) ) );
			} else {
				$data = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id) ) );
			}
			$this->set(compact('data'));
		}
	}
	
	function getproposals() {
		$this->layout = 'ajax';
		
		if ($this->Session->read('User.group_id') == 2) {
			// jika yang meminta adalah mahasiswa
			$data = $this->Proposal->find('all', array('conditions' => array('Proposal.userid'=> $this->Session->read('User.id'), 'Proposal.status_proposal' => 3) ) );
			$selected = array();
			foreach($data as $t) {
				array_push($selected, array('id' => $t['Proposal']['id'], 'dosen' => $t['Proposal']['dosen'], 'nama_dosen' => $t['Dosen']['nama_dosen'], 'judul' => $t['Proposal']['judul'], 'konsentrasi' => $t['Proposal']['konsentrasi'], 'nim' => $t['User']['nim'], 'nama' => $t['User']['fullname'] ));
			}
			$d['Status']['return'] = 0;
			$d['Status']['msg'] = $selected;
		} else {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Maaf, fasilitas ini hanya untuk mahasiswa!';
		}
		$this->set(compact('d'));
	}
	
	function report($tglpro=null) {
		$this->set('judul', 'Daftar Peserta Desk Evaluation');
		if (!$tglpro) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-2'));
		}
		$data = $this->Proposal->De->find('first', array('conditions' => array('De.id' => $tglpro), 'recursive' => 2 ) );
		$this->set('data', $data);
		$this->layout = "report";
	}

	function validasi($id=null, $val=0) {
		$this->layout = 'ajax';
		if (!$id) {
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Request tidak valid!';
		} else {
			$adakah = $this->Proposal->find('first', array('recursive'=>2, 'conditions' => array('Proposal.id' => $id)));
			if ($adakah) {
				$this->Proposal->id = $id;
				if ($this->Proposal->saveField('valid', $val)) {
					$d['Status']['return'] = 0;
					if ($val == 1) {
						$d['Status']['msg'] = 'Proposal telah divalidasi pembayarannya!';
					} else {
						$d['Status']['msg'] = 'Proposal telah dinonvalidasikan pembayarannya!';
					}
					$data['status_proposal'] = $adakah['Proposal']['status_proposal'];
					$data['kosong'] = empty($adakah['De']);
					$d['Status']['data'] = $data;
				} else {
					$d['Status']['return'] = 3;
					$d['Status']['msg'] = 'Sistem tidak dapat melakukan validasi untuk proposal ID ' . $id . '!';
				}
			} else {
				$d['Status']['return'] = 2;
				$d['Status']['msg'] = 'Tidak ditemukan Proposal dengan ID ' . $id . '!';
			}
		}
		$this->set(compact('d'));
	}
}
?>