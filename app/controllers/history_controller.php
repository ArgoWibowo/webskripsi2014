<?php
class HistoryController extends AppController {
	public $name = 'History';
	public $uses = array('Pendadaran', 'Mahasiswa', 'User', 'Kolokium', 'Ta');
	var $components = array('RequestHandler');
	var $helpers = array('Html', 'Tools');
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		}
		$groupid = $this->Session->read('User.group_id');
		if ($groupid != 1) {
			$this->Session->setFlash('Anda tidak memiliki hak akses untuk fungsi ini.');
			$this->redirect(array('controller' => 'admin', 'action' => 'home', '#tabs-5'));
		}
	}
	
	function history($nim = null) {
		if (!$nim) {
			$this->Session->setFlash('Maaf, tidak ada NIM yang diberikan!');
			$this->redirect(array('action'=>'index', null, true));
		}
		$mhs = $this->Mahasiswa->find('first', array('conditions' => array('Mahasiswa.nim' => $nim) ));
		$this->set(compact('mhs'));

		$userid = $this->User->find('first', array('conditions' => array('User.nim' => $nim) ));
		
		$data = $this->Kolokium->Proposal->find('all', array('conditions' => array('Proposal.userid' => $userid['User']['id']), 'recursive' => 4));
		$this->set(compact('data'));

		$kolokium = array();
		$ta = array();
		$pendadaran = array();
		if (!empty($data)) {
			foreach($data as $d) {
				$tmp = $this->Kolokium->find('all', 
						array('conditions' => array('Kolokium.proposal_id' => $d['Proposal']['id']) ));
				$kolokium[$d['Proposal']['id']] = $tmp;
				foreach($tmp as $t) {
					$tmpta = $this->Ta->find('all', array('conditions' => array('Ta.kolokium_id' => $t['Kolokium']['id']) ));
					$ta[$t['Kolokium']['id']] = $tmpta;
					foreach($tmpta as $a) {
						$tmpdadar = $this->Pendadaran->find('all', array('conditions' => array('Pendadaran.id_ta' => $a['Ta']['id']) ));
						$pendadaran[$a['Ta']['id']] = $tmpdadar;
					}
				}
			}
		} else {
			$tmpta = $this->Ta->find('all', array('conditions' => array('Ta.nim' => $nim) ));
			$ta[0] = $tmpta;
			foreach($tmpta as $a) {
				$tmpdadar = $this->Pendadaran->find('all', array('conditions' => array('Pendadaran.id_ta' => $a['Ta']['id']) ));
				$pendadaran[$a['Ta']['id']] = $tmpdadar;
			}
		}
		$this->set('kolokium', $kolokium);
		$this->set('tas', $ta);
		$this->set('pendadaran', $pendadaran);
		$this->layout = 'baseform';
	}
}
?>