<?php
/**
 * 
 * @author budi susanto
 *
 */
class BlogsController extends AppController 
{
	public $name = 'Blogs';	
	//public $paginate = array('limit' => 3, 'page' => 1, 'order' => array('Blog.created'=>'desc'));	
	public $layout = 'baseform';
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
			//exit();
		} else {
			if(($this->Session->read('User.group_id') != 1)) {
				$this->Session->setFlash('Sorry, you don\'t have any privileges to access this page.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home'));
				//exit();
			}
		}
	}
	
	//function index() {
	//	$this->set('posts', $this->paginate());
	//	$this->set('judul', 'News Management');
	//}
	
	function add() {
		$this->set('judul', 'Tambah Pengumuman');
		if (!empty($this->data)) {
			$this->Blog->create();
			if ($this->Blog->save($this->data)) {
				$this->Session->setFlash('Your news has been saved and published!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home'));
			} else {
				$this->Session->setFlash('Sorry, there are any error that can not be handled by system in order to save your news!');
				$this->set('data', $this->data);
				$this->render('add');
			}
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update Pengumuman');
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('action' => 'index'));
		}
		
		if (!empty($this->data)) {
			if ($this->Blog->save($this->data)) {
				$this->Session->setFlash('Your news has been updated!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home'));
			} else {
				$this->Session->setFlash('Sorry, there are any error that can not be handled by system in order to save your news!');
				$this->set('data', $this->data);
				$this->render('edit');
			}
		} else {
			$data = $this->Blog->find('first', array('conditions' => array('Blog.id' => $id)));
			$this->set('data', $data);
		}
	}
	
	function enable($id = null) {
		$data = $this->Blog->read(null, $id);
		if (!$id && empty($data)) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home'));
		}
		
		if (!empty($data)) {
			$data['Blog']['published'] = 1;
			if ($this->Blog->save($data)) {
				$this->Session->setFlash('The news is published!', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('News ' . $id . ' can not be saved right now!');
			}
			$this->redirect(array('controller'=>'admin','action' => 'home'));
		} else {
			$this->Session->setFlash('News data is not found!');
			$this->redirect(array('controller'=>'admin','action' => 'home'));
		}
	}
	
	function disable($id = null) {
		$data = $this->Blog->read(null, $id);
		if (!$id && empty($data)) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home'));
		}
		
		if (!empty($data)) {
			$data['Blog']['published'] = 0;
			if ($this->Blog->save($data)) {
				$this->Session->setFlash('The news has been postponed!', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('The news ' . $id . ' can not be updated!');
			}
			$this->redirect(array('controller'=>'admin','action' => 'home'));
		} else {
			$this->Session->setFlash('The news data is not found!');
			$this->redirect(array('controller'=>'admin','action' => 'home'));
		}
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home'));
		}
		
		if ($this->Blog->delete($id)) {
			$this->Session->setFlash('The selected news has been deleted!', 'default', array('class' => 'success'));
			$this->redirect(array('controller'=>'admin','action' => 'home'));
		}
	}
}
?>