<?php
class MainController extends AppController {
	public $name = 'Main';
	public $uses = array('Login', 'Blog', 'Jadwal', 'Log');
	public $layout = 'main';
	//public $components = array('RecaptchaPlugin.Recaptcha', 'Email');
	//public $helpers = array('RecaptchaPlugin.Recaptcha', 'Html');
	public $components = array('Email');
	public $helpers = array('Html', 'Tools');
 
	function index() {
		$this->set('judul', 'Home');
		if($this->Session->check('User') == true) {
			$this->Session->setFlash('You are already login.', 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			exit();
		}
		$news = $this->Blog->find('all', array('conditions' => array('Blog.published' => '1'), 'limit' => 6, 'order' => array('Blog.modified desc') ) );
		$this->set('news', $news);
		$jadwals = $this->Jadwal->find('all', array('conditions' => array('batas >= ' => date('Y-m-d H:i:s') ) ) );
		$this->set(compact('jadwals'));
	}
	
	function login() {
		$jadwals = $this->Jadwal->find('all', array('conditions' => array('batas >= ' => date('Y-m-d H:i:s') ) ) );
		$this->set(compact('jadwals'));
		$this->set('judul', 'Home Login');
		if($this->Session->check('User') == true) {
			$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			exit();
		} else if(empty($this->data) == false) {
			$this->data['Login']['email'] = 'testing@gmail.com';
			$this->Login->set($this->data);
			if ($this->Login->validates()):
				$user = $this->Login->validateLogin($this->data['Login']);
				if( $user ) {
					if ($user['active'] == 1):
						$this->Session->write('User', $user);
						$this->Log->addLog($this->data['Login']['username'], 'Login', 'Login success. Session created');
						$this->Session->setFlash('Your Login is success.', 'default', array('class' => 'success'));
						$this->redirect(array('controller' => 'admin', 'action' => 'home'));
					else:
						$this->Session->setFlash('Your account is not active yet. Please verify first!');
						$news = $this->Blog->find('all', array('conditions' => array('Blog.published' => '1'), 'limit' => 6, 'order' => array('Blog.modified desc') ) );
						$this->set('news', $news);
						$this->render('index');
					endif;
				} else {
					$this->Session->setFlash('Your username and/or password incorrect!');
					$news = $this->Blog->find('all', array('conditions' => array('Blog.published' => '1'), 'limit' => 6, 'order' => array('Blog.modified desc') ) );
					$this->set('news', $news);
					$this->render('index');
				}
			else:
				$news = $this->Blog->find('all', array('conditions' => array('Blog.published' => '1'), 'limit' => 6, 'order' => array('Blog.modified desc') ) );
				$this->set('news', $news);
				$this->set( 'data', $this->params['data'] );
				$this->validateErrors( $this->Login );			
				$this->render('index');
			endif;
		} else {
			$news = $this->Blog->find('all', array('conditions' => array('Blog.published' => '1'), 'limit' => 6, 'order' => array('Blog.modified desc') ) );
			$this->set('news', $news);
			$this->render('index');
		}
	}
	
	function logout() {
		if($this->Session->check('User') == false) {
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		} else {
			$username = $this->Session->read('User.username');
			$this->Session->destroy('user');
			$this->Session->setFlash('Your login session has been deleted.', 'default', array('class' => 'success'));
			$this->Log->addLog($username, 'Logout', 'Logout success. Session deleted');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		}
	}
}
?>