<?php
/**
 * 
 * @author budi susanto
 *
 */
class JwddeskevaluationsController extends AppController 
{
	public $name = 'Jwddeskevaluations';
	public $layout = 'baseform';
	
	function beforeFilter() {
		if($this->Session->check('User') == false) {
			$this->Session->setFlash('You have to login first before accessing this page.');
			$this->redirect(array('controller' => 'main', 'action' => 'index'));
		} else {
			if(($this->Session->read('User.group_id') != 1)) {
				$this->Session->setFlash('Sorry, you don\'t have any privileges to access this page.');
				$this->redirect(array('controller' => 'admin', 'action' => 'home'));
			}
		}
	}
	
	function add() {
		$this->set('judul', 'Tambah Jadwal Batas Kumpul Desk Evaluation');
		if (!empty($this->data)) {
			$this->Jwddeskevaluation->create();
			if ($this->Jwddeskevaluation->save($this->data)) {
				$this->Session->setFlash('Jadwal Desk Evaluation telah ditambahkan!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
			} else {
				$this->Session->setFlash('Maaf, sistem tidak dapat menambahkan data jadwal Desk Evaluation!');
				$this->set('data', $this->data);
				$this->render('add');
			}
		}
	}
	
	function edit($id = null) {
		$this->set('judul', 'Update Jadwal Batas Kumpul Desk Evaluation');
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		}
		
		if (!empty($this->data)) {
			if ($this->Jwddeskevaluation->save($this->data)) {
				$this->Session->setFlash('Jadwal Desk Evaluation telah terupdate!', 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
			} else {
				$this->Session->setFlash('Maaf sistem tidak dapat menyimpan perubahan data Jadwal Desk Evaluation yang Anda lakukan!');
				$this->set('data', $this->data);
				$this->render('edit');
			}
		} else {
			$data = $this->Jwddeskevaluation->find('first', array('conditions' => array('Jwddeskevaluation.id' => $id)));
			$this->set('data', $data);
		}
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		}
		
		if ($this->Jwddeskevaluation->delete($id)) {
			$this->Session->setFlash('Jadwal Desk Evaluation terpilih telah terhapus!', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash('Jadwal desk evaluation terpilih tidak dapat dihapus!', 'default');
		}
		$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
	}
	
	/*function catat($id=null) {
		if (!$id) {
			$this->Session->setFlash('Your request is not valid!');
			$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
		} else {
			$data = $this->Jwddeskevaluation->find('first', array('conditions' => array('Jwddeskevaluation.id' => $id) ));
			$this->set(compact('data'));
			$dosens = $this->Jwddeskevaluation->Dosen->find('list', array('fields' => array('id', 'nama_dosen'), 'conditions' => array('boleh' => 'B', 'status' => 'K') ));
			$this->set(compact('dosens'));
		}
	}
	
	function simpancatat() {
		if ($this->data) {
			$this->Jwddeskevaluation->id = $this->data['Jwddeskevaluation']['idjwd'];
			if ($this->Jwddeskevaluation->save($this->data)) {
				$this->Session->setFlash('Setup Data Dosen pencatat telah disimpan!', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('Setup Data Dosen pencatat tidak dapat disimpan saat ini!');
			}
		} else {
			$this->Session->setFlash('Your request is not valid!');
		}
		$this->redirect(array('controller'=>'admin','action' => 'home', '#tabs-8'));
	} */
	
	function getdates() {
		$this->layout = 'ajax';
		$data = $this->Jwddeskevaluation->find('list', array('fields' => array('id', 'tanggal'), 'conditions' => array('Jwddeskevaluation.batas >= ' => date('Y-m-d H:i:s')) ));
		if (!empty($data)):
			$d['Status']['return'] = 0;
			$d['Status']['msg'] = $data;
		else:
			$d['Status']['return'] = 1;
			$d['Status']['msg'] = 'Tidak ada tenggat waktu untuk Desk Evaluation!';
		endif;
		$this->set(compact('d'));
	}
}
?>